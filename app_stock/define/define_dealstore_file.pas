unit define_dealstore_file;

interface

//uses
//  define_dealitem;
  
const                               
  FilePath_StockData      = 'sdata';
  FilePath_StockIndexData = 'sidat';     
  FilePath_FutureData     = 'fdat';
                    
  // 股票及股票指数
  //FilePath_DBType_Item          = DBType_Item_China;
  //FilePath_DBType_Index         = DBType_Index_China;
                      
  DataType_TimeEventItem = 1001;

  // 股票及股票指数
  DataType_Item        = 3;

  // 股票分类信息
  DataType_Class       = 5;
  
  
  // 股票日线行情数据
  DataType_DayData     = 11;

  // 股票日交易明细
  DataType_DetailData  = 21;

  DataType_MinuteData  = 25;
                                      
  // 股票信息
  DataType_Info        = 31;
  
  // 多股票汇总数据
  DataType_InstantData   = 41;
  DataType_WeightData    = 42;
  DataType_ValueData     = 43;
  
  // 日线
  // sd_31  sdw_31
  // 5日线
  // sd5_31 sdw5_31
  FileExt_StockDay        = 'd';


  FileExt_StockDayWeight  = 'dw';
  FileExt_StockDetail     = 'sdt';  // rename to se

  // 分钟线 由 detail 线 分析总结来的
  // sm60_31 60 分钟线 如果不带 分钟数 则为月线
  // smw60_31 smw60_32
  FileExt_StockMinute     = 'm';     
  FileExt_StockWeek       = 'wk';

  // sm_31 smw_31
  FileExt_StockMonth      = 'mt';

  FileExt_StockAnalysis   = 'as';
  FileExt_StockInstant    = 'it';

  FileExt_StockWeight     = 'wt';

  // 总体统计分析
  FileExt_StockSummaryValue = 'sv';

  FileExt_FuturesDetail   = 'qe';
  FileExt_FuturesAnalysis = 'qa';

implementation

end.
