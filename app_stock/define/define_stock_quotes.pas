unit define_stock_quotes;

interface

uses
  Define_Price,
  define_datetime;
  
type                    
  { 日线数据 }
  PRT_Quote_Day       = ^TRT_Quote_Day;
  TRT_Quote_Day       = packed record           // 56
    DealDate          : TDateStock;    // 4
    PriceRange        : TRT_PricePack_Range; // 16 - 20
    DealVolume        : Int64;               // 8 - 28 成交量
    DealAmount        : Int64;               // 8 - 36 成交金额
    Weight            : TRT_WeightPackValue;      // 4 - 40 复权权重 * 100
    TotalValue        : Int64;               // 8 - 48 总市值
    DealValue         : Int64;               // 8 - 56 流通市值
  end;

  { 及时行情数据
    1 分钟 -- 60 分钟 < 日线
  }
  PRT_Quote_Minute    = ^TRT_Quote_Minute;
  TRT_Quote_Minute    = packed record
    DealDateTime      : TDateTimeStock;    // 8  
    PriceRange        : TRT_PricePack_Range; // 16 - 24   
    DealVolume        : Integer;             // 4 - 28 成交量
    DealAmount        : Integer;             // 4 - 32 成交金额
  end;
  
  { 交易明细 }
  PRT_Quote_Detail    = ^TRT_Quote_Detail;
  TRT_Quote_Detail    = packed record
    DealDateTime      : TDateTimeStock;    // 4 - 4
    Price             : TRT_PricePack;       // 4 - 8
    DealVolume        : Integer;             // 4 - 12 成交量
    DealAmount        : Integer;             // 4 - 16
    DealType          : Integer;
    // Buyer: Integer;
    // Saler: Integer;
  end;

{
  M1 区间数据
     1 分钟 5 分钟 --- 日线 周线 月线 都是 区间数据
     Open High Low Close
  M2 明细数据
     Price Time Volume Acount
}             
  PStore32              = ^TStore32;
  TStore32              = packed record
    Data                : array[0..32 - 1] of Byte;
  end;

  PStore64              = ^TStore64;
  TStore64              = packed record
    Data                : array[0..64 - 1] of Byte;
  end;
  
  { 日线数据 }
  PStore_Quote64_Day    = ^TStore_Quote64_Day;
  TStore_Quote64_Day    = packed record  // 56
    PriceRange          : TStore_PriceRange;  // 16
    DealVolume          : Int64;         // 8 - 24 成交量
    DealAmount          : Int64;         // 8 - 32 成交金额
    DealDate            : Integer;       // 4 - 36 交易日期
    Weight              : TStore_Weight; // 4 - 40 复权权重 * 100
    TotalValue          : Int64;         // 8 - 48 总市值
    DealValue           : Int64;         // 8 - 56 流通市值 
  end;
           
  { 分时数据 detail }
  PStore_Quote32_Minute = ^TStore_Quote32_Minute;  //
  TStore_Quote32_Minute = packed record  // 28
    PriceRange          : TStore_PriceRange;  // 16
    DealVolume          : Integer;      // 4 - 20 成交量
    DealAmount          : Integer;      // 4 - 24 成交金额
    StockDateTime       : TDateTimeStock;  // 4 - 28 交易日期
  end;
  
  // 只有一个价格 必然是 及时报价数据
  PStore_Quote_Detail32 = ^TStore_Quote_Detail32;
  TStore_Quote_Detail32 = packed record  // 8
    QuoteDealTime       : Word;          // 2 -  1 小时 3600 秒  9 -- 15
    QuoteDealDate       : Word;          // 2 - 4
    Price               : TStore_Price;  // 4 - 8
    DealVolume          : Integer;       // 4 - 12 成交量
    DealAmount          : Integer;       // 4 - 16 成交金额
    DealType            : Byte;          // 1 - 17 买盘卖盘中性盘
  end;

  PStore_Quote32_Detail = ^TStore_Quote32_Detail;
  TStore_Quote32_Detail = packed record
    Quote               : TStore_Quote_Detail32;
    Reserve             : array [0..32 - SizeOf(TStore_Quote_Detail32) - 1] of Byte;
  end;          
           
implementation

uses
  Sysutils;
  
end.
