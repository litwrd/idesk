unit define_datetime;

interface

type
  PDatePack       = ^TDatePack;
  TDatePack       = packed record
    Year          : Word;
    Month         : Word;
    Day           : Word;
  end;

  PTimePack       = ^TTimePack;
  TTimePack       = packed record
    Hour          : Word;
    Minute        : Word;
    Second        : Word;  
    MSecond       : Word;
  end;

  PDateTimePack   = ^TDateTimePack;
  TDateTimePack   = packed record
    Date          : TDatePack;
    Time          : TTimePack;
  end;

  PDateStock      = ^TDateStock;
  TDateStock      = packed record
    Value         : Word;
  end;

  PTimeStock      = ^TTimeStock;
  TTimeStock      = packed record
    Value         : Word;
  end;

  PDateTimeStock  = ^TDateTimeStock;
  TDateTimeStock  = packed record  // 4
    Date          : TDateStock;  // 2
    Time          : TTimeStock;  // 2
  end;

  PRT_DateTime    = ^TRT_DateTime;
  TRT_DateTime    = packed record
    Value         : double;     // 8
  end;     

  (*//
  DayTime M0 模式
    6:00 开始计算
    
    06:00 -- 24:00 计算到秒
    18 * 3600 =  64800
    65565 - 64800 = 765

    00:00 -- 06:00 以分钟计
    6 * 60 = 360   

    00:01
    00:02
    765 - 360 = 405

    1    -- 06:00:00
    2    -- 06:00:01
    60   -- 06:00:59
    61   -- 06:01:00
  //*)
  PDayTimeM0      = ^TDayTimeM0;
  TDayTimeM0      = packed record  // 4
    Value         : Word;
  end;
  
  PDateTimeM0     = ^TDayTimeM0;
  TDateTimeM0     = packed record  // 4
    Date          : Word;
    Time          : TDayTimeM0;
  end;
  
  procedure Date2DatePack(ADate: Integer; ADatePack: PDatePack);   
  procedure Time2TimePack(ATime: TDateTime; ATimePack: PTimePack);

  function Time2TimeStock(ATime: TDateTime; ATimeStock: PTimeStock): Boolean;
  function TimeStock2Time(ATimeStock: PTimeStock): TDateTime;
  
  function DateTime2DateTimeStock(ATime: TDateTime; ADateTimeStock: PDateTimeStock): Boolean;     
  function DateTimeStock2DateTime(ADateTimeStock: PDateTimeStock): TDateTime; 
                             
  function GetTimeText(ATime: PTimeStock): AnsiString;

  function DecodeTimeStock(AStockTime: TTimeStock; var AHour: integer; var AMinute: integer; var ASecond: integer): Boolean;
  
  function EncodeDayTime0(AHour: Byte; AMinute: Byte; ASecond: Byte): Word;
  function DecodeDayTime0(ADayTimeM0: Word; var AHour: Byte; var AMinute: Byte; var ASecond: Byte): Boolean;

implementation

uses
  Sysutils;
  
{$IFNDEF RELEASE}  
const
  LOGTAG = 'define_datetime.pas';
{$ENDIF}
  
//(*//                       
function DecodeTimeStock(AStockTime: TTimeStock; var AHour: integer; var AMinute: integer; var ASecond: integer): Boolean;
begin
  Result := false;
  if 0 < AStockTime.Value then
  begin
    AHour := Trunc(AStockTime.Value div 3600);
    if AHour < 7 {16 - 9} then
    begin
      ASecond := AStockTime.Value - AHour * 3600;
      AMinute := Trunc(ASecond div 60);
      ASecond := ASecond - AMinute * 60;
      AHour := AHour + 9;
      Result := true;
    end;
  end;
end;
//*)
procedure Date2DatePack(ADate: Integer; ADatePack: PDatePack);
begin
  DecodeDate(ADate, ADatePack.Year, ADatePack.Month, ADatePack.Day);
end;
           
procedure Time2TimePack(ATime: TDateTime; ATimePack: PTimePack);
begin
  DecodeTime(ATime, ATimePack.Hour, ATimePack.Minute, ATimePack.Second, ATimePack.MSecond);
end;
                          
function GetTimeText(ATime: PTimeStock): AnsiString;
var
  tmpHour: Integer;
  tmpMinute: Integer;
  tmpSecond: Integer;
begin
  Result := '';
  if nil <> ATime then
  begin
    tmpHour := Trunc(ATime.Value div 3600);
    tmpSecond := ATime.Value - tmpHour * 3600;
    tmpMinute := Trunc(tmpSecond div 60);
    tmpSecond := tmpSecond - tmpMinute * 60;
    Result := IntToStr(tmpHour + 9) + ':' + IntToStr(tmpMinute) + ':' + IntToStr(tmpSecond);
  end;
end;

function Time2TimeStock(ATime: TDateTime; ATimeStock: PTimeStock): Boolean;
var
  tmpTimePack: TTimePack;
begin
  result := false;
  if nil = ATimeStock then
    Exit;                               
  Time2TimePack(ATime, @tmpTimePack);
  // 9 点开始 3600 秒
  ATimeStock.Value := 0;
  if (8 < tmpTimePack.Hour) and (16 > tmpTimePack.Hour) then
  begin        
    ATimeStock.Value := (3600 * (tmpTimePack.Hour - 9)) + (60 * tmpTimePack.Minute) + tmpTimePack.Second;
    result := true;
  end;
end;
           
function TimeStock2Time(ATimeStock: PTimeStock): TDateTime;
var
  tmpTimePack: TTimePack;
  tmpValue: Word;
begin
  tmpValue := ATimeStock.Value;
  tmpTimePack.Hour := tmpValue div 3600;
  tmpValue := tmpValue - tmpTimePack.Hour * 3600;
  tmpTimePack.Minute := tmpValue div 60;
  tmpValue := tmpValue - tmpTimePack.Minute * 60;
  tmpTimePack.Second := tmpValue;
  tmpTimePack.MSecond := 0;
  tmpTimePack.Hour := tmpTimePack.Hour + 9;
  Result := EncodeTime(tmpTimePack.Hour, tmpTimePack.Minute, tmpTimePack.Second, tmpTimePack.MSecond);
end;

function DateTime2DateTimeStock(ATime: TDateTime; ADateTimeStock: PDateTimeStock): Boolean;
begin
  ADateTimeStock.Date.Value := Trunc(ATime);
  Result := Time2TimeStock(ATime, @ADateTimeStock.Time);
end;
       
function DateTimeStock2DateTime(ADateTimeStock: PDateTimeStock): TDateTime; 
begin
  Result := ADateTimeStock.Date.Value + TimeStock2Time(@ADateTimeStock.Time);
end;

function EncodeDayTime0(AHour: Byte; AMinute: Byte; ASecond: Byte): Word;
begin
  Result := 0;
  if AHour < 6 then
  begin
    (*
      00:00:00 -- 1
      00:01:00 -- 2
      00:59:00 -- 60
      01:00:00 -- 61
      01:59:00 -- 120
      02:00:00 -- 121
      03:00:00 -- 181
      04:00:00 -- 241
      05:00:00 -- 301
      06:00:00 -- 361
    *)
    if (AMinute < 60) and (ASecond < 60) then
    begin     
      Result := AHour * 60 + AMinute + 1;
    end;
  end else
  begin
    {
      06:00:01 -- 362    
      06:00:02 -- 363
    }
    if (AHour < 24) and (AMinute < 60) and (ASecond < 60) then
    begin
      Result := (AHour - 6) * 3600 + (AMinute * 60) + ASecond + 361;
    end;
  end;
end;

function DecodeDayTime0(ADayTimeM0: Word; var AHour: Byte; var AMinute: Byte; var ASecond: Byte): Boolean;
begin
  Result := false;
  if 360 < ADayTimeM0 then
  begin           
    Result := true;
    AHour := (ADayTimeM0 - 361) div 3600;
    ADayTimeM0 := (ADayTimeM0 - 361) - AHour * 3600;
    AHour := AHour + 6;
    ASecond := ADayTimeM0 mod 60;
    AMinute := ADayTimeM0 div 60;
  end else
  begin
    if 0 < ADayTimeM0 then
    begin          
      Result := true;   
      AHour := (ADayTimeM0 - 1) div 60;
      AMinute := (ADayTimeM0 - 1) mod 60;
      ASecond := 0;
    end;
  end;
end;
(*//   
procedure TfrmSDConsole.btn1Click(Sender: TObject);

  procedure AddTimeLog(AHour, AMinute, ASecond: Byte);  
  var
    dayTime: Word;
    tmpHour: Byte;
    tmpMinute: Byte;
    tmpSecond: Byte;
    tmpStr: string;
  begin
    dayTime := EncodeDayTime0(AHour, AMinute, ASecond);
    DecodeDayTime0(dayTime, tmpHour, tmpMinute, tmpSecond);
    tmpStr := IntToStr(AHour) + ':' + IntToStr(AMinute) + ':' + IntToStr(ASecond) + ' -- ' +
              IntToStr(dayTime) + ' -- ' +
              IntToStr(tmpHour) + ':' + IntToStr(tmpMinute) + ':' + IntToStr(tmpSecond);
    mmo1.Lines.Add(tmpStr);
  end;

begin
  inherited;
  mmo1.Lines.Clear;
  mmo1.Lines.BeginUpdate;
  try
    AddTimeLog(0, 0, 0);
    AddTimeLog(1, 0, 0);
    AddTimeLog(2, 0, 0);
    AddTimeLog(3, 0, 0);
    AddTimeLog(4, 0, 0);            
    AddTimeLog(5, 0, 0);
    AddTimeLog(6, 0, 0);   
    AddTimeLog(6, 0, 1);
    AddTimeLog(6, 0, 2);
    AddTimeLog(6, 0, 59);
    AddTimeLog(6, 1, 1);
    AddTimeLog(6, 2, 1);
    AddTimeLog(6, 3, 1);
    AddTimeLog(6, 4, 1);
    AddTimeLog(6, 5, 1);
    AddTimeLog(6, 10, 59);
    AddTimeLog(10, 40, 59);   
    AddTimeLog(10, 58, 59);   
    AddTimeLog(10, 59, 59);
    AddTimeLog(23, 0, 0);
    AddTimeLog(23, 1, 1);   
    AddTimeLog(23, 1, 59);
    AddTimeLog(23, 2, 59);    
    AddTimeLog(23, 3, 59);
    AddTimeLog(23, 4, 59);
    AddTimeLog(23, 5, 59);
    AddTimeLog(23, 10, 1);  
    AddTimeLog(23, 20, 1);        
    AddTimeLog(23, 55, 1);
    AddTimeLog(23, 59, 58);
    AddTimeLog(23, 59, 59); // -- 65160 
  finally
    mmo1.Lines.EndUpdate;
  end;
end;
//*)
end.
