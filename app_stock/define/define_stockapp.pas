unit define_stockapp;

interface
             
const
  // 下载控制台
  AppMutexName_StockDataDownloader_Console = 'StockDataDownloadConsole';
  AppMutexName_StockDataDownloader_Auto = 'StockDataDownloadAuto';  
  AppMutexName_StockSmarter_Console = 'StockSmarterConsole';  
  //AppMutexName_StockDataDownloader = 'StockDataDownloader';

  AppMutexName_StockDataRepairer = 'StockDataRepairer';
  AppMutexName_StockDataWeight = 'StockDataWeight';
  AppMutexName_StockDataPacker = 'StockDataPacker';

  // 交易控制台 (招商证券)
  AppMutexName_StockDeal_Agent_ZSZQ = 'StockDealAgent_ZSZQ';  
  AppMutexName_Stock_Agent_Wind = 'StockAgent_Wind';
  
  AppCmdWndClassName_StockDataDownloader_Console = 'StockDataDownloadConsole';
  AppCmdWndClassName_StockDataDownloader = 'StockDataDownloader';
  
  AppCmdWndClassName_StockDealAgent_ZSZQ = 'StockDealAgent_ZSZQ';    
  AppCmdWndClassName_StockAgent_Wind = 'StockAgent_WIND';
  
implementation

end.
