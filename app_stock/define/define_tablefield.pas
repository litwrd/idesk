unit define_tablefield;

interface
                  
const                  
  // b f g h j k l m q r t w x y z
  Field_AutoKeyID         = 'a';
  Field_CreateTime        = 'c'; // CRUD time create
  Field_EndTime           = 'd'; // CRUD time end
  Field_ValidStatus       = 'e'; // end
  Field_KeyID             = 'i'; // id = index
  Field_Memo              = 'm';
  Field_Name              = 'n'; // name
  Field_Object            = 'o';
  Field_Parent            = 'p';
  Field_Relation          = 'r'; // link relation
  Field_DataSrc           = 's'; // src
  Field_Type              = 't'; // type
  Field_UpdateTime        = 'u'; // CRUD time update
  Field_Value             = 'v';
  Field_Owner             = 'w';
                                    
  // do classifer for some thing is always need todo
  Field_Class       = 'cs';
    
  Field_ObjectA     = 'oa';
  Field_ObjectB     = 'ob';
  // ???
  // Field_Attrib      = 'r';
  // Field_Link        = 'r';
  //Field_Owner             = 'own';
                                
  Field_InfoCreateTime      = 'tc'; // CRUD 信息采集时间 create time
  Field_InfoBeginTime       = 'tb'; // 信息记录开始时间 begin time
  Field_InfoLastUpdateTime  = 'tu'; // CRUD 信息最新持续有效时间 last time
  Field_InfoInvalidTime     = 'td'; // CRUD 信息过期时间 end time

  // f0 f1 f2 ...
  Field_FieldBusi   = 'f';

  FieldType_Key   = #32 + 'INTEGER PRIMARY KEY' + #32;
  FieldType_Key_AutoInc = #32 + 'INTEGER PRIMARY KEY AUTOINCREMENT' + #32;
                                     
  //FieldType_Str = #32 + 'varchar' + #32;
  FieldType_Text  = #32 + 'TEXT' + #32;
  FieldType_Int   = #32 + 'INT NOT NULL DEFAULT 0' + #32;
  FieldType_DateTimeM0= FieldType_Int;
  FieldType_Real  = #32 + 'REAL NOT NULL DEFAULT 0' + #32;

  CreateTableHeader = 'Create Table if not exists ';

  Table_Define_Class      = 'd_cs'; // class
  Table_Class_Link        = 'cs_lk'; // class -- class link
  Table_Stock_Class       = 'sk_cs_lk'; // stock -- class link

  function CreateTableSQL_StockClassDefine: string;
  function CreateTableSQL_StockClass: string;
      
implementation

function CreateTableSQL_StockClassDefine: string;
begin
  Result := CreateTableHeader + #32 + Table_Define_Class + #32 +
      '(' +               
        Field_AutoKeyID  + FieldType_Key_AutoInc + ',' +
        Field_KeyID      + FieldType_Int + ',' +
        Field_DataSrc    + FieldType_Int + ',' +
        Field_Parent     + FieldType_Int + ',' +
        //Field_BeginTime  + FieldType_DateTimeM0 + ',' +
        //Field_EndTime    + FieldType_DateTimeM0 + ',' +
        //Field_CreateTime + FieldType_DateTimeM0 + ',' +
        Field_Name       + FieldType_Text +
      ');';
end;

function CreateTableSQL_StockClass: string;
begin                
  Result := CreateTableHeader + #32 + Table_Stock_Class + #32 +
      '(' +               
        Field_AutoKeyID  + FieldType_Key_AutoInc + ',' +    
        Field_Class      + FieldType_Int + ',' +
        Field_Object     + FieldType_Int + ',' +
        //Field_BeginTime  + FieldType_DateTimeM0 + ',' +
        //Field_EndTime    + FieldType_DateTimeM0 + ',' +
        Field_Memo       + FieldType_Text + 
      ');';
end;

end.
