unit define_stockclass;

interface

uses
  define_datetime,
  define_dealitem;

const
  LinkRelation_ParentChild = 1001;        
  LinkRelation_Extend      = 1009;
    
type
  PRT_ClassDef_Rec    = ^TRT_ClassDef_Rec;
  TRT_ClassDef_Rec    = record
    Id                : integer;
    ParentId          : integer;   
    DataSrc           : Word;
    Name              : string;
    //BeginDateTime : TDateTimeM0;
    //EndDateTime   : TDateTimeM0;
    Parent            : PRT_ClassDef_Rec;
    ExData            : Pointer;
  end;

  { 类之间关系 }
  TLinkRelationType   = Word;
  
  PRT_ClassLink_Rec   = ^TRT_ClassLink_Rec;   
  TRT_ClassLink_Rec   = record
    LinkRelationType  : TLinkRelationType;
    ClassA            : PRT_ClassDef_Rec;
    ClassB            : PRT_ClassDef_Rec;
  end;
  
  PRT_StockClassLink_Rec  = ^TRT_StockClassLink_Rec;
  TRT_StockClassLink_Rec  = record
    Id                : integer;
    ClassDef          : PRT_ClassDef_Rec;
    DealItem          : PRT_DealItem;
    StockCode         : integer;
  end;

  function CheckOutClassDef(AParent: PRT_ClassDef_Rec): PRT_ClassDef_Rec;

implementation

function CheckOutClassDef(AParent: PRT_ClassDef_Rec): PRT_ClassDef_Rec;
begin
  Result := System.New(PRT_ClassDef_Rec);
  FillChar(Result^, SizeOf(TRT_ClassDef_Rec), 0);
  Result.Parent := AParent;
  if nil <> AParent then
    Result.ParentId := AParent.Id;
end;

end.
