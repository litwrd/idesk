unit define_deal;

interface

uses
  define_datetime,
  define_dealitem,
  define_dealmarket,
  define_price;
          
const                               
  DealType_Unknown    = 0;  // 
  DealType_Buy        = 1;  // 买盘
  DealType_Sale       = 2;  // 卖盘
  DealType_Neutral    = 3;  // 中性盘
  DealType_Cancel     = 4;  // 撤单

  DealResult_Unknown  = 0; // 未知
  DealResult_None     = 1; // 未成交
  DealResult_Part     = 2; // 部分成交
  DealResult_All      = 3; // 全部成交
  DealResult_Cancel   = 4; // 撤单
  DealResult_Fail     = 5; // 出错

type
  // 下单
  TRT_DealRequest       = packed record  { 22 }
    // ??? DataVersion     : Word;      
    ActionDate          : TDateStock;
    ActionTime          : TDayTimeM0; // 9:00 3600 * 
    
    ActionID            : integer;
    Price               : TRT_PricePack;
    Num                 : Integer;
    ResultCode          : Word;
    // 0 未成交
    // 1 部分成交    
    // 2 全部成交    
    // 3 撤单
    RequestNo           : Integer; //AnsiString; // 委托编号
    //RequestSeqNo    : AnsiString; // 委托序号
  end;
                   
  // 成交
  TRT_DealResultClose   = packed record   { 28 }   
    // ??? DataVersion     : Word;
    ActionDate          : TDateStock;
    ActionTime          : TDayTimeM0; // 9:00 3600 *
     
    ActionID            : integer;
    TargetID            : Integer; // --> request.ActionID
    Price               : TRT_PricePack; // 成交价格
    Num                 : Integer; // 成交数量
    Amount              : Integer; // 金额
    Fee                 : Integer; // 手续费 等等
  end;

  // 撤单
  TRT_DealResultCancel  = packed record   { 16 }
    // ??? DataVersion     : Word;
    ActionDate          : TDateStock;
    ActionTime          : TDayTimeM0; // 9:00 3600 *

    ActionID            : Integer;    // 4 - 12
    TargetID            : Integer; // 16 --> request.ActionID
  end;

  // 交易请求
  TStore_DealRequest    = packed record  { 32 }
    RequestId           : LongWord;      // 4
    DealType            : Byte;          // 1
    DealResult          : Byte;          // 1
    RequestDate         : TDateStock;    // 2 8
    RequestTime         : TDayTimeM0;    // 2 10 9:00 3600 *
    DealMarket          : TStore_DealMarketCode; // 2 12
    DealItem            : TStore_DealItemCode;  // 4 16

    Price               : TStore_Price;  // 4
    DealPrice           : TStore_Price;  // 4
    Num                 : LongWord;      // 4
    DealNum             : LongWord;      // 4
  end;

implementation

end.
