unit define_message;

interface

uses
  windef_msg;

const                                              
  WM_Data_Base            = WM_CustomAppBase + $100;
  
  WM_Deal_Base            = WM_CustomAppBase + $200;
  
  WM_Deal_Request_Buy     = WM_Deal_Base + 1;
  WM_Deal_Request_Sale    = WM_Deal_Base + 2;
  WM_Deal_Request_Cancel  = WM_Deal_Base + 3;
                                                
  WM_Export_SetConfig     = WM_Data_Base + 2;
  WM_Export_Data          = WM_Data_Base + 1;
  WM_Export_FirstListInfo = WM_Data_Base + 3;
  WM_Export_InfoF10_1     = WM_Data_Base + 11;

  Config_Export_DownClick   = 1;
  Config_Export_MinuteData  = 2;

implementation

end.
