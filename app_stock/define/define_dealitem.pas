unit define_dealitem;

interface
                   
uses
  Define_Price,
  define_dealmarket;
  
const
  // 股票及股票指数
  DBType_Unknown        = 0;
  DBType_Item_China     = 1;
  DBType_Index_China    = 2;
  DBType_All_Memory     = 3;
  DBType_None           = 9;

type            
  TDealCodePack = integer;

  PRT_DealItem          = ^TRT_DealItem;   
  TOnDealItemEvent      = procedure(ADealItem: PRT_DealItem) of object;
  TOnDealItemFunc       = function(AParam: Pointer): PRT_DealItem of object;

  TRT_DealItem          = packed record
    IsDataChange        : Byte;
    DBType              : Byte;
    Country             : Word;
    //MarketCode          : array[0..3] of AnsiChar;
    sMarketCode         : AnsiString;
    iMarketCode         : TRT_DealMarketCode;
    //Code                : array[0..7] of AnsiChar;
    sCode               : AnsiString;
    iCode               : TDealCodePack;
    //Name                : array[0..5] of WideChar;
    Name                : AnsiString;
    //ItemFlag            : Byte;
    FirstDealDate       : Word;          // 2
    LastDealDate        : Word;          // 2
    EndDealDate         : Word;          // 2
  end;

  TStore_DealItemCode   = packed record
    Value               : LongWord;
  end;
  
  PStore_DealItem32     = ^TStore_DealItem32;
  TStore_DealItem32     = packed record // 128
    Code                : array[0..12 - 1] of AnsiChar; // 12   
    Name                : array[0..6 - 1] of WideChar; // 12    
    Country             : Word; // 2 26
    FirstDealDate       : Word; // 2 28
    EndDate             : Word; // 2 30
  end;

  PStore_DealItem32Rec  = ^TStore_DealItem32Rec;
  TStore_DealItem32Rec  = packed record
    ItemRecord          : TStore_DealItem32;
    Reserve             : array[0..32 - 1 - SizeOf(TStore_DealItem32)] of Byte;
  end;

  function getStockCodePack(AStockCode: AnsiString) : integer; overload;
  function getSimpleStockCodeByPackCode(AStockCode: integer) : AnsiString; overload;
  function getFullStockCodeByPackCode(AStockCode: integer) : AnsiString; overload;
    
implementation

uses
  SysUtils, sysdef_string;
  
function getStockCodePack(AStockCode: AnsiString) : integer;
var
  tmpMarketCode: string;
begin
  Result := 0;                 
  if 6 > Length(AStockCode) then
    AStockCode := Copy('000000', 1, 6 - length(AStockCode)) + AStockCode;
  tmpMarketCode := '';      
  if 6 < Length(AStockCode) then
  begin
    tmpMarketCode := Copy(AStockCode, 1, Length(AStockCode) - 6);
    AStockCode := Copy(AStockCode, Length(AStockCode) - 6 + 1, maxint);
  end;
  if 6 = Length(AStockCode) then
  begin
    if '6' = AStockCode[FirstStringIndex] then
    begin
      Result := StrToIntDef(AStockCode, 0);
    end else
    begin
      if '' <> tmpMarketCode then
      begin
        if SameText(Market_SH, tmpMarketCode) then
        begin
          Result := StrToIntDef(AStockCode, 0);
        end else
        begin
          Result := StrToIntDef('1' + AStockCode, 0);
        end;
      end else
      begin
        Result := StrToIntDef('1' + AStockCode, 0);
      end;
    end;
  end;
end;

function getSimpleStockCodeByPackCode(AStockCode: integer) : AnsiString;
begin
  Result := IntToStr(AStockCode);
  if 6 > Length(Result) then
  begin
    Result := Copy('000000', 1, 6 - Length(Result)) + Result;
    exit;
  end;
  if 6 < Length(Result) then
  begin
    Result := Copy(Result, Length(Result) - 6 + 1, maxint);
    exit;
  end;
  Result := Result;
end;
           
function getFullStockCodeByPackCode(AStockCode: integer) : AnsiString;
begin
  Result := IntToStr(AStockCode);
  if 6 > Length(Result) then
  begin
    Result := Market_SH + Copy('000000', 1, 6 - Length(Result)) + Result;
    exit;
  end;
  if 6 < Length(Result) then
  begin
    Result := Market_SZ + Copy(Result, Length(Result) - 6 + 1, maxint);
    exit;
  end;
  Result := Market_SH + Result;
end;

end.
