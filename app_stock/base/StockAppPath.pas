unit StockAppPath;

interface

uses
  SysUtils, BaseApp, BaseWinApp, 
  define_datasrc,
  define_dealitem,
  define_dealstore_file;
  
type
  TStockAppPathData = record
    StockItemDBPathRoot: string;
    StockIndexDBPathRoot: string;
        
    StockDetailDBPath_163: string;
    StockDetailDBPath_Sina: string;
  end;
  
  TStockAppPath = class(TBaseWinAppPath)
  protected
    fStockAppPathData: TStockAppPathData;
    function GetDataBasePath(ADBType, ADataType: integer; ADataSrc: integer): WideString; overload; override;
    function GetDataBasePath(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString; overload; override;
    function GetInstallPath: WideString; override;
  public                    
    constructor Create(App: TBaseApp); override;
    function GetRootPath: WideString; override;
    
    function GetFileRelativePath(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString; override;
    function GetFilePath(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString; override;

    function GetFileName(ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer; AFileExt: WideString): WideString; override; 
    function GetFileExt(ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString; override;

    function GetFileUrl(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer; AFileExt: WideString): WideString; override;

    function CheckOutFileUrl(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer; AFileExt: WideString): WideString; override;
  end;
  
implementation

uses
  IniFiles,
  {$IFDEF LOG}
  UtilsLog,
  {$ENDIF}
  define_price;
            
{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockAppPath.pas';
{$ENDIF}
             
{ TStockDay163AppPath }

constructor TStockAppPath.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStockAppPathData, SizeOf(fStockAppPathData), 0);
  fStockAppPathData.StockItemDBPathRoot := FilePath_StockData;
  fStockAppPathData.StockIndexDBPathRoot := FilePath_StockIndexData;
end;
                          
function TStockAppPath.GetRootPath: WideString;
begin
  Result := GetInstallPath;
end;
                    
function TStockAppPath.GetFileRelativePath(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString; 
var
  tmpDataSrcCode: string;
  tmpFileUrl: string;
  tmpReadIni: string;
  tmpIni: TIniFile;
begin
  Result := '';
  if DBType_Item_China = ADBType then
    Result := fStockAppPathData.StockItemDBPathRoot;
  if DBType_Index_China = ADBType then
    Result := fStockAppPathData.StockIndexDBPathRoot;
  if '' = Result then
    exit;
  tmpDataSrcCode := GetDataSrcCode(ADataSrc);
  case ADataType of
    DataType_Item: begin  
      Result := Result + '\' + 's_dic' + '\';
    end;      
    DataType_Class: begin
      Result := Result + '\' + 's_class' + '\';
    end;          
    DataType_Info: begin
      Result := Result + '\' + 's_info' + '\';
    end;
    DataType_DayData: begin
      if AParamType = Integer(weightNone) then
      begin
        Result := Result + '\' + FileExt_StockDay + tmpDataSrcCode + '\';
      end else
      begin
        Result := Result + '\' + FileExt_StockDay + tmpDataSrcCode + IntToStr(AParamType) + '\';
      end;
    end;
    DataType_DetailData: begin
      if DBType_Item_China = ADBType  then
      begin
        if DataSrc_163 = ADataSrc then
        begin
          if '' <> fStockAppPathData.StockDetailDBPath_163 then
          begin
            //Log(LOGTAG, 'DataType_DetailData 163: ' + fStockAppPathData.StockDetailDBPath_163);
            Result := fStockAppPathData.StockDetailDBPath_163;    
            exit;
          end;
        end;      
        if DataSrc_Sina = ADataSrc then
        begin
          if '' <> fStockAppPathData.StockDetailDBPath_Sina then
          begin                                              
            //Log(LOGTAG, 'DataType_DetailData sina: ' + fStockAppPathData.StockDetailDBPath_Sina);
            Result := fStockAppPathData.StockDetailDBPath_Sina;
            exit;
          end;
        end;
        tmpReadIni := '';
        tmpFileUrl := ChangeFileExt(ParamStr(0), '.ini');
        if FileExists(tmpFileUrl) then
        begin
          tmpIni := TIniFile.Create(tmpFileUrl);
          try
            tmpReadIni := tmpIni.ReadString('ExportPath', 'DetailRoot_' + tmpDataSrcCode, '');
          finally
            tmpIni.Free;
          end;
        end;
        if '' = tmpReadIni then
        begin           
          tmpFileUrl := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'StockApp.ini';
          if FileExists(tmpFileUrl) then
          begin
            tmpIni := TIniFile.Create(tmpFileUrl);
            try
              tmpReadIni := tmpIni.ReadString('ExportPath', 'DetailRoot_' + tmpDataSrcCode, '');
            finally
              tmpIni.Free;
            end;
          end;
        end;
        if '' = tmpReadIni then
        begin
          tmpReadIni := Result + '\' + FileExt_StockDetail + tmpDataSrcCode + '\';
        end;       
        if DataSrc_163 = ADataSrc then
        begin
          fStockAppPathData.StockDetailDBPath_163 := tmpReadIni;
          //Log(LOGTAG, 'fStockAppPathData.StockDetailDBPath_163: ' + fStockAppPathData.StockDetailDBPath_163);
        end;
        if DataSrc_Sina = ADataSrc then
        begin
          fStockAppPathData.StockDetailDBPath_Sina := tmpReadIni;
          //Log(LOGTAG, 'fStockAppPathData.StockDetailDBPath_Sina: ' + fStockAppPathData.StockDetailDBPath_Sina);          
        end;
        Result := tmpReadIni;
      end else
      begin      
        Result := Result + '\' + FileExt_StockDetail + tmpDataSrcCode + '\';
      end;
    end;
    DataType_MinuteData: begin    
      Result := Result + '\' + FileExt_StockMinute + IntToStr(AParamType) + tmpDataSrcCode + '\';
    end;
    DataType_InstantData: begin    
      Result := Result + '\' + FileExt_StockInstant + tmpDataSrcCode + '\';
    end;
    DataType_ValueData: begin
      Result := Result + '\' + FileExt_StockSummaryValue + tmpDataSrcCode + '\';
    end;
    else begin
      if '' <> tmpDataSrcCode then
        Result := Result + '\' + 's' + tmpDataSrcCode + '\'
      else
        Result := Result + '\';
    end;
  end;
end;

function TStockAppPath.GetDataBasePath(ADBType, ADataType: integer; ADataSrc: integer): WideString;
begin
  Result := GetDataBasePath(ADBType, ADataType, ADataSrc, 0, nil);
end;
            
function TStockAppPath.GetDataBasePath(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString;
var
  tmpPath: string;
begin
  Result := '';      
  tmpPath := GetFileRelativePath(ADBType, ADataType, ADataSrc, AParamType, AParam);
  if '' = tmpPath then
    exit;
  if DataType_DetailData = ADataType then
  begin                
    {$IFDEF LOG}
    //Log(LOGTAG, 'GetDataBasePath Detail1:' + tmpPath);
    {$ENDIF}
    if '' <> tmpPath then
    begin
      if DirectoryExists(tmpPath) then
      begin
        if '' = ExtractFileDrive(tmpPath) then
        begin
          if PathDelim = tmpPath[1] then
          begin
            Result := ExtractFilePath(ParamStr(0)) + tmpPath;
          end else
          begin
            Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + tmpPath;
          end;
        end else
        begin
          Result := tmpPath;
        end;
      end else
      begin          
        if '' = ExtractFileDrive(tmpPath) then
        begin
          Result := GetInstallPath + tmpPath;
        end else
        begin
          Result := tmpPath;
        end;
      end;
    end;
  end else
  begin
    Result := GetInstallPath + tmpPath;
  end;
  if '' <> Result then
  begin
    Sysutils.ForceDirectories(Result);
  end;
end;

function TStockAppPath.GetFilePath(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString;
begin
  Result := '';
  case ADataType of
    DataType_DayData: begin
      Result := GetDataBasePath(ADBType, ADataType, ADataSrc, AParamType, AParam);
    end;
    DataType_DetailData: begin
      if nil = AParam then
        exit;
      Result := GetDataBasePath(ADBType, ADataType, ADataSrc, AParamType, AParam);
      {$IFDEF LOG}
      Log(LOGTAG, 'GetFilePath Detail:' + Result);
      {$ENDIF}
      if '' <> Result then
      begin
        Result := Result + Copy(PRT_DealItem(AParam).sCode, 1, 4) + '\' + PRT_DealItem(AParam).sCode + '\';
        if 0 < AParamType then
        begin
          Result := Result + Copy(FormatDateTime('yyyymmdd', AParamType), 1, 4) + '\';
        end;
      end;
    end;
    else begin
      Result := GetDataBasePath(ADBType, ADataType, ADataSrc, AParamType, AParam);
    end;
  end;
end;

function TStockAppPath.GetFileName(ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer; AFileExt: WideString): WideString;
begin
  Result := '';
  case ADataType of
    DataType_Item: begin
      Result := 'items';
    end;
    DataType_DayData: begin  
      if nil <> AParam then
      begin
        Result := PRT_DealItem(AParam).sCode;
      end;
    end;
    DataType_MinuteData: begin
      Result := PRT_DealItem(AParam).sCode;
    end;    
    DataType_DetailData: begin
      Result := PRT_DealItem(AParam).sCode + '_' + FormatDateTime('yyyymmdd', AParamType);
    end;    
    DataType_Class: begin    
      Result := 'sclass';
    end;
    DataType_Info: begin
    end;
  end;
  if '' <> Result then
  begin
    if '' <> AFileExt then
    begin
      if Pos('.', AFileExt) > 0 then
      begin
        Result := Result + AFileExt;
      end else
      begin
        Result := Result + '.' + AFileExt;
      end;
    end else
    begin
      Result := Result + '.' + GetFileExt(ADataType, ADataSrc, AParamType, AParam);
    end;
  end;
  {$IFDEF LOG}
  Log(LOGTAG, 'GetFileName:' + Result);
  {$ENDIF}
end;

function TStockAppPath.GetFileExt(ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer): WideString;
begin
  Result := '';
  case ADataType of
    DataType_Item: begin
      Result := 'dic';
    end;
    DataType_DayData: begin
      if Integer(weightNone) = AParamType then
      begin
        Result := FileExt_StockDay + '_' + IntToStr(ADataSrc);
      end else
      begin
        Result := FileExt_StockDay + IntToStr(AParamType) + '_' + IntToStr(ADataSrc);
      end;
    end;           
    DataType_Class: begin    
      Result := 's3';
    end;
    DataType_Info: begin
    end;
    DataType_DetailData: begin   
      Result := FileExt_StockDetail + IntToStr(ADataSrc);
    end;
    DataType_MinuteData: begin       
      Result := FileExt_StockMinute + inttostr(AParamType) + '_' + IntToStr(ADataSrc);
    end;         
    DataType_InstantData: begin
      Result := FileExt_StockInstant + IntToStr(ADataSrc);
    end;
    DataType_WeightData: begin
      Result := FileExt_StockWeight + IntToStr(ADataSrc);
    end;
    DataType_ValueData: begin
      Result := FileExt_StockSummaryValue + IntToStr(ADataSrc);
    end;
  end;
end;

function TStockAppPath.CheckOutFileUrl(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer; AFileExt: WideString): WideString;
var
  tmpFileName: AnsiString;
  tmpFilePath: AnsiString;
begin
  Result := '';
  tmpFilePath := GetFilePath(ADBType, ADataType, ADataSrc, AParamType, AParam);
  tmpFileName := GetFileName(ADataType, ADataSrc, AParamType, AParam, AFileExt);  
  if ('' <> tmpFilePath) and ('' <> tmpFileName) then
  begin
    ForceDirectories(tmpFilePath);
    Result := tmpFilePath + tmpFileName;
  end;
end;

function TStockAppPath.GetFileUrl(ADBType, ADataType: integer; ADataSrc: integer; AParamType: integer; AParam: Pointer; AFileExt: WideString): WideString;
var
  tmpExeName: AnsiString;
  tmpFileName: AnsiString;
  tmpFilePath: AnsiString;
begin
  Result := '';  
  {$IFDEF LOG}  
  Log(LOGTAG, 'GetFileUrl begin');
  {$ENDIF}           
  if DataType_Item = ADataType then
  begin
    tmpExeName := lowercase(ParamStr(0));
    tmpFileName := '';
    if 0 < Pos('sh', tmpExeName) then
      tmpFileName := 'items_sh.dic';
    if 0 < Pos('sz', tmpExeName) then
      tmpFileName := 'items_sz.dic';
    if 0 < Pos('zx', tmpExeName) then
      tmpFileName := 'items_zx.dic';
    if 0 < Pos('cy', tmpExeName) then
      tmpFileName := 'items_cy.dic';
    if '' <> tmpFileName then
    begin
      tmpFilePath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
      if '' <> tmpFilePath then
      begin
        if FileExists(IncludeTrailingPathDelimiter(tmpFilePath) + tmpFileName) then
        begin
          Result := IncludeTrailingPathDelimiter(tmpFilePath) + tmpFileName;
        end;
      end;
      if '' = Result then
      begin
        tmpFilePath := GetFilePath(ADBType, ADataType, ADataSrc, AParamType, AParam); 
        if '' <> tmpFilePath then
        begin
          if FileExists(IncludeTrailingPathDelimiter(tmpFilePath) + tmpFileName) then
          begin
            Result := IncludeTrailingPathDelimiter(tmpFilePath) + tmpFileName;
          end;
        end;
      end;
    end;  
    if '' = Result then
    begin
      tmpFileName := ChangeFileExt(ParamStr(0), '.dic');
      if FileExists(tmpFileName) then
        Result := tmpFileName;
    end;
  end;
  if '' = Result then
  begin
    tmpFilePath := GetFilePath(ADBType, ADataType, ADataSrc, AParamType, AParam);
    if ('' <> tmpFilePath) then
    begin
      tmpFileName := GetFileName(ADataType, ADataSrc, AParamType, AParam, AFileExt);
      if ('' <> tmpFileName) then
      begin
        Result := tmpFilePath + tmpFileName;
      end;
    end;
  end; 
  {$IFDEF LOG}  
  Log(LOGTAG, 'GetFileUrl end:' + Result);
  {$ENDIF}          
end;

function TStockAppPath.GetInstallPath: WideString;
begin
  Result := ExtractFilePath(ParamStr(0));
end;

end.

