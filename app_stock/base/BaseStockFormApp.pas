unit BaseStockFormApp;

interface

uses
  BaseWinFormApp, BaseWinApp, BaseApp, SysUtils,
  define_dealmarket,
  define_dealitem,
  StockAppPath,
  db_dealitem,     
  {$IFDEF DEALCLASS}
  //db_dealclass,
  DBStockClass,       
  {$ENDIF}
  {$IFDEF DEALINSTANT}
  db_quote_instant,
  {$ENDIF}
  db_stock;
  
type
  TBaseStockAppData = record
    StockDB       : TDBStock;
  end;
  
  TBaseStockApp = class(TBaseWinFormApp)
  protected
    fBaseStockAppData: TBaseStockAppData; 
    function GetPath: TBaseAppPath; override;    
    function GetStockItemDB: TDBDealItem;    
    function GetStockIndexDB: TDBDealItem;
    {$IFDEF DEALCLASS}
    function GetStockClassDB: TDBStockClass;
    {$ENDIF}
    {$IFDEF DEALINSTANT}
    function GetQuoteInstantDB: TDBQuoteInstant;
    {$ENDIF}
    function GetStockAppPath: TStockAppPath;
  public
    constructor Create(AppClassId: AnsiString); override;
    destructor Destroy; override;     
    procedure InitializeDBStockItem(AIsLoadDBStockItemDic: Boolean = True);
    procedure InitializeDBStockIndex;
    {$IFDEF DEALINSTANT}
    procedure InitializeDBQuoteInstant; overload;
    procedure InitializeDBQuoteInstant(ADBDealItem: TDBDealItem); overload;
    {$ENDIF}
    property StockItemDB: TDBDealItem read GetStockItemDB;
    property StockIndexDB: TDBDealItem read GetStockIndexDB;
    {$IFDEF DEALINSTANT}
    property QuoteInstantDB: TDBQuoteInstant read GetQuoteInstantDB;
    {$ENDIF}                  
    {$IFDEF DEALCLASS}
    property StockClassDB: TDBStockClass read GetStockClassDB;
    {$ENDIF}
    property StockDB: TDBStock read fBaseStockAppData.StockDB;  
    property StockAppPath: TStockAppPath read GetStockAppPath;
  end;

var
  GlobalBaseStockApp: TBaseStockApp = nil;
    
implementation

uses               
  {$IFDEF DEALINSTANT}
  define_stock_quotes_instant,
  {$ENDIF}
  db_dealitem_Load;
                 
{$IFNDEF RELEASE}  
const
  LOGTAG = 'BaseStockFormApp.pas';
{$ENDIF}
    
constructor TBaseStockApp.Create(AppClassId: AnsiString);
begin
  inherited;
  FillChar(fBaseStockAppData, SizeOf(fBaseStockAppData), 0);  
  fBaseStockAppData.StockDB := TDBStock.Create;
  GlobalBaseStockApp := Self;
end;

destructor TBaseStockApp.Destroy;
begin
  if GlobalBaseStockApp = Self then
    GlobalBaseStockApp := nil;
  if nil <> fBaseStockAppData.StockDB then
  begin
    fBaseStockAppData.StockDB.Free;
    fBaseStockAppData.StockDB := nil;
  end;
  inherited;
end;

function TBaseStockApp.GetPath: TBaseAppPath;
begin
  Result := GetStockAppPath;
end;

function TBaseStockApp.GetStockAppPath: TStockAppPath;
begin
  if nil = fBaseWinAppData.AppPath then
    fBaseWinAppData.AppPath := TStockAppPath.Create(Self);
  Result := TStockAppPath(fBaseWinAppData.AppPath);
end;
                           
function TBaseStockApp.GetStockIndexDB: TDBDealItem;
begin
  Result := fBaseStockAppData.StockDB.StockIndexDB;
end;

function TBaseStockApp.GetStockItemDB: TDBDealItem;
begin
  Result := fBaseStockAppData.StockDB.StockItemDB;
end;

{$IFDEF DEALINSTANT}
function TBaseStockApp.GetQuoteInstantDB: TDBQuoteInstant;
begin
  Result := fBaseStockAppData.StockDB.QuoteInstantDB;
end;
{$ENDIF}

{$IFDEF DEALCLASS}
function TBaseStockApp.GetStockClassDB: TDBStockClass;
begin
  Result := fBaseStockAppData.StockDB.StockClassDB;
end;
{$ENDIF}

procedure TBaseStockApp.InitializeDBStockItem(AIsLoadDBStockItemDic: Boolean = True);

  function AddIndexStockItem(AMarket, ACode, AName: string; AFirstDate: Word): PRT_DealItem;   
  begin
    Result := fBaseStockAppData.StockDB.StockIndexDB.AddDealItem(AMarket, ACode);
    if nil <> Result then
    begin
      Result.FirstDealDate := AFirstDate;
      Result.Name := AName;
    end;                          
  end;

begin              
  if nil = fBaseStockAppData.StockDB.StockItemDB then
  begin              
    fBaseStockAppData.StockDB.StockItemDB := TDBDealItem.Create(DBType_Item_China, 0);
    if AIsLoadDBStockItemDic then
    begin
      db_dealitem_Load.LoadDBStockItemDic(Self, fBaseStockAppData.StockDB.StockItemDB);
    end;
  end;        
  if nil = fBaseStockAppData.StockDB.StockIndexDB then
  begin              
    fBaseStockAppData.StockDB.StockIndexDB := TDBDealItem.Create(DBType_Index_China, 0);

    fBaseStockAppData.StockDB.IndexItem_SH := AddIndexStockItem(Market_SH, '999999', '上证', Trunc(EncodeDate(1990, 12, 19)));
    //AddIndexStockItem(Market_SH, '000300', '沪深300', Trunc(EncodeDate(2005, 1, 4)));
    //AddIndexStockItem(Market_SH, '000016', '上证50');
    //AddIndexStockItem(Market_SH, '000010', '上证180');
                                                          
    //AddIndexStockItem(Market_SH, '000903', '中证100');
    //AddIndexStockItem(Market_SH, '000905', '中证500');
    
    fBaseStockAppData.StockDB.IndexItem_SZ := AddIndexStockItem(Market_SZ, '399001', '深成', Trunc(EncodeDate(1991, 4, 3)));
    fBaseStockAppData.StockDB.IndexItem_ZX := AddIndexStockItem(Market_SZ, '399005', '中小', Trunc(EncodeDate(2006, 1, 24)));
    fBaseStockAppData.StockDB.IndexItem_CY := AddIndexStockItem(Market_SZ, '399006', '创业', Trunc(EncodeDate(2010, 6, 1)));     
    //AddIndexStockItem(Market_SZ, '399106', '深综');
    //AddIndexStockItem(Market_SZ, '399007', '深证300');
    //AddIndexStockItem(Market_SZ, '399008', '中小300');
  end;
end;

procedure TBaseStockApp.InitializeDBStockIndex;
begin              
  if nil = fBaseStockAppData.StockDB.StockIndexDB then
  begin              
    fBaseStockAppData.StockDB.StockIndexDB := TDBDealItem.Create(DBType_Index_China, 0);
  end;
end;

{$IFDEF DEALINSTANT}
procedure TBaseStockApp.InitializeDBQuoteInstant;
begin
  if nil = fBaseStockAppData.StockDB.QuoteInstantDB then
  begin
    fBaseStockAppData.StockDB.QuoteInstantDB := TDBQuoteInstant.Create(DBType_All_Memory);
  end;
end;

procedure TBaseStockApp.InitializeDBQuoteInstant(ADBDealItem: TDBDealItem);
var
  i: integer;
  tmpDealItem: PRT_DealItem;
  tmpInstantQuote: PRT_InstantQuote;
begin
  InitializeDBQuoteInstant;
  if nil <> ADBDealItem then
  begin
    for i := 0 to ADBDealItem.RecordCount - 1 do
    begin
      tmpDealItem := ADBDealItem.Items[i];

      tmpInstantQuote := fBaseStockAppData.StockDB.QuoteInstantDB.CheckOutInstantQuote(ADBDealItem.DataSrcID, tmpDealItem.iCode);
      tmpInstantQuote.Item := tmpDealItem;
    end;
  end;
end;
{$ENDIF}

end.
