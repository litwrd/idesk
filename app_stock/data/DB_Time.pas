unit DB_Time;

interface

uses
  define_timemachine,
  BaseDataSet;

type
  TTimeEventDB = class(TBaseDataSetAccess)
  protected
  public
    constructor Create(ADBType, ADataSrcId: integer); reintroduce;
    destructor Destroy; override;   
    class function DataTypeDefine: integer; override;
  end;
  
implementation

uses
  define_dealstore_file;
  
{ TTimeEventDB }

constructor TTimeEventDB.Create(ADBType, ADataSrcId: integer);
begin
  inherited Create(ADBType, ADataSrcId); 
end;
         
destructor TTimeEventDB.Destroy;
begin

  inherited;
end;

class function TTimeEventDB.DataTypeDefine: integer;
begin
  Result := DataType_TimeEventItem;
end;

end.
