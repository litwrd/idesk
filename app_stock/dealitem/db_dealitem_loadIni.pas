unit db_dealitem_loadIni;

interface

uses
  BaseApp, db_dealItem, Define_DealMarket;
  
  procedure LoadDBStockItemIni(App: TBaseApp; ADB: TDBDealItem);     
  procedure LoadDBStockItemIniFromFile(App: TBaseApp; ADB: TDBDealItem; AFileUrl: string);
  procedure SaveDBStockItemIniToFile(App: TBaseApp; ADB: TDBDealItem; AFileUrl: string);

implementation

uses
  define_dealItem,
  define_dealstore_file,
  Windows,
  Classes,
  SysUtils,
  IniFiles;

procedure LoadDBStockItemIni(App: TBaseApp; ADB: TDBDealItem);
var                           
  tmpFilePath: string;
  tmpFileUrl: string;
  tmpFileName: string;
  tmpDicIni: TIniFile;
  tmpCnt: integer;
  idxFile: integer;
begin
  tmpFilePath := App.Path.DataBasePath[ADB.DBType, DataType_Item, 0] + 'sdic\';
  tmpFileUrl := tmpFilePath + 'items.ini';
  if App.Path.IsFileExists(tmpFileUrl) then
  begin
    tmpDicIni := TIniFile.Create(tmpFileUrl);
    try
      tmpCnt := tmpDicIni.ReadInteger('items', 'count', 0);
      for idxFile := 0 to tmpCnt - 1 do
      begin
        tmpFileName := tmpDicIni.ReadString('items', 'item' + IntToStr(idxFile + 1), '');
        tmpFileUrl := tmpFilePath + tmpFileName;
        if App.Path.IsFileExists(tmpFileUrl) then
        begin
          LoadDBStockItemIniFromFile(App, ADB, tmpFileUrl);      
        end;
      end;
    finally
      tmpDicIni.Free;
    end;
  end;
end;

procedure LoadDBStockItemIniFromFile(App: TBaseApp; ADB: TDBDealItem; AFileUrl: string);
var            
  i: integer;
  tmpSections: TStringList;
  tmpStockItem: PRT_DealItem;   
  tmpItemsIni: TIniFile;   
  tmpStockCode: string;
  tmpMarket: string;
begin
  if not App.Path.IsFileExists(AFileUrl) then
    exit;
  tmpSections := TStringList.Create;
  tmpItemsIni := TIniFile.Create(AFileUrl);
  try
    tmpItemsIni.ReadSections(tmpSections);
    for i := 0 to tmpSections.Count - 1 do
    begin
      tmpStockCode := Trim(LowerCase(tmpSections[i]));
      tmpMarket := '';
      if '' <> tmpStockCode then
      begin          
        if Pos(Market_SH, tmpStockCode) > 0 then
        begin
          tmpMarket := Market_SH;    
          tmpStockCode := Trim(Copy(tmpStockCode, 3, maxint));
        end;
        if Pos(Market_SZ, tmpStockCode) > 0 then
        begin
          tmpMarket := Market_SZ;
          tmpStockCode := Trim(Copy(tmpStockCode, 3, maxint));
        end;          
        if Length(tmpStockCode) = 6 then
        begin
          if '6' = tmpStockCode[1] then
          begin
            tmpMarket := Market_SH
          end else
          begin
            tmpMarket := Market_SZ;
          end;
        end;
        if ('' <> tmpMarket) and (6 > Length(tmpStockCode)) then
        begin
          tmpStockCode := Copy('000000', 1, 6 - Length(tmpStockCode)) + tmpStockCode;
        end;
        if ('' <> tmpMarket) and (6 = Length(tmpStockCode)) then
        begin
          tmpStockItem := ADB.AddDealItem(tmpMarket, tmpStockCode);
          if nil <> tmpStockItem then
          begin
            tmpStockItem.FirstDealDate := tmpItemsIni.ReadInteger(tmpSections[i], 'FirstDeal', 0);
            if 0 = tmpStockItem.FirstDealDate then
            begin
              tmpStockItem.FirstDealDate := tmpItemsIni.ReadInteger(tmpSections[i], 'f', 0);
            end;
            tmpStockItem.EndDealDate := tmpItemsIni.ReadInteger(tmpSections[i], 'e', 0);
            if 0 = tmpStockItem.EndDealDate then
            begin
              tmpStockItem.EndDealDate := tmpItemsIni.ReadInteger(tmpSections[i], 'EndDeal', 0);
            end;
            tmpStockItem.Name := Trim(tmpItemsIni.ReadString(tmpSections[i], 'n', ''));
            if '' = tmpStockItem.Name then
            begin
              tmpStockItem.Name := Trim(tmpItemsIni.ReadString(tmpSections[i], 'name', ''));
            end;
          end;
        end;
      end;
    end;
  finally
    tmpItemsIni.Free;
    tmpSections.Free;
  end;
end;

procedure SaveDBStockItemIniToFile(App: TBaseApp; ADB: TDBDealItem; AFileUrl: string);
var
  i: integer;      
  tmpStockItem: PRT_DealItem;   
  tmpItemsIni: TIniFile;
  tmpSection: string;  
begin
  tmpItemsIni := TIniFile.Create(AFileUrl);
  try
    for i := 0 to ADB.RecordCount - 1 do
    begin
      tmpStockItem := ADB.RecordItem[i];
      if '' <> tmpStockItem.Name then
      begin
        tmpSection := tmpStockItem.sMarketCode + tmpStockItem.sCode;
        tmpItemsIni.WriteString(tmpSection, 'n', tmpStockItem.Name);
        if 0 < tmpStockItem.FirstDealDate then
          tmpItemsIni.WriteInteger(tmpSection, 'f', tmpStockItem.FirstDealDate);
      end;
    end;
  finally
    tmpItemsIni.Free;
  end;
end;

end.
