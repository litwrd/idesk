unit db_dealclass_load;

interface

{$IFDEF DEALCLASS}
uses
  baseApp,       
  db_dealclass;
            
  procedure LoadDBStockClassDic(App: TBaseApp; ADB: TDBDealClassDictionary);
  procedure LoadDBStockClassDicFromFile(App: TBaseApp; ADB: TDBDealClassDictionary; AFileUrl: string);
{$ENDIF}

implementation
       
{$IFDEF DEALCLASS}
uses
  define_tablefield,
  define_dealclass,
  define_datasrc,
  Sqlite3Wrap,
  SQLite3WrapApi,
  SQLite3Utils,
  Sqlite3;

(*
http://vip.stock.finance.sina.com.cn/
    quotes_service/api/json_v2.php/Market_Center.getHQNodeStockCount?node=gn_jght
(new String("130"))
    
*)
procedure LoadDBStockClassDicFromFile(App: TBaseApp; ADB: TDBDealClassDictionary; AFileUrl: string);
var
  tmpSqliteDB: TSQLite3DataBase;
  tmpQuery: TSQLite3Statement;
  tmpSQL: string;
  tmpDefClass: PRT_DefClass;
  tmpDataSrc: integer;
  tmpId: integer;
  tmpName: string;
begin
  tmpSqliteDB := TSQLite3DataBase.Create;
  try
    tmpSqliteDB.Open(AFileUrl,
      SQLITE_OPEN_CREATE or
      SQLITE_OPEN_READWRITE or
      SQLITE_OPEN_NOMUTEX
    );
    tmpSqliteDB.Execute(CreateTableSQL_StockClassDefine);
    tmpSqliteDB.Execute(CreateTableSQL_StockClass);
    (*//
    tmpSQL := 'Insert Into ' + Table_StockClassDefine + '(' +
              Field_DataSrc + ',' +
              Field_Name +               
              ')' + ' values ' + '(' +
              ':' + Field_DataSrc + ',' +
              ':' + Field_Name +               
              ');';
    tmpQuery := tmpSqliteDB.Prepare(tmpSQL);
    tmpQuery.BindInt(1, DataSrc_Sina);     
    tmpQuery.BindText(2, '֤�����ҵ');
    tmpRet := tmpQuery.Step;   // SQLITE_DONE 101
    //*)

    tmpSQL := ' Select ' +
              Field_DataSrc    + ',' +  // 0
              Field_ID_AutoInc + ',' +  // 1              
              Field_ID         + ',' +  // 2
              Field_ID_Parent  + ',' +  // 3
              Field_BeginTime  + ',' +  // 4
              Field_EndTime    + ',' +  // 5
              Field_CreateTime + ',' +  // 6
              Field_Name       +        // 7
              ' from ' + Table_StockClassDefine;
    tmpQuery := tmpSqliteDB.Prepare(tmpSQL);
    if nil <> tmpQuery then
    begin
      while SQLite_Row = tmpQuery.Step do
      begin
        tmpDataSrc := tmpQuery.ColumnInt(0);
        tmpId := tmpQuery.ColumnInt(1);
        tmpName := tmpQuery.ColumnText(7);
        

        tmpDefClass := ADB.CheckOutDealClass(tmpDataSrc, tmpId, tmpName);
        tmpDefClass.DataSrc   := tmpDataSrc;
        tmpDefClass.AutoId    := tmpQuery.ColumnInt(1);
        tmpDefClass.Id        := tmpId;
        tmpDefClass.ParentId  := tmpQuery.ColumnInt(3);
        //tmpDefClass.BeginDateTime := tmpQuery.ColumnInt(3);
        //tmpDefClass.EndDateTime := tmpQuery.ColumnInt(4);
        tmpDefClass.Name      := tmpName;
      end;
    end;
  finally
    tmpSqliteDB.Free;
  end;  
end;

procedure LoadDBStockClassDic(App: TBaseApp; ADB: TDBDealClassDictionary);
var
  tmpFileUrl: string;
begin
  tmpFileUrl := App.Path.GetFileUrl(ADB.DBType, ADB.DataType, ADB.DataSrcID);
  //if App.Path.IsFileExists(tmpFileUrl) then
  begin
    LoadDBStockClassDicFromFile(App, ADB, tmpFileUrl);  
  end;
end;
{$ENDIF}

end.
