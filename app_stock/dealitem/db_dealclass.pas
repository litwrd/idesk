unit db_dealclass;

interface

{$IFDEF DEALCLASS}
uses
  BaseDataSet, define_dealclass, QuickList_DealClass;
  
type              
  TDBDealClassDictionary = class(TBaseDataSetAccess)
  protected
    fDealClassList: TDealClassList;
    function GetRecordCount: Integer; override;  
    function GetRecordItem(AIndex: integer): Pointer; override;
    function GetItem(AIndex: integer): PRT_DefClass; overload;
    function GetItem(ACode: string): PRT_DefClass; overload;
  public
    constructor Create(ADBType: integer); reintroduce;
    destructor Destroy; override;
    class function DataTypeDefine: integer; override;
    procedure Sort; override;
    procedure Clear; override;
    function AddDealClass(ADataSrc: integer; AId: integer; AName: AnsiString): PRT_DefClass;  
    function CheckOutDealClass(ADataSrc: integer; AId: integer; AName: AnsiString): PRT_DefClass;
    
    function DealClassByIndex(AIndex: integer): PRT_DefClass;
    
    property Items[AIndex: integer]: PRT_DefClass read GetItem;       
    property Item[ACode: string]: PRT_DefClass read GetItem;
  end;
{$ENDIF}

implementation

{$IFDEF DEALCLASS}
uses
  define_datasrc,
  QuickSortList,
  define_dealstore_file;

constructor TDBDealClassDictionary.Create(ADBType: integer); 
begin
  inherited Create(ADBType, 0);
  fDealClassList := TDealClassList.Create;
  fDealClassList.Duplicates := QuickSortList.lstDupAccept;
end;

destructor TDBDealClassDictionary.Destroy;
begin
  Clear;
  fDealClassList.Free;
  inherited;
end;

class function TDBDealClassDictionary.DataTypeDefine: integer;
begin
  Result := DataType_Class;
end;


function TDBDealClassDictionary.GetRecordItem(AIndex: integer): Pointer;
begin
  Result := Pointer(fDealClassList.DealClass[AIndex]);
end;

function TDBDealClassDictionary.GetItem(AIndex: integer): PRT_DefClass;
begin
  Result := GetRecordItem(AIndex);
end;

function TDBDealClassDictionary.GetItem(ACode: string): PRT_DefClass;
begin
  Result := nil;
end;

function TDBDealClassDictionary.GetRecordCount: integer;
begin
  Result := fDealClassList.Count;
end;

procedure TDBDealClassDictionary.Sort;
begin
  fDealClassList.Sort;
end;
          
procedure TDBDealClassDictionary.Clear;
var
  i: integer;
  tmpDealClass: PRT_DefClass;
begin
  for i := fDealClassList.Count - 1 downto 0 do
  begin
    tmpDealClass := fDealClassList.DealClass[i];
    FreeMem(tmpDealClass);
  end;
  fDealClassList.Clear;
end;

function TDBDealClassDictionary.AddDealClass(ADataSrc: integer; AId: integer; AName: AnsiString): PRT_DefClass;
begin
  Result := CheckOutDefClass(nil);
  if nil <> Result then
  begin
    Result.DataSrc := ADataSrc;
    Result.Id := AId;
    Result.Name := AName;
    fDealClassList.AddDealClass(AId, Result);
  end;
end;

function TDBDealClassDictionary.DealClassByIndex(AIndex: integer): PRT_DefClass;
begin
  Result := GetRecordItem(AIndex);
end;

function TDBDealClassDictionary.CheckOutDealClass(ADataSrc: integer; AId: integer; AName: AnsiString): PRT_DefClass;
begin
  Result := nil; //FindDealClassByCode(AMarketCode + AStockCode);
  if nil = Result then
  begin
    Result := AddDealClass(ADataSrc, AId, AName);
  end;
end;
{$ENDIF}

end.
