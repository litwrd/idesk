unit db_dealclass_save;

interface

{$IFDEF DEALCLASS}              
uses
  baseApp,
  db_dealclass;
            
  procedure SaveDBStockClassDic(App: TBaseApp; ADB: TDBDealClassDictionary);
  procedure SaveDBStockClassDicToFile(App: TBaseApp; ADB: TDBDealClassDictionary; AFileUrl: string);
{$ENDIF}

implementation
         
{$IFDEF DEALCLASS}
uses
  define_tablefield,
  define_dealclass,
  define_datasrc,
  Sqlite3Wrap,
  SQLite3WrapApi,
  SQLite3Utils,
  Sqlite3;

procedure SaveDBStockClassDicToFile(App: TBaseApp; ADB: TDBDealClassDictionary; AFileUrl: string);

  procedure InsertDefClassRecord(ASqliteDB: TSQLite3DataBase; ADefClass: PRT_DefClass);
  var
    tmpQuery: TSQLite3Statement;  
    tmpSQL: string;
  begin
    if 0 = ADefClass.AutoId then
    begin                 
      tmpSQL := 'Insert Into ' + Table_StockClassDefine + '(' +
                  Field_DataSrc + ',' +
                  Field_Name + ',' +
                  Field_ID_Parent +
                  ')' + ' values ' + '(' +
                  ':' + Field_DataSrc + ',' +
                  ':' + Field_Name + ',' +
                  ':' + Field_ID_Parent +
                  ');';
      tmpQuery := ASqliteDB.Prepare(tmpSQL);
      if nil <> tmpQuery then
      begin
        try
          tmpQuery.BindInt(1, ADefClass.DataSrc);// DataSrc_Sina);
          tmpQuery.BindText(2, ADefClass.Name);// '֤�����ҵ');
          if 0 = ADefClass.ParentId then
          begin
            if nil <> ADefClass.Parent then
            begin
              if 0 = ADefClass.Parent.Id then
              begin
                InsertDefClassRecord(ASqliteDB, ADefClass.Parent);
              end;
              ADefClass.ParentId := ADefClass.Parent.Id;
            end;
          end;
          tmpQuery.BindInt(3, ADefClass.ParentId);
          if SQLITE_DONE = tmpQuery.Step then // SQLITE_DONE 101
          begin
            ADefClass.AutoId := ASqliteDB.LastInsertRowID;
          end;
        finally
          tmpQuery.Free;
        end;
      end;
    end;
  end;
  
var
  tmpSqliteDB: TSQLite3DataBase;
  i: integer;
begin
  tmpSqliteDB := TSQLite3DataBase.Create;
  try
    tmpSqliteDB.Open(AFileUrl,
      SQLITE_OPEN_CREATE or
      SQLITE_OPEN_READWRITE or
      SQLITE_OPEN_NOMUTEX
    );
    tmpSqliteDB.Execute(CreateTableSQL_StockClassDefine);
    tmpSqliteDB.Execute(CreateTableSQL_StockClass);    
    tmpSqliteDB.BeginTransaction;
    try
      for i := 0 to ADB.RecordCount - 1 do
        InsertDefClassRecord(tmpSqliteDB, ADB.RecordItem[i]);
    finally
      tmpSqliteDB.Commit;
    end;
  finally
    tmpSqliteDB.Free;
  end;  
end;
        
procedure SaveDBStockClassDic(App: TBaseApp; ADB: TDBDealClassDictionary);
var
  tmpFileUrl: string;
begin
  tmpFileUrl := App.Path.GetFileUrl(ADB.DBType, ADB.DataType, ADB.DataSrcID);
  //if App.Path.IsFileExists(tmpFileUrl) then
  begin
    SaveDBStockClassDicToFile(App, ADB, tmpFileUrl);  
  end;
end;
{$ENDIF}

end.
