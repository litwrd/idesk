unit QuickList_QuoteInstant;

interface
    
{$IFDEF DEALINSTANT}
uses
  QuickSortList,
  define_stock_quotes_instant;
  
type       
  PInstantQuoteListItem = ^TInstantQuoteListItem;
  TInstantQuoteListItem = record
    StockPackCode       : integer;
    InstantQuote        : PRT_InstantQuote;
  end;
                        
  TQuoteInstantList = class(TALBaseQuickSortList)
  public
    function  GetItem(Index: Integer): Integer;
    procedure SetItem(Index: Integer; const AStockPackCode: Integer);
    function  GetInstantQuote(Index: Integer): PRT_InstantQuote;
    procedure PutInstantQuote(Index: Integer; AInstantQuote: PRT_InstantQuote);
  public
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
    procedure InsertItem(Index: Integer; const AStockPackCode: integer; AInstantQuote: PRT_InstantQuote);
    function  CompareItems(const Index1, Index2: Integer): Integer; override;
  public
    function  IndexOf(AStockPackCode: Integer): Integer;
    function  IndexOfInstantQuote(AInstantQuote: PRT_InstantQuote): Integer;
    Function  AddInstantQuote(const AStockPackCode: integer; AInstantQuote: PRT_InstantQuote): Integer;
    function  Find(AStockPackCode: Integer; var Index: Integer): Boolean;
    procedure InsertObject(Index: Integer; const AStockPackCode: integer; AInstantQuote: PRT_InstantQuote);
    //property  Items[Index: Integer]: Integer read GetItem write SetItem; default;
    property  StockPackCode[Index: Integer]: Integer read GetItem write SetItem; default;
    property  InstantQuote[Index: Integer]: PRT_InstantQuote read GetInstantQuote write PutInstantQuote;
  end;
{$ENDIF}

implementation


{$IFDEF DEALINSTANT}
function TQuoteInstantList.AddInstantQuote(const AStockPackCode: integer; AInstantQuote: PRT_InstantQuote): Integer;
begin
  if not Sorted then
  begin
    Result := FCount
  end else if Find(AStockPackCode, Result) then
  begin
    case Duplicates of
      lstDupIgnore: Exit;
      lstDupError: Error(@SALDuplicateItem, 0);
    end;
  end;
  InsertItem(Result, AStockPackCode, AInstantQuote);
end;

{*****************************************************************************************}
procedure TQuoteInstantList.InsertItem(Index: Integer; const AStockPackCode: integer; AInstantQuote: PRT_InstantQuote);
var
  tmpListItem: PInstantQuoteListItem;
begin
  New(tmpListItem);
  tmpListItem^.StockPackCode := AStockPackCode;
  tmpListItem^.InstantQuote := AInstantQuote;
  try
    inherited InsertItem(index, tmpListItem);
  except
    Dispose(tmpListItem);
    raise;
  end;
end;

{***************************************************************************}
function TQuoteInstantList.CompareItems(const Index1, Index2: integer): Integer;
begin
  result := PInstantQuoteListItem(Get(Index1))^.StockPackCode - PInstantQuoteListItem(Get(Index2))^.StockPackCode;
end;

{***********************************************************************}
function TQuoteInstantList.Find(AStockPackCode: Integer; var Index: Integer): Boolean;
var
  L, H, I, C: Integer;
begin
  Result := False;
  L := 0;
  H := FCount - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := GetItem(I) - AStockPackCode;
    if C < 0 then
    begin
      L := I + 1
    end else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Result := True;
        if Duplicates <> lstDupAccept then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

{*******************************************************}
function TQuoteInstantList.GetItem(Index: Integer): Integer;
begin
  Result := PInstantQuoteListItem(Get(index))^.StockPackCode;
end;

{******************************************************}
function TQuoteInstantList.IndexOf(AStockPackCode: Integer): Integer;
begin
  if not Sorted then
  Begin
    Result := 0;
    while (Result < FCount) and (GetItem(result) <> AStockPackCode) do
    begin
      Inc(Result);
    end;
    if Result = FCount then
    begin
      Result := -1;
    end;
  end else if not Find(AStockPackCode, Result) then
    Result := -1;
end;
{*******************************************************************************************}
procedure TQuoteInstantList.InsertObject(Index: Integer; const AStockPackCode: integer; AInstantQuote: PRT_InstantQuote);
var
  tmpListItem: PInstantQuoteListItem;
begin
  New(tmpListItem);
  tmpListItem^.StockPackCode := AStockPackCode;
  tmpListItem^.InstantQuote := AInstantQuote;
  try
    inherited insert(index, tmpListItem);
  except
    Dispose(tmpListItem);
    raise;
  end;
end;

{***********************************************************************}
procedure TQuoteInstantList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Action = lstDeleted then
    dispose(ptr);
  inherited Notify(Ptr, Action);
end;

{********************************************************************}
procedure TQuoteInstantList.SetItem(Index: Integer; const AStockPackCode: Integer);
Var
  tmpListItem: PInstantQuoteListItem;
begin
  New(tmpListItem);
  tmpListItem^.StockPackCode := AStockPackCode;
  tmpListItem^.InstantQuote := nil;
  Try
    Put(Index, tmpListItem);
  except
    Dispose(tmpListItem);
    raise;
  end;
end;

{*********************************************************}
function TQuoteInstantList.GetInstantQuote(Index: Integer): PRT_InstantQuote;
begin
  if (Index < 0) or (Index >= FCount) then
    Error(@SALListIndexError, Index);
  Result :=  PInstantQuoteListItem(Get(index))^.InstantQuote;
end;

{***************************************************************}
function TQuoteInstantList.IndexOfInstantQuote(AInstantQuote: PRT_InstantQuote): Integer;
begin
  for Result := 0 to Count - 1 do
  begin
    if GetInstantQuote(Result) = AInstantQuote then
    begin
      Exit;
    end;
  end;
  Result := -1;
end;

{*******************************************************************}
procedure TQuoteInstantList.PutInstantQuote(Index: Integer; AInstantQuote: PRT_InstantQuote);
begin
  if (Index < 0) or (Index >= FCount) then
    Error(@SALListIndexError, Index);
  PInstantQuoteListItem(Get(index))^.InstantQuote := AInstantQuote;
end;
{$ENDIF}

end.
