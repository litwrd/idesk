unit QuickList_DayData;

interface
               
uses
  QuickSortList,
  define_stock_quotes;

type         
  PDayDataListItem = ^TDayDataListItem;
  TDayDataListItem = record
    DealDate: Word;
    DayData : PRT_Quote_Day;
  end;
          
  { 行情分钟线数据访问 }
  TDayDataList = class(TALBaseQuickSortList)
  public
    function  GetItem(Index: Integer): Word;
    procedure SetItem(Index: Integer; const ADealDate: Word);
    function  GetObject(Index: Integer): PRT_Quote_Day;
    procedure PutObject(Index: Integer; ADayData: PRT_Quote_Day);
  public
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
    procedure InsertItem(Index: Integer; const ADealDate: Word; ADayData: PRT_Quote_Day);
    function  CompareItems(const Index1, Index2: Integer): Integer; override;
  public
    function  IndexOf(ADealDate: Word): Integer;
    function  IndexOfObject(ADayData: PRT_Quote_Day): Integer;
    Function  AddDayData(const ADealDate: Word; ADayData: PRT_Quote_Day): Integer;
    function  Find(ADealDate: Word; var Index: Integer): Boolean;
    procedure InsertObject(Index: Integer; const ADealDate: Word; ADayData: PRT_Quote_Day);
    property  DealDate[Index: Integer]: Word read GetItem write SetItem; default;
    property  DayData[Index: Integer]: PRT_Quote_Day read GetObject write PutObject;
  end;
  
implementation

{********************************************************}
function TDayDataList.AddDayData(const ADealDate: Word; ADayData: PRT_Quote_Day): Integer;
begin
  if not Sorted then
  begin
    Result := FCount
  end else if Find(ADealDate, Result) then
  begin
    case Duplicates of
      lstDupIgnore: Exit;
      lstDupError: Error(@SALDuplicateItem, 0);
    end;
  end;
  InsertItem(Result, ADealDate, ADayData);
end;

{*****************************************************************************************}
procedure TDayDataList.InsertItem(Index: Integer; const ADealDate: Word; ADayData: PRT_Quote_Day);
Var aPDayDataListItem: PDayDataListItem;
begin
  New(aPDayDataListItem);
  aPDayDataListItem^.DealDate := ADealDate;
  aPDayDataListItem^.DayData := ADayData;
  try
    inherited InsertItem(index,aPDayDataListItem);
  except
    Dispose(aPDayDataListItem);
    raise;
  end;
end;

{***************************************************************************}
function TDayDataList.CompareItems(const Index1, Index2: integer): Integer;
begin
  result := PDayDataListItem(Get(Index1))^.DealDate - PDayDataListItem(Get(Index2))^.DealDate;
end;

{***********************************************************************}
function TDayDataList.Find(ADealDate: Word; var Index: Integer): Boolean;
var
  L, H, I, C: Integer;
begin
  Result := False;
  L := 0;
  H := FCount - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := GetItem(I) - ADealDate;
    if C < 0 then
    begin
      L := I + 1
    end else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Result := True;
        if Duplicates <> lstDupAccept then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

{*******************************************************}
function TDayDataList.GetItem(Index: Integer): Word;
begin
  Result := PDayDataListItem(Get(index))^.DealDate;
end;

{******************************************************}
function TDayDataList.IndexOf(ADealDate: Word): Integer;
begin
  if not Sorted then
  Begin
    Result := 0;
    while (Result < FCount) and (GetItem(result) <> ADealDate) do
    begin
      Inc(Result);
    end;
    if Result = FCount then
      Result := -1;
  end else if not Find(ADealDate, Result) then
  begin
    Result := -1;
  end;
end;
{*******************************************************************************************}
procedure TDayDataList.InsertObject(Index: Integer; const ADealDate: Word; ADayData: PRT_Quote_Day);
Var aPDayDataListItem: PDayDataListItem;
begin
  New(aPDayDataListItem);
  aPDayDataListItem^.DealDate := ADealDate;
  aPDayDataListItem^.DayData := ADayData;
  try
    inherited insert(index,aPDayDataListItem);
  except
    Dispose(aPDayDataListItem);
    raise;
  end;
end;

{***********************************************************************}
procedure TDayDataList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Action = lstDeleted then
    dispose(ptr);
  inherited Notify(Ptr, Action);
end;

{********************************************************************}
procedure TDayDataList.SetItem(Index: Integer; const ADealDate: Word);
var
  tmpDayDataListItem: PDayDataListItem;
begin
  New(tmpDayDataListItem);
  tmpDayDataListItem^.DealDate := ADealDate;
  tmpDayDataListItem^.DayData := nil;
  Try
    Put(Index, tmpDayDataListItem);
  except
    Dispose(tmpDayDataListItem);
    raise;
  end;
end;

{*********************************************************}
function TDayDataList.GetObject(Index: Integer): PRT_Quote_Day;
begin
  if (Index < 0) or (Index >= FCount) then
    Error(@SALListIndexError, Index);
  Result :=  PDayDataListItem(Get(index))^.DayData;
end;

{***************************************************************}
function TDayDataList.IndexOfObject(ADayData: PRT_Quote_Day): Integer;
begin
  for Result := 0 to Count - 1 do
  begin
    if GetObject(Result) = ADayData then
    begin
      Exit;
    end;
  end;
  Result := -1;
end;

{*******************************************************************}
procedure TDayDataList.PutObject(Index: Integer; ADayData: PRT_Quote_Day);
begin
  if (Index < 0) or (Index >= FCount) then
    Error(@SALListIndexError, Index);
  PDayDataListItem(Get(index))^.DayData := ADayData;
end;

end.
