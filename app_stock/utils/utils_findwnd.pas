unit utils_findwnd;

interface

uses
  Windows, Sysutils;
                
type
  PExWindowEnumFind   = ^TExWindowEnumFind;
  TExWindowEnumFind   = record
    FindCount         : integer;
    FindWindow        : array[0..255] of HWND;
    NeedWinCount      : integer;
    ProcessId         : DWORD;
    ExcludeWnd        : HWND;
    ParentWnd         : HWND;
    WndClassKey       : string;
    WndCaptionKey     : string;
    WndCaptionExcludeKey: string;
    CheckWndFunc      : function(AWnd: HWND; AFind: PExWindowEnumFind): Boolean;
  end;
          
  function EnumFindDesktopWindowProc(AWnd: HWND; lParam: LPARAM): BOOL; stdcall;
  
implementation

uses
  UtilsWindows;

function EnumFindDesktopWindowProc(AWnd: HWND; lParam: LPARAM): BOOL; stdcall;  
var
  tmpStr: string;
  tmpIsFind: Boolean;
  tmpFind: PExWindowEnumFind;
  tmpProcessId: DWORD;
begin      
  Result := true;
  tmpFind := PExWindowEnumFind(lparam);
  if nil = tmpFind then
  begin      
    Result := false;
    exit;
  end;  
  if not IsWindowVisible(AWnd) then
    exit;       
  tmpIsFind := true;
  if '' <> tmpFind.WndClassKey then
  begin
    tmpStr := GetWndClassName(AWnd);
    if (not SameText(tmpStr, tmpFind.WndClassKey)) then
    begin
      if Pos(tmpFind.WndClassKey, tmpStr) < 1 then
      begin
        tmpIsFind := false;
      end;
    end;
  end;
  if tmpIsFind then
  begin
    if 0 <> tmpFind.ProcessId then
    begin
      GetWindowThreadProcessId(AWnd, tmpProcessId);
      if tmpProcessId <> tmpFind.ProcessId then
      begin
        tmpIsFind := false;
      end;
    end;
  end;
  if tmpIsFind then
  begin
    if ('' <> Trim(tmpFind.WndCaptionKey)) or
       ('' <> Trim(tmpFind.WndCaptionExcludeKey))then
    begin         
      tmpStr := GetWndTextName(AWnd);
      if ('' <> Trim(tmpFind.WndCaptionKey)) then
      begin
        if Pos(tmpFind.WndCaptionKey, tmpStr) < 1 then
        begin
          tmpIsFind := false;
        end;
      end;
      if ('' <> Trim(tmpFind.WndCaptionExcludeKey)) then
      begin
        if Pos(tmpFind.WndCaptionExcludeKey, tmpStr) > 0 then
        begin
          tmpIsFind := false;
        end;
      end;
    end;
  end;    
  if tmpIsFind then
  begin
    if Assigned(tmpFind.CheckWndFunc) then
    begin
      if (not tmpFind.CheckWndFunc(AWnd, tmpFind)) then
      begin
        tmpIsFind := false;
      end;
    end;
  end;
  if tmpIsFind then
  begin
    if 255 > tmpFind.FindCount then
    begin
      tmpFind.FindWindow[tmpFind.FindCount] := AWnd;
      tmpFind.FindCount := tmpFind.FindCount + 1;
    end;
    if 255 <> tmpFind.NeedWinCount then
      tmpFind.NeedWinCount := tmpFind.NeedWinCount - 1;
    if tmpFind.NeedWinCount < 1 then
      // 找到不用再找了
      Result := false;
  end;
end;
       
end.
