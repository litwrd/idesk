unit QuickList_MinuteData;

interface

uses
  QuickSortList,
  define_datetime,
  define_stock_quotes;

type           
  {---------------------------------------}
  PMinuteDataListItem = ^TMinuteDataListItem;
  TMinuteDataListItem = record
    //StockDateTime: Double;
    StockDateTime: TDateTimeStock;
    MinuteData: PRT_Quote_Minute;
  end;
  
  { 行情分钟线数据访问 }
  TMinuteDataList = class(TALBaseQuickSortList)
  public
    function  GetItem(Index: Integer): TDateTimeStock;
    procedure SetItem(Index: Integer; const AStockDateTime: TDateTimeStock);
    function  GetMinuteData(Index: Integer): PRT_Quote_Minute;
    procedure PutMinuteData(Index: Integer; AMinuteData: PRT_Quote_Minute);
  public
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
    procedure InsertItem(Index: Integer; const AStockDateTime: TDateTimeStock; AMinuteData: PRT_Quote_Minute);
    function  CompareItems(const Index1, Index2: Integer): Integer; override;
  public
    function  IndexOf(AStockDateTime: TDateTimeStock): Integer;
    function  IndexOfMinuteData(AMinuteData: PRT_Quote_Minute): Integer;
    Function  AddMinuteData(const AStockDateTime: TDateTimeStock; AMinuteData: PRT_Quote_Minute): Integer;
    function  Find(AStockDateTime: TDateTimeStock; var Index: Integer): Boolean;

    procedure InsertObject(Index: Integer; const AStockDateTime: TDateTimeStock; AMinuteData: PRT_Quote_Minute);
    property  MinuteTime[Index: Integer]: TDateTimeStock read GetItem write SetItem; default;
    property  MinuteData[Index: Integer]: PRT_Quote_Minute read GetMinuteData write PutMinuteData;
  end;
  
implementation

{********************************************************************************}
function TMinuteDataList.AddMinuteData(const AStockDateTime: TDateTimeStock; AMinuteData: PRT_Quote_Minute): Integer;
begin
  if not Sorted then
  begin
    Result := FCount
  end else if Find(AStockDateTime, Result) then
  begin
    case Duplicates of
      lstDupIgnore: Exit;
      lstDupError: Error(@SALDuplicateItem, 0);
    end;
  end;
  InsertItem(Result, AStockDateTime, AMinuteData);
end;

{*****************************************************************************************}
procedure TMinuteDataList.InsertItem(Index: Integer; const AStockDateTime: TDateTimeStock; AMinuteData: PRT_Quote_Minute);
Var
  tmpDoubleListItem: PMinuteDataListItem;
begin
  New(tmpDoubleListItem);
  tmpDoubleListItem^.StockDateTime := AStockDateTime;
  tmpDoubleListItem^.MinuteData := AMinuteData;
  try
    inherited InsertItem(index, tmpDoubleListItem);
  except
    Dispose(tmpDoubleListItem);
    raise;
  end;
end;

{***************************************************************************}
function TMinuteDataList.CompareItems(const Index1, Index2: integer): Integer;
var
  tmpItem1, tmpItem2: PMinuteDataListItem;
begin  
  tmpItem1 := PMinuteDataListItem(Get(Index1));
  tmpItem2 := PMinuteDataListItem(Get(Index2));
  Result := tmpItem1^.StockDateTime.Date.Value - tmpItem2^.StockDateTime.Date.Value;
  if 0 = Result then
  begin
    Result := tmpItem1^.StockDateTime.Time.Value - tmpItem2^.StockDateTime.Time.Value;
  end;
  //result := PMinuteDataListItem(Get(Index1))^.FDouble - PMinuteDataListItem(Get(Index2))^.FDouble;
end;

{***********************************************************************}
function TMinuteDataList.Find(AStockDateTime: TDateTimeStock; var Index: Integer): Boolean;
var
  L: Integer;
  H: Integer;
  I: Integer;
  C: double;
begin
  Result := False;
  L := 0;
  H := FCount - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := Integer(GetItem(I)) - Integer(AStockDateTime);
    if C < 0 then
    begin
      L := I + 1
    end else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Result := True;
        if Duplicates <> lstDupAccept then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

{*******************************************************}
function TMinuteDataList.GetItem(Index: Integer): TDateTimeStock;
begin
  Result := PMinuteDataListItem(Get(index))^.StockDateTime
end;

{******************************************************}
function TMinuteDataList.IndexOf(AStockDateTime: TDateTimeStock): Integer;
begin
  if not Sorted then
  Begin
    Result := 0;
    while (Result < FCount) and (Integer(GetItem(result)) <> Integer(AStockDateTime)) do
    begin
      Inc(Result);
    end;
    if Result = FCount then
    begin
      Result := -1;
    end;
  end else
  begin
    if not Find(AStockDateTime, Result) then
      Result := -1;
  end;
end;
{*******************************************************************************************}
procedure TMinuteDataList.InsertObject(Index: Integer; const AStockDateTime: TDateTimeStock; AMinuteData: PRT_Quote_Minute);
Var
  tmpDoubleListItem: PMinuteDataListItem;
begin
  New(tmpDoubleListItem);
  tmpDoubleListItem^.StockDateTime := AStockDateTime;
  tmpDoubleListItem^.MinuteData := AMinuteData;
  try
    inherited insert(index, tmpDoubleListItem);
  except
    Dispose(tmpDoubleListItem);
    raise;
  end;
end;

{***********************************************************************}
procedure TMinuteDataList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Action = lstDeleted then
    dispose(ptr);
  inherited Notify(Ptr, Action);
end;

{********************************************************************}
procedure TMinuteDataList.SetItem(Index: Integer; const AStockDateTime: TDateTimeStock);
Var
  tmpDoubleListItem: PMinuteDataListItem;
begin
  New(tmpDoubleListItem);
  tmpDoubleListItem^.StockDateTime := AStockDateTime;
  tmpDoubleListItem^.MinuteData := nil;
  Try
    Put(Index, tmpDoubleListItem);
  except
    Dispose(tmpDoubleListItem);
    raise;
  end;
end;

{*********************************************************}
function TMinuteDataList.GetMinuteData(Index: Integer): PRT_Quote_Minute;
begin
  if (Index < 0) or (Index >= FCount) then
    Error(@SALListIndexError, Index);
  Result :=  PMinuteDataListItem(Get(index))^.MinuteData;
end;

{***************************************************************}
function TMinuteDataList.IndexOfMinuteData(AMinuteData: PRT_Quote_Minute): Integer;
begin
  for Result := 0 to Count - 1 do
  begin
    if GetMinuteData(Result) = AMinuteData then
    begin
      Exit;
    end;
  end;
  Result := -1;
end;

{*******************************************************************}
procedure TMinuteDataList.PutMinuteData(Index: Integer; AMinuteData: PRT_Quote_Minute);
begin
  if (Index < 0) or (Index >= FCount) then
  begin
    Error(@SALListIndexError, Index);
  end;
  PMinuteDataListItem(Get(index))^.MinuteData := AMinuteData;
end;

end.
