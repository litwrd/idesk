unit UtilsHttp;

interface

uses
  Windows, ActiveX, UrlMon, win.iobuffer;

type
  PClientConnectSession = ^TClientConnectSession;
  TClientConnectSession = record
    Connection      : Pointer; 
    SendTimeOut     : Cardinal;
    ConnectTimeOut  : Cardinal;
    ReceiveTimeOut  : Cardinal;
  end;
           
  PHttpHeadParseSession = ^THttpHeadParseSession;
  THttpHeadParseSession = record
    RetCode           : integer;
    HeadEndPos        : integer;
    Transfer_Encoding : Integer;
    Charset           : Integer;
    ContentLength     : Integer;
    Location          : string;
  end;
            
  PHttpClientSession = ^THttpClientSession;
  THttpClientSession = record
    ConnectionSession: TClientConnectSession;  
    IsKeepAlive     : Boolean;
    HttpHeadSession : THttpHeadParseSession;
  end;
  
  PHttpUrlInfo      = ^THttpUrlInfo;
  THttpUrlInfo      = record
    Protocol        : AnsiString;
    Host            : AnsiString;
    Port            : AnsiString;
    PathName        : AnsiString;
    UserName        : AnsiString;
    Password        : AnsiString;
  end;

  PHttpBuffer       = ^THttpBuffer;
  THttpBuffer       = record

  end;
  
  function CheckOutHttpClientSession: PHttpClientSession;
  procedure CheckInHttpClientSession(var ANetClientSession: PHttpClientSession);

  { 这里可能有问题
    获取的数据 有些 Http 头 解析不正确
  }
  function GetHttpUrlData(AUrl: AnsiString; ANetSession: PHttpClientSession; ABuffer: PIOBuffer; ABufferSizeMode: Integer = SizeMode_16k): PIOBuffer; overload;      
  function GetHttpUrlData(AUrl: AnsiString; APost: AnsiString; ANetSession: PHttpClientSession; ABuffer: PIOBuffer; ABufferSizeMode: Integer = SizeMode_16k): PIOBuffer; overload;
  
  function GetHttpUrlFile(AUrl: AnsiString; AOutputFile: AnsiString; ANetSession: PHttpClientSession): Boolean; overload;

  function PostHttpUrlData(AUrl: AnsiString; APost: AnsiString; ANetSession: PHttpClientSession): PIOBuffer;

  function GetDefaultUserAgent: AnsiString;    
  function ParseHttpUrlInfo(AUrl: AnsiString; AInfo: PHttpUrlInfo): Boolean;

  procedure HttpBufferHeader_Parser(AHttpBuffer: PIOBuffer; AHttpHeadParseSession: PHttpHeadParseSession);
  procedure SaveHttpResponseToFile(AHttpBuffer: PIOBuffer; AHttpHeadParseSession: PHttpHeadParseSession; AFileName: AnsiString);

  {* 将 URL 中的特殊字符转换成 %XX 的形式}
  function EncodeURL(const AUrl: string): string;

const
  SAcceptEncoding = 'Accept-Encoding: gzip,deflate';
  
implementation

uses
  Sysutils,
  //UtilsHttp_Indy,
  //win.diskfile,
  UtilsHttp_Socket;

function CheckOutHttpClientSession: PHttpClientSession;
begin
  Result := System.New(PHttpClientSession);
  FillChar(Result^, SizeOf(THttpClientSession), 0);
  Result.IsKeepAlive := True;
end;

procedure CheckInHttpClientSession(var ANetClientSession: PHttpClientSession);
begin
  if nil <> ANetClientSession then
  begin
    if nil <> ANetClientSession.ConnectionSession.Connection then
    begin
      CheckInSocketConnection(ANetClientSession);
    end;
    FreeMem(ANetClientSession);
    ANetClientSession := nil;
  end;
end;

function EncodeURL(const AUrl: string): string;
const
  UnsafeChars = ['*', '#', '%', '<', '>', '+', ' '];
var
  i: Integer;
  InStr, OutStr: AnsiString;
begin
  InStr := AnsiString(AUrl);
  OutStr := '';
  for i := 1 to Length(InStr) do begin
    if (InStr[i] in UnsafeChars) or (InStr[i] >= #$80) or (InStr[i] < #32) then
      OutStr := OutStr + '%' + AnsiString(IntToHex(Ord(InStr[i]), 2))
    else
      OutStr := OutStr + InStr[i];
  end;
  Result := string(OutStr);
end;

function GetDefaultUserAgent: AnsiString;
var
  s: AnsiString;
  cbSize: Cardinal;
  hRet: HRESULT;
begin
  cbSize := MAX_PATH;
  repeat
    SetLength(s, cbSize);
    hRet := UrlMon.ObtainUserAgentString(0, PAnsiChar(s), cbSize);
    case hRet of
      E_OUTOFMEMORY:
        cbSize := cbSize * 2;
      NOERROR:
        SetLength(s, cbSize - 1);
      else
        SetLength(s, 0);
    end;
  until hRet <> E_OUTOFMEMORY;
  Result := s;
end;

function ParseHttpUrlInfo(AUrl: AnsiString; AInfo: PHttpUrlInfo): Boolean;
var
  Idx: Integer;
  Buff: AnsiString;
  
  function ExtractStr(var ASrc: AnsiString; ADelim: AnsiString; ADelete: Boolean = True): AnsiString;
  var
    Idx: Integer;
  begin
    Idx := Pos(ADelim, ASrc);
    if Idx = 0 then
    begin
      Result := ASrc;
      if ADelete then
        ASrc := '';
    end else
    begin
      Result := Copy(ASrc, 1, Idx - 1);
      if ADelete then
        ASrc := Copy(ASrc, Idx + Length(ADelim), MaxInt);
    end;
  end;
  
begin
  Result := False;
  AUrl := Trim(AUrl);
  Idx := Pos('://', AUrl);
  if Idx > 0 then
  begin
    AInfo.Protocol := Copy(AUrl, 1, Idx  - 1);
    Delete(AUrl, 1, Idx + 2);
    if AUrl = '' then
      Exit;

    Buff := ExtractStr(AUrl, '/');
    Idx := Pos('@', Buff);
    AInfo.Password := Copy(Buff, 1, Idx  - 1);
    if Idx > 0 then
      Delete(Buff, 1, Idx);

    AInfo.UserName := ExtractStr(AInfo.Password, ':');
    if Length(AInfo.UserName) = 0 then
      AInfo.Password := '';

    AInfo.Host := ExtractStr(Buff, ':');
    AInfo.Port := Buff;
    AInfo.PathName := '/' + AUrl;
    Result := True;
  end;
end;

function GetHttpUrlData(AUrl: AnsiString; ANetSession: PHttpClientSession; ABuffer: PIOBuffer; ABufferSizeMode: Integer = SizeMode_16k): PIOBuffer;
var
//  tmpIConnection: PIndyConnectionSession;
  tmpOwnedConnection: Boolean;
begin    
  tmpOwnedConnection := false;
  if nil <> ANetSession then
  begin
    if nil = ANetSession.ConnectionSession.Connection then
    begin
      CheckOutSocketConnection(ANetSession);
      if not ANetSession.IsKeepAlive then
        tmpOwnedConnection := true;
    end;
  end;
  //Result := Http_WinInet.Http_GetString(AUrl);
  //Result := UtilsHttp_Indy.Http_GetString(AUrl, tmpConnection);
  Result := UtilsHttp_Socket.Http_GetString(AUrl, '', ANetSession, ABuffer, ABufferSizeMode);
  if tmpOwnedConnection then
  begin
    CheckInSocketConnection(ANetSession);
  end;
end;

function GetHttpUrlFile(AUrl: AnsiString; AOutputFile: AnsiString; ANetSession: PHttpClientSession): Boolean;
begin    
  if nil <> ANetSession then
  begin
    if nil = ANetSession.ConnectionSession.Connection then
    begin                
      //ANetSession.Connection := CheckOutIndyConnection;
      CheckOutSocketConnection(ANetSession);
    end;  
  end;
  //Result := UtilsHttp_Indy.Http_GetFile(AUrl, AOutputFile, tmpConnection);
  Result := UtilsHttp_Socket.Http_GetFile(AUrl, AOutputFile, ANetSession);
end;

function GetHttpUrlData(AUrl: AnsiString; APost: AnsiString; ANetSession: PHttpClientSession; ABuffer: PIOBuffer; ABufferSizeMode: Integer = SizeMode_16k): PIOBuffer;
var
//  tmpIConnection: PIndyConnectionSession;
  tmpOwnedConnection: Boolean;
begin    
  tmpOwnedConnection := false;
  if nil <> ANetSession then
  begin
    if nil = ANetSession.ConnectionSession.Connection then
    begin
      CheckOutSocketConnection(ANetSession);
      if not ANetSession.IsKeepAlive then
        tmpOwnedConnection := true;
    end;
  end;
  //Result := Http_WinInet.Http_GetString(AUrl);
  //Result := UtilsHttp_Indy.Http_GetString(AUrl, tmpConnection);
  Result := UtilsHttp_Socket.Http_GetString(AUrl, APost, ANetSession, ABuffer, ABufferSizeMode);
  if tmpOwnedConnection then
  begin
    CheckInSocketConnection(ANetSession);
  end;
end;

function PostHttpUrlData(AUrl: AnsiString; APost: AnsiString; ANetSession: PHttpClientSession): PIOBuffer;
var
//  tmpIConnection: PIndyConnectionSession;
  tmpOwnedConnection: Boolean;
begin    
  tmpOwnedConnection := false;
  if nil <> ANetSession then
  begin
    if nil = ANetSession.ConnectionSession.Connection then
    begin
      CheckOutSocketConnection(ANetSession);
      if not ANetSession.IsKeepAlive then
        tmpOwnedConnection := true;
    end;
  end;
  //Result := Http_WinInet.Http_GetString(AUrl);
  //Result := UtilsHttp_Indy.Http_GetString(AUrl, tmpConnection);
  Result := UtilsHttp_Socket.Http_GetString(AUrl, APost, ANetSession, nil);
  if tmpOwnedConnection then
  begin
    CheckInSocketConnection(ANetSession);
  end;
end;

procedure HttpBufferHeader_Parser(AHttpBuffer: PIOBuffer; AHttpHeadParseSession: PHttpHeadParseSession);
var  
  tmpHttpHeadBuffer: array[0..256 - 1] of AnsiChar; 
  i: integer;
  tmpStr: AnsiString;
  tmpValue: AnsiString;  
  tmpHead: AnsiString;
  tmpPos: integer;
  tmpLastPos_CRLF: integer;
begin
  if nil = AHttpBuffer then
    exit;
  FillChar(tmpHttpHeadBuffer, SizeOf(tmpHttpHeadBuffer), 0);
  tmpLastPos_CRLF := 0;
  for i := 0 to AHttpBuffer.BufferHead.TotalLength - 1 do
  begin            
    if (#13 = AHttpBuffer.Data[i]) then
    begin
      //#13#10
      if 0 = tmpLastPos_CRLF then
      begin
        CopyMemory(@tmpHttpHeadBuffer[0], @AHttpBuffer.Data[0], i);
        tmpStr := tmpHttpHeadBuffer;
        tmpPos := Pos(#32, tmpStr);
        if tmpPos > 0 then
        begin
          tmpStr := Copy(tmpStr, tmpPos + 1, maxint);
          tmpPos := Pos(#32, tmpStr);
          if tmpPos > 0 then
          begin          
            tmpStr := Copy(tmpStr, 1, tmpPos - 1); 
            AHttpHeadParseSession.RetCode := StrToIntDef(tmpStr, 0);
          end;
        end;
      end else
      begin
        // Http Header Field Definitions
        // http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
        CopyMemory(@tmpHttpHeadBuffer[0], @AHttpBuffer.Data[tmpLastPos_CRLF + 1], i - tmpLastPos_CRLF - 1); 
        tmpStr := Trim(tmpHttpHeadBuffer);
        if '' <> tmpStr then
        begin
          tmpPos := Pos(':', tmpStr);
          if 0 < tmpPos then
          begin
            tmpHead := Copy(tmpStr, 1, tmpPos - 1);       
            if SameText('Server', tmpHead) then
            begin
              // web服务器软件名称
            end;                          
            if SameText('Allow', tmpHead) then
            begin
              // Allow: GET, HEAD 对某网络资源的有效的请求行为，不允许则返回405                        
            end;                           
            if SameText('Date', tmpHead) then
            begin                                     
            end;              
            if SameText('Connection', tmpHead) then
            begin                                     
            end;
            if SameText('Accept-Ranges', tmpHead) then
            begin
              // 表明服务器是否支持指定范围请求及哪种类型的分段请求 Accept-Ranges: bytes
            end;
            if SameText('Set-Cookie', tmpHead) then
            begin                                     
            end;          
            if SameText('Proxy-Authenticate', tmpHead) then
            begin
              // Proxy-Authenticate: Basic
              // 	它指出认证方案和可应用到代理的该URL上的参数           
            end;            
            if SameText('WWW-Authenticate', tmpHead) then
            begin
              // 表明客户端请求实体应该使用的授权方案	WWW-Authenticate: Basic
            end;
            if SameText('Cache-Control', tmpHead) then
            begin
              // Cache-Control: no-cache 	告诉所有的缓存机制是否可以缓存及哪种类型                                  
            end;                   
            if SameText('Location', tmpHead) then
            begin
              // Location: http://www.zcmhi.com/archives/94.html
              // 用来重定向接收方到非请求URL的位置来完成请求或标识新的资源 
              AHttpHeadParseSession.Location := Trim(Copy(tmpStr, tmpPos + 1, maxint));                                 
            end;           
            if SameText('Expires', tmpHead) then
            begin
              // 响应过期的日期和时间                              
            end;     
            if SameText('Last-Modified', tmpHead) then
            begin
              // 请求资源的最后修改时间                                     
            end;
            if SameText('ETag', tmpHead) then
            begin
              // 	请求变量的实体标签的当前值
              // ETag: “737060cd8c284d8af7ad3082f209582d”                         
            end;
            if SameText('Transfer-Encoding', tmpHead) then
            begin
              tmpStr := Trim(Copy(tmpStr, tmpPos + 1, maxint));
              if SameText('chunked', tmpStr) then
              begin
                AHttpHeadParseSession.Transfer_Encoding := 1;
              end;
            end;       
            if SameText('Trailer', tmpHead) then
            begin
              //指出头域在分块传输编码的尾部存在 Trailer: Max-Forwards
            end;
            if SameText('Vary', tmpHead) then
            begin
              //告诉下游代理是使用缓存响应还是从原始服务器请求	Vary: *
            end;         
            if SameText('Via', tmpHead) then
            begin
              //告知代理客户端响应是通过哪里发送的	Via: 1.0 fred, 1.1 nowhere.com (Apache/1.1)
            end;
            if SameText('Content-Encoding', tmpHead) then
            begin
              // Content-Encoding: gzip web服务器支持的返回内容压缩编码类型
            end;        
            if SameText('Content-Language', tmpHead) then
            begin
              //响应体的语言	Content-Language: en,zh
            end;         
            if SameText('Content-Length', tmpHead) then
            begin
              //响应体的长度	Content-Length: 348    
              AHttpHeadParseSession.ContentLength := 0;
            end;         
            if SameText('Content-Location', tmpHead) then
            begin
              // 请求资源可替代的备用的另一地址	Content-Location: /index.htm
            end;
            if SameText('Content-MD5', tmpHead) then
            begin
              // 返回资源的MD5校验值	Content-MD5: Q2hlY2sgSW50ZWdyaXR5IQ==
            end;        
            if SameText('Content-Range', tmpHead) then
            begin
              //在整个返回体中本部分的字节位置	Content-Range: bytes 21010-47021/47022
            end;
            if SameText('Content-Type', tmpHead) then
            begin
              // 	Content-Type: text/html; charset=utf-8
              tmpStr := Trim(Copy(tmpStr, tmpPos + 1, maxint));
              tmpPos := Pos('charset=', tmpStr);
              if 0 < tmpPos then
              begin
                tmpValue := Copy(tmpStr, tmpPos + 8, maxint);
                if '' <> tmpValue then
                begin            
                  if SameText('utf-8', tmpValue) then
                  begin
                    //AHttpHeadParseSession.Charset := ANSI_CHARSET;
                    AHttpHeadParseSession.Charset := CP_UTF8;
                  end;
                end;
              end;
            end;   
            if SameText('Retry-After', tmpHead) then
            begin
              // 如果实体暂时不可取，通知客户端在指定时间之后再次尝试	Retry-After: 120
            end;  
            if SameText('Warning', tmpHead) then
            begin
              // 警告实体可能存在的问题	Warning: 199 Miscellaneous warning
            end;
          end;
        end;
      end;               
      FillChar(tmpHttpHeadBuffer, SizeOf(tmpHttpHeadBuffer), 0);                
      if i - tmpLastPos_CRLF < 3 then
      begin
        AHttpHeadParseSession.HeadEndPos := i;
        if #10 = AHttpBuffer.Data[i + 1] then
        begin
          AHttpHeadParseSession.HeadEndPos := i + 1;
        end;
        exit;
      end;
      tmpLastPos_CRLF := i;
    end;
  end;
end;

procedure SaveHttpResponseToFile(AHttpBuffer: PIOBuffer; AHttpHeadParseSession: PHttpHeadParseSession; AFileName: AnsiString);
var
  tmpFileHandle: THandle;
  tmpBytesWrite: DWORD;
  tmpBytesWritten: DWORD;  
begin
  tmpFileHandle := Windows.CreateFileA(PAnsiChar(AFileName),
          Windows.GENERIC_ALL,         // GENERIC_READ
          Windows.FILE_SHARE_READ or
          Windows.FILE_SHARE_WRITE or
          Windows.FILE_SHARE_DELETE,
        nil,
        CREATE_NEW,
        FILE_ATTRIBUTE_NORMAL, 0);
  if (0 <> tmpFileHandle) and (INVALID_HANDLE_VALUE <> tmpFileHandle) then
  begin
    try
      tmpBytesWrite := AHttpBuffer.BufferHead.BufDataLength - AHttpHeadParseSession.HeadEndPos; 
      if Windows.WriteFile(
        tmpFileHandle, //hFile: THandle;
        AHttpBuffer.Data[AHttpHeadParseSession.HeadEndPos + 1], // const Buffer;
        tmpBytesWrite, //nNumberOfBytesToWrite: DWORD;
        tmpBytesWritten, //var lpNumberOfBytesWritten: DWORD;
        nil {lpOverlapped: POverlapped}) then
      begin
      
      end;
      //Windows.WriteFileEx();
    finally
      Windows.CloseHandle(tmpFileHandle);
    end;
  end;
end;

end.
