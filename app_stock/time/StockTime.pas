unit StockTime;

interface

uses
  BaseApp,
  TimeMaster;
  
type
  TStockTime = class(TTimeMaster)
  protected
  public
    // 是否是交易日
    function IsDealDay(ADay: Word): Boolean;
  end;
  
implementation

{ TStockTime }

function TStockTime.IsDealDay(ADay: Word): Boolean;
begin              
  Result := false;
  if 0 < ADay then
  begin
    if IsWeekend(ADay) then
    begin
      Result := false;
      exit;
    end;
    if IsHoliday(ADay) then
    begin
      Result := false;
      exit;
    end;
    Result := true;
  end;
end;

end.
