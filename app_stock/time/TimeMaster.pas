unit TimeMaster;

interface
          
uses
  BaseApp;
  
(*//
  Holiday 中国传统节日
    除夕 年三十
    春节 年初一
  中国节气
    立春    元宵     雨水     惊蛰      春分    清明      谷雨
    七九 -- 数九节气之一
    龙抬头 二月二

    立夏    小满     端午     芒种    夏至    小暑    入伏   大暑
    立秋    末伏     处暑     七夕    白露    秋分    中秋   寒露   霜降   重阳
    立冬    小雪     大雪     冬至    小寒    大寒    腊八


    
  国家节日
       1.1  元旦
       3.12 植树节
       5.1  劳动节
       5.4  青年节   
  国际节日   
       2.14 情人节 
       3.8  妇女节  
       4.1  愚人节
       6.1  儿童节
//*)

type
  TTimeMaster = class(TBaseAppObj)
  protected
  public                        
    // 周末
    function IsWeekend(ADay: Word): Boolean;  
    // 休息日
    function IsDayOff(ADay: Word): Boolean;    
    // 是否是国家节假日
    function IsHoliday(ADay: Word): Boolean;
  end;

implementation

uses
  SysUtils, DateUtils;
  
{ TTimeMaster }

// 周末
function TTimeMaster.IsWeekend(ADay: Word): Boolean;
var
  tmpDayOfWeek: integer;
begin
  Result := true;
  if 0 < ADay then
  begin
    tmpDayOfWeek := DayOfWeek(ADay);
    Result := (tmpDayOfWeek = 1) or (tmpDayOfWeek = 7);
  end;
end;
             
function TTimeMaster.IsDayOff(ADay: Word): Boolean;
begin
  Result := IsWeekend(ADay);
  if Result then
  begin
    // 周末可能调休
  end else
  begin
    // 平时可能放假
    Result := IsHoliday(ADay);
  end;
end;
                
function TTimeMaster.IsHoliday(ADay: Word): Boolean;
begin
  // find from holiday db
  Result := true;  
  if 0 < ADay then
  begin
    Result := false;
  end;
end;

end.
