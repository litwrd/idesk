object Form1: TForm1
  Left = 329
  Top = 193
  Caption = 'Form1'
  ClientHeight = 398
  ClientWidth = 720
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnMA: TButton
    Left = 550
    Top = 144
    Width = 123
    Height = 25
    Caption = 'Compute MA'
    OnClick = btnMAClick
  end
  object mmoSrc: TMemo
    Left = 0
    Top = 0
    Width = 120
    Height = 369
  end
  object mmoResult: TMemo
    Left = 126
    Top = 0
    Width = 200
    Height = 369
  end
  object btnInit: TButton
    Left = 550
    Top = 113
    Width = 123
    Height = 25
    Caption = 'Init Source Data'
    OnClick = btnInitClick
  end
  object edtNum: TEdit
    Left = 550
    Top = 56
    Width = 107
    Height = 21
    Text = '20'
  end
  object mmoResult2: TMemo
    Left = 332
    Top = 0
    Width = 200
    Height = 369
  end
  object btnLLV: TButton
    Left = 550
    Top = 175
    Width = 123
    Height = 25
    Caption = 'Compute LLV'
    OnClick = btnLLVClick
  end
  object btnHHV: TButton
    Left = 550
    Top = 206
    Width = 123
    Height = 25
    Caption = 'Compute HHV'
    OnClick = btnHHVClick
  end
  object btnEMA: TButton
    Left = 550
    Top = 237
    Width = 123
    Height = 25
    Caption = 'Compute EMA'
    OnClick = btnEMAClick
  end
  object btnSTD: TButton
    Left = 550
    Top = 268
    Width = 123
    Height = 25
    Caption = 'Compute STD'
    OnClick = btnSTDClick
  end
end
