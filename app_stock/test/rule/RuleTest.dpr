program RuleTest;

uses
  Forms,                                                     
  QuickList_Int in '..\..\..\..\devwintech\comps\list\QuickList_Int.pas',  
  BaseDataSet in '..\..\..\..\devwintech\v0000\app_base\BaseDataSet.pas',
  QuickSortList in '..\..\Utils\QuickSortList.pas',
  BaseRule in 'ver0001\BaseRule.pas',
  BaseRuleData in 'ver0001\BaseRuleData.pas',
  Rule_MA in 'ver0001\Rule_MA.pas',
  Rule_EMA in 'ver0001\Rule_EMA.pas',
  Rule_HHV in 'ver0001\Rule_HHV.pas',
  Rule_LLV in 'ver0001\Rule_LLV.pas',
  RuleTestForm in 'RuleTestForm.pas' {Form1},
  Rule_STD in 'ver0001\Rule_STD.pas',
  RuleTestData in 'ver0001\RuleTestData.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
