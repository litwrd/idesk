unit RuleTestData;

interface

uses
  SysUtils,
  BaseDataSet,
  QuickList_Int;
              
type
  TRuleTestData = class(TBaseDataSetAccess)
  protected
    fIntList: TALIntegerList;   
    function GetRecordCount: Integer; override;
  public              
    constructor Create(ADBType, ADataSrcId: integer); override;
    destructor Destroy; override;          
    class function DataTypeDefine: integer; override;
    function GetDataLength: integer;
    function GetDataF(AIndex: integer): double;
    procedure AddIntData(AValue: integer);
  end;
  
implementation

{ TRuleTestData }
              
constructor TRuleTestData.Create(ADBType, ADataSrcId: integer);
begin
  inherited;
  fIntList := TALIntegerList.Create;
end;
         
destructor TRuleTestData.Destroy;
begin
  if nil <> fIntList then
  begin
    fIntList.Clear;
    FreeAndNil(fIntList);
  end;
  inherited;
end;

function TRuleTestData.GetDataLength: integer;
begin
  Result := Self.GetRecordCount;
end;

function TRuleTestData.GetRecordCount: Integer;
begin
  Result := fIntList.Count;
end;

class function TRuleTestData.DataTypeDefine: integer;
begin
  Result := 0;
end;
                           
procedure TRuleTestData.AddIntData(AValue: integer);
begin
  fIntList.Add(AValue);
end;

function TRuleTestData.GetDataF(AIndex: integer): double;
begin
  Result := fIntList.Items[AIndex];
end;

end.
