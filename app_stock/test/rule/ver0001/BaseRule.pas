unit BaseRule;

interface

type
  TRuleDataType = (
    dtNone,
    dtInt,    
    dtInt64,
    dtDouble,
    dtMultiData);

  TOnGetDataLength = function: integer of object;
  TOnGetDataI = function (AIndex: integer): integer of object;
  TOnGetDataI64 = function (AIndex: integer): int64 of object;
  TOnGetDataF = function (AIndex: integer): double of object;

  TBaseRuleData = record
    OnGetDataLength: TOnGetDataLength; 
    DataType: TRuleDataType;
    DataLength: integer;
  end;
  
  TBaseRule = class
  protected
    fBaseRuleData: TBaseRuleData;
  public
    constructor Create(ADataType: TRuleDataType = dtNone); virtual;
    destructor Destroy; override;
    procedure Execute; virtual;
    procedure Clear; virtual;
    property OnGetDataLength: TOnGetDataLength read fBaseRuleData.OnGetDataLength write fBaseRuleData.OnGetDataLength;
    property DataType: TRuleDataType read fBaseRuleData.DataType write fBaseRuleData.DataType;
    property DataLength: integer read fBaseRuleData.DataLength;
  end;
                       
  TBaseRuleI = class(TBaseRule)
  protected
    fOnGetDataI: TOnGetDataI;
    fMaxI: integer;
    fMinI: integer;
    function GetMaxI: integer; virtual;
    function GetMinI: integer; virtual;
  public                 
    constructor Create; reintroduce; virtual;
    property OnGetDataI: TOnGetDataI read fOnGetDataI write fOnGetDataI;
    property MaxI: integer read GetMaxI;
    property MinI: integer read GetMinI;
  end;
                   
  TBaseRuleI64 = class(TBaseRule)
  protected             
    fOnGetDataI64: TOnGetDataI64;  
    fMaxI: int64;
    fMinI: int64;
    function GetMaxI: int64; virtual;
    function GetMinI: int64; virtual;
  public                 
    constructor Create; reintroduce; virtual;
    property OnGetDataI64: TOnGetDataI64 read fOnGetDataI64 write fOnGetDataI64;
    property MaxI: int64 read GetMaxI;
    property MinI: int64 read GetMinI;
  end;
           
  TBaseRuleI_ParamN = class(TBaseRuleI)
  protected     
    fParamN: Word;        
    function GetValueI(AIndex: integer): Int64; virtual;
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);
  public          
    constructor Create; override;
    property ParamN: Word read GetParamN write SetParamN;  
    property ValueI[AIndex: integer]: int64 read GetValueI; 
  end;
  
  TBaseRuleF = class(TBaseRule)
  protected
    fOnGetDataF: TOnGetDataF;  
    fMaxF: double;
    fMinF: double;
    function GetMaxF: double; virtual;
    function GetMinF: double; virtual;
  public            
    constructor Create; reintroduce; virtual;
    property OnGetDataF: TOnGetDataF read fOnGetDataF write fOnGetDataF;
    property MaxF: double read GetMaxF;
    property MinF: double read GetMinF;
  end;
        
  TBaseRuleF_ParamN = class(TBaseRuleF)
  protected     
    fParamN: Word;         
    function GetValueF(AIndex: integer): double; virtual;
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);
  public              
    constructor Create; override;
    property ParamN: Word read GetParamN write SetParamN;      
    property ValueF[AIndex: integer]: double read GetValueF;
  end;
  
  TBaseRuleMultiRet = class(TBaseRule)
  public            
    constructor Create; reintroduce; virtual;
  end;

  TBaseRuleTest = class
  protected
    fRule: TBaseRule;
  public
    property Rule: TBaseRule read fRule write fRule;
  end;

implementation

{ TBaseRule }

constructor TBaseRule.Create(ADataType: TRuleDataType = dtNone);
begin
  FillChar(fBaseRuleData, SizeOf(fBaseRuleData), 0);
  fBaseRuleData.DataType := ADataType;
end;

destructor TBaseRule.Destroy;
begin
  Clear;
  inherited;
end;

procedure TBaseRule.Execute;
begin  
end;

procedure TBaseRule.Clear;
begin
end;

{ TBaseRuleI }
constructor TBaseRuleI.Create;
begin
  inherited Create(dtInt);
end;

function TBaseRuleI.GetMaxI: integer;
begin
  Result := fMaxI;
end;

function TBaseRuleI.GetMinI: integer;
begin
  Result := fMinI;
end;
            
{ TBaseRuleI }
constructor TBaseRuleI64.Create;
begin
  inherited Create(dtInt64);
end;

function TBaseRuleI64.GetMaxI: int64;
begin
  Result := fMaxI;
end;

function TBaseRuleI64.GetMinI: int64;
begin
  Result := fMinI;
end;

{ TBaseRuleF }
constructor TBaseRuleF.Create;
begin
  inherited Create(dtDouble);
end;

function TBaseRuleF.GetMaxF: double;
begin
  Result := fMaxF;
end;

function TBaseRuleF.GetMinF: double;
begin
  Result := fMinF;
end;

{ TBaseRuleMultiResult }

constructor TBaseRuleMultiRet.Create;
begin
  inherited Create(dtMultiData);
end;
                    
{ TBaseRuleI_ParamN }
constructor TBaseRuleI_ParamN.Create;
begin
  inherited;
  fParamN := 5;
end;

function TBaseRuleI_ParamN.GetParamN: Word;
begin
  Result := fParamN;
end;

function TBaseRuleI_ParamN.GetValueI(AIndex: integer): Int64;
begin
  Result := 0;
end;

procedure TBaseRuleI_ParamN.SetParamN(const Value: Word);
begin
  if 0 < Value then
    fParamN := Value;
end;

{ TBaseRuleF_ParamN }                   
constructor TBaseRuleF_ParamN.Create;
begin
  inherited;
  fParamN := 5;
end;

function TBaseRuleF_ParamN.GetParamN: Word;
begin
  Result := fParamN;
end;

function TBaseRuleF_ParamN.GetValueF(AIndex: integer): double;
begin
  Result := 0;
end;

procedure TBaseRuleF_ParamN.SetParamN(const Value: Word);
begin
  if 0 < Value then
    fParamN := Value;
end;

end.
