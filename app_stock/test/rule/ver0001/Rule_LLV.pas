unit Rule_LLV;

interface

uses
  BaseRule,
  BaseRuleData;

type
  TRule_LLV_I = class(TBaseRuleI_ParamN)
  protected
    fInt64Ret: PArrayInt64;
    function GetValueI(AIndex: integer): int64; override;
    procedure ComputeInt64;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
  end;
            
  TRule_LLV_F = class(TBaseRuleF_ParamN)
  protected
    fFloatRet: PArrayDouble;
    function GetValueF(AIndex: integer): double; override;
    procedure ComputeFloat;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
  end;
                     
  TPrice_Data_LLV = record   
    ExecuteResult: PArrayDouble;
    Cache_LLV: PChainArray;
  end;
                      
  // 三种模式
  //     1 预先计算
  //     2 Cache 计算
  //     3 实时计算
  TRule_LLV_Price = class(TBaseRuleF_ParamN)
  protected
    fPriceData_LLV: TPrice_Data_LLV;
    function ComputeValue_RT(AIndex: integer): double;  
    procedure ComputeExecute;
    function GetValueF(AIndex: integer): double; override;
  public       
    constructor Create; override;
    destructor Destroy; override;   
    procedure Clear; override;     
    procedure Execute; override;
  end;

implementation

{ TRule_LLV }

constructor TRule_LLV_I.Create;
begin
  inherited;
  fParamN := 20;          
  fInt64Ret := nil;
//  SetLength(fFloatRet, 0);
//  SetLength(fInt64Ret, 0);
end;
            
constructor TRule_LLV_F.Create;
begin
  inherited;
  fParamN := 20;     
  fFloatRet := nil;
//  SetLength(fFloatRet, 0);
//  SetLength(fInt64Ret, 0);
end;

destructor TRule_LLV_I.Destroy;
begin
  CheckInArrayInt64(fInt64Ret);
  inherited;
end;
               
destructor TRule_LLV_F.Destroy;
begin
  CheckInArrayDouble(fFloatRet);
  inherited;
end;

procedure TRule_LLV_I.Execute;
begin
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeInt64;
  end;
end;
       
procedure TRule_LLV_F.Execute;
begin
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeFloat;
  end;
end;

procedure TRule_LLV_I.ComputeInt64;
var
  tmpInt64_Meta: PArrayInt64;  
  i: integer;
  tmpCounter: integer;
  tmpValue: int64;
begin
  if Assigned(OnGetDataI) then
  begin                 
    if fInt64Ret = nil then
      fInt64Ret := CheckOutArrayInt64;
    tmpInt64_Meta := CheckOutArrayInt64;
    try
      SetArrayInt64Length(fInt64Ret, fBaseRuleData.DataLength);
      SetArrayInt64Length(tmpInt64_Meta, fBaseRuleData.DataLength);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpInt64_Meta.Value[i] := OnGetDataI(i);
        tmpValue := tmpInt64_Meta.Value[i];
        tmpCounter := fParamN - 1;
        while tmpCounter > 0 do
        begin
          if i > tmpCounter - 1 then
          begin
            if tmpValue > tmpInt64_Meta.Value[i - tmpCounter] then
            begin
              tmpValue := tmpInt64_Meta.Value[i - tmpCounter];
            end;
          end;
          Dec(tmpCounter);
        end;  
        SetArrayInt64Value(fInt64Ret, i, tmpValue);
      end;
    finally
      CheckInArrayInt64(tmpInt64_Meta);
    end;
  end;
end;

procedure TRule_LLV_F.ComputeFloat;
var
  tmpFloat_Meta: PArrayDouble;
  i: integer;
  tmpCounter: integer; 
  tmpValue: double;
begin
  if Assigned(OnGetDataF) then
  begin                 
    if fFloatRet = nil then
      fFloatRet := CheckOutArrayDouble;
    tmpFloat_Meta := CheckOutArrayDouble;
    try
      SetArrayDoubleLength(fFloatRet, fBaseRuleData.DataLength);
      SetArrayDoubleLength(tmpFloat_Meta, fBaseRuleData.DataLength);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpFloat_Meta.Value[i] := OnGetDataF(i);
        tmpValue := tmpFloat_Meta.Value[i];
        tmpCounter := fParamN - 1;
        while tmpCounter > 0 do
        begin
          if i > tmpCounter - 1 then
          begin
            if tmpValue > tmpFloat_Meta.Value[i - tmpCounter] then
            begin
              tmpValue := tmpFloat_Meta.Value[i - tmpCounter];
            end;
          end;
          Dec(tmpCounter);
        end;        
        SetArrayDoubleValue(fFloatRet, i, tmpValue);
      end;
    finally
      CheckInArrayDouble(tmpFloat_Meta);
    end;
  end;
end;

function TRule_LLV_I.GetValueI(AIndex: integer): int64;
begin
  Result := 0;
  if dtInt64 <> fBaseRuleData.DataType then
    exit;
  if nil <> fInt64Ret then
    Result := GetArrayInt64Value(fInt64Ret, AIndex);
end;

function TRule_LLV_F.GetValueF(AIndex: integer): double;
begin
  Result := 0;
  if dtDouble <> fBaseRuleData.DataType then
    exit;
  if nil <> fFloatRet then
    Result := GetArrayDoubleValue(fFloatRet, AIndex);
end;

{ TRule_LLV_Price }
constructor TRule_LLV_Price.Create;
begin
  inherited;   
  FillChar(fPriceData_LLV, SizeOf(fPriceData_LLV), 0);
end;

destructor TRule_LLV_Price.Destroy;
begin

  inherited;
end;
           

procedure TRule_LLV_Price.Clear;
begin
  inherited;
  if nil <> fPriceData_LLV.Cache_LLV then
  begin
    CheckInChainArray(fPriceData_LLV.Cache_LLV);
  end;
  if nil <> fPriceData_LLV.ExecuteResult then
  begin
    SetArrayDoubleLength(fPriceData_LLV.ExecuteResult, 0);   
  end;
end;
                                                                  
function TRule_LLV_Price.GetValueF(AIndex: integer): double;
begin
  if nil <> fPriceData_LLV.ExecuteResult then
  begin
    Result := GetArrayDoubleValue(fPriceData_LLV.ExecuteResult, AIndex);
  end else
  begin
    Result := ComputeValue_RT(AIndex);
  end;
end;
                         
procedure TRule_LLV_Price.Execute;
begin
  inherited;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeExecute;
  end;
end;
                 
procedure TRule_LLV_Price.ComputeExecute;
begin
end;

function TRule_LLV_Price.ComputeValue_RT(AIndex: integer): double;
var
  tmpCounter: integer;
  tmpIndex: integer; 
  tmpDouble: Double;
begin
  Result := 0;     
  if 1 > fParamN then
    exit;
  Result := OnGetDataF(AIndex);  
  tmpCounter := fParamN - 1;
  tmpIndex := AIndex - 1;
  while 0 < tmpCounter do
  begin
    if 0 <= tmpIndex then
    begin
      tmpDouble := OnGetDataF(tmpIndex);
      if tmpDouble < Result then
        Result := tmpDouble;        
    end else
      Break;
    tmpIndex := tmpIndex - 1;
    tmpCounter := tmpCounter - 1;
  end;
end;

end.
