unit Rule_MA;

interface

uses
  BaseRule,
  BaseRuleData;

type
  TRule_MA_I = class(TBaseRuleI_ParamN)    
  protected           
    fInt64Ret: PArrayInt64;
    function GetValueI(AIndex: integer): Int64; override;
    procedure ComputeInt64;
  public
    constructor Create; override;
    procedure Execute; override;
    procedure Clear; override;
    property ParamN: Word read GetParamN write SetParamN;
  end;
  
  TRule_MA_F = class(TBaseRuleF_ParamN)
  protected      
    fFloatRet: PArrayDouble;
    function GetValueF(AIndex: integer): double; override;
    procedure ComputeFloat;
  public
    constructor Create; override;
    procedure Execute; override;
    procedure Clear; override;
  end;

  TPrice_Data_MA = record
    ExecuteResult: PArrayDouble;
    Cache_MA: PChainArray;
  end;

  // 三种模式
  //     1 预先计算
  //     2 Cache 计算
  //     3 实时计算
  TRule_MA_Price = class(TBaseRuleF_ParamN)
  protected
    fPriceData_MA: TPrice_Data_MA;
    // 实时计算
    function ComputeValue_RT(AIndex: integer): double;
    function ComputeValue_RT_Cache(AIndex: integer): double;  
    procedure ComputeExecute;
    procedure ComputeExecute1; 
    function GetValueF(AIndex: integer): double; override;
  public
    constructor Create; override;
    procedure Clear; override; 
    procedure Execute; override;
  end;
  
implementation

{ TRule_MA }

constructor TRule_MA_I.Create;
begin
  inherited;
  fParamN := 20;
  fInt64Ret := nil;
end;
         
constructor TRule_MA_F.Create;
begin
  inherited;
  fParamN := 20;
  fFloatRet := nil;
end;
        
procedure TRule_MA_I.Execute;
begin
  Clear;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    if fBaseRuleData.DataLength > 0 then
      ComputeInt64;
  end;
end;

procedure TRule_MA_F.Execute;
begin
  Clear;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    if fBaseRuleData.DataLength > 0 then
      ComputeFloat;
  end;
end;

procedure TRule_MA_I.Clear;
begin
  CheckInArrayInt64(fInt64Ret);
  fBaseRuleData.DataLength := 0;
end;
               
procedure TRule_MA_F.Clear;
begin
  CheckInArrayDouble(fFloatRet);
  fBaseRuleData.DataLength := 0;
end;

procedure TRule_MA_I.ComputeInt64;
var
  tmpInt64_Meta: PArrayInt64; 
  i: integer;
  tmpCounter: integer;
  tmpValue: Int64;
begin
  if Assigned(OnGetDataI) then
  begin
    if fInt64Ret = nil then
      fInt64Ret := CheckOutArrayInt64;
    tmpInt64_Meta := CheckOutArrayInt64;
    try
      SetArrayInt64Length(fInt64Ret, fBaseRuleData.DataLength);
      SetArrayInt64Length(tmpInt64_Meta, fBaseRuleData.DataLength);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpInt64_Meta.value[i] := OnGetDataI(i);
        tmpValue := tmpInt64_Meta.value[i];
        tmpCounter := fParamN - 1;
        while tmpCounter > 0 do
        begin
          if i > tmpCounter - 1 then
          begin
            tmpValue := tmpValue + tmpInt64_Meta.value[i - tmpCounter];
          end;
          Dec(tmpCounter);
        end;
        if 1 < fParamN then
        begin
          if i > fParamN - 1 then
          begin
            tmpValue := tmpValue div fParamN;
          end else
          begin
            tmpValue := tmpValue div (i + 1);
          end;
        end;
        SetArrayInt64Value(fInt64Ret, i, tmpValue);
      end;
    finally
      CheckInArrayInt64(tmpInt64_Meta);
    end;
  end;
end;

procedure TRule_MA_F.ComputeFloat;
var
  tmpFloat_Meta: PArrayDouble;  
  i: integer;
  tmpCounter: integer;
  tmpDouble: Double;
begin
  if Assigned(OnGetDataF) then
  begin                  
    if fFloatRet = nil then
      fFloatRet := CheckOutArrayDouble;
    tmpFloat_Meta := CheckOutArrayDouble;
    try
      SetArrayDoubleLength(fFloatRet, fBaseRuleData.DataLength);   
      SetArrayDoubleLength(tmpFloat_Meta, fBaseRuleData.DataLength);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpFloat_Meta.Value[i] := OnGetDataF(i);
        tmpDouble := tmpFloat_Meta.Value[i];
        tmpCounter := fParamN - 1;
        while tmpCounter > 0 do
        begin
          if i > tmpCounter - 1 then
          begin
            tmpDouble := tmpDouble + tmpFloat_Meta.Value[i - tmpCounter];
          end;
          Dec(tmpCounter);
        end;       
        if fParamN > 1 then
        begin      
          if i > fParamN - 1 then
          begin
            tmpDouble := tmpDouble / fParamN;
          end else
          begin
            tmpDouble := tmpDouble / (i + 1);
          end;
        end;
        SetArrayDoubleValue(fFloatRet, i, tmpDouble);
      end;
    finally
      CheckInArrayDouble(tmpFloat_Meta);
    end;
  end;
end;

function TRule_MA_I.GetValueI(AIndex: integer): Int64;
begin
  Result := 0;
  if dtInt64 <> fBaseRuleData.DataType then
    exit;
  if nil <> fInt64Ret then
    Result := GetArrayInt64Value(fInt64Ret, AIndex);
end;

function TRule_MA_F.GetValueF(AIndex: integer): double;
begin
  Result := 0;
  if dtDouble <> fBaseRuleData.DataType then
    exit;
  if nil <> fFloatRet then
    Result := GetArrayDoubleValue(fFloatRet, AIndex);
end;

{ TRule_MA_Price }
                 
constructor TRule_MA_Price.Create;
begin
  inherited;
  FillChar(fPriceData_MA, SizeOf(fPriceData_MA), 0);
end;

procedure TRule_MA_Price.Clear;
begin
  inherited;
  if nil <> fPriceData_MA.Cache_MA then
  begin
    CheckInChainArray(fPriceData_MA.Cache_MA);
  end;
  if nil <> fPriceData_MA.ExecuteResult then
  begin
    SetArrayDoubleLength(fPriceData_MA.ExecuteResult, 0);   
  end;
  fBaseRuleData.DataLength := 0;
end;
        
function TRule_MA_Price.GetValueF(AIndex: integer): double;
begin
  if nil <> fPriceData_MA.ExecuteResult then
  begin
    Result := GetArrayDoubleValue(fPriceData_MA.ExecuteResult, AIndex);
  end else
  begin
    //Result := ComputeValue_RT(AIndex); 
    Result := ComputeValue_RT_Cache(AIndex);
  end;
end;
               
procedure TRule_MA_Price.Execute;
begin
  inherited;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeExecute;
  end;
end;
                               
procedure TRule_MA_Price.ComputeExecute;
var
  i: integer;
  tmpCounter: integer;
  tmpDouble: Double;
  tmpSum: double;
  tmpCache: array of double;
  tmpCacheIndex: integer;
begin
  if Assigned(OnGetDataF) then
  begin                  
    if nil = fPriceData_MA.ExecuteResult then
      fPriceData_MA.ExecuteResult := CheckOutArrayDouble;
    SetArrayDoubleLength(fPriceData_MA.ExecuteResult, fBaseRuleData.DataLength);
    tmpSum := 0;
    tmpCounter := 0;
    SetLength(tmpCache, fParamN);
    tmpCacheIndex := 0;
    for i := 0 to fBaseRuleData.DataLength - 1 do
    begin
      tmpDouble := OnGetDataF(i);  
      tmpSum := tmpSum + tmpDouble;
      if tmpCounter < fParamN then
      begin
        tmpCounter := tmpCounter + 1;
      end else
      begin
        tmpSum := tmpSum - tmpCache[tmpCacheIndex];
      end;
      fPriceData_MA.ExecuteResult.Value[i] := tmpSum / tmpCounter;
      tmpCache[tmpCacheIndex] := tmpDouble;
      Inc(tmpCacheIndex);
      if tmpCacheIndex = fParamN then
        tmpCacheIndex := 0; 
    end;
  end;
end;

procedure TRule_MA_Price.ComputeExecute1;
var
  i: integer;
  tmpCounter: integer;
  tmpDouble: Double;
  tmpSum: double;
  tmpPrevIndex: integer;
begin
  if Assigned(OnGetDataF) then
  begin                  
    if nil = fPriceData_MA.ExecuteResult then
      fPriceData_MA.ExecuteResult := CheckOutArrayDouble;
    SetArrayDoubleLength(fPriceData_MA.ExecuteResult, fBaseRuleData.DataLength);
    tmpSum := 0;
    tmpCounter := 0;
    tmpPrevIndex := 0;
    for i := 0 to fBaseRuleData.DataLength - 1 do
    begin
      tmpDouble := OnGetDataF(i);  
      tmpSum := tmpSum + tmpDouble;
      if tmpCounter < fParamN then
      begin
        tmpCounter := tmpCounter + 1;
        fPriceData_MA.ExecuteResult.Value[i] := tmpSum / tmpCounter;
      end else
      begin
        tmpSum := tmpSum - OnGetDataF(tmpPrevIndex);
        fPriceData_MA.ExecuteResult.Value[i] := tmpSum / tmpCounter;
        Inc(tmpPrevIndex);
      end;
    end;
  end;
end;

function TRule_MA_Price.ComputeValue_RT_Cache(AIndex: integer): double;
var
  tmpCounter: integer;
  tmpIndex: integer; 
  tmpDouble: Double;
begin
  Result := 0;
  { 方法主要在于 如何 cache GetData 用于加速 }
  if 1 > fParamN then
    exit;
  tmpDouble := 0;     
  tmpCounter := fParamN;
  tmpIndex := AIndex;
  while 0 < tmpCounter do
  begin
    if 0 <= tmpIndex then
    begin
      tmpDouble := tmpDouble + OnGetDataF(tmpIndex);
    end else
      Break;
    tmpIndex := tmpIndex - 1;
    tmpCounter := tmpCounter - 1;
  end;
  result := tmpDouble / (fParamN - tmpCounter);
end;

function TRule_MA_Price.ComputeValue_RT(AIndex: integer): double;
var
  tmpCounter: integer;
  tmpIndex: integer; 
  tmpDouble: Double;
begin
  Result := 0;
  { 方法主要在于 如何 cache GetData 用于加速 }
  if 1 > fParamN then
    exit;
  tmpDouble := 0;     
  tmpCounter := fParamN;
  tmpIndex := AIndex;
  while 0 < tmpCounter do
  begin
    if 0 <= tmpIndex then
    begin
      tmpDouble := tmpDouble + OnGetDataF(tmpIndex);
    end else
      Break;
    tmpIndex := tmpIndex - 1;
    tmpCounter := tmpCounter - 1;
  end;
  result := tmpDouble / (fParamN - tmpCounter);
end;

end.
