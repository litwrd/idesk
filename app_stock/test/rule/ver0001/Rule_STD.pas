unit Rule_STD;

interface

uses
  BaseRule,
  BaseRuleData;

(*//
STD
含义：求标准差。
用法：STD(X,N)为X的N日估算标准差。
//*)
type
  TRule_STD = class(TBaseRuleF_ParamN)
  protected
    fFloatRet: PArrayDouble;
    function GetValueF(AIndex: integer): double; override;
    procedure ComputeFloat;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Clear; override;
  end;
          
  TPrice_Data_STD = record  
    ExecuteResult: PArrayDouble;
    Cache_STD: PChainArray;
  end;
                 
  // 三种模式
  //     1 预先计算
  //     2 Cache 计算
  //     3 实时计算
  TRule_STD_Price = class(TBaseRuleF_ParamN)  
  protected          
    fPriceData_STD: TPrice_Data_STD;
    function GetValueF(AIndex: integer): double; override;
    function ComputeValue_RT(AIndex: integer): double;    
    procedure ComputeExecute;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Clear; override;      
    procedure Execute; override;
  end;
  
implementation

{ TRule_STD }

constructor TRule_STD.Create;
begin
  inherited;
  fParamN := 2;
  fFloatRet := nil;
end;

destructor TRule_STD.Destroy;
begin
  Clear;
  inherited;
end;

procedure TRule_Std.Execute;
begin
  Clear;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeFloat;
  end;
end;

procedure TRule_Std.Clear;
begin
  CheckInArrayDouble(fFloatRet);
  fBaseRuleData.DataLength := 0;
end;
(*//
procedure TRule_Std.ComputeInt64;
var
  tmpInt64_Meta: PArrayInt64;   
  tmpInt64_v1: PArrayInt64;
  tmpInt64_Ret: PArrayInt64;
  i, j: integer;
  tmpv: int64;
begin
  if Assigned(OnGetDataI) then
  begin
    if fFloatRet = nil then
      fFloatRet := CheckOutArrayDouble;
    tmpInt64_Ret := CheckOutArrayInt64;
    tmpInt64_Meta := CheckOutArrayInt64;
    tmpInt64_v1 := CheckOutArrayInt64;
    try
      SetArrayInt64Length(tmpInt64_Ret, fBaseRuleData.DataLength);
      SetArrayInt64Length(tmpInt64_Meta, fBaseRuleData.DataLength);
      SetArrayInt64Length(tmpInt64_v1, fBaseRuleData.DataLength);
      SetArrayDoubleLength(fFloatRet, fBaseRuleData.DataLength);

      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpInt64_Meta.Value[i] := OnGetDataI(i);
        //fFloatRet.value[i] := 0;
        // -------------------------------
        tmpInt64_v1.Value[i] := 0;
        for j := 0 to fParamN - 1 do
        begin
          tmpInt64_v1.Value[i] := tmpInt64_v1.Value[i] + tmpInt64_v1.Value[i - j];
        end;
        tmpInt64_v1.Value[i] := Round(tmpInt64_v1.Value[i] / fParamN);
        // -------------------------------
        tmpInt64_Ret.Value[i] := 0;
        for j := 0 to fParamN - 1 do
        begin
          tmpv := (tmpInt64_Meta.Value[i - j] - tmpInt64_v1.Value[i]);
          tmpInt64_Ret.Value[i] := tmpInt64_Ret.Value[i] + tmpv * tmpv;
        end;
        // -------------------------------
        SetArrayDoubleValue(fFloatRet, i, Sqrt(tmpInt64_Ret.Value[i] / (fParamN - 1)));
        // -------------------------------
      end;
      SetArrayInt64Length(tmpInt64_Meta, 0);
      SetArrayInt64Length(tmpInt64_v1, 0);
      SetArrayInt64Length(tmpInt64_Ret, 0);
    finally
      CheckInArrayInt64(tmpInt64_Ret);
      CheckInArrayInt64(tmpInt64_Meta);
      CheckInArrayInt64(tmpInt64_v1);
    end;
  end;
end;
//*)
(*
  STD(CLOSE,10),
    -------------------------------------
    x := (x[n] + x[n - 1] + x[1]) / n
    -------------------------------------
    v := (x[n]-x)^2 + (x[n - 1]-x)^2 + (x[1]-x)^2
    v := v / (n - 1)
    std = sqrt(v);
    -------------------------------------
    sqrt（4） = 2
*)
procedure TRule_Std.ComputeFloat;
var
  tmpFloat_Meta: PArrayDouble;
  tmpFloat_v1: PArrayDouble;
  i, j, tmpCounter: integer;
  tmpV: double;
  tmpValue: double;
begin
  if Assigned(OnGetDataF) then
  begin
    if fFloatRet = nil then
      fFloatRet := CheckOutArrayDouble;
    tmpFloat_Meta := CheckOutArrayDouble;
    tmpFloat_v1 := CheckOutArrayDouble;
    try
      SetArrayDoubleLength(fFloatRet, fBaseRuleData.DataLength);
      SetArrayDoubleLength(tmpFloat_Meta, fBaseRuleData.DataLength);
      SetArrayDoubleLength(tmpFloat_v1, fBaseRuleData.DataLength);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpFloat_Meta.Value[i] := OnGetDataF(i);
        // -------------------------------
        tmpFloat_v1.Value[i] := 0;
        tmpCounter := 0;
        for j := 0 to fParamN - 1 do
        begin
          if i >= j then
          begin
            tmpFloat_v1.value[i] := tmpFloat_v1.value[i] + tmpFloat_Meta.value[i - j];
            Inc(tmpCounter);
          end else
          begin
            Break;
          end;
        end;
        tmpFloat_v1.value[i] := tmpFloat_v1.value[i] / tmpCounter;
        // -------------------------------
        tmpValue := 0;
        for j := 0 to tmpCounter - 1 do
        begin
          tmpv := (tmpFloat_Meta.value[i - j] - tmpFloat_v1.value[i]);
          tmpValue := tmpValue + tmpv * tmpv;
        end;
        // -------------------------------
        if tmpCounter > 1 then
        begin
          tmpValue := tmpValue / (tmpCounter - 1);
          tmpValue := Sqrt(tmpValue);
        end else
        begin
          tmpValue := 0;
        end;   
        SetArrayDoubleValue(fFloatRet, i, tmpValue);
        // -------------------------------
      end;
      SetArrayDoubleLength(tmpFloat_Meta, 0);
      SetArrayDoubleLength(tmpFloat_v1, 0);
    finally
      CheckInArrayDouble(tmpFloat_Meta);
      CheckInArrayDouble(tmpFloat_v1);
    end;
  end;
end;

function TRule_Std.GetValueF(AIndex: integer): double;
begin
  Result := 0;
  if dtDouble <> fBaseRuleData.DataType then
    exit;
  if nil <> fFloatRet then
    Result := GetArrayDoubleValue(fFloatRet, AIndex);
end;

{ TRule_STD_Price }

constructor TRule_STD_Price.Create;
begin
  inherited;
  FillChar(fPriceData_STD, SizeOf(fPriceData_STD), 0);
end;

destructor TRule_STD_Price.Destroy;
begin
  inherited;
end;
    
procedure TRule_STD_Price.Clear;
begin
  inherited;   
  if nil <> fPriceData_STD.Cache_STD then
  begin
    CheckInChainArray(fPriceData_STD.Cache_STD);
  end;        
  if nil <> fPriceData_STD.ExecuteResult then
  begin
    SetArrayDoubleLength(fPriceData_STD.ExecuteResult, 0);   
  end;
end;

function TRule_STD_Price.GetValueF(AIndex: integer): double;
begin                  
  if nil <> fPriceData_STD.ExecuteResult then
  begin
    Result := GetArrayDoubleValue(fPriceData_STD.ExecuteResult, AIndex);
  end else
  begin
    Result := ComputeValue_RT(AIndex);
  end;
end;
                      
procedure TRule_STD_Price.Execute;
begin
  inherited;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeExecute;
  end;
end;
                 
procedure TRule_STD_Price.ComputeExecute;
var
  i: integer;
  tmpCounter: integer;
  tmpCounter2: integer;
  tmpOriginData: Double;
  tmpCacheOriginData: array of double;
  tmpCacheOriginDataIndex: integer;
  tmpPrevIndex: integer; 
  tmpSum: double;
  tmpAverage: double;
  tmpDouble: Double;
begin  
  if Assigned(OnGetDataF) then
  begin                  
    if nil = fPriceData_STD.ExecuteResult then
      fPriceData_STD.ExecuteResult := CheckOutArrayDouble;
    SetArrayDoubleLength(fPriceData_STD.ExecuteResult, fBaseRuleData.DataLength);
    tmpSum := 0;
    tmpCounter := 0;
    SetLength(tmpCacheOriginData, fParamN);
    tmpCacheOriginDataIndex := 0;
    for i := 0 to fBaseRuleData.DataLength - 1 do
    begin
      tmpOriginData := OnGetDataF(i);  
      tmpSum := tmpSum + tmpOriginData;
      if tmpCounter < fParamN then
      begin
        tmpCounter := tmpCounter + 1;
      end else
      begin
        tmpSum := tmpSum - tmpCacheOriginData[tmpCacheOriginDataIndex];
      end;
      tmpAverage := tmpSum / tmpCounter;
      tmpDouble := (tmpOriginData - tmpAverage) * (tmpOriginData - tmpAverage);
      if 1 < tmpCounter then
      begin
        tmpCounter2 := tmpCounter - 1;
        tmpPrevIndex := tmpCacheOriginDataIndex - 1; 
        if 0 > tmpPrevIndex then
          tmpPrevIndex := tmpCounter - 1;
        while 0 < tmpCounter2 do
        begin
          tmpDouble := tmpDouble + (tmpCacheOriginData[tmpPrevIndex] - tmpAverage) * (tmpCacheOriginData[tmpPrevIndex] - tmpAverage);
          tmpCounter2 := tmpCounter2 - 1;
          tmpPrevIndex := tmpPrevIndex - 1;
          if 0 > tmpPrevIndex then
            tmpPrevIndex := tmpCounter - 1;
        end;
        tmpDouble := tmpDouble / (tmpCounter - 1);
        fPriceData_STD.ExecuteResult.Value[i] := Sqrt(tmpDouble);
      end;      
      tmpCacheOriginData[tmpCacheOriginDataIndex] := tmpOriginData;
      Inc(tmpCacheOriginDataIndex);
      if tmpCacheOriginDataIndex = fParamN then
        tmpCacheOriginDataIndex := 0; 
    end;
  end;
end;

function TRule_STD_Price.ComputeValue_RT(AIndex: integer): double;
var
  tmpCounter: integer;
  tmpIndex: integer;
  tmpDouble: Double;
  tmpAverage: Double;   
  tmpValue: Double;
begin
  Result := 0;
  { 方法主要在于 如何 cache GetData 用于加速 }
  if 2 > fParamN then
    exit;
  tmpDouble := 0;     
  tmpCounter := fParamN;
  tmpIndex := AIndex;
  while 0 < tmpCounter do
  begin
    if 0 <= tmpIndex then
    begin
      tmpDouble := tmpDouble + OnGetDataF(tmpIndex);
    end else
      Break;
    tmpIndex := tmpIndex - 1;
    tmpCounter := tmpCounter - 1;
  end;
  tmpAverage := tmpDouble / (fParamN - tmpCounter);
  (*//
  v := (x[n]-x)^2 + (x[n - 1]-x)^2 + (x[1]-x)^2
  v := v / (n - 1)
  std = sqrt(v);
  //*)
  tmpValue := 0;   
  tmpCounter := fParamN;
  tmpIndex := AIndex;
  while 0 < tmpCounter do
  begin
    if 0 <= tmpIndex then
    begin
      tmpDouble := OnGetDataF(tmpIndex);
      tmpValue := tmpValue + (tmpDouble - tmpAverage) * (tmpDouble - tmpAverage);
    end else
      Break;
    tmpIndex := tmpIndex - 1;
    tmpCounter := tmpCounter - 1;
  end;
  if fParamN - tmpCounter > 1 then
  begin
    tmpValue := tmpValue / (fParamN - tmpCounter - 1);
    result := Sqrt(tmpValue);
  end else
  begin
    Result := 0;
  end;
end;

end.
