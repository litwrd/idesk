unit RuleTestForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BaseRule, RuleTestData;

type
  TForm1 = class(TForm)
    btnMA: TButton;
    mmoSrc: TMemo;
    mmoResult: TMemo;
    btnInit: TButton;
    edtNum: TEdit;
    mmoResult2: TMemo;
    btnLLV: TButton;
    btnHHV: TButton;
    btnEMA: TButton;
    btnSTD: TButton;
    procedure btnMAClick(Sender: TObject);
    procedure btnInitClick(Sender: TObject);
    procedure btnLLVClick(Sender: TObject);
    procedure btnHHVClick(Sender: TObject);
    procedure btnEMAClick(Sender: TObject);
    procedure btnSTDClick(Sender: TObject);
  private
    { Private declarations }
    fTestData: TRuleTestData;
    procedure BuildTestData;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  Rule_LLV,
  Rule_HHV,
  Rule_EMA,
  Rule_STD,
  Rule_MA;
        
constructor TForm1.Create(Owner: TComponent);
begin
  inherited;
  fTestData := nil;// TRuleTestData;
end;

procedure TForm1.btnInitClick(Sender: TObject);
var
  i: integer;
begin
  mmoSrc.Lines.BeginUpdate;
  try
    mmoSrc.Lines.Clear;
    for i := 1 to StrToIntDef(edtNum.Text, 0) do
    begin
      mmoSrc.Lines.Add(IntToStr(Trunc(Random(i * 10) + Random(100))));
    end;
  finally
    mmoSrc.Lines.EndUpdate;
  end;
  BuildTestData;
end;

procedure TForm1.BuildTestData;
var
  i: Integer;
begin
  if nil = fTestData then
  begin
    fTestData := TRuleTestData.Create(0, 0);
  end;          
  for i := 0 to mmoSrc.Lines.Count - 1 do
  begin
    fTestData.AddIntData(StrToIntDef(mmoSrc.Lines[i], 0));
  end;
end;

procedure TForm1.btnLLVClick(Sender: TObject);
var
  tmpRule: TBaseRuleF_ParamN;
  tmpStr: string;
  i: integer;
  tmpParamN: integer;
begin
  tmpParamN := 4;
  tmpRule := TRule_LLV_F.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;
    tmpRule.Execute;

    mmoResult.Lines.BeginUpdate;
    try
      mmoResult.Lines.Clear;
      mmoResult.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult.Lines.Add(tmpStr);
      end;
    finally
      mmoResult.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
  tmpRule := TRule_LLV_Price.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;  
    mmoResult2.Lines.BeginUpdate;
    try
      mmoResult2.Lines.Clear;
      mmoResult2.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult2.Lines.Add(tmpStr);
      end;
    finally
      mmoResult2.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
end;

procedure TForm1.btnHHVClick(Sender: TObject);
var
  tmpRule: TBaseRuleF_ParamN;
  tmpStr: string;
  i: integer;
  tmpParamN: integer;
begin
  tmpParamN := 4;
  tmpRule := TRule_HHV_F.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;
    tmpRule.Execute;

    mmoResult.Lines.BeginUpdate;
    try
      mmoResult.Lines.Clear;
      mmoResult.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult.Lines.Add(tmpStr);
      end;
    finally
      mmoResult.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
  tmpRule := TRule_HHV_Price.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;  
    mmoResult2.Lines.BeginUpdate;
    try
      mmoResult2.Lines.Clear;
      mmoResult2.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult2.Lines.Add(tmpStr);
      end;
    finally
      mmoResult2.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
end;

procedure TForm1.btnMAClick(Sender: TObject);
var
  tmpRule: TBaseRuleF_ParamN;
  tmpStr: string;
  i: integer;
  tmpParamN: integer;
begin
  tmpParamN := 6;
  tmpRule := TRule_MA_F.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;
    tmpRule.Execute;

    mmoResult.Lines.BeginUpdate;
    try
      mmoResult.Lines.Clear;
      mmoResult.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult.Lines.Add(tmpStr);
      end;
    finally
      mmoResult.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
  tmpRule := TRule_MA_Price.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;  
    mmoResult2.Lines.BeginUpdate;
    try
      mmoResult2.Lines.Clear;
      mmoResult2.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult2.Lines.Add(tmpStr);
      end;
    finally
      mmoResult2.Lines.EndUpdate;
    end;  
  finally
    tmpRule.Free;
  end;
end;

procedure TForm1.btnEMAClick(Sender: TObject);
var
  tmpRule: TBaseRuleF_ParamN;
  tmpStr: string;
  i: integer;
  tmpParamN: integer;
begin
  tmpParamN := 6;
  tmpRule := TRule_EMA_F.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;
    tmpRule.Execute;

    mmoResult.Lines.BeginUpdate;
    try
      mmoResult.Lines.Clear;
      mmoResult.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult.Lines.Add(tmpStr);
      end;
    finally
      mmoResult.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
end;

procedure TForm1.btnSTDClick(Sender: TObject);
var
  tmpRule: TBaseRuleF_ParamN;
  tmpStr: string;
  i: integer;
  tmpParamN: integer;
begin
  tmpParamN := 6;
  tmpRule := TRule_STD.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;
    tmpRule.Execute;

    mmoResult.Lines.BeginUpdate;
    try
      mmoResult.Lines.Clear;
      mmoResult.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult.Lines.Add(tmpStr);
      end;
    finally
      mmoResult.Lines.EndUpdate;
    end;
  finally
    tmpRule.Free;
  end;
  tmpRule := TRule_STD_Price.Create;
  try
    tmpRule.ParamN := tmpParamN;
    tmpRule.OnGetDataLength := fTestData.GetDataLength;
    tmpRule.OnGetDataF := fTestData.GetDataF;  
    mmoResult2.Lines.BeginUpdate;
    try
      mmoResult2.Lines.Clear;
      mmoResult2.Lines.Add('----------------------------');
      for i := fTestData.RecordCount - 1 downto 0 do
      begin
        tmpStr := FormatFloat('0.00', fTestData.GetDataF(i));
        tmpStr := tmpStr + #9 + ' ==> '+ FormatFloat('0.00', tmpRule.ValueF[i]);
        mmoResult2.Lines.Add(tmpStr);
      end;
    finally
      mmoResult2.Lines.EndUpdate;
    end;  
  finally
    tmpRule.Free;
  end;
end;

end.
