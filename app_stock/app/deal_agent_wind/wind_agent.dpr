program wind_agent;
         
{$IFDEF CONSOLE}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  windef_msg in '..\..\..\..\devwintech\v0001\windef\windef_msg.pas',
  win.process in '..\..\..\..\devwintech\v0001\rec\win_sys\win.process.pas',
  win.wnd_cmd in '..\..\..\..\devwintech\v0001\rec\win_sys\win.wnd_cmd.pas',
  win.thread in '..\..\..\..\devwintech\v0001\rec\win_sys\win.thread.pas',
  win.app in '..\..\..\..\devwintech\v0001\rec\win_app\win.app.pas',
  base.thread in '..\..\..\..\devwintech\v0001\rec\app_base\base.thread.pas',
  base.run in '..\..\..\..\devwintech\v0001\rec\app_base\base.run.pas',
  BaseApp in '..\..\..\devwintech\v0001\app_base\BaseApp.pas',
  BasePath in '..\..\..\devwintech\v0001\app_base\BasePath.pas',
  BaseWinApp in '..\..\..\devwintech\v0001\win_app\BaseWinApp.pas',
  UtilsWindows in '..\..\..\..\devwintech\v0000\win_utils\UtilsWindows.pas',
  define_price in '..\..\define\define_price.pas',
  define_stockapp in '..\..\define\define_stockapp.pas',
  define_message in '..\..\define\define_message.pas',
  utils_findwnd in '..\..\Utils\utils_findwnd.pas',
  define_windprocess in '..\..\dealagent\define_windprocess.pas',
  utils_windprocess in '..\..\dealagent\utils_windprocess.pas',
  utils_wind_login in '..\..\dealagent\utils_wind_login.pas',
  utils_wind_main in '..\..\dealagent\utils_wind_main.pas',
  utils_wind_dialog in '..\..\dealagent\utils_wind_dialog.pas',
  utils_wind_export in '..\..\dealagent\utils_wind_export.pas',
  windAgentApp in 'windAgentApp.pas';

{$R *.res}
{$R uac.res}

begin
  RunApp(TWindAgentApp, TWindAgentApp.ClassName, TBaseApp(GlobalApp));
//  GlobalApp := TzsHelperApp.Create('zshelperApp');
//  try
//    if GlobalApp.Initialize then
//    begin
//      GlobalApp.Run;
//    end;
//    GlobalApp.Finalize;
//  finally
//    GlobalApp.Free;
//  end;
end.
