unit windAgentApp;

interface

uses
  Windows, Messages, Sysutils,
  BaseApp, BaseWinApp, define_windprocess;
  
type
  TWindAgentAppData   = record
    WindProcess       : TWindProcess;
    WindMainWnd       : TWindWndMain;
  end;

  TWindAgentApp = class(TBaseWinApp)
  protected
    fWindAgentAppData: TWindAgentAppData;     
    function CreateAppCommandWindow: Boolean;
    procedure Test;
  public
    function Initialize: Boolean; override;
    procedure Finalize; override;
    procedure Run; override;
  end;

var
  GlobalApp: TWindAgentApp = nil;

implementation

uses
  UtilsWindows,
  windef_msg,
  define_stockapp,
  define_message,
  //UtilsLog,
  utils_wind_dialog,
  utils_wind_main,
  utils_wind_login,
  utils_wind_export,
  utils_windprocess;
           
{$IFNDEF RELEASE}  
const
  LOGTAG = 'windAgentApp.pas';//TWindAgentApp.ClassName;
{$ENDIF}
    
{ TWindAgentApp }                           
function TWindAgentApp.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin
    Result := CheckSingleInstance(AppMutexName_Stock_Agent_Wind);
    if Result then
    begin
      FillChar(fWindAgentAppData, SizeOf(fWindAgentAppData), 0);
      fWindAgentAppData.WindProcess.LoginAccountId := 1808175116;
      //Application.Initialize;
    end;
  end;
end;

procedure TWindAgentApp.Finalize;
begin
  inherited;
end;

procedure TWindAgentApp.Test;
var
  tmpMainWnd: TWindWndMain;   
  tmpLoginWnd: TWindWndLogin;
  //tmpMainDealWnd: TWindWndMainDeal;    
//  tmpWndDialogs: TWindWndDialogs;
begin
  FillChar(tmpLoginWnd, SizeOf(tmpLoginWnd), 0);                           
  if FindWindLoginWnd(@fWindAgentAppData.WindProcess, @tmpLoginWnd) then
  begin
    AutoLogin(@fWindAgentAppData.WindProcess, @tmpLoginWnd, 12177);
  end; 
  FillChar(tmpMainWnd, SizeOf(tmpMainWnd), 0);
  if FindWindMainWnd(@fWindAgentAppData.WindProcess, @tmpMainWnd) then
  begin
    CloseWindDialog(@fWindAgentAppData.WindProcess, nil);
    TraverseCheckMainChildWindowA(tmpMainWnd.Core.WindowHandle, @tmpMainWnd);
    if IsWindow(tmpMainWnd.Core.WindowHandle) then
    begin
      if IsWindow(tmpMainWnd.WndFunctionTree) then
      begin
        ForceBringFrontWindow(tmpMainWnd.Core.WindowHandle);
        //ClickFunctionTreeBuyNode(@tmpMainWnd);
        //ClickFunctionTreeSaleNode(@tmpMainWnd);
        //ClickFunctionTreeCancelNode(@tmpMainWnd);
        //ClickFunctionTreeQueryNode(@tmpMainWnd);
        //ClickFunctionTreeQueryDealNode(@tmpMainWnd);
        Exit;
        (*//
        FillChar(tmpMainDealWnd, SizeOf(tmpMainDealWnd), 0);
        TraverseCheckMainDealPanelChildWindowA(tmpMainWnd.WndDealPanel, @tmpMainWnd, @tmpMainDealWnd);
        if IsWindow(tmpMainDealWnd.WndAccountCombo) then
        begin
          //InputEditWnd(tmpMainDealWnd.WndStockCodeEdit, '002414');   
          InputEditWnd(tmpMainDealWnd.WndStockCodeEdit, '601199');
          SleepWait(20);
          //InputEditWnd(tmpMainDealWnd.WndPriceEdit, FormatFloat('0.00', 25.1));
          InputEditWnd(tmpMainDealWnd.WndPriceEdit, FormatFloat('0.00', 8.5));
          SleepWait(20);
          InputEditWnd(tmpMainDealWnd.WndNumEdit, IntToStr(100));
          SleepWait(20);
          ClickButtonWnd(tmpMainDealWnd.WndOrderButton);


          FillChar(tmpWndDialogs, SizeOf(tmpWndDialogs), 0);
          if FindWindDialogWnd(@fWindAgentAppData.WindProcess, @tmpWndDialogs, 0) then
          begin
            if 1 = tmpWndDialogs.WndCount then
            begin
              ForceBringFrontWindow(tmpWndDialogs.WndArray[0].Core.WindowHandle);
              ClickButtonWnd(tmpWndDialogs.WndArray[0].OKButton);
            end;
          end;
          Sleep(100);
          while CloseWindDialog(@fWindAgentAppData.WindProcess, 0) do
          begin
            Sleep(100);
          end;
        end;
        //*)
      end;
    end;
  end;
end;
                      
function BusinessCommandWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM; var AResult: LRESULT): Boolean;
begin
  Result := true;
  case AMsg of
    WM_Deal_Request_Buy: begin
    end;               
    WM_Deal_Request_Sale: begin    
    end;
    WM_Deal_Request_Cancel: begin
    end;
    WM_Export_Data: begin
      //Log(LOGTAG, 'WM_Data_Export');
      utils_wind_export.CallExport_QuoteData(
        @GlobalApp.fWindAgentAppData.WindProcess,
        @GlobalApp.fWindAgentAppData.WindMainWnd,
        wParam,
        lparam);
    end;   
    WM_Export_FirstListInfo: begin
      utils_wind_export.CallExport_FirstListInfo(
        @GlobalApp.fWindAgentAppData.WindProcess,
        @GlobalApp.fWindAgentAppData.WindMainWnd);
    end;
    WM_Export_InfoF10_1: begin
      utils_wind_export.CallExport_InfoF10_1(
        @GlobalApp.fWindAgentAppData.WindProcess,
        @GlobalApp.fWindAgentAppData.WindMainWnd,
        wParam);
    end;
    WM_Export_SetConfig: begin
      //Log(LOGTAG, 'WM_Data_Config:' + IntToStr(wParam) + '/' + IntToStr(lParam));    
      if Config_Export_DownClick = wParam then
      begin
        Global_Export_DownClickCount := lParam;   
      end;
      AResult := 1;
    end;
    else
      Result := false;
  end;
end;

function AppCommandWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := 0;
  case AMsg of
    WM_AppStart: begin
    end;
    WM_AppRequestEnd: begin
    end;
    WM_AppNotifyOS: begin
    end;
    else
      if BusinessCommandWndProcA(AWnd, AMsg, wParam, lParam, Result) then
        exit;
  end;        
  Result := DefWindowProcA(AWnd, AMsg, wParam, lParam);
end;
                
function TWindAgentApp.CreateAppCommandWindow: Boolean;
var
  tmpRegWinClass: TWndClassA;  
  tmpGetWinClass: TWndClassA;
  tmpIsReged: Boolean;
begin
  Result := false;          

  ChangeWindowMessageFilter(WM_COPYDATA, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_AppRequestEnd, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_AppNotifyOS, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Export_SetConfig, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Export_Data, MSGFLT_ADD);   

  FillChar(tmpRegWinClass, SizeOf(tmpRegWinClass), 0);
  tmpRegWinClass.hInstance := HInstance;
  tmpRegWinClass.lpfnWndProc := @AppCommandWndProcA;
  tmpRegWinClass.lpszClassName := define_stockapp.AppCmdWndClassName_StockAgent_WIND;
  tmpIsReged := GetClassInfoA(HInstance, tmpRegWinClass.lpszClassName, tmpGetWinClass);
  if tmpIsReged then
  begin
    if (tmpGetWinClass.lpfnWndProc <> tmpRegWinClass.lpfnWndProc) then
    begin                           
      UnregisterClassA(tmpRegWinClass.lpszClassName, HInstance);
      tmpIsReged := false;
    end;
  end;
  if not tmpIsReged then
  begin
    if 0 = RegisterClassA(tmpRegWinClass) then
      exit;
  end;

  //TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow := CreateWindowExA(
  Self.AppWindow := CreateWindowExA(
    WS_EX_TOOLWINDOW
    //or WS_EX_APPWINDOW
    //or WS_EX_TOPMOST
    ,
    tmpRegWinClass.lpszClassName,
    '', WS_POPUP {+ 0},
    0, 0, 0, 0,
    HWND_MESSAGE, 0, HInstance, nil);
  Result := Windows.IsWindow(Self.AppWindow);
  if Result then
  begin
    //Log(LOGTAG, 'CreateAppCommandWindow Succ');                  
  end else
  begin
    //Log(LOGTAG, 'CreateAppCommandWindow Fail');  
  end;
end;
                                 
procedure TWindAgentApp.Run;
var
  tmpProcessHandle: THandle;
begin          
  inherited;
  if FindWindProcess(@fWindAgentAppData.WindProcess) then
  begin                            
    tmpProcessHandle := Windows.OpenProcess(PROCESS_TERMINATE, FALSE, fWindAgentAppData.WindProcess.Process.Core.ProcessId);
    if (0 <> tmpProcessHandle) and (INVALID_HANDLE_VALUE <> tmpProcessHandle) then
    begin
      // 发布的时候 必须 杀非管理进程 调试的时候不用 杀进程
//      Windows.TerminateProcess(tmpProcessHandle, 0);
//      fWindAgentAppData.WindProcess.Process.Core.ProcessId := 0;
    end;
  end;         
  //if (0 <> fWindAgentAppData.WindProcess.Process.Core.ProcessId) then
  begin
    if CreateAppCommandWindow then
    begin
      PostMessage(Self.AppWindow, WM_AppStart, 0, 0);
      RunAppMsgLoop;
    end;
  end;
end;

end.
