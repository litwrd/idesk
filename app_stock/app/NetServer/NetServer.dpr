program NetServer;

// {$APPTYPE CONSOLE}

{$IFDEF CONSOLE}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  WinSock2 in '..\..\..\..\devwintech\common\WinSock2.pas',
  Base.Run in '..\..\..\..\devwintech\v0001\rec\app_base\Base.Run.pas',
  Base.thread in '..\..\..\..\devwintech\v0001\rec\app_base\Base.thread.pas',
  win.app in '..\..\..\..\devwintech\v0001\rec\win_app\win.app.pas',
  win.diskfile in '..\..\..\..\devwintech\v0001\rec\win_sys\win.diskfile.pas',
  win.cpu in '..\..\..\..\devwintech\v0001\rec\win_sys\win.cpu.pas',
  win.iocp in '..\..\..\..\devwintech\v0001\rec\win_sys\win.iocp.pas',
  win.thread in '..\..\..\..\devwintech\v0001\rec\win_sys\win.thread.pas',
  win.error in '..\..\..\..\devwintech\v0001\rec\win_sys\win.error.pas',
  BaseThread in '..\..\..\devwintech\v0001\app_base\BaseThread.pas',
  BaseApp in '..\..\..\devwintech\v0001\app_base\BaseApp.pas',
  BasePath in '..\..\..\devwintech\v0001\app_base\BasePath.pas',
  BaseWinApp in '..\..\..\devwintech\v0001\win_app\BaseWinApp.pas',
  windef_msg in '..\..\..\..\devwintech\v0001\windef\windef_msg.pas',
  win.wnd in '..\..\..\..\devwintech\v0001\winproc\win.wnd.pas',
  win.shutdown in '..\..\..\..\devwintech\v0001\winproc\win.shutdown.pas',
  define_message in '..\..\define\define_message.pas',
  define_stockapp in '..\..\define\define_stockapp.pas',
  utils_zs_agentconsole in '..\..\dealagent\utils_zs_agentconsole.pas',
  netprotocol in '..\NetClient\net\netprotocol.pas',
  DataChain in 'net\DataChain.pas',
  NetBase in 'net\NetBase.pas',
  NetMgr in 'net\NetMgr.pas',
  NetBaseObj in 'net\NetBaseObj.pas',
  NetSrvClientIocp in 'net\NetSrvClientIocp.pas',
  NetServerIocp in 'net\NetServerIocp.pas',
  BaseDataIO in 'net\BaseDataIO.pas',
  NetServerApp in 'NetServerApp.pas',
  NetServerAppStart in 'NetServerAppStart.pas',
  NetServerCommandWnd in 'NetServerCommandWnd.pas';

{$R *.res}

begin
  {$IFDEF CONSOLE}
  Writeln('Server Run');
  {$ENDIF}
  GlobalApp := TNetServerApp.Create('NetServerApp');
  try                
    {$IFDEF CONSOLE}
    GlobalApp.IsConsoleMode := true;
    {$ENDIF}
    if GlobalApp.Initialize then
    begin
      GlobalApp.Run;
    end;
    GlobalApp.Finalize;
  finally
    GlobalApp.Free;
  end;
end.
