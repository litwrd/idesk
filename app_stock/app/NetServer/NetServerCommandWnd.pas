unit NetServerCommandWnd;

interface

uses
  Windows, Messages;
  
  function WndProcW_NetServerApp(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  
implementation

uses
  windef_msg, Sysutils,
  define_message,
  win.shutdown,
  NetServerAppStart;
  
function WndProcW_NetServerApp(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := 0;
  case AMsg of
    WM_COPYDATA: begin

    end;
    WM_AppStart: begin
      WMAppStart;
    end;       
    WM_AppRequestEnd: begin
    end;
    WM_AppNotifyOS: begin  
      {$IFDEF CONSOLE}
      Writeln('WndProcW_NetServerApp WM_AppNotifyOS:' + intToStr(wParam));
      //exit;
      {$ENDIF}
      if AppNotify_ShutDown = wParam then
        win.shutdown.ShutDown;       
      if AppNotify_Restart = wParam then
        win.shutdown.Reboot;
      if AppNotify_Logout = wParam then 
        win.shutdown.LoginOff;
    end;
    else
      Result := DefWindowProcW(AWnd, AMsg, wParam, lParam);
  end;
end;

end.
