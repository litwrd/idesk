unit NetServerApp;

interface

uses
  Windows, BaseApp, BaseWinApp, NetMgr;
  
type
  TDealAgentServerAppData = record
    NetMgr: TNetMgr;
  end;
  
  TNetServerApp = Class(TBaseWinApp)
  protected
    fSrvAppData: TDealAgentServerAppData;
  public
    constructor Create(AppClassId: AnsiString); override;
    destructor Destroy; override;        
    function Initialize: Boolean; override;
    procedure Finalize; override;
    procedure Run; override;     
    property NetMgr: TNetMgr read fSrvAppData.NetMgr;
  end;

var
  GlobalApp: TNetServerApp = nil;
    
implementation

uses
  Messages, windef_msg, win.wnd, Sysutils,
  NetServerCommandWnd;
  
{ THttpApiSrvApp }

constructor TNetServerApp.Create(AppClassId: AnsiString);
begin
  inherited;
  FillChar(fSrvAppData, SizeOf(fSrvAppData), 0);
end;

destructor TNetServerApp.Destroy;
begin
  inherited;
end;

procedure TNetServerApp.Finalize;
begin
  //DestroyCommandWindow(@fSrvAppData.AppWindow.CommandWindow);
  FreeAndNIl(fSrvAppData.NetMgr);
end;
                      
function TNetServerApp.Initialize: Boolean;
begin
  fBaseWinAppData.WinAppRecord.AppCmdWnd := CreateCommandWndW(@WndProcW_NetServerApp, 'NetServerConsoleWindow');
  Result := IsWindow(fBaseWinAppData.WinAppRecord.AppCmdWnd);
  //Result := CreateCommandWindow(@fSrvAppData.AppWindow.CommandWindow, @AppWndProcA, 'dealAgentServerWindow');
  if not Result then
    exit;
  fSrvAppData.NetMgr := TNetMgr.Create(Self);
end;

procedure TNetServerApp.Run;
begin
  //AppStartProc := DealAgentServerAppStart.WMAppStart;
  //PostMessage(fSrvAppData.AppWindow.CommandWindow.WindowHandle, WM_AppStart, 0, 0);
  PostMessage(fBaseWinAppData.WinAppRecord.AppCmdWnd, WM_AppStart, 0, 0);
  RunAppMsgLoop;
end;

end.
