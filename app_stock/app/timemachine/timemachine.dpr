program timemachine;

uses
  Forms,
  DB_Time in '..\..\data\DB_Time.pas',
  BaseDataSet in '..\..\..\devwintech\v0001\app_base\BaseDataSet.pas',
  BaseApp in '..\..\..\devwintech\v0001\app_base\BaseApp.pas',
  BasePath in '..\..\..\devwintech\v0001\app_base\BasePath.pas',
  define_dealstore_file in '..\..\define\define_dealstore_file.pas',
  define_timemachine in '..\..\define\define_timemachine.pas',
  QuickList_TimeEventItem in '..\..\utils\QuickList_TimeEventItem.pas',
  QuickSortList in '..\..\Utils\QuickSortList.pas',
  define_datetime in '..\..\define\define_datetime.pas',
  StockTime in '..\..\time\StockTime.pas',
  TimeMaster in '..\..\time\TimeMaster.pas',
  timemachineform in 'timemachineform.pas' {frmTimeMachine};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmTimeMachine, frmTimeMachine);
  Application.Run;
end.
