unit timemachineform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmTimeMachine = class(TForm)
    btn1: TButton;
    mmo1: TMemo;
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTimeMachine: TfrmTimeMachine;

implementation

{$R *.dfm}

uses
  StockTime;
  
procedure TfrmTimeMachine.btn1Click(Sender: TObject);
var
  tmpStockTime: TStockTime;
  
  procedure CheckDayOff(ADay: Word);
  begin
    if tmpStockTime.IsDayOff(ADay) then
    begin
      mmo1.Lines.Add(FormatDateTime('yyyy/mm/dd', ADay) + ' is day off');
    end else
    begin
      mmo1.Lines.Add(FormatDateTime('yyyy/mm/dd', ADay) + ' is work day');
    end;
  end;
          
  procedure CheckDealDay(ADay: Word);
  begin
    if tmpStockTime.IsDealDay(ADay) then
    begin
      mmo1.Lines.Add(FormatDateTime('yyyy/mm/dd', ADay) + ' is deal day');
    end else
    begin
      mmo1.Lines.Add(FormatDateTime('yyyy/mm/dd', ADay) + ' is not deal day');
    end;
  end;
  
begin
  tmpStockTime := TStockTime.Create(nil);
  try
    CheckDealDay(Trunc(EncodeDate(2017, 2, 4)));
    CheckDealDay(Trunc(EncodeDate(2017, 2, 5)));
    CheckDealDay(Trunc(EncodeDate(2017, 2, 6)));
    CheckDealDay(Trunc(EncodeDate(2017, 2, 7)));
  finally
    tmpStockTime.Free;
  end;
end;

end.
