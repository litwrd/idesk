unit zsAgentApp;

interface

uses
  Windows, Messages, Sysutils,
  BaseApp, BaseWinApp, define_zsprocess;
  
type
  TZSAgentAppData   = record
    ZsProcess       : TZsProcess;
    ZsMainWnd       : TZsWndMain;
  end;

  TzsAgentApp = class(TBaseWinApp)
  protected
    fZSAgentAppData: TZSAgentAppData;     
    function CreateAppCommandWindow: Boolean;
    procedure Test;
  public
    function Initialize: Boolean; override;
    procedure Finalize; override;
    procedure Run; override;
  end;

var
  GlobalApp: TzsAgentApp = nil;

implementation

uses
  UtilsWindows,
  windef_msg,
  define_stockapp,
  define_message,
  //UtilsLog,
  utils_zs_deal,
  utils_zs_dealquery,
  utils_zs_dialog,
  utils_zs_main,
  utils_zs_login,
  utils_zs_export,
  utils_zsprocess;
           
{$IFNDEF RELEASE}  
const
  LOGTAG = 'zsAgentApp.pas'; 
{$ENDIF}
    
{ TzsAgentApp }                           
function TzsAgentApp.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin
    Result := CheckSingleInstance(AppMutexName_StockDeal_Agent_ZSZQ);
    if Result then
    begin
      FillChar(fZSAgentAppData, SizeOf(fZSAgentAppData), 0);
      fZSAgentAppData.ZsProcess.LoginAccountId := 1808175166;
      //Application.Initialize;
    end;
  end;
end;

procedure TzsAgentApp.Finalize;
begin
  inherited;
end;

procedure TzsAgentApp.Test;
var
  tmpMainWnd: TZsWndMain;   
  tmpLoginWnd: TZsWndLogin;
  tmpMainDealWnd: TZSWndMainDeal;    
  tmpWndDialogs: TZsWndDialogs;
begin
  FillChar(tmpLoginWnd, SizeOf(tmpLoginWnd), 0);                           
  if FindZSLoginWnd(@fZSAgentAppData.ZsProcess, @tmpLoginWnd) then
  begin
    AutoLogin(@fZSAgentAppData.ZsProcess, @tmpLoginWnd, 204077);
  end;
  FillChar(tmpMainWnd, SizeOf(tmpMainWnd), 0);
  if FindZSMainWnd(@fZSAgentAppData.ZsProcess, @tmpMainWnd) then
  begin
    CloseZsDialog(@fZSAgentAppData.ZsProcess, nil);
    TraverseCheckMainChildWindowA(tmpMainWnd.Core.WindowHandle, @tmpMainWnd);
    if IsWindow(tmpMainWnd.Core.WindowHandle) then
    begin
      if IsWindow(tmpMainWnd.WndFunctionTree) then
      begin
        ForceBringFrontWindow(tmpMainWnd.Core.WindowHandle);
        //ClickFunctionTreeBuyNode(@tmpMainWnd);
        //ClickFunctionTreeSaleNode(@tmpMainWnd);
        //ClickFunctionTreeCancelNode(@tmpMainWnd);
        //ClickFunctionTreeQueryNode(@tmpMainWnd);
        ClickFunctionTreeQueryDealNode(@tmpMainWnd);
        Exit;

        FillChar(tmpMainDealWnd, SizeOf(tmpMainDealWnd), 0);
        TraverseCheckMainDealPanelChildWindowA(tmpMainWnd.WndDealPanel, @tmpMainWnd, @tmpMainDealWnd);
        if IsWindow(tmpMainDealWnd.WndAccountCombo) then
        begin
          //InputEditWnd(tmpMainDealWnd.WndStockCodeEdit, '002414');   
          InputEditWnd(tmpMainDealWnd.WndStockCodeEdit, '601199');
          SleepWait(20);
          //InputEditWnd(tmpMainDealWnd.WndPriceEdit, FormatFloat('0.00', 25.1));
          InputEditWnd(tmpMainDealWnd.WndPriceEdit, FormatFloat('0.00', 8.5));
          SleepWait(20);
          InputEditWnd(tmpMainDealWnd.WndNumEdit, IntToStr(100));
          SleepWait(20);
          ClickButtonWnd(tmpMainDealWnd.WndOrderButton);


          FillChar(tmpWndDialogs, SizeOf(tmpWndDialogs), 0);
          if FindZSDialogWnd(@fZSAgentAppData.ZsProcess, @tmpWndDialogs, 0) then
          begin
            if 1 = tmpWndDialogs.WndCount then
            begin
              ForceBringFrontWindow(tmpWndDialogs.WndArray[0].Core.WindowHandle);
              ClickButtonWnd(tmpWndDialogs.WndArray[0].OKButton);
            end;
          end;
          Sleep(100);
          while CloseZsDialog(@fZSAgentAppData.ZsProcess, 0) do
          begin
            Sleep(100);
          end;
        end;
      end;
    end;
  end;
end;
                      
function BusinessCommandWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM; var AResult: LRESULT): Boolean;
begin
  Result := true;
  case AMsg of
    WM_Deal_Request_Buy: begin 
      {$IFDEF CONSOLE}
      Writeln('Request_Deal_Buy:' + IntToStr(wParam) + '/' + FormatFloat('0.00', LoWord(lParam) / 100) + '/' + IntToStr(HiWord(lParam)));
      {$ENDIF}
      CallBuy(@GlobalApp.fZSAgentAppData.ZsProcess,
        @GlobalApp.fZSAgentAppData.ZsMainWnd,
        wParam,
        LoWord(lParam) / 100,
        HiWord(lParam));
    end;               
    WM_Deal_Request_Sale: begin  
      {$IFDEF CONSOLE}
      Writeln('Request_Deal_Sale' + IntToStr(wParam) + '/' + FormatFloat('0.00', LoWord(lParam) / 100) + '/' + IntToStr(HiWord(lParam)));
      {$ENDIF}  
      CallSale(@GlobalApp.fZSAgentAppData.ZsProcess,
        @GlobalApp.fZSAgentAppData.ZsMainWnd,
        wParam,
        LoWord(lParam) / 100,
        HiWord(lParam));
    end;
    WM_Deal_Request_Cancel: begin 
      {$IFDEF CONSOLE}
      Writeln('Request_Deal_Cancel' + IntToStr(wParam) + '/' + IntToStr(lParam));
      {$ENDIF}  
    end;
    WM_Export_Data: begin        
      {$IFDEF CONSOLE}
      Writeln('Request_Export_Data');
      {$ENDIF}  
      //Log(LOGTAG, 'WM_Data_Export');
      utils_zs_export.CallExport_QuoteData(
        @GlobalApp.fZSAgentAppData.ZsProcess,
        @GlobalApp.fZSAgentAppData.ZsMainWnd,
        wParam,
        lparam);
    end;   
    WM_Export_FirstListInfo: begin     
      {$IFDEF CONSOLE}
      Writeln('Request_Export_FirstList');
      {$ENDIF}  
      utils_zs_export.CallExport_FirstListInfo(
        @GlobalApp.fZSAgentAppData.ZsProcess,
        @GlobalApp.fZSAgentAppData.ZsMainWnd);
    end;
    WM_Export_InfoF10_1: begin     
      {$IFDEF CONSOLE}
      Writeln('Request_Export_InfoF10_1');
      {$ENDIF}  
      utils_zs_export.CallExport_InfoF10_1(
        @GlobalApp.fZSAgentAppData.ZsProcess,
        @GlobalApp.fZSAgentAppData.ZsMainWnd,
        wParam);
    end;
    WM_Export_SetConfig: begin     
      {$IFDEF CONSOLE}
      Writeln('Request_Export_Config');
      {$ENDIF}  
      //Log(LOGTAG, 'WM_Data_Config:' + IntToStr(wParam) + '/' + IntToStr(lParam));    
      if Config_Export_DownClick = wParam then
      begin
        Global_Export_DownClickCount := lParam;   
      end;
      AResult := 1;
    end;
    else
      Result := false;
  end;
end;

function AppCommandWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := 0;
  case AMsg of
    WM_AppStart: begin      
    end;
    WM_AppRequestEnd: begin
    end;
    WM_AppNotifyShutdownMachine: begin
    end;
    else
      if BusinessCommandWndProcA(AWnd, AMsg, wParam, lParam, Result) then
        exit;
  end;        
  Result := DefWindowProcA(AWnd, AMsg, wParam, lParam);
end;
                
function TzsAgentApp.CreateAppCommandWindow: Boolean;
var
  tmpRegWinClass: TWndClassA;  
  tmpGetWinClass: TWndClassA;
  tmpIsReged: Boolean;
begin
  Result := false;          

  ChangeWindowMessageFilter(WM_COPYDATA, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_AppRequestEnd, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_AppNotifyShutdownMachine, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Export_SetConfig, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Export_Data, MSGFLT_ADD);   
  ChangeWindowMessageFilter(WM_Export_FirstListInfo, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Export_InfoF10_1, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Deal_Request_Buy, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Deal_Request_Sale, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_Deal_Request_Cancel, MSGFLT_ADD);

  FillChar(tmpRegWinClass, SizeOf(tmpRegWinClass), 0);
  tmpRegWinClass.hInstance := HInstance;
  tmpRegWinClass.lpfnWndProc := @AppCommandWndProcA;
  tmpRegWinClass.lpszClassName := define_stockapp.AppCmdWndClassName_StockDealAgent_ZSZQ;
  tmpIsReged := GetClassInfoA(HInstance, tmpRegWinClass.lpszClassName, tmpGetWinClass);
  if tmpIsReged then
  begin
    if (tmpGetWinClass.lpfnWndProc <> tmpRegWinClass.lpfnWndProc) then
    begin                           
      UnregisterClassA(tmpRegWinClass.lpszClassName, HInstance);
      tmpIsReged := false;
    end;
  end;
  if not tmpIsReged then
  begin
    if 0 = RegisterClassA(tmpRegWinClass) then
      exit;
  end;

  //TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow := CreateWindowExA(
  Self.AppWindow := CreateWindowExA(
    WS_EX_TOOLWINDOW
    //or WS_EX_APPWINDOW
    //or WS_EX_TOPMOST
    ,
    tmpRegWinClass.lpszClassName,
    '', WS_POPUP {+ 0},
    0, 0, 0, 0,
    HWND_MESSAGE, 0, HInstance, nil);
  Result := Windows.IsWindow(Self.AppWindow);
  if Result then
  begin
    //Log(LOGTAG, 'CreateAppCommandWindow Succ');                  
  end else
  begin
    //Log(LOGTAG, 'CreateAppCommandWindow Fail');  
  end;
end;
                                 
procedure TzsAgentApp.Run;
var
  tmpProcessHandle: THandle;
begin          
  inherited;
  if FindZSProcess(@fZSAgentAppData.ZsProcess) then
  begin                            
    tmpProcessHandle := Windows.OpenProcess(PROCESS_TERMINATE, FALSE, fZSAgentAppData.ZsProcess.Process.Core.ProcessId);
    if (0 <> tmpProcessHandle) and (INVALID_HANDLE_VALUE <> tmpProcessHandle) then
    begin
      // 发布的时候 必须 杀非管理进程 调试的时候不用 杀进程
//      Windows.TerminateProcess(tmpProcessHandle, 0);
//      fZSAgentAppData.ZsProcess.Process.Core.ProcessId := 0;
    end;
  end;         
  //if (0 <> fZSAgentAppData.ZsProcess.Process.Core.ProcessId) then
  begin
    if CreateAppCommandWindow then
    begin
      PostMessage(Self.AppWindow, WM_AppStart, 0, 0);
      RunAppMsgLoop;
    end;
  end;
end;

end.
