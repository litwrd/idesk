object frmNetClientConsole: TfrmNetClientConsole
  Left = 329
  Top = 173
  Caption = 'frmNetClientConsole'
  ClientHeight = 511
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object edtIp: TEdit
    Left = 32
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '127.0.0.1'
  end
  object edtPort: TEdit
    Left = 159
    Top = 24
    Width = 50
    Height = 21
    TabOrder = 1
    Text = '7785'
  end
  object btnConnect: TButton
    Left = 224
    Top = 22
    Width = 100
    Height = 25
    Caption = 'Connect'
    TabOrder = 2
    OnClick = btnConnectClick
  end
  object mmo1: TMemo
    Left = 32
    Top = 300
    Width = 177
    Height = 185
    Lines.Strings = (
      '127.0.0.1'
      '139.224.226.138'
      '192.168.31.212')
    TabOrder = 3
  end
  object btnSend: TButton
    Left = 224
    Top = 254
    Width = 100
    Height = 25
    Caption = 'Send'
    TabOrder = 4
    OnClick = btnSendClick
  end
  object edtSendData: TEdit
    Left = 32
    Top = 256
    Width = 177
    Height = 21
    TabOrder = 5
    Text = 'abcd'
  end
  object mmo2: TMemo
    Left = 224
    Top = 300
    Width = 177
    Height = 185
    Lines.Strings = (
      'mmo1')
    TabOrder = 6
  end
  object btnDisconnect: TButton
    Left = 330
    Top = 22
    Width = 100
    Height = 25
    Caption = 'Disconnect'
    TabOrder = 7
    OnClick = btnDisconnectClick
  end
  object btnbuy: TButton
    Left = 159
    Top = 205
    Width = 75
    Height = 25
    Caption = 'buy'
    TabOrder = 8
    OnClick = btnbuyClick
  end
  object edtstock: TEdit
    Left = 32
    Top = 153
    Width = 121
    Height = 21
    TabOrder = 9
    Text = '600000'
  end
  object edtprice: TEdit
    Left = 32
    Top = 180
    Width = 121
    Height = 21
    TabOrder = 10
    Text = '10.00'
  end
  object edtnum: TEdit
    Left = 32
    Top = 207
    Width = 121
    Height = 21
    TabOrder = 11
    Text = '1000'
  end
  object edtaccount: TEdit
    Left = 32
    Top = 126
    Width = 121
    Height = 21
    TabOrder = 12
    Text = '00001'
  end
  object btnShutdown: TButton
    Left = 330
    Top = 70
    Width = 100
    Height = 25
    Caption = 'Shutdown'
    TabOrder = 13
    OnClick = btnShutdownClick
  end
  object btnRestart: TButton
    Left = 330
    Top = 101
    Width = 100
    Height = 25
    Caption = 'Restart'
    TabOrder = 14
    OnClick = btnRestartClick
  end
  object btnLogout: TButton
    Left = 330
    Top = 132
    Width = 100
    Height = 25
    Caption = 'Logout'
    TabOrder = 15
    OnClick = btnLogoutClick
  end
end
