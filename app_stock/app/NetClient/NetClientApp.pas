unit NetClientApp;

interface

uses
  Windows,
  BaseApp,
  {$IFDEF WINFORM}
  BaseForm,
  BaseWinFormApp,      
  {$ELSE}
  BaseWinApp,          
  {$ENDIF}
  NetMgr;
  
type
  TNetClientAppData = record
    NetMgr: TNetMgr;         
    {$IFDEF WINFORM}
    ConsoleForm: TfrmBase;   
    {$ELSE}                  
    {$ENDIF}
  end;
                                   
  {$IFDEF WINFORM}
  TNetClientApp = Class(TBaseWinFormApp)    
  {$ELSE}                 
  TNetClientApp = Class(TBaseWinApp)   
  {$ENDIF}
  protected
    fAppData: TNetClientAppData;
  public
    constructor Create(AppClassId: AnsiString); override;
    destructor Destroy; override;        
    function Initialize: Boolean; override;
    procedure Finalize; override;
    procedure Run; override;     
    property NetMgr: TNetMgr read fAppData.NetMgr;
  end;

var
  GlobalApp: TNetClientApp = nil;
    
implementation

uses
  Messages, Sysutils, 
  {$IFDEF WINFORM}
  Forms,
  NetClientConsoleForm,      
  {$ENDIF}
  NetClientCommandWnd,
  windef_msg,
  win.wnd,
  NetClientAppStart;
  
{ THttpApiSrvApp }

constructor TNetClientApp.Create(AppClassId: AnsiString);
begin
  inherited;
  FillChar(fAppData, SizeOf(fAppData), 0);
end;

destructor TNetClientApp.Destroy;
begin
  inherited;
end;

procedure TNetClientApp.Finalize;
begin
  //DestroyCommandWindow(@fSrvAppData.AppWindow.CommandWindow);
  FreeAndNIl(fAppData.NetMgr);
end;
                      
function TNetClientApp.Initialize: Boolean;
begin                    
  {$IFDEF WINFORM}
  Application.Initialize;
  {$ENDIF}
  fBaseWinAppData.WinAppRecord.AppCmdWnd := win.wnd.CreateCommandWndA(@WndProcA_NetClientApp, 'NetClientCommandWnd');
  Result := IsWindow(fBaseWinAppData.WinAppRecord.AppCmdWnd);
  //Result := CreateCommandWindow(@fSrvAppData.AppWindow.CommandWindow, @AppWndProcA, 'DealAgentClientWindow');
  if not Result then
    exit;
  fAppData.NetMgr := TNetMgr.Create(Self);
end;

procedure TNetClientApp.Run;
begin
  //AppStartProc := DealAgentClientAppStart.WMAppStart;
  //PostMessage(fSrvAppData.AppWindow.CommandWindow.WindowHandle, WM_AppStart, 0, 0);
  PostMessage(fBaseWinAppData.WinAppRecord.AppCmdWnd, WM_AppStart, 0, 0);
  {$IFDEF WINFORM}
  Application.CreateForm(TfrmNetClientConsole, fAppData.ConsoleForm);
  fAppData.ConsoleForm.Initialize(Self);         
  Application.Run;
  {$ELSE}          
  RunAppMsgLoop;   
  {$ENDIF}
end;

end.
