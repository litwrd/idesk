unit NetClientConsoleForm;

interface

uses
  Windows, Forms, SysUtils,
  Classes, Controls, StdCtrls,
  BaseForm, BaseApp,
  NetBaseObj,
  NetObjClient;

type
  TfrmNetClientConsole = class(TfrmBase)
    edtIp: TEdit;
    edtPort: TEdit;
    btnConnect: TButton;
    mmo1: TMemo;
    btnSend: TButton;
    edtSendData: TEdit;
    mmo2: TMemo;
    btnDisconnect: TButton;
    btnbuy: TButton;
    edtstock: TEdit;
    edtprice: TEdit;
    edtnum: TEdit;
    edtaccount: TEdit;
    btnShutdown: TButton;
    btnRestart: TButton;
    btnLogout: TButton;
    procedure btnSendClick(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure btnDisconnectClick(Sender: TObject);
    procedure btnbuyClick(Sender: TObject);
    procedure btnShutdownClick(Sender: TObject);
    procedure btnRestartClick(Sender: TObject);
    procedure btnLogoutClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }   
    procedure Initialize(App: TBaseApp); override;  
  end;

implementation

{$R *.dfm}

uses
  NetClientIocp,
  NetObjClientProc,
  netprotocol,
  NetClientApp;

var
  Client: PNetClientIocp = nil;
  ServerAddr: TNetServerAddress;
               
procedure TfrmNetClientConsole.Initialize(App: TBaseApp);
begin
  inherited;

end;

procedure TfrmNetClientConsole.btnConnectClick(Sender: TObject);
begin
  inherited;
  if nil <> Self.App then
  begin
    if nil = Client then
    begin
      Client := TNetClientApp(Self.App).NetMgr.CheckOutNetClient;
    end;
    ServerAddr.Host := edtIp.Text;
    ServerAddr.Port := StrToIntDef(edtPort.text, 7785);
    NetClientIocp.NetClientConnect(Client, @ServerAddr);
  end;
end;

procedure TfrmNetClientConsole.btnDisconnectClick(Sender: TObject);
begin
  inherited;
  if nil <> Client then
  begin
    NetClientIocp.NetClientDisconnect(Client);
  end;
end;

var                     
  SendDataBuffer: array[0..4 * 1024 - 1] of Byte;

procedure TfrmNetClientConsole.btnbuyClick(Sender: TObject);  
var
  tmpSendLength: integer;
  tmpSendCount: integer;
  tmpNetCommand: PNetCommandBuffer;
  tmpCommandBody: PNetCommandData_Buy;
begin
  inherited;        
  if nil <> Client then
  begin
    FillChar(SendDataBuffer, SizeOf(SendDataBuffer), 0);
    tmpNetCommand := @SendDataBuffer[0];
    tmpNetCommand.Head.Magic := cNetCommandMagic;
    tmpNetCommand.Head.Version := 1001;
    tmpNetCommand.Head.CommandType := Cmd_Buy;
    tmpNetCommand.Head.CommandID := 1;

    tmpCommandBody := @tmpNetCommand.Data;
    tmpCommandBody.StockId := StrToIntDef(edtstock.Text, 0);
    tmpCommandBody.Price := Trunc(StrToFloatDef(edtprice.Text, 0) * 100);
    tmpCommandBody.Num := StrToIntDef(edtnum.Text, 0);
          
    tmpSendCount := 0;
    tmpSendLength := SizeOf(TNetCommandBuffer_Head) + SizeOf(TNetCommandData_Buy);
    NetClientIocp.NetClientSendBuf(Client, @SendDataBuffer[0], tmpSendLength, tmpSendCount);
  end;
end;

procedure TfrmNetClientConsole.btnSendClick(Sender: TObject);
var
  tmpSendLength: integer;
  tmpSendCount: integer;
  tmpAnsi: AnsiString;
  i: integer;
begin
  inherited;
  if nil <> Client then
  begin
    for i := 1 to 10 do
    begin            
      FillChar(SendDataBuffer, SizeOf(SendDataBuffer), 0);
      tmpAnsi := IntToStr(i) + Trim(edtSendData.text) + #13#10;

      tmpSendLength := Length(tmpAnsi);
      CopyMemory(@SendDataBuffer[0], @tmpAnsi[1], tmpSendLength);

      tmpSendCount := 0;
      NetClientIocp.NetClientSendBuf(Client, @SendDataBuffer[0], tmpSendLength, tmpSendCount);
      Sleep(100);
    end;
  end;
//
end;

procedure TfrmNetClientConsole.btnShutdownClick(Sender: TObject);
var
  tmpSendLength: integer;
  tmpSendCount: integer;
  tmpNetCommand: PNetCommandBuffer;
begin
  inherited;
  if nil <> Client then
  begin
    FillChar(SendDataBuffer, SizeOf(SendDataBuffer), 0);
    tmpNetCommand := @SendDataBuffer[0];
    tmpNetCommand.Head.Magic := cNetCommandMagic;
    tmpNetCommand.Head.Version := 1001;
    tmpNetCommand.Head.CommandType := Cmd_ShutDown;
    tmpNetCommand.Head.CommandID := 1;

    tmpSendCount := 0;
    tmpSendLength := SizeOf(TNetCommandBuffer_Head);
    NetClientIocp.NetClientSendBuf(Client, @SendDataBuffer[0], tmpSendLength, tmpSendCount);
  end;
end;

procedure TfrmNetClientConsole.btnRestartClick(Sender: TObject);
var
  tmpSendLength: integer;
  tmpSendCount: integer;
  tmpNetCommand: PNetCommandBuffer;
begin
  inherited;
  if nil <> Client then
  begin
    FillChar(SendDataBuffer, SizeOf(SendDataBuffer), 0);
    tmpNetCommand := @SendDataBuffer[0];
    tmpNetCommand.Head.Magic := cNetCommandMagic;
    tmpNetCommand.Head.Version := 1001;
    tmpNetCommand.Head.CommandType := Cmd_Restart;
    tmpNetCommand.Head.CommandID := 1;

    tmpSendCount := 0;
    tmpSendLength := SizeOf(TNetCommandBuffer_Head);
    NetClientIocp.NetClientSendBuf(Client, @SendDataBuffer[0], tmpSendLength, tmpSendCount);
  end;
end;

procedure TfrmNetClientConsole.btnLogoutClick(Sender: TObject);
var
  tmpSendLength: integer;
  tmpSendCount: integer;
  tmpNetCommand: PNetCommandBuffer;
begin
  inherited;
  if nil <> Client then
  begin
    FillChar(SendDataBuffer, SizeOf(SendDataBuffer), 0);
    tmpNetCommand := @SendDataBuffer[0];
    tmpNetCommand.Head.Magic := cNetCommandMagic;
    tmpNetCommand.Head.Version := 1001;
    tmpNetCommand.Head.CommandType := Cmd_Logoff;
    tmpNetCommand.Head.CommandID := 1;

    tmpSendCount := 0;
    tmpSendLength := SizeOf(TNetCommandBuffer_Head);
    NetClientIocp.NetClientSendBuf(Client, @SendDataBuffer[0], tmpSendLength, tmpSendCount);
  end;
end;

end.
