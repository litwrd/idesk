unit netprotocol;

interface

const
  cNetCommandMagic        = 7784;

  Cmd_Buy                 = 102;
  Cmd_Sale                = 103;
  Cmd_Cancel              = 104;

  Cmd_QueryHold           = 112;
  Cmd_QueryDeal           = 113;
                                
  Cmd_ShutDown            = 171;
  Cmd_Restart             = 172;
  Cmd_Logoff              = 173;
  
type
  PNetCommandBuffer_Head  = ^TNetCommandBuffer_Head;
  TNetCommandBuffer_Head  = packed record
    Magic                 : Word;
    Version               : Word;
    CommandType           : Word;
    CommandID             : Word;
  end;

  PNetCommandBuffer_Data  = ^TNetCommandBuffer_Data;
  TNetCommandBuffer_Data  = packed record
    Value                 : array[0..255] of AnsiChar;
  end;

  PNetCommandBuffer       = ^TNetCommandBuffer;       
  TNetCommandBuffer       = packed record
    Head                  : TNetCommandBuffer_Head;
    Data                  : TNetCommandBuffer_Data;
  end;
               
  PNetCommandData_Buy     = ^TNetCommandData_Buy;
  TNetCommandData_Buy     = packed record
    StockId               : Integer;
    Price                 : Integer;
    Num                   : Word;
  end;

implementation

end.
