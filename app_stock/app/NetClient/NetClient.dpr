program NetClient;

// {$APPTYPE CONSOLE}

{$IFDEF CONSOLE}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  WinSock2 in '..\..\..\..\devwintech\common\WinSock2.pas',
  windef_msg in '..\..\..\..\devwintech\v0001\windef\windef_msg.pas',
  Base.Run in '..\..\..\..\devwintech\v0001\rec\app_base\Base.Run.pas',
  Base.thread in '..\..\..\..\devwintech\v0001\rec\app_base\Base.thread.pas',
  win.app in '..\..\..\..\devwintech\v0001\rec\win_app\win.app.pas',
  win.diskfile in '..\..\..\..\devwintech\v0001\rec\win_sys\win.diskfile.pas',
  win.cpu in '..\..\..\..\devwintech\v0001\rec\win_sys\win.cpu.pas',
  win.error in '..\..\..\..\devwintech\v0001\rec\win_sys\win.error.pas',
  win.iocp in '..\..\..\..\devwintech\v0001\rec\win_sys\win.iocp.pas',
  win.thread in '..\..\..\..\devwintech\v0001\rec\win_sys\win.thread.pas',
  BaseThread in '..\..\..\devwintech\v0001\app_base\BaseThread.pas',
  BaseApp in '..\..\..\devwintech\v0001\app_base\BaseApp.pas',
  BasePath in '..\..\..\devwintech\v0001\app_base\BasePath.pas',
  BaseWinFormApp in '..\..\..\devwintech\v0001\win_app\BaseWinFormApp.pas',
  BaseWinApp in '..\..\..\devwintech\v0001\win_app\BaseWinApp.pas',
  BaseForm in '..\..\..\devwintech\v0001\win_uiform\BaseForm.pas' {frmBase},
  win.wnd in '..\..\..\..\devwintech\v0001\winproc\win.wnd.pas',
  DataChain in 'net\DataChain.pas',
  NetBase in 'net\NetBase.pas',
  NetMgr in 'net\NetMgr.pas',
  NetObjClient in 'net\NetObjClient.pas',
  NetBaseObj in 'net\NetBaseObj.pas',
  NetClientIocp in 'net\NetClientIocp.pas',
  NetObjClientProc in 'net\NetObjClientProc.pas',
  BaseDataIO in 'net\BaseDataIO.pas',
  netprotocol in 'net\netprotocol.pas',
  NetClientApp in 'NetClientApp.pas',
  NetClientAppStart in 'NetClientAppStart.pas',
  NetClientConsoleForm in 'NetClientConsoleForm.pas' {frmNetClientConsole},
  NetClientCommandWnd in 'NetClientCommandWnd.pas';

{$R *.res}

begin
  GlobalApp := TNetClientApp.Create('NetClientApp');
  try
    if GlobalApp.Initialize then
    begin
      GlobalApp.Run;
    end;
    GlobalApp.Finalize;
  finally
    GlobalApp.Free;
  end;
end.
