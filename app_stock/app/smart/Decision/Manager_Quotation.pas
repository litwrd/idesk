unit Manager_Quotation;

interface

uses             
  baseSmart;
  
type
  TQuotationManager = class(TBaseQuotationManager)
  protected  
    function GetRealQuote: TBaseRealQuote; override;
    // Day行情
    function GetDayQuote: TBaseDayQuote; override;
    // 周期汇总行情 60 分钟线 ...
    function GetPeriodQuote: TBasePeriodQuote; override;
  public
  end;
  
implementation

{ TQuotationManager }

function TQuotationManager.GetDayQuote: TBaseDayQuote;
begin
  Result := nil;
end;

function TQuotationManager.GetPeriodQuote: TBasePeriodQuote;
begin
  Result := nil;
end;

function TQuotationManager.GetRealQuote: TBaseRealQuote;
begin
  Result := nil;
end;

end.
