unit Manager_Strategy;

interface
              
uses
  SysUtils, BaseApp, baseSmart;

(*//

   策略管理器
       |====================|===========|==========|============|==========|
       | (各种关联属性)     |  权重     |  准确度  | 关联性     |          |
       |                    |           |          | (大盘???)  |          |
       |====================|===========|==========|============|==========|
       |CYHT 策略           |           |          |            |          |
       |        60分钟线    |           |          |            |          |
       |        日线        |           |          |            |          |
       |        周线        |           |          |            |          |
       |        月线        |           |          |            |          |
       |BDZX 策略           |           |          |            |          |
       |        60分钟线    |           |          |            |          |
       |        日线        |           |          |            |          |
       |        周线        |           |          |            |          |
       |        月线        |           |          |            |          |
       |BOLL 策略           |           |          |            |          |
       |        60分钟线    |           |          |            |          |
       |        日线        |           |          |            |          |
       |        周线        |           |          |            |          |
       |        月线        |           |          |            |          |
       |1272 策略           |           |          |            |          |
       |        60分钟线    |           |          |            |          |
       |        日线        |           |          |            |          |
       |===================================================================|
               
//*)
type
  PStrategyAttrib = ^TStrategyAttrib;
  TStrategyAttrib = record
    Attrib        : TBaseStrategyAttribDefine;
  end;

  TStrategyManagerData = record  
    // 宏观指数策略
    Strategy_Macro_IndexData: TBaseInvestStrategy;//TStrategy_Macro_IndexData;
  end;

  TStrategyManager = class(TBaseStrategyManager)
  protected
    fStrategyManagerData: TStrategyManagerData;
  public
    constructor Create(App: TBaseApp); override;
    destructor Destroy; override;  
    function Initialize: Boolean; override;
    procedure Finalize; override;      
    property Strategy_Macro_Index: TBaseInvestStrategy read fStrategyManagerData.Strategy_Macro_IndexData;//TStrategy_Macro_IndexData;
  end;

implementation

uses
  Strategy_Macro_Index,
  Strategy_Time,            
  //Strategy_1272,
  //Strategy_BDZX,
  Strategy_FundsAllocation,
  Strategy_AmountRate,
  Strategy_BOLL,
  Strategy_CYHT
  ;
  
{ TStrategyManager }

constructor TStrategyManager.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStrategyManagerData, SizeOf(fStrategyManagerData), 0);
end;

destructor TStrategyManager.Destroy;
begin
  Finalize;
  inherited;
end;

function TStrategyManager.Initialize: Boolean;
begin
  Result := True;
  if nil = fStrategyManagerData.Strategy_Macro_IndexData then
  begin
    // 宏观指数策略
    fStrategyManagerData.Strategy_Macro_IndexData := TStrategy_Macro_IndexData.Create(App);
    fStrategyManagerData.Strategy_Macro_IndexData.Initialize;
  end;
end;

procedure TStrategyManager.Finalize;
begin
  inherited;
  if nil <> fStrategyManagerData.Strategy_Macro_IndexData then
    FreeAndNil(fStrategyManagerData.Strategy_Macro_IndexData);
end;

end.
