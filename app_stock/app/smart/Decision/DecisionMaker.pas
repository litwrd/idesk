unit DecisionMaker;

interface

uses
  Sysutils,
  BaseApp,
  baseSmart,
  DB_Quote_Instant_Refresh;
  
(*//
  ������
//*)

type
  TDecisionMakerData = record
    AssetManager: TBaseAssetManager;
    BusiManager: TBaseBusinessManager;
    DealManager : TBaseDealManager;
    DealObjectManager: TBaseDealObjectMgr;
    PublicInfoManager: TBasePublicInfoManager;
    QuotationManager: TBaseQuotationManager;
    StrategyManager: TBaseStrategyManager;

    {$IFDEF DEALINSTANT}
    Quote_InstantDB_Refresh: TDB_Quote_Instant_Refresh;
    {$ENDIF}
  end;
  
  TDecisionMaker = class(TBaseDecisionMaker)
  protected
    fDecisionMakerData: TDecisionMakerData; 
    function GetBusiManager: TBaseBusinessManager; override;
    function GetAssetManager: TBaseAssetManager; override;
    function GetStrategyManager: TBaseStrategyManager; override;
    function GetPublicInfoManager: TBasePublicInfoManager; override;
    function GetQuotationMgr: TBaseQuotationManager; override;

    procedure SetAssetManager(const Value: TBaseAssetManager); override;
    procedure SetStrategyManager(const Value: TBaseStrategyManager); override;
  public
    constructor Create(App: TBaseApp); override;
    destructor Destroy; override;   
    function Initialize: Boolean; override;
    procedure Run; override;
  end;
  
implementation

uses
  BaseStockFormApp,
  Manager_Asset,
  Manager_Busi,
  Manager_Deal,
  Manager_DealObject,
  Manager_PubInfo,
  Manager_Quotation,
  Manager_Strategy;
  
{ TDecisionMaker }

constructor TDecisionMaker.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fDecisionMakerData, SizeOf(fDecisionMakerData), 0);
end;

destructor TDecisionMaker.Destroy;
begin
  FreeAndNil(fDecisionMakerData.AssetManager);  
  FreeAndNil(fDecisionMakerData.BusiManager);
  FreeAndNil(fDecisionMakerData.DealManager);
  FreeAndNil(fDecisionMakerData.PublicInfoManager);
  FreeAndNil(fDecisionMakerData.QuotationManager);
  FreeAndNil(fDecisionMakerData.StrategyManager);
  inherited;
end;
           
function TDecisionMaker.Initialize: Boolean;
begin               
  Result := inherited Initialize;
  if Result then
  begin
    fDecisionMakerData.AssetManager := TAssetManager.Create(App);
    fDecisionMakerData.AssetManager.Initialize;
    
    fDecisionMakerData.BusiManager := TBusinessManager.Create(App);
    fDecisionMakerData.BusiManager.Initialize;
    
    fDecisionMakerData.DealManager := TDealManager.Create(App);
    fDecisionMakerData.DealManager.Initialize;

    fDecisionMakerData.PublicInfoManager := TPublicInfoManager.Create(App);
    fDecisionMakerData.PublicInfoManager.Initialize;

    fDecisionMakerData.QuotationManager := TQuotationManager.Create(App);
    fDecisionMakerData.QuotationManager.Initialize;

    fDecisionMakerData.StrategyManager := TStrategyManager.Create(App);
    fDecisionMakerData.StrategyManager.Initialize;
    
    fDecisionMakerData.DealObjectManager := TDealObjectManager.Create(App);
    fDecisionMakerData.DealObjectManager.Initialize;
  end;
end;

function TDecisionMaker.GetAssetManager: TBaseAssetManager;
begin
  Result := fDecisionMakerData.AssetManager;
end;

function TDecisionMaker.GetBusiManager: TBaseBusinessManager;
begin
  Result := fDecisionMakerData.BusiManager;
end;

function TDecisionMaker.GetPublicInfoManager: TBasePublicInfoManager;
begin
  Result := fDecisionMakerData.PublicInfoManager;
end;

function TDecisionMaker.GetQuotationMgr: TBaseQuotationManager;
begin
  Result := fDecisionMakerData.QuotationManager;
end;

function TDecisionMaker.GetStrategyManager: TBaseStrategyManager;
begin
  Result := fDecisionMakerData.StrategyManager;
end;

procedure TDecisionMaker.SetAssetManager(const Value: TBaseAssetManager);
begin
  fDecisionMakerData.AssetManager := Value;
end;

procedure TDecisionMaker.SetStrategyManager(const Value: TBaseStrategyManager);
begin
  fDecisionMakerData.StrategyManager := Value;
end;

procedure TDecisionMaker.Run;
begin
  inherited;
  TStrategyManager(fDecisionMakerData.StrategyManager).Strategy_Macro_Index.Run;
  {$IFDEF DEALINSTANT}
  fDecisionMakerData.Quote_InstantDB_Refresh.QuoteInstantDB := TBaseStockApp(App).QuoteInstantDB;   
  ThreadRunDBQuoteInstantRefresh(@fDecisionMakerData.Quote_InstantDB_Refresh);
  {$ENDIF}
end;

end.
