unit smartStockConsoleForm;

interface

uses
  Windows, Forms, BaseForm, BaseFrame, Classes, Controls,
  StdCtrls, Sysutils, ExtCtrls, Dialogs, VirtualTrees,
  define_dealitem, define_dealmarket, define_datasrc, define_price, define_datetime,
  smartStockConsoleApp, DealItemsTreeView, StockDayDataAccess,
  SDFrameDealItemViewer,
  ComCtrls, Tabs;

type
  TFormSDConsoleData = record
    AppStartTimer: TTimer;       
    DealItemTree: TDealItemTreeCtrl;
    FrameDealItemViewer: TfmeDealItemViewer;
    FrameDealConsole: TfmeBase;
    FrameGraph: TfmeBase;
    FrameGraphM: TfmeBase;
    FrameTest: TfmeBase;
//    FrameDataViewer: TfmeDataViewer;
//    FrameDataRepair: TfmeDataRepair;
//    FrameStockClass: TfmeStockClass;
//    FrameStockInfo: TfmeStockInfo;
//    FrameTDXData: TfmeTDXData;
//    FrameStockInstant: TfmeStockInstant;
  end;

  TfrmSDConsole = class(TfrmBase)
    pnlBottom: TPanel;
    pnlTop: TPanel;
    pnlMain: TPanel;
    pnlLeft: TPanel;
    splLeft: TSplitter;
    tsfunc: TTabSet;
    lblSmartConsole: TLabel;
    btnSmarterRun: TButton;
    procedure tsfuncChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btnSmarterRunClick(Sender: TObject);
  private
    { Private declarations }
    fFormSDConsoleData: TFormSDConsoleData;
    procedure tmrAppStartTimer(Sender: TObject);     
    procedure DoNotifyDealItemChange(ADealItem: PRT_DealItem);
    procedure DoNotifyDealItemDblClick(ADealItem: PRT_DealItem);
  protected
    procedure FormCreate(Sender: TObject);
  public
    constructor Create(Owner: TComponent); override;
  end;

implementation

{$R *.dfm}

uses            
  define_stock_quotes,
  define_dealstore_file,
  windef_msg,
  win.shutdown,   
  win.iobuffer,
  ShellAPI,
  UtilsHttp,
  //UtilsLog,
  SDFrameBaseDealItem,
  BaseStockFormApp,
  SDFrameDealConsole,
  SDFrameGraphM,
  SDFrameGraph,
  SDFrameTest,
  StockDayData_Load,
  BaseWinApp,
  smartStockApp,
  db_dealitem_load;

constructor TfrmSDConsole.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fFormSDConsoleData, SizeOf(fFormSDConsoleData), 0);   
  fFormSDConsoleData.AppStartTimer := TTimer.Create(Application);
  fFormSDConsoleData.AppStartTimer.Interval := 100;
  fFormSDConsoleData.AppStartTimer.OnTimer := tmrAppStartTimer;
  fFormSDConsoleData.AppStartTimer.Enabled := true;
  Self.OnCreate := FormCreate;
end;
                          
procedure TfrmSDConsole.FormCreate(Sender: TObject);
begin
  DragAcceptFiles(Handle, True) ;
//
end;
     
procedure TfrmSDConsole.tmrAppStartTimer(Sender: TObject);
begin
  if nil <> Sender then
  begin
    if Sender is TTimer then
    begin
      TTimer(Sender).Enabled := false;
      TTimer(Sender).OnTimer := nil;
    end;
    if Sender = fFormSDConsoleData.AppStartTimer then
    begin
      fFormSDConsoleData.AppStartTimer.Enabled := false;
      fFormSDConsoleData.AppStartTimer.OnTimer := nil;
    end;
  end;
  // initialize
  fFormSDConsoleData.FrameDealItemViewer := TfmeDealItemViewer.Create(Self);
  fFormSDConsoleData.FrameDealItemViewer.App := Self.App;
  fFormSDConsoleData.FrameDealItemViewer.Parent := Self.pnlLeft;
  fFormSDConsoleData.FrameDealItemViewer.Align := alClient;
  fFormSDConsoleData.FrameDealItemViewer.Visible := True;
  fFormSDConsoleData.FrameDealItemViewer.Initialize;
  fFormSDConsoleData.FrameDealItemViewer.OnDealItemChange := DoNotifyDealItemChange;
  fFormSDConsoleData.FrameDealItemViewer.OnDealItemDblClick := DoNotifyDealItemDblClick;
  
  tsfunc.Tabs.Clear;
                                                   
  fFormSDConsoleData.FrameTest := TfmeTest.Create(Self);
  fFormSDConsoleData.FrameTest.App := Self.App;
  fFormSDConsoleData.FrameTest.Initialize;
  tsfunc.Tabs.AddObject('Test', fFormSDConsoleData.FrameTest);

  fFormSDConsoleData.FrameDealConsole := TfmeDealConsole.Create(Self);
  fFormSDConsoleData.FrameDealConsole.App := Self.App;
  fFormSDConsoleData.FrameDealConsole.Initialize;
  tsfunc.Tabs.AddObject('Deal', fFormSDConsoleData.FrameDealConsole);
                   
  fFormSDConsoleData.FrameGraph := TfmeStockGraph.Create(Self);
  fFormSDConsoleData.FrameGraph.App := Self.App;
  fFormSDConsoleData.FrameGraph.Initialize;
  tsfunc.Tabs.AddObject('Graph', fFormSDConsoleData.FrameGraph);
                               
  fFormSDConsoleData.FrameGraphM := TfmeStockGraphM.Create(Self);
  fFormSDConsoleData.FrameGraphM.App := Self.App;
  fFormSDConsoleData.FrameGraphM.Initialize;
  tsfunc.Tabs.AddObject('GraphM', fFormSDConsoleData.FrameGraphM);

//  fFormSDConsoleData.FrameDataViewer := TfmeDataViewer.Create(Self);
//  fFormSDConsoleData.FrameDataViewer.App := Self.App;
//  fFormSDConsoleData.FrameDataViewer.Initialize;
//  tsfunc.Tabs.AddObject('DayDataView', fFormSDConsoleData.FrameDataViewer);
  if 0 < tsfunc.Tabs.Count then
    tsfunc.TabIndex := 0;
end;

procedure TfrmSDConsole.tsfuncChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
var
  tmpFrame: TfmeBase;  
begin
  inherited;
  if 0 <= tsfunc.TabIndex then
  begin
    tmpFrame := TfmeBase(tsfunc.Tabs.Objects[tsfunc.TabIndex]);
    tmpFrame.CallDeactivate;
    tmpFrame.Visible := false;
  end;
  tmpFrame := TfmeBase(tsfunc.Tabs.Objects[NewTab]);            
  tmpFrame.Parent := Self.pnlMain;
  tmpFrame.Align := alClient;
  tmpFrame.Visible := true;
  tmpFrame.CallActivate;   
  if tmpFrame is TfmeBaseStockItem then
  begin
    TfmeBaseStockItem(tmpFrame).NotifyDealItem(fFormSDConsoleData.FrameDealItemViewer.FocusDealItem);
    exit;
  end;
end;

procedure TfrmSDConsole.DoNotifyDealItemDblClick(ADealItem: PRT_DealItem);    
var
  tmpFrame: TfmeBase;
begin
  if 0 <= tsfunc.TabIndex then
  begin
    tmpFrame := TfmeBase(tsfunc.Tabs.Objects[tsfunc.TabIndex]);
    if tmpFrame = fFormSDConsoleData.FrameDealConsole then
    begin
      TfmeDealConsole(fFormSDConsoleData.FrameDealConsole).NotifyDealItemDblClick(ADealItem);
      exit;
    end;
  end;
end;

procedure TfrmSDConsole.DoNotifyDealItemChange(ADealItem: PRT_DealItem);
var
  tmpFrame: TfmeBase;
begin
  if 0 <= tsfunc.TabIndex then
  begin
    tmpFrame := TfmeBase(tsfunc.Tabs.Objects[tsfunc.TabIndex]);
    if tmpFrame is TfmeBaseStockItem then
    begin
      TfmeBaseStockItem(tmpFrame).NotifyDealItem(ADealItem);
      exit;
    end; 
  end;
end;

procedure TfrmSDConsole.btnSmarterRunClick(Sender: TObject);
begin
  inherited;
  TStockSmartApp(App).DecisionMaker.Run;
end;

end.
