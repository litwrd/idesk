unit smartStockApp;

interface

uses
  define_stockapp,  
  define_dealItem,

  BaseApp,
  //BaseStockApp,
  baseSmart,
  BaseStockFormApp,  
  smartStockConsoleApp;

type
  TStockSmartAppData = record
    AppAgent: TBaseAppAgent;
    DecisionMaker: TBaseDecisionMaker;    
    //ConsoleAppData: TConsoleAppData;
    //DownloaderAppData: TDownloaderAppData;
  end;
  
  TStockSmartApp = class(TBaseStockApp)
  protected
    fStockSmartAppData: TStockSmartAppData;
  public
    constructor Create(AppClassId: AnsiString); override;
    procedure Run; override;   
    function Initialize: Boolean; override;
    procedure Finalize; override;
    
    property DecisionMaker: TBaseDecisionMaker read fStockSmartAppData.DecisionMaker;
  end;

implementation

uses
  Windows,
  Sysutils,
  Classes,     
  DecisionMaker,
  db_dealitem,
  UtilsLog,
  DB_dealItem_Load;
              
{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDataApp.pas';
{$ENDIF}

{ TStockDataApp }

constructor TStockSmartApp.Create(AppClassId: AnsiString);
begin
  inherited;
  FillChar(fStockSmartAppData, SizeOf(fStockSmartAppData), 0);
  fStockSmartAppData.DecisionMaker := TDecisionMaker.Create(Self);   
  fStockSmartAppData.DecisionMaker.Initialize;
end;

function TStockSmartApp.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin
    Result := CheckSingleInstance(AppMutexName_StockSmarter_Console);
    //Result := false;
    if Result then
    begin
      InitializeDBStockItem;
      Result := 0 < Self.StockItemDB.RecordCount;
      if Result then
      begin
        {$IFDEF DEALINSTANT}
        InitializeDBQuoteInstant(Self.StockItemDB);
        {$ENDIF}        
        fStockSmartAppData.AppAgent := TStockDataConsoleApp.Create(Self);
        Result := fStockSmartAppData.AppAgent.Initialize;
      end;
    end;
  end;
end;

procedure TStockSmartApp.Finalize;
begin
  //Log(LOGTAG, 'TStockDataApp.Finalize');
  if nil <> fStockSmartAppData.DecisionMaker then
  begin
    fStockSmartAppData.DecisionMaker.Finalize;
    FreeAndNil(fStockSmartAppData.DecisionMaker);
  end;
  if nil <> fStockSmartAppData.AppAgent then
  begin
    fStockSmartAppData.AppAgent.Finalize;
    FreeAndNil(fStockSmartAppData.AppAgent);
  end;
end;
            
(*//
function AppCommandWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := 0;
  case AMsg of
    WM_AppStart: begin
      if nil <> GlobalBaseStockApp then
      begin
        TStockDataApp(GlobalBaseStockApp).RunStart;
      end;
      exit;
    end;
    WM_AppRequestEnd: begin    
      TStockDataApp(GlobalBaseStockApp).Terminate;
    end;
    WM_Downloader2Console_Command_DownloadOK: begin    
      TStockDataApp(GlobalBaseStockApp).Console_Notify_DownloadOK(wParam);
    end;
    WM_Console2Downloader_Command_Download: begin
      PostMessage(AWnd, WM_Downloader_Command_Download, wParam, 0)
    end;
    WM_Downloader_Command_Download: begin
      TStockDataApp(GlobalBaseStockApp).Downloader_Download(wParam);
    end;
  end;
  Result := DefWindowProcA(AWnd, AMsg, wParam, lParam);
end;
//*)
(*//
function TStockSmartApp.CreateAppCommandWindow: Boolean;
var
  tmpRegWinClass: TWndClassA;  
  tmpGetWinClass: TWndClassA;
  tmpIsReged: Boolean;
begin
  Result := false;          
  if runMode_Undefine = fStockSmartAppData.RunMode then
    exit;
  FillChar(tmpRegWinClass, SizeOf(tmpRegWinClass), 0);
  tmpRegWinClass.hInstance := HInstance;
  tmpRegWinClass.lpfnWndProc := @AppCommandWndProcA;
  if runMode_Console = fStockSmartAppData.RunMode then
  begin
    tmpRegWinClass.lpszClassName := AppCmdWndClassName_StockDataDownloader_Console;
  end;
  if runMode_DataDownloader = fStockSmartAppData.RunMode then
  begin
    tmpRegWinClass.lpszClassName := AppCmdWndClassName_StockDataDownloader;
  end;
  tmpIsReged := GetClassInfoA(HInstance, tmpRegWinClass.lpszClassName, tmpGetWinClass);
  if tmpIsReged then
  begin
    if (tmpGetWinClass.lpfnWndProc <> tmpRegWinClass.lpfnWndProc) then
    begin                           
      UnregisterClassA(tmpRegWinClass.lpszClassName, HInstance);
      tmpIsReged := false;
    end;
  end;
  if not tmpIsReged then
  begin
    if 0 = RegisterClassA(tmpRegWinClass) then
      exit;
  end;
  fBaseWinAppData.AppCmdWnd := CreateWindowExA(
    WS_EX_TOOLWINDOW
    //or WS_EX_APPWINDOW
    //or WS_EX_TOPMOST
    ,
    tmpRegWinClass.lpszClassName,
    '', WS_POPUP {+ 0},
    0, 0, 0, 0,
    HWND_MESSAGE, 0, HInstance, nil);
  Result := Windows.IsWindow(fBaseWinAppData.AppCmdWnd);
end;
//*)
(*//                                        
                                   
//*)   
(*//
procedure TStockSmartApp.RunStart_Console(AConsoleApp: PConsoleAppData);
begin
  // run downloader process
  //if Console_CheckDownloaderProcess(AConsoleApp) then
  begin
    AConsoleApp.Download_DealItemIndex := 0;
    //Console_NotifyDownloadData(AConsoleApp);
  end;      
end;  
//*)  
(*//
//*)   
(*//
        if 0 = tmpDealItem.FirstDealDate then
        begin
          tmpIsNeedSaveStockItemDB := true;
        end;
        tmpDealItem.IsDataChange := 0;
        if GetStockDataDay_163(Self, tmpDealItem, false, @tmpHttpClientSession) then
        begin
          Sleep(2000);
        end;                                      
        if 0 <> tmpDealItem.IsDataChange then
        begin
          tmpIsNeedSaveStockItemDB := true;
        end;
      end;
    end;     
    if tmpIsNeedSaveStockItemDB then
    begin
      SaveDBStockItem(Self, Self.StockItemDB);
    end;
//*)
     
(*//                        
procedure TStockSmartApp.RunStart;
begin
  case fStockSmartAppData.RunMode of
    //runMode_Console: RunStart_Console(@fStockSmartAppData.ConsoleAppData);
    runMode_DataDownloader: RunStart_Downloader;
  end;
end;
//*)  

procedure TStockSmartApp.Run;
begin
  fStockSmartAppData.AppAgent.Run;
end;

end.
