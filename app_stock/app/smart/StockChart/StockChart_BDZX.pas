unit StockChart_BDZX;

interface
                 
uses
  StockChart,
  RuleEx_BDZX,
  GR32, SysUtils;

type                
  PChartData_BDZX   = ^TChartData_BDZX;
  TChartData_BDZX   = record
    IsInited        : Boolean;
    Prev_X          : integer;
    Prev_Y_AJ       : integer;
    Prev_Y_AK       : integer;
    Prev_Y_AD1      : integer;
    Rule_Data       : TRule_BDZX_Price;
  end;
  
  TStockChart_BDZX = class(TBaseStockChart)
  protected
    fChart_Data: TChartData_BDZX;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure DrawChartItem(AChartPriceItem: PChartPriceItem); override;  
    property Rule_Data: TRule_BDZX_Price read fChart_Data.Rule_Data write fChart_Data.Rule_Data;
  end;
  
implementation

procedure DrawChart_Item_Rule_BDZX(AChartPriceItem: PChartPriceItem; AChartData: PChartData_BDZX; ACoord: TChartCoord);
var                  
  tmpY: Integer;
  tmpY_AJ: Integer;
  tmpY_AK: Integer;
  tmpY_AD1: Integer;    
begin
  if nil = AChartData.Rule_Data then
    exit;
  if not AChartData.IsInited then
  begin
    AChartData.IsInited := true;
    AChartData.Prev_X := 0;
    tmpY := ACoord.Position(100);  
    AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartPriceItem.ChartPrice.Rect_PriceChart.Left,
            tmpY,
            AChartPriceItem.ChartPrice.Rect_PriceChart.Right,
            tmpY,
            clGreen32);
    tmpY := ACoord.Position(0);  
    AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartPriceItem.ChartPrice.Rect_PriceChart.Left,
            tmpY,
            AChartPriceItem.ChartPrice.Rect_PriceChart.Right,
            tmpY,
            clWhite32);
  end;
  tmpY_AJ := ACoord.Position(Round(AChartData.Rule_Data.Ret_AJ.Value[AChartPriceItem.ItemIndex]));
  tmpY_AK := ACoord.Position(Round(AChartData.Rule_Data.Ret_AK_Float.Value[AChartPriceItem.ItemIndex]));
  tmpY_AD1 := ACoord.Position(Round(AChartData.Rule_Data.Ret_AD1_EMA.ValueF[AChartPriceItem.ItemIndex]));  
  if 0 = AChartData.Prev_X then
  begin
    AChartData.Prev_X := AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf;
  end else
  begin             
    if IsValidChartPointY(AChartData.Prev_Y_AK, ACoord.Top, ACoord.Bottom) and
       IsValidChartPointY(tmpY_AK, ACoord.Top, ACoord.Bottom) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_AK,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmpY_AK,
            clWhite32);
    end;       
    if IsValidChartPointY(AChartData.Prev_Y_AD1, ACoord.Top, ACoord.Bottom) and
       IsValidChartPointY(tmpY_AD1, ACoord.Top, ACoord.Bottom) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_AD1,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmpY_AD1,
            clYellow32);
    end;
    if IsValidChartPointY(AChartData.Prev_Y_AJ, ACoord.Top, ACoord.Bottom) and
       IsValidChartPointY(tmpY_AJ, ACoord.Top, ACoord.Bottom) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_AJ,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmpY_AJ,
            clFuchsia32);
    end;
    AChartData.Prev_X := AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf;
  end;
  AChartData.Prev_Y_AK := tmpY_AK;
  AChartData.Prev_Y_AJ := tmpY_AJ;
  AChartData.Prev_Y_AD1 := tmpY_AD1;  
end;

{ TStockChart_BDZX }

constructor TStockChart_BDZX.Create;
begin
  inherited;
  FillChar(fChart_Data, SizeOf(fChart_Data), 0);
  fBaseChartData.ChartCoord := TChartCoord.Create;  
end;

destructor TStockChart_BDZX.Destroy;
begin
  if nil <> fBaseChartData.ChartCoord then
    FreeAndNil(fBaseChartData.ChartCoord);
  inherited;
end;

procedure TStockChart_BDZX.DrawChartItem(AChartPriceItem: PChartPriceItem);
begin
  DrawChart_Item_Rule_BDZX(AChartPriceItem, @fChart_Data, fBaseChartData.ChartCoord);
end;

end.
