unit StockChart_Price;

interface

uses
  GR32,
  StockChart,
  StockDayDataAccess,
  StockMinuteDataAccess,
  define_stock_quotes;
  
  procedure DrawChart_Item_Price_Day(AChartItemData: PChartPriceItem; ADayData: TStockDayDataAccess);
  procedure DrawChart_Item_Price_Minute(AChartItemData: PChartPriceItem; AMinuteData: TStockMinuteDataAccess);

implementation

procedure DrawChart_Item_Price_Day(AChartItemData: PChartPriceItem; ADayData: TStockDayDataAccess);
var  
  tmpData: PRT_Quote_Day;  
  tmpY: integer;
begin
  tmpData := ADayData.DayDataByIndex(AChartItemData.ItemIndex);
  if tmpData.PriceRange.PriceOpen.Value = tmpData.PriceRange.PriceClose.Value then
  begin       
    AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceOpen.Value);
    AChartItemData.ItemRect.Bottom := AChartItemData.ItemRect.Top + 1;
    AChartItemData.ItemColor := clWhite32;
  end else
  begin
    if tmpData.PriceRange.PriceOpen.Value < tmpData.PriceRange.PriceClose.Value then
    begin
      AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceClose.Value);
      AChartItemData.ItemRect.Bottom := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceOpen.Value);
      AChartItemData.ItemColor := Def_Price_GoUp_Color;
      //tmpItemColor := clRed32;
    end else
    begin
      AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceOpen.Value);
      AChartItemData.ItemRect.Bottom := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceClose.Value);
      AChartItemData.ItemColor := Def_Price_GoDown_Color;
      //tmpItemColor := clGreen32;            
    end;
  end;               
  if tmpData.PriceRange.PriceOpen.Value < tmpData.PriceRange.PriceClose.Value then
  begin
    AChartItemData.ChartPrice.BackgroundBitmap.FrameRectS(
        AChartItemData.ItemRect.Left,
        AChartItemData.ItemRect.Top,
        AChartItemData.ItemRect.Right,
        AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemColor);
    tmpY := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceHigh.Value);
    AChartItemData.ChartPrice.BackgroundBitmap.Line(
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, tmpY,
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, AChartItemData.ItemRect.Top,
        AChartItemData.ItemColor);
    tmpY := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceLow.Value);
    AChartItemData.ChartPrice.BackgroundBitmap.Line(
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, tmpY,
        AChartItemData.ItemColor);

    //tmpRect_Item.Top := GetCoordPosition(tmpRect_PriceChart.Top, tmpRect_PriceChart.Bottom, tmpMaxPriceHigh, tmpMinPriceLow, tmpData.PriceRange.PriceHigh.Value);
    //tmpRect_Item.Bottom := GetCoordPosition(tmpRect_PriceChart.Top, tmpRect_PriceChart.Bottom, tmpMaxPriceHigh, tmpMinPriceLow, tmpData.PriceRange.PriceLow.Value);
    //ABitmap.Line(tmpRect_Item.Left + tmpChartItemWidthHalf, tmpRect_Item.Top, tmpRect_Item.Left + tmpChartItemWidthHalf, tmpRect_Item.Bottom, tmpItemColor);
  end else
  begin
    AChartItemData.ChartPrice.BackgroundBitmap.FillRect(
        AChartItemData.ItemRect.Left, AChartItemData.ItemRect.Top,
        AChartItemData.ItemRect.Right, AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemColor);
    AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceHigh.Value);
    AChartItemData.ItemRect.Bottom := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceLow.Value);
    AChartItemData.ChartPrice.BackgroundBitmap.Line(
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf,
        AChartItemData.ItemRect.Top,
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf,
        AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemColor);
  end;
end;

procedure DrawChart_Item_Price_Minute(AChartItemData: PChartPriceItem; AMinuteData: TStockMinuteDataAccess);
var  
  tmpData: PRT_Quote_Minute;  
  tmpY: integer;
begin
  tmpData := AMinuteData.MinuteDataByIndex(AChartItemData.ItemIndex);
  if tmpData.PriceRange.PriceOpen.Value = tmpData.PriceRange.PriceClose.Value then
  begin       
    AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceOpen.Value);
    AChartItemData.ItemRect.Bottom := AChartItemData.ItemRect.Top + 1;
    AChartItemData.ItemColor := clWhite32;
  end else
  begin
    if tmpData.PriceRange.PriceOpen.Value < tmpData.PriceRange.PriceClose.Value then
    begin
      AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceClose.Value);
      AChartItemData.ItemRect.Bottom := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceOpen.Value);
      AChartItemData.ItemColor := Def_Price_GoUp_Color;
      //tmpItemColor := clRed32;
    end else
    begin
      AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceOpen.Value);
      AChartItemData.ItemRect.Bottom := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceClose.Value);
      AChartItemData.ItemColor := Def_Price_GoDown_Color;
      //tmpItemColor := clGreen32;            
    end;
  end;               
  if tmpData.PriceRange.PriceOpen.Value < tmpData.PriceRange.PriceClose.Value then
  begin
    AChartItemData.ChartPrice.BackgroundBitmap.FrameRectS(
        AChartItemData.ItemRect.Left,
        AChartItemData.ItemRect.Top,
        AChartItemData.ItemRect.Right,
        AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemColor);
    tmpY := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceHigh.Value);
    AChartItemData.ChartPrice.BackgroundBitmap.Line(
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, tmpY,
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, AChartItemData.ItemRect.Top,
        AChartItemData.ItemColor);
    tmpY := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceLow.Value);
    AChartItemData.ChartPrice.BackgroundBitmap.Line(
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf, tmpY,
        AChartItemData.ItemColor);

    //tmpRect_Item.Top := GetCoordPosition(tmpRect_PriceChart.Top, tmpRect_PriceChart.Bottom, tmpMaxPriceHigh, tmpMinPriceLow, tmpData.PriceRange.PriceHigh.Value);
    //tmpRect_Item.Bottom := GetCoordPosition(tmpRect_PriceChart.Top, tmpRect_PriceChart.Bottom, tmpMaxPriceHigh, tmpMinPriceLow, tmpData.PriceRange.PriceLow.Value);
    //ABitmap.Line(tmpRect_Item.Left + tmpChartItemWidthHalf, tmpRect_Item.Top, tmpRect_Item.Left + tmpChartItemWidthHalf, tmpRect_Item.Bottom, tmpItemColor);
  end else
  begin
    AChartItemData.ChartPrice.BackgroundBitmap.FillRect(
        AChartItemData.ItemRect.Left, AChartItemData.ItemRect.Top,
        AChartItemData.ItemRect.Right, AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemColor);
    AChartItemData.ItemRect.Top := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceHigh.Value);
    AChartItemData.ItemRect.Bottom := AChartItemData.ChartPrice.StockChartCoord.Position(tmpData.PriceRange.PriceLow.Value);
    AChartItemData.ChartPrice.BackgroundBitmap.Line(
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf,
        AChartItemData.ItemRect.Top,
        AChartItemData.ItemRect.Left + AChartItemData.ChartPrice.ChartItemWidthHalf,
        AChartItemData.ItemRect.Bottom,
        AChartItemData.ItemColor);
  end;
end;

end.
