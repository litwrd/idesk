unit StockChart_CYHT;

interface
             
uses
  StockChart,
  RuleEx_CYHT,
  Sysutils;

type                
  PChartData_CYHT   = ^TChartData_CYHT;
  TChartData_CYHT   = record
    IsInited        : Boolean;
    Prev_X          : integer;
    Prev_Y_SK       : integer;
    Prev_Y_SD       : integer;
    Rule_Data       : TRule_CYHT_Price;
  end;
                              
  TStockChart_CYHT = class(TBaseStockChart)
  protected
    fChart_Data: TChartData_CYHT;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure BeginDraw; override;
    procedure DrawChartItem(AChartPriceItem: PChartPriceItem); override;
    property Rule_Data: TRule_CYHT_Price read fChart_Data.Rule_Data write fChart_Data.Rule_Data;
  end;
  
implementation

uses
  GR32;
  
(*//
  ���� 80 ���� 20
//*)

procedure DrawChart_Item_Rule_Cyht(AChartPriceItem: PChartPriceItem; AChartData: PChartData_CYHT; ACoord: TChartCoord);
var                  
  tmpY: Integer;
  tmpY_SK: Integer;
  tmpY_SD: Integer;
begin
  if nil = AChartData.Rule_Data then
    exit;
  if not AChartData.IsInited then
  begin                   
    AChartData.IsInited := true;
    AChartData.Prev_X := 0;
    tmpY := ACoord.Position(80);  
    AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartPriceItem.ChartPrice.Rect_PriceChart.Left,
            tmpY,
            AChartPriceItem.ChartPrice.Rect_PriceChart.Right,
            tmpY,
            clWhite32);    
    tmpY := ACoord.Position(20);  
    AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartPriceItem.ChartPrice.Rect_PriceChart.Left,
            tmpY,
            AChartPriceItem.ChartPrice.Rect_PriceChart.Right,
            tmpY,
            clGreen32);
  end;
  tmpY_SK := ACoord.Position(Round(AChartData.Rule_Data.SK[AChartPriceItem.ItemIndex]));
  tmpY_SD := ACoord.Position(Round(AChartData.Rule_Data.SD[AChartPriceItem.ItemIndex]));
  if 0 = AChartData.Prev_X then
  begin
    AChartData.Prev_X := AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf;
  end else
  begin
    if IsValidChartPointY(AChartData.Prev_Y_SK, ACoord.Top, ACoord.Bottom) and
       IsValidChartPointY(tmpY_SK, ACoord.Top, ACoord.Bottom) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_SK,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmpY_SK,
            clYellow32);
    end;
    if IsValidChartPointY(AChartData.Prev_Y_SD, ACoord.Top, ACoord.Bottom) and
       IsValidChartPointY(tmpY_SD, ACoord.Top, ACoord.Bottom) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_SD,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmpY_SD,
            clFuchsia32);
    end;
    AChartData.Prev_X := AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf;
  end;
  AChartData.Prev_Y_SD := tmpY_SD;
  AChartData.Prev_Y_SK := tmpY_SK;
end;

{ TStockChart_CYHT }

constructor TStockChart_CYHT.Create;
begin
  inherited;
  FillChar(fChart_Data, SizeOf(fChart_Data), 0);
  fBaseChartData.ChartCoord := TChartCoord.Create;  
end;

destructor TStockChart_CYHT.Destroy;
begin
  if nil <> fBaseChartData.ChartCoord then
    FreeAndNil(fBaseChartData.ChartCoord);
  inherited;
end;
                            
procedure TStockChart_CYHT.BeginDraw;
begin
  fChart_Data.IsInited := false;
  fChart_Data.Prev_X := 0;
end;

procedure TStockChart_CYHT.DrawChartItem(AChartPriceItem: PChartPriceItem);
begin
  DrawChart_Item_Rule_CYHT(AChartPriceItem, @fChart_Data, fBaseChartData.ChartCoord);
end;

end.
