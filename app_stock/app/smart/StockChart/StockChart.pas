unit StockChart;

interface

uses
  GR32;
  
type            
  TChartCoordRec  = record
    Top           : integer;
    Bottom        : integer;
    MaxValue      : double;
    MinValue      : double;
    IsInitValue   : Boolean;
  end;

  {
    普通坐标系
        等差坐标  -- (默认)   arithmetic progression 
        等比坐标              Geometric progression  Geometric coordinates
        等分坐标              Uniform coordinates
        百分比坐标            普通坐标[KCNORMAL] 百分比坐标[KCPERCENT] 蜡烛图[CANDLE]
        黄金分割坐标          The golden coordinates
    对数坐标系                semi-logarithmic coordinate system
  }
  TChartCoord     = class
  protected
    fCoordData: TChartCoordRec;  
  public
    constructor Create;
    function Position(AValue: double): integer; virtual;
    property Top: integer read fCoordData.Top write fCoordData.Top;
    property Bottom: integer read fCoordData.Bottom write fCoordData.Bottom;
    property MaxValue: double read fCoordData.MaxValue write fCoordData.MaxValue;
    property MinValue: double read fCoordData.MinValue write fCoordData.MinValue;
    property IsInitValue: Boolean read fCoordData.IsInitValue write fCoordData.IsInitValue;
  end;
            
  // 对数坐标系
  TChartCoord_Log = class(TChartCoord)
  protected 
  public   
    function Position(AValue: double): integer; override;
  end;
       
  // 等比
  TChartCoord_Normal_Geometric = class(TChartCoord)
  protected 
  public   
    function Position(AValue: double): integer; override;
  end;
       
  // 等分
  TChartCoord_Normal_Uniform = class(TChartCoord)
  protected 
  public   
    function Position(AValue: double): integer; override;
  end;
        
  // 黄金分割
  TChartCoord_Normal_Golden = class(TChartCoord)
  protected 
  public   
    function Position(AValue: double): integer; override;
  end;
  
  PChartPriceItem     = ^TChartPriceItem;
  PStockChartPrice    = ^TStockChartPrice;
  PStockChartRec      = ^TStockChartRec;
  
  TChartPriceItem     = record
    ChartPrice        : PStockChartPrice;
    ItemIndex         : integer;
    ItemRect          : TRect;
    ItemColor         : TColor32;
  end;

  TStockChartPrice    = record
    Rect_Chart        : TRect;
    Rect_PriceChart   : TRect;
    BackgroundBitmap  : TBitmap32;
    ChartItemWidth    : integer;
    ChartItemWidthHalf: integer;
    ChartItemGap      : integer;
    ChartItemCount    : integer;
    StockChartCoord   : TChartCoord;
    MouseDownStatus   : Byte;
    MouseDownPoint    : TPoint;
    RT_ChartItem      : TChartPriceItem;
  end;
            
  TStockChartRec      = record
    Rect_Chart        : TRect;
    ChartCoord        : TChartCoord; 
  end;

  TBaseStockChart     = class
  protected
    fBaseChartData    : TStockChartRec;
  public
    constructor Create; virtual;
    procedure BeginDraw; virtual;
    procedure DrawChartItem(AChartPriceItem: PChartPriceItem); virtual;    
    property ChartCoord: TChartCoord read fBaseChartData.ChartCoord;
  end;
  
const
  Def_Chart_Bg_Color: TColor32 = clBlack32;
  // 上涨
  Def_Price_GoUp_Color : TColor32 = $FFFF3232;
  Def_Price_GoDown_Color : TColor32 = $FF54FFFF;
  
  //function GetCoordPosition(ATop, ABottom: integer; AMax, AMin, AValue: double): integer;
                               
  function IsValidChartPointY(Y: integer; AChart: PStockChartPrice): Boolean; overload;
  function IsValidChartPointY(Y: integer; ARect: TRect): Boolean; overload;
  function IsValidChartPointY(Y: integer; ATop, ABottom: Integer): Boolean; overload;
    
implementation

function IsValidChartPointY(Y: integer; ATop, ABottom: Integer): Boolean;
begin
  Result := (Y >= ATop) and (Y <= ABottom);
end;

function IsValidChartPointY(Y: integer; ARect: TRect): Boolean;
begin
  Result := IsValidChartPointY(Y, ARect.Top, ARect.Bottom);
end;

function IsValidChartPointY(Y: integer; AChart: PStockChartPrice): Boolean;
begin
  Result := IsValidChartPointY(Y, AChart.Rect_PriceChart);
end;

function GetCoordPosition(ATop, ABottom: integer; AMax, AMin, AValue: double): integer;
begin
  Result := ABottom - Round(((AValue - AMin) * (ABottom - ATop)) / (AMax - AMin));
end;

{ TStockChartCoord }

constructor TChartCoord.Create;
begin
  FillChar(fCoordData, SizeOf(fCoordData), 0);
end;

function TChartCoord.Position(AValue: double): integer;
begin
  Result := GetCoordPosition(fCoordData.Top, fCoordData.Bottom, fCoordData.MaxValue, fCoordData.MinValue, AValue);
end;

{ TBaseStockChart }

constructor TBaseStockChart.Create;
begin
  FillChar(fBaseChartData, SizeOf(fBaseChartData), 0);
end;

procedure TBaseStockChart.DrawChartItem(AChartPriceItem: PChartPriceItem);
begin
end;

procedure TBaseStockChart.BeginDraw;
begin
end;

{ TChartCoord_Geometric }

function TChartCoord_Normal_Geometric.Position(AValue: double): integer;
begin
  Result := 0;
end;

{ TChartCoord_Uniform }

function TChartCoord_Normal_Uniform.Position(AValue: double): integer;
begin
  Result := 0;
end;

{ TChartCoord_Golden }

function TChartCoord_Normal_Golden.Position(AValue: double): integer;
begin
  Result := 0;
end;

{ TChartCoord_Log }

function TChartCoord_Log.Position(AValue: double): integer;
begin
  Result := 0;
end;

end.
