unit StockChart_1272;

interface
            
uses
  StockChart,
  GR32,
  Rule_MA;

type                
  PChartData_1272   = ^TChartData_1272;
  TChartData_1272   = record
    Prev_X     : integer;
    Prev_Y_12  : Integer;
    Prev_Y_72  : Integer;
    Rule_MA_12: TRule_MA_F;
    Rule_MA_72: TRule_MA_F;
  end;

  procedure DrawChart_Item_Price_1272(AChartPriceItem: PChartPriceItem; AChartData: PChartData_1272);
  
implementation
                
procedure DrawChart_Item_Price_1272(AChartPriceItem: PChartPriceItem; AChartData: PChartData_1272);
var
  tmp_Y_12: integer;
  tmp_Y_72: integer;
begin                     
  if nil = AChartData.Rule_MA_12 then
    exit;
  if nil = AChartData.Rule_MA_72 then
    exit;
  if 0 = AChartData.Prev_X then
  begin
    AChartData.Prev_X := AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf;
    AChartData.Prev_Y_12 := AChartPriceItem.ChartPrice.StockChartCoord.Position(Round(AChartData.Rule_MA_12.ValueF[AChartPriceItem.ItemIndex]));
    AChartData.Prev_Y_72 := AChartPriceItem.ChartPrice.StockChartCoord.Position(Round(AChartData.Rule_MA_72.ValueF[AChartPriceItem.ItemIndex]));
  end else
  begin
    tmp_Y_12 := AChartPriceItem.ChartPrice.StockChartCoord.Position(Round(AChartData.Rule_MA_12.ValueF[AChartPriceItem.ItemIndex]));
    if IsValidChartPointY(AChartData.Prev_Y_12, AChartPriceItem.ChartPrice) and
       IsValidChartPointY(tmp_Y_12, AChartPriceItem.ChartPrice) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_12,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmp_Y_12,
            clYellow32);
    end;
    
    tmp_Y_72 := AChartPriceItem.ChartPrice.StockChartCoord.Position(Round(AChartData.Rule_MA_72.ValueF[AChartPriceItem.ItemIndex]));
    if IsValidChartPointY(AChartData.Prev_Y_72, AChartPriceItem.ChartPrice) and
       IsValidChartPointY(tmp_Y_72, AChartPriceItem.ChartPrice) then
    begin
      AChartPriceItem.ChartPrice.BackgroundBitmap.Line(
            AChartData.Prev_X, AChartData.Prev_Y_72,
            AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf, tmp_Y_72,
            clFuchsia32);
    end;      
    AChartData.Prev_X := AChartPriceItem.ItemRect.Left + AChartPriceItem.ChartPrice.ChartItemWidthHalf;
    AChartData.Prev_Y_12 := tmp_Y_12;
    AChartData.Prev_Y_72 := tmp_Y_72;
  end;
end;

end.
