object frmSDConsole: TfrmSDConsole
  Left = 161
  Top = 170
  Caption = 'frmSDConsole'
  ClientHeight = 571
  ClientWidth = 1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object splLeft: TSplitter
    Left = 380
    Top = 2
    Height = 509
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 511
    Width = 1081
    Height = 60
    Align = alBottom
    object lblSmartConsole: TLabel
      Left = 16
      Top = 6
      Width = 69
      Height = 13
      Caption = 'Smart Console'
    end
    object btnSmarterRun: TButton
      Left = 104
      Top = 6
      Width = 89
      Height = 49
      Caption = 'Smarter Run'
      OnClick = btnSmarterRunClick
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 1081
    Height = 2
    Align = alTop
  end
  object pnlLeft: TPanel
    Left = 0
    Top = 2
    Width = 380
    Height = 509
    Align = alLeft
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
  end
  object pnlMain: TPanel
    Left = 383
    Top = 2
    Width = 698
    Height = 509
    Align = alClient
    object tsfunc: TTabSet
      Left = 1
      Top = 487
      Width = 696
      Height = 21
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      OnChange = tsfuncChange
    end
  end
end
