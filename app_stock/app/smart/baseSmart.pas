unit baseSmart;

interface

uses
  BaseApp,
  define_dealitem,
  BaseDataSet;
               
(*//
  策略   TBaseStrategy
  决策   TBaseDecisionMaker
  资产管理 TBaseAssetManager
//*)
  
type
  TBaseSmartObject = class(TBaseAppObj)
  protected
  public
    function Initialize: Boolean; virtual;
    procedure Finalize; virtual;
  end;

  // 对策略属性的 定义
  // 一个属性的值 可能有多个 ???
  TBaseStrategyAttribDefine = class
  protected
    AttribName: string; // 权重   准确度  关联性
  public
  end;
  
  // 策略
  //     实时策略  ???
  //     盘后分析策略 ???
  TBaseStrategy = class(TBaseSmartObject)
  protected
  public
    procedure Run; virtual;
  end;

  // 投资策略
  TBaseInvestStrategyData = record
    DealItem: PRT_DealItem;
    DealItemDB: TBaseDataSetAccess;
    DayData: TBaseDataSetAccess;
    MinuteData: TBaseDataSetAccess;
  end;
  
  TBaseInvestStrategy = class(TBaseStrategy)
  protected
    fInvestStrategyData: TBaseInvestStrategyData;  
    procedure SetDayData(const Value: TBaseDataSetAccess); virtual;   
    procedure SetMinuteData(const Value: TBaseDataSetAccess); virtual;
    procedure SetDealItem(const Value: PRT_DealItem); virtual;
    procedure SetDealItemDB(const Value: TBaseDataSetAccess); virtual;
  public          
    constructor Create(App: TBaseApp); override; 
    property DealItem: PRT_DealItem read fInvestStrategyData.DealItem write SetDealItem;
    property DealItemDB: TBaseDataSetAccess read fInvestStrategyData.DealItemDB write SetDealItemDB;
    property DayData: TBaseDataSetAccess read fInvestStrategyData.DayData write SetDayData;
    property MinuteData: TBaseDataSetAccess read fInvestStrategyData.MinuteData write SetMinuteData;
  end; 

  // 策略管理
  TBaseStrategyManager = class(TBaseSmartObject)
  protected
  public
  end;

  // 资产
  TBaseAssetClass = class(TBaseSmartObject)
  protected
  public
  end;
  
  // 资产管理
  TBaseAssetManager = class(TBaseSmartObject)
  protected
  public
  end;

  TBaseBusinessEventManager = class(TBaseSmartObject)
  protected
  public
  end;

  // 经济架构
  //     -- 行业 产业 比重 人口 ... 地域 气候
  TBaseBusinessFrameWork = class
  protected
  public
  end;
  
  // 经济框架管理
  TBaseBusinessManager = class(TBaseSmartObject)
  protected
    function GetEventMgr: TBaseBusinessEventManager; virtual; // 动态
    function GetFrameWork: TBaseBusinessFrameWork; virtual;// 静态
  public
    property EventMgr: TBaseBusinessEventManager read GetEventMgr; // 动态
    property FrameWork: TBaseBusinessFrameWork read GetFrameWork; // 静态
  end;

  TBasePublicInfoSource = class(TBaseSmartObject)
  protected
  public
  end;
  
  TBasePublicInfoManager = class(TBaseSmartObject)
  protected
  public
  end;

  TBaseRealQuote = class(TBaseSmartObject)
  protected
  public
  end;

  TBaseDayQuote = class(TBaseSmartObject)
  protected
  public
  end;

  TBasePeriodQuote = class(TBaseSmartObject)
  protected
  public
  end;

  TBaseQuotationManager = class(TBaseSmartObject)
  protected
    function GetRealQuote: TBaseRealQuote; virtual;
    // Day行情
    function GetDayQuote: TBaseDayQuote; virtual;
    // 周期汇总行情 60 分钟线 ...
    function GetPeriodQuote: TBasePeriodQuote; virtual;
  public
    // 实时行情
    property RealQuote: TBaseRealQuote read GetRealQuote;
    // Day行情
    property DayQuote: TBaseDayQuote read GetDayQuote;
    // 周期汇总行情 60 分钟线 ...
    property PeriodQuote: TBasePeriodQuote read GetPeriodQuote;
  end;

  TBaseDealHistory = class(TBaseSmartObject)
  protected
  public
  end;

  TBaseDealAgent = class(TBaseSmartObject)
  protected
  public
  end;
  
  TBaseDealManager = class(TBaseSmartObject)
  protected
    function GetDealHistory: TBaseDealHistory; virtual;
    function GetDealAgent: TBaseDealAgent; virtual;
  public
    property DealHistory: TBaseDealHistory read GetDealHistory;
    property DealAgent: TBaseDealAgent read GetDealAgent;
  end;

  TBaseDealObjectMgr = class(TBaseSmartObject)
  protected
  public
    // StockItem 股票项目
    // IndexItem 指数
    // 基金
  end;
  
  // 决策
  //     决策系统由时间驱动
  
  TBaseDecisionMaker = class(TBaseSmartObject)
  protected
    function GetAssetManager: TBaseAssetManager; virtual;
    function GetBusiManager: TBaseBusinessManager; virtual;
    function GetDealMgr: TBaseDealManager; virtual;
    function GetDealObjectMgr: TBaseDealObjectMgr; virtual;
    function GetPublicInfoManager: TBasePublicInfoManager; virtual;
    function GetQuotationMgr: TBaseQuotationManager; virtual;
    function GetStrategyManager: TBaseStrategyManager; virtual;
    
    procedure SetAssetManager(const Value: TBaseAssetManager); virtual;
    procedure SetStrategyManager(const Value: TBaseStrategyManager); virtual;
  public
    procedure Run; virtual;

    // 资产管理器
    property AssetMgr: TBaseAssetManager read GetAssetManager write SetAssetManager;
    // 经济
    property BusiMgr: TBaseBusinessManager read GetBusiManager;
    // 交易管理
    property DealMgr: TBaseDealManager read GetDealMgr;
    // 股票
    property DealObjectMgr: TBaseDealObjectMgr read GetDealObjectMgr;
    // 公共信息管理  雪球 ...
    property PublicInfoMgr: TBasePublicInfoManager read GetPublicInfoManager;
    // 股票行情走势 stock quotations tendency
    // 交易行情数据管理
    property QuotationMgr: TBaseQuotationManager read GetQuotationMgr;
    // 策略管理器
    property StrategyMgr: TBaseStrategyManager read GetStrategyManager write SetStrategyManager;
  end;

implementation

{ TBaseDecisionMaker }

function TBaseDecisionMaker.GetAssetManager: TBaseAssetManager;
begin
  Result := nil;
end;

function TBaseDecisionMaker.GetBusiManager: TBaseBusinessManager;
begin
  Result := nil;
end;

function TBaseDecisionMaker.GetDealMgr: TBaseDealManager;
begin
  Result := nil;
end;

function TBaseDecisionMaker.GetDealObjectMgr: TBaseDealObjectMgr; 
begin
  Result := nil;
end;

function TBaseDecisionMaker.GetPublicInfoManager: TBasePublicInfoManager;
begin
  Result := nil;
end;

function TBaseDecisionMaker.GetQuotationMgr: TBaseQuotationManager;
begin
  Result := nil;
end;

function TBaseDecisionMaker.GetStrategyManager: TBaseStrategyManager;
begin
  Result := nil;
end;

procedure TBaseDecisionMaker.Run;
begin
end;

procedure TBaseDecisionMaker.SetAssetManager(const Value: TBaseAssetManager);
begin             
end;

procedure TBaseDecisionMaker.SetStrategyManager(const Value: TBaseStrategyManager);
begin             
end;

{ TBaseBusinessManager }

function TBaseBusinessManager.GetEventMgr: TBaseBusinessEventManager;
begin
  Result := nil;
end;

function TBaseBusinessManager.GetFrameWork: TBaseBusinessFrameWork;
begin
  Result := nil;
end;

{ TBaseSmartObject }        
function TBaseSmartObject.Initialize: Boolean;
begin
  Result := true;
end;

procedure TBaseSmartObject.Finalize;
begin
end;

{ TBaseDealManager }

function TBaseDealManager.GetDealAgent: TBaseDealAgent;
begin
  Result := nil;
end;

function TBaseDealManager.GetDealHistory: TBaseDealHistory;
begin
  Result := nil;
end;

{ TBaseQuotationManager }

function TBaseQuotationManager.GetDayQuote: TBaseDayQuote;
begin
  Result := nil;
end;

function TBaseQuotationManager.GetPeriodQuote: TBasePeriodQuote;
begin
  Result := nil;
end;

function TBaseQuotationManager.GetRealQuote: TBaseRealQuote;
begin
  Result := nil;
end;

{ TBaseStrategy }

procedure TBaseStrategy.Run;
begin
end;

{ TBaseInvestStrategy }

constructor TBaseInvestStrategy.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fInvestStrategyData, SizeOf(fInvestStrategyData), 0);
end;

procedure TBaseInvestStrategy.SetDayData(const Value: TBaseDataSetAccess);
begin
  fInvestStrategyData.DayData := Value;
end;

procedure TBaseInvestStrategy.SetMinuteData(const Value: TBaseDataSetAccess);
begin
  fInvestStrategyData.MinuteData := Value;
end;

procedure TBaseInvestStrategy.SetDealItem(const Value: PRT_DealItem);
begin
  fInvestStrategyData.DealItem := Value;
end;

procedure TBaseInvestStrategy.SetDealItemDB(const Value: TBaseDataSetAccess);
begin
  fInvestStrategyData.DealItemDB := Value;
end;

end.
