unit Strategy_FundsAllocation;

interface

uses
  baseSmart;

(*
  // 可用资金分配策略
  //     总资产
  //         -- 市值
  //            --  被锁定的市值
  //            --  继续持仓金额
  //            --  计划回收金额
  //         -- 资金回收计划
  //            --  持仓时间 最多 5 天 (一周)
  //            --  回收比率
  //         -- 可用金额
*)

type
  // Strategy_AvailableMoneyDistribution
  TStrategy_FundsAllocation = class(TBaseInvestStrategy)
  protected
  public
  end;

implementation
         
uses
  RuleEx_Boll;
  
end.
