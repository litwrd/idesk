unit Strategy_Macro_Index;

interface

uses
  Classes, Sysutils, baseSmart, BaseApp, BaseRule,
  define_datasrc, define_price,
  StockMinuteDataAccess;

(*//
  Macroscopic scale
  宏观大盘指数策略
//*)
type
  TStrategyMacro_IndexData = record
    IndexData_SH_Min60: TStockMinuteDataAccess;
  end;
  
  TStrategy_Macro_IndexData = class(TBaseInvestStrategy)
  protected
    fStrategyData: TStrategyMacro_IndexData;
  public                              
    constructor Create(App: TBaseApp); override;
    procedure Run; override;
  end;

implementation

uses
  smartStockApp,
  StockRule,
  StockMinuteData_Load,
  define_datetime,
  define_stock_quotes,
  RuleEx_CYHT,
  RuleEx_BDZX,
  RuleEx_Boll
  //,RuleEx_1272
  ;

{ TStrategy_Macro_IndexData }
constructor TStrategy_Macro_IndexData.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStrategyData, SizeOf(fStrategyData), 0);
end;

procedure TStrategy_Macro_IndexData.Run;
var
  tmpRule: TBaseStockRule;
  i: integer;
  tmpTxt: TStringList;
  tmpStr: string;  
  tmpMinuteData: PRT_Quote_Minute;
begin
  inherited;
  if nil = App then
    exit;
  if nil = TStockSmartApp(App).StockDB.IndexItem_SH then
    exit;
  fStrategyData.IndexData_SH_Min60 := TStockMinuteDataAccess.Create(TStockSmartApp(App).StockDB.IndexItem_SH, src_tongdaxin, weightNone);    
  fStrategyData.IndexData_SH_Min60.Minute := 60;
  if LoadStockMinuteData(App, fStrategyData.IndexData_SH_Min60) then
  begin
    if 0 < fStrategyData.IndexData_SH_Min60.RecordCount then
    begin
      tmpRule := TRule_BDZX_Price.Create(dtMultiData);
      tmpRule.DataSet := fStrategyData.IndexData_SH_Min60;
      tmpRule.Execute;
      
      tmpTxt := TStringList.Create;
      try
        for i := 0 to tmpRule.DataSet.RecordCount - 1 do
        begin
          tmpMinuteData := TStockMinuteDataAccess(tmpRule.DataSet).MinuteDataByIndex(i);
          tmpStr := FormatDateTime('yyyy/mm/dd', tmpMinuteData.DealDateTime.Date.Value);
          tmpStr := tmpStr + #32 + GetTimeText(@tmpMinuteData.DealDateTime.Time);
          tmpStr := tmpStr + #32 + IntToStr(tmpMinuteData.PriceRange.PriceOpen.Value);
          tmpStr := tmpStr + ' -----AK:' + #32 + FormatFloat('0.00', TRule_BDZX_Price(tmpRule).Ret_AK_Float.Value[i]);
          tmpTxt.Add(tmpStr);
        end;
        tmpTxt.SaveToFile('e:\bdzx.txt');
      finally
        tmpTxt.Free;
      end;
      
      //tmpRule.
    end;
  end;
end;

end.
