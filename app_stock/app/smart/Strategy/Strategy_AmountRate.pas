unit Strategy_AmountRate;

interface

uses
  baseSmart;

(*
  // 实时分析策略
  // 实时放量 比率
  //     60分钟比率  本60分钟段成交金额 / 前一60分钟端成交金额
  //     日比率      及时成交金额 / 前一日成交金额
  //     3日平均成交金额比率  及时成交金额 / 前3日平均成交金额
*)

type
  TStrategy_AmountRate = class(TBaseInvestStrategy)
  protected
  public
  end;

implementation
         
uses
  RuleEx_Boll;
  
end.
