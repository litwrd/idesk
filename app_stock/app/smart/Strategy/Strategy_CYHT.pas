unit Strategy_CYHT;

interface

uses
  SysUtils,
  BaseApp,
  BaseDataSet,
  baseSmart,
  StockChart_CYHT,
  RuleEx_CYHT;
  
type    
  TStrategy_CYHT_Data = record
    Rule_Data: TRule_CYHT_Price;    
    Chart: TStockChart_CYHT;
  end;
             
  TStrategy_CYHT = class(TBaseInvestStrategy)
  protected   
    fStrategy_Data: TStrategy_CYHT_Data;                            
    function GetChart: TStockChart_CYHT;
    procedure SetDayData(const Value: TBaseDataSetAccess); override;  
    procedure SetMinuteData(const Value: TBaseDataSetAccess); override;
  public    
    constructor Create(App: TBaseApp); override;    
    destructor Destroy; override;                                
    property Chart: TStockChart_CYHT read GetChart;
    property Rule_Data: TRule_CYHT_Price read fStrategy_Data.Rule_Data;
  end;

implementation
         
uses
  StockMinuteDataAccess,
  StockDayDataAccess;
  
{ TStrategy_CYHT }

constructor TStrategy_CYHT.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStrategy_Data, SizeOf(fStrategy_Data), 0);
end;

destructor TStrategy_CYHT.Destroy;
begin
  if nil <> fStrategy_Data.Rule_Data then
    FreeAndNil(fStrategy_Data.Rule_Data);
  if nil <> fStrategy_Data.Chart then
    FreeAndNil(fStrategy_Data.Chart);  
  inherited;
end;
                            
function TStrategy_CYHT.GetChart: TStockChart_CYHT;
begin
  if nil = fStrategy_Data.Chart then
  begin
    fStrategy_Data.Chart := TStockChart_CYHT.Create;    
    fStrategy_Data.Chart.Rule_Data := fStrategy_Data.Rule_Data;
  end;
  Result := fStrategy_Data.Chart;
end;

procedure TStrategy_CYHT.SetDayData(const Value: TBaseDataSetAccess);
begin
  inherited;
  if nil <> fStrategy_Data.Rule_Data then
    FreeAndNil(fStrategy_Data.Rule_Data);
  fStrategy_Data.Rule_Data := TRule_CYHT_Price.Create;
  fStrategy_Data.Rule_Data.OnGetDataLength := TStockDayDataAccess(DayData).DoGetRecords;
  fStrategy_Data.Rule_Data.OnGetPriceOpen := TStockDayDataAccess(DayData).DoGetStockOpenPrice;  
  fStrategy_Data.Rule_Data.OnGetPriceClose := TStockDayDataAccess(DayData).DoGetStockClosePrice;
  fStrategy_Data.Rule_Data.OnGetPriceHigh := TStockDayDataAccess(DayData).DoGetStockHighPrice;
  fStrategy_Data.Rule_Data.OnGetPriceLow := TStockDayDataAccess(DayData).DoGetStockLowPrice;
  fStrategy_Data.Rule_Data.Execute;
end;

procedure TStrategy_CYHT.SetMinuteData(const Value: TBaseDataSetAccess); 
begin
  inherited;
  if nil <> fStrategy_Data.Rule_Data then
    FreeAndNil(fStrategy_Data.Rule_Data);
  fStrategy_Data.Rule_Data := TRule_CYHT_Price.Create;
  fStrategy_Data.Rule_Data.OnGetDataLength := TStockMinuteDataAccess(MinuteData).DoGetRecords;
  fStrategy_Data.Rule_Data.OnGetPriceOpen := TStockMinuteDataAccess(MinuteData).DoGetStockOpenPrice;
  fStrategy_Data.Rule_Data.OnGetPriceClose := TStockMinuteDataAccess(MinuteData).DoGetStockClosePrice;
  fStrategy_Data.Rule_Data.OnGetPriceHigh := TStockMinuteDataAccess(MinuteData).DoGetStockHighPrice;
  fStrategy_Data.Rule_Data.OnGetPriceLow := TStockMinuteDataAccess(MinuteData).DoGetStockLowPrice;
  fStrategy_Data.Rule_Data.Execute;
end;

end.
