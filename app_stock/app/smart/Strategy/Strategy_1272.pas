unit Strategy_1272;

interface

uses
  baseSmart,
  BaseApp,
  BaseDataSet,
  Rule_MA,
  SysUtils;
  
type          
  TStrategy_1272_Data = record
    Rule_MA_12: TRule_MA_F;
    Rule_MA_72: TRule_MA_F;       
  end;
              
  TStrategy_1272 = class(TBaseInvestStrategy)
  protected     
    fStrategy_1272_Data: TStrategy_1272_Data;         
    procedure SetDayData(const Value: TBaseDataSetAccess); override;
  public    
    constructor Create(App: TBaseApp); override;    
    destructor Destroy; override;
    property MA_12: TRule_MA_F read fStrategy_1272_Data.Rule_MA_12;
    property MA_72: TRule_MA_F read fStrategy_1272_Data.Rule_MA_72;
  end;

implementation

uses
  StockDayDataAccess;
  
{ TStrategy_1272 }

constructor TStrategy_1272.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStrategy_1272_Data, SizeOf(fStrategy_1272_Data), 0);
end;

destructor TStrategy_1272.Destroy;
begin
  if nil <> fStrategy_1272_Data.Rule_MA_12 then
    FreeAndNil(fStrategy_1272_Data.Rule_MA_72);
  if nil <> fStrategy_1272_Data.Rule_MA_12 then
    FreeAndNil(fStrategy_1272_Data.Rule_MA_72);
  inherited;
end;

procedure TStrategy_1272.SetDayData(const Value: TBaseDataSetAccess);
begin
  inherited;
  if nil <> fStrategy_1272_Data.Rule_MA_12 then
    FreeAndNil(fStrategy_1272_Data.Rule_MA_12);
  fStrategy_1272_Data.Rule_MA_12 := TRule_MA_F.Create;
  fStrategy_1272_Data.Rule_MA_12.ParamN := 12;
  fStrategy_1272_Data.Rule_MA_12.OnGetDataLength := TStockDayDataAccess(DayData).DoGetRecords;
  fStrategy_1272_Data.Rule_MA_12.OnGetDataF := TStockDayDataAccess(DayData).DoGetStockClosePrice;
  fStrategy_1272_Data.Rule_MA_12.Execute;
  
  if nil <> fStrategy_1272_Data.Rule_MA_72 then
    FreeAndNil(fStrategy_1272_Data.Rule_MA_72);
  fStrategy_1272_Data.Rule_MA_72 := TRule_MA_F.Create;
  fStrategy_1272_Data.Rule_MA_72.ParamN := 72;  
  fStrategy_1272_Data.Rule_MA_72.OnGetDataLength := TStockDayDataAccess(DayData).DoGetRecords;
  fStrategy_1272_Data.Rule_MA_72.OnGetDataF := TStockDayDataAccess(DayData).DoGetStockClosePrice;
  fStrategy_1272_Data.Rule_MA_72.Execute;
end;

end.
