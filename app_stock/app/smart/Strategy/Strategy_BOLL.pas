unit Strategy_BOLL;

interface

uses
  SysUtils,
  baseSmart,
  BaseApp,
  BaseDataSet,
  RuleEx_Boll;
  
type
  TStrategy_BOLL_Data = record
    Rule_Boll: TRule_Boll_Price;   
  end;
              
  TStrategy_BOLL = class(TBaseInvestStrategy)
  protected
    fStrategy_BOLL_Data: TStrategy_BOLL_Data;
    procedure SetDayData(const Value: TBaseDataSetAccess); override;    
    procedure SetMinuteData(const Value: TBaseDataSetAccess); override;
  public
    constructor Create(App: TBaseApp); override;    
    destructor Destroy; override;    
    property Rule_Boll: TRule_Boll_Price read fStrategy_BOLL_Data.Rule_Boll;   
  end;

implementation

uses
  StockMinuteDataAccess,
  StockDayDataAccess;
           
{ TStrategy_BOLL }

constructor TStrategy_BOLL.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStrategy_BOLL_Data, SizeOf(fStrategy_BOLL_Data), 0);

end;

destructor TStrategy_BOLL.Destroy;
begin
  if nil <> fStrategy_BOLL_Data.Rule_Boll then
    FreeAndNil(fStrategy_BOLL_Data.Rule_Boll);
  inherited;
end;

procedure TStrategy_BOLL.SetDayData(const Value: TBaseDataSetAccess);
begin
  inherited;              
  if nil <> fStrategy_BOLL_Data.Rule_Boll then
    FreeAndNil(fStrategy_BOLL_Data.Rule_Boll);
  fStrategy_BOLL_Data.Rule_Boll := TRule_Boll_Price.Create;
  fStrategy_BOLL_Data.Rule_Boll.OnGetDataLength := TStockDayDataAccess(DayData).DoGetRecords;
  fStrategy_BOLL_Data.Rule_Boll.OnGetDataF := TStockDayDataAccess(DayData).DoGetStockClosePrice;
  fStrategy_BOLL_Data.Rule_Boll.Execute;
end;

procedure TStrategy_BOLL.SetMinuteData(const Value: TBaseDataSetAccess);
begin
  inherited;   
  if nil <> fStrategy_BOLL_Data.Rule_Boll then
    FreeAndNil(fStrategy_BOLL_Data.Rule_Boll);
  fStrategy_BOLL_Data.Rule_Boll := TRule_Boll_Price.Create;
  fStrategy_BOLL_Data.Rule_Boll.OnGetDataLength := TStockMinuteDataAccess(Value).DoGetRecords;
  fStrategy_BOLL_Data.Rule_Boll.OnGetDataF := TStockMinuteDataAccess(Value).DoGetStockClosePrice;
  fStrategy_BOLL_Data.Rule_Boll.Execute;
end;

end.
