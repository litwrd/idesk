unit Strategy_BDZX;

interface

uses      
  SysUtils,
  BaseApp,
  BaseDataSet,
  baseSmart,
  StockChart,
  StockChart_BDZX,
  RuleEx_BDZX;
  
type                  
  TStrategy_BDZX_Data = record
    Rule_Data: TRule_BDZX_Price;
    Chart: TStockChart_BDZX;
  end;
             
  TStrategy_BDZX = class(TBaseInvestStrategy)
  protected
    fStrategy_Data: TStrategy_BDZX_Data;         
    function GetChart: TStockChart_BDZX;
    procedure SetDayData(const Value: TBaseDataSetAccess); override;   
    procedure SetMinuteData(const Value: TBaseDataSetAccess); override;
  public    
    constructor Create(App: TBaseApp); override;    
    destructor Destroy; override;
    property Rule_Data: TRule_BDZX_Price read fStrategy_Data.Rule_Data;
    property Chart: TStockChart_BDZX read GetChart;
  end;

implementation
                   
uses
  StockMinuteDataAccess,
  StockDayDataAccess;
  
{ TStrategy_BDZX }

constructor TStrategy_BDZX.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fStrategy_Data, SizeOf(fStrategy_Data), 0);
end;

destructor TStrategy_BDZX.Destroy;
begin
  if nil <> fStrategy_Data.Rule_Data then
    FreeAndNil(fStrategy_Data.Rule_Data);
  if nil <> fStrategy_Data.Chart then
    FreeAndNil(fStrategy_Data.Chart);  
  inherited;
end;

function TStrategy_BDZX.GetChart: TStockChart_BDZX;
begin
  if nil = fStrategy_Data.Chart then
  begin
    fStrategy_Data.Chart := TStockChart_BDZX.Create;    
    fStrategy_Data.Chart.Rule_Data := fStrategy_Data.Rule_Data;
  end;
  Result := fStrategy_Data.Chart;
end;

procedure TStrategy_BDZX.SetDayData(const Value: TBaseDataSetAccess);
begin
  inherited;
  if nil <> fStrategy_Data.Rule_Data then
    FreeAndNil(fStrategy_Data.Rule_Data);
  fStrategy_Data.Rule_Data := TRule_BDZX_Price.Create;
  fStrategy_Data.Rule_Data.OnGetDataLength := TStockDayDataAccess(DayData).DoGetRecords;
  fStrategy_Data.Rule_Data.OnGetPriceOpen := TStockDayDataAccess(DayData).DoGetStockOpenPrice;  
  fStrategy_Data.Rule_Data.OnGetPriceClose := TStockDayDataAccess(DayData).DoGetStockClosePrice;
  fStrategy_Data.Rule_Data.OnGetPriceHigh := TStockDayDataAccess(DayData).DoGetStockHighPrice;
  fStrategy_Data.Rule_Data.OnGetPriceLow := TStockDayDataAccess(DayData).DoGetStockLowPrice;
  fStrategy_Data.Rule_Data.Execute;

  if nil <> fStrategy_Data.Chart then
    fStrategy_Data.Chart.Rule_Data := fStrategy_Data.Rule_Data;
end;

procedure TStrategy_BDZX.SetMinuteData(const Value: TBaseDataSetAccess);
begin
  inherited;
  if nil <> fStrategy_Data.Rule_Data then
    FreeAndNil(fStrategy_Data.Rule_Data);
  fStrategy_Data.Rule_Data := TRule_BDZX_Price.Create;
  fStrategy_Data.Rule_Data.OnGetDataLength := TStockMinuteDataAccess(MinuteData).DoGetRecords;
  fStrategy_Data.Rule_Data.OnGetPriceOpen := TStockMinuteDataAccess(MinuteData).DoGetStockOpenPrice;
  fStrategy_Data.Rule_Data.OnGetPriceClose := TStockMinuteDataAccess(MinuteData).DoGetStockClosePrice;
  fStrategy_Data.Rule_Data.OnGetPriceHigh := TStockMinuteDataAccess(MinuteData).DoGetStockHighPrice;
  fStrategy_Data.Rule_Data.OnGetPriceLow := TStockMinuteDataAccess(MinuteData).DoGetStockLowPrice;
  fStrategy_Data.Rule_Data.Execute;

  if nil <> fStrategy_Data.Chart then
    fStrategy_Data.Chart.Rule_Data := fStrategy_Data.Rule_Data;
end;

end.
