unit SDFrameDealConsole;

interface

uses
  Windows, Messages, Classes, Forms, BaseFrame, StdCtrls, SDFrameBaseDealItem,
  Controls, ComCtrls, ExtCtrls, SysUtils, Dialogs, VirtualTrees,
  define_price, define_dealstore_file, define_message, define_dealitem,
  define_stockapp, define_datasrc, define_stock_quotes, define_datetime,
  StockMinuteDataAccess;

type          
  TfmeDealConsole = class(TfmeBaseStockItem)
    lblStock: TLabel;
    lblPrice: TLabel;
    lblNum: TLabel;
    btnBuy: TButton;
    edtBuyStock: TEdit;
    edtBuyPrice: TEdit;
    edtBuyNum: TEdit;
    btnSale: TButton;
    lstHold: TListBox;
    lblSalePrice: TLabel;
    lblSaleNum: TLabel;
    edtSalePrice: TEdit;
    edtSaleNum: TEdit;
    lblSaleStock: TLabel;
    edtSaleStock: TEdit;
    procedure btnBuyClick(Sender: TObject);
    procedure btnSaleClick(Sender: TObject);
  private
  public
    { Public declarations }     
    constructor Create(Owner: TComponent); override;
    procedure NotifyDealItem(ADealItem: PRT_DealItem); override;
    procedure NotifyDealItemDblClick(ADealItem: PRT_DealItem);
  end;

implementation

{$R *.dfm}

uses
  IniFiles,
  UtilsLog,
  utils_zs_agentconsole,
  BaseStockFormApp;
             
{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDFrameDealConsole.pas';
{$ENDIF}

{ TfmeDealConsole }       
constructor TfmeDealConsole.Create(Owner: TComponent);     
begin
  inherited;
end;

procedure TfmeDealConsole.NotifyDealItem(ADealItem: PRT_DealItem);
begin

end;

procedure TfmeDealConsole.NotifyDealItemDblClick(ADealItem: PRT_DealItem);
begin
  if nil <> ADealItem then
  begin
    edtBuyStock.Text := ADealItem.sCode;
  end;
end;

procedure TfmeDealConsole.btnBuyClick(Sender: TObject);
begin
  inherited;
  CallAgentBuy(nil, Trim(edtBuyStock.Text), StrToFloatDef(edtBuyPrice.Text, 0), StrToIntDef(edtBuyNum.Text, 0));
end;

procedure TfmeDealConsole.btnSaleClick(Sender: TObject);
begin
  inherited;
  CallAgentSale(nil, Trim(edtSaleStock.Text), StrToFloatDef(edtSalePrice.Text, 0), StrToIntDef(edtSaleNum.Text, 0));
end;

end.
