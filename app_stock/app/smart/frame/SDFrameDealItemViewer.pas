unit SDFrameDealItemViewer;

interface

uses
  Windows, Messages, Forms, BaseFrame, StdCtrls, Classes, Controls, ExtCtrls,
  Dialogs, Sysutils, VirtualTrees,
  define_dealitem, define_dealmarket, define_stock_quotes_instant,
  StockInstantData_Get_Sina,  
  DB_Quote_Instant_Refresh,
  DealItemsTreeView;

type                  
  TfmeDealItemViewerData = record   
    DealItemTree: TDealItemTreeCtrl;
    OnDealItemChange: TOnDealItemEvent;
    OnDealItemDblClick: TOnDealItemEvent;
    FocusDealItem: PRT_DealItem;
  end;
  
  TfmeDealItemViewer = class(TfmeBase)
    pnlLeftBottom: TPanel;
  protected
    { Private declarations }
    fDealItemViewerData: TfmeDealItemViewerData;
    procedure DealItemChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure DealItemDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);
  public
    { Public declarations }      
    constructor Create(AOwner: TComponent); override;
    procedure Initialize; override;
    property OnDealItemChange: TOnDealItemEvent read fDealItemViewerData.OnDealItemChange write fDealItemViewerData.OnDealItemChange;
    property OnDealItemDblClick: TOnDealItemEvent read fDealItemViewerData.OnDealItemDblClick write fDealItemViewerData.OnDealItemDblClick;     
    property FocusDealItem: PRT_DealItem read fDealItemViewerData.FocusDealItem;
  end;

implementation

uses
  UtilsHttp,
  win.iobuffer,
  BaseStockFormApp,
  db_dealitem_load;

{$R *.dfm}
                 
constructor TfmeDealItemViewer.Create(AOwner: TComponent);
begin
  inherited;
  FillChar(fDealItemViewerData, SizeOf(fDealItemViewerData), 0);
end;

procedure TfmeDealItemViewer.Initialize; 
{$IFDEF DEALINSTANT}
var
  tmpVNode: PVirtualNode;
  tmpVNodeChild: PVirtualNode;
  tmpVNodeData: PDealItemNode;
  tmpQuote: PRT_InstantQuote;
{$ENDIF}
begin
  fDealItemViewerData.DealItemTree := TDealItemTreeCtrl.Create(Self);
  fDealItemViewerData.DealItemTree.InitializeDealItemsTree(fDealItemViewerData.DealItemTree.TreeView);

  //fDealItemViewerData.DealItemTree.AddDealItemsTreeColumn_FirstDeal;
  fDealItemViewerData.DealItemTree.AddDealItemsTreeColumn_PriceClose;
  //fFormSDConsoleData.DealItemTree.AddDealItemsTreeColumn_lastDeal;
  //fDealItemViewerData.DealItemTree.AddDealItemsTreeColumn_EndDeal;
                                                         
  fDealItemViewerData.DealItemTree.TreeView.Clear;
  fDealItemViewerData.DealItemTree.BuildDealItemsTreeNodes(TBaseStockApp(App).StockIndexDB);
  fDealItemViewerData.DealItemTree.BuildDealItemsTreeNodes(TBaseStockApp(App).StockItemDB);

  if nil <>fDealItemViewerData.DealItemTree.TreeView.RootNode then
  begin                   
    {$IFDEF DEALINSTANT}
    if (nil <> TBaseStockApp(App).QuoteInstantDB) then
    begin
      if 0 < TBaseStockApp(App).QuoteInstantDB.RecordCount then
      begin
        tmpVNode := fDealItemViewerData.DealItemTree.TreeView.RootNode.FirstChild;
        while nil <> tmpVNode do
        begin
          tmpVNodeData := fDealItemViewerData.DealItemTree.TreeView.GetNodeData(tmpVNode);
          if nil <>tmpVNodeData then
          begin
            if nil <> tmpVNodeData.DealItem then
            begin
              tmpQuote := TBaseStockApp(App).QuoteInstantDB.FindRecordByKey(tmpVNodeData.DealItem.iCode);
              if tmpQuote.Item = tmpVNodeData.DealItem then
              begin
                tmpVNodeData.InstantQuote := tmpQuote;
              end;
            end;
          end;
          tmpVNodeChild := tmpVNode.FirstChild;
          while nil <> tmpVNodeChild do
          begin                                  
            tmpVNodeData := fDealItemViewerData.DealItemTree.TreeView.GetNodeData(tmpVNodeChild);
            if nil <>tmpVNodeData then
            begin
              if nil <> tmpVNodeData.DealItem then
              begin
                tmpQuote := TBaseStockApp(App).QuoteInstantDB.FindRecordByKey(tmpVNodeData.DealItem.iCode);
                if nil <> tmpQuote then
                begin
                  if tmpQuote.Item = tmpVNodeData.DealItem then
                  begin
                    tmpVNodeData.InstantQuote := tmpQuote;
                  end;
                end;
              end;
            end;
            tmpVNodeChild := tmpVNodeChild.NextSibling;
          end;
          tmpVNode := tmpVNode.NextSibling;
        end;                                  
      end;
    end;
    {$ENDIF}
  end;
  
  TVirtualStringTree(fDealItemViewerData.DealItemTree.TreeView).TreeOptions.SelectionOptions :=
    TVirtualStringTree(fDealItemViewerData.DealItemTree.TreeView).TreeOptions.SelectionOptions + [toMultiSelect];
  TVirtualStringTree(fDealItemViewerData.DealItemTree.TreeView).OnChange := DealItemChange;
  TVirtualStringTree(fDealItemViewerData.DealItemTree.TreeView).OnNodeDblClick := DealItemDblClick;  
end;

procedure TfmeDealItemViewer.DealItemDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);  
var
  tmpVNodeData: PDealItemNode;
begin
  if nil <> HitInfo.HitNode then
  begin
    tmpVNodeData := fDealItemViewerData.DealItemTree.TreeView.GetNodeData(HitInfo.HitNode);
    if nil <> tmpVNodeData then
    begin
      if Assigned(OnDealItemDblClick) then
      begin
        OnDealItemDblClick(tmpVNodeData.DealItem);
      end;
    end;
  end;
end;

procedure TfmeDealItemViewer.DealItemChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  tmpVNodeData: PDealItemNode;
//  tmpThreadHandle: THandle;
//  tmpThreadId: DWORD;
begin
  if nil <> Node then
  begin
    tmpVNodeData := fDealItemViewerData.DealItemTree.TreeView.GetNodeData(Node);
    if nil <> tmpVNodeData then
    begin
      //LoadDayDataTreeView(tmpNode);
      if Assigned(OnDealItemChange) then
      begin
        fDealItemViewerData.FocusDealItem := tmpVNodeData.DealItem;
        OnDealItemChange(tmpVNodeData.DealItem);
      end;
    end;
  end;
end;

end.
