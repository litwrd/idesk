inherited fmeDealConsole: TfmeDealConsole
  Width = 712
  Height = 398
  object lblStock: TLabel
    Left = 8
    Top = 19
    Width = 47
    Height = 13
    Caption = 'Buy Stock'
  end
  object lblPrice: TLabel
    Left = 8
    Top = 46
    Width = 44
    Height = 13
    Caption = 'Buy Price'
  end
  object lblNum: TLabel
    Left = 8
    Top = 73
    Width = 42
    Height = 13
    Caption = 'Buy Num'
  end
  object lblSalePrice: TLabel
    Left = 9
    Top = 246
    Width = 46
    Height = 13
    Caption = 'Sale Price'
  end
  object lblSaleNum: TLabel
    Left = 9
    Top = 273
    Width = 44
    Height = 13
    Caption = 'Sale Num'
  end
  object lblSaleStock: TLabel
    Left = 9
    Top = 219
    Width = 49
    Height = 13
    Caption = 'Sale Stock'
  end
  object btnBuy: TButton
    Left = 107
    Top = 97
    Width = 75
    Height = 25
    Caption = 'Buy'
    TabOrder = 0
    OnClick = btnBuyClick
  end
  object edtBuyStock: TEdit
    Left = 64
    Top = 16
    Width = 121
    Height = 21
    Text = '600000'
  end
  object edtBuyPrice: TEdit
    Left = 64
    Top = 43
    Width = 121
    Height = 21
    Text = '0.00'
  end
  object edtBuyNum: TEdit
    Left = 64
    Top = 70
    Width = 121
    Height = 21
    Text = '100'
  end
  object btnSale: TButton
    Left = 107
    Top = 299
    Width = 75
    Height = 25
    Caption = 'Sale'
    OnClick = btnSaleClick
  end
  object lstHold: TListBox
    Left = 208
    Top = 19
    Width = 225
    Height = 272
    ItemHeight = 13
  end
  object edtSalePrice: TEdit
    Left = 64
    Top = 243
    Width = 121
    Height = 21
    Text = '0.00'
  end
  object edtSaleNum: TEdit
    Left = 64
    Top = 270
    Width = 121
    Height = 21
    Text = '100'
  end
  object edtSaleStock: TEdit
    Left = 64
    Top = 216
    Width = 121
    Height = 21
    Text = '600000'
  end
end
