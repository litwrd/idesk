unit SDFrameTest;

interface

uses
  Windows, Messages, Classes, Forms, BaseFrame, StdCtrls,
  Controls, ComCtrls, ExtCtrls, SysUtils, Dialogs, VirtualTrees,
  define_price, define_dealstore_file, define_message, define_dealitem,
  define_stockapp, define_datasrc, define_stock_quotes, define_datetime,
  StockMinuteDataAccess;

type          
  TfmeTest = class(TfmeBase)
    btn1: TButton;
    mmo1: TMemo;
    procedure btn1Click(Sender: TObject);
  private
  public
    { Public declarations }     
    constructor Create(Owner: TComponent); override;
  end;

implementation

{$R *.dfm}

uses
  IniFiles,
  UtilsLog,     
  smartStockApp,
  BaseStockFormApp;
             
{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDFrameTest.pas';
{$ENDIF}
            
constructor TfmeTest.Create(Owner: TComponent);     
begin
  inherited;
end;

procedure TfmeTest.btn1Click(Sender: TObject);
begin
  inherited;
  if nil = App then
    exit;
  TStockSmartApp(App).DecisionMaker.Run;
end;

end.
