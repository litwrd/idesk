unit SDFrameBaseDealItem;

interface

uses
  Classes, Forms, BaseFrame,
  define_dealitem;

type           
  TFrameDataDealItem = record
    DealItem: PRT_DealItem;
  end;
  
  TfmeBaseStockItem = class(TfmeBase)
  protected
    fFrameDataDealItem: TFrameDataDealItem; 
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;   
    procedure NotifyDealItem(ADealItem: PRT_DealItem); virtual;
  end;

implementation

{$R *.dfm}

{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDFrameBaseDealItem.pas';
{$ENDIF}

{ TfmeBaseStockItem }
constructor TfmeBaseStockItem.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fFrameDataDealItem, SizeOf(fFrameDataDealItem), 0);
end;

procedure TfmeBaseStockItem.NotifyDealItem(ADealItem: PRT_DealItem);
begin
end;

end.
