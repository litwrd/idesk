unit SDFrameGraphM;

interface

uses
  Windows, Messages, Classes, Forms, BaseFrame, StdCtrls, Graphics,
  Controls, ComCtrls, ExtCtrls, SysUtils, VirtualTrees, SDFrameBaseDealItem,
  define_price, define_dealstore_file, define_message, define_dealitem,
  define_stockapp, define_datasrc, define_stock_quotes, define_datetime,
  StockMinuteDataAccess,
  Strategy_BOLL,
  Strategy_CYHT,
  Strategy_BDZX,         
  Strategy_1272,
  StockChart,
  GR32,
  GR32_Layers,
  GR32_LowLevel,
  GR32_Image;

type           
  TFrameDataStockGraph = record
    DealItem: PRT_DealItem;
    MinuteData: TStockMinuteDataAccess;
    Strategy_Boll: TStrategy_BOLL;    
    //Strategy_1272: TStrategy_1272;
    Strategy_CYHT: TStrategy_CYHT;
    //Strategy_BDZX: TStrategy_BDZX;
    
    //Background: TBitmapLayer;
    
    MouseMoveLine: TBitmapLayer;
    MouseMoveLineBitmap: TBitmap32;
    
    MouseMoveRect: TBitmapLayer;
    MouseMoveRectBitmap: TBitmap32;
    
    ChartPrice: TStockChartPrice;
  end;
  
  TfmeStockGraphM = class(TfmeBaseStockItem)
    imgStock: TImage32;
    procedure imgStockInitStages(Sender: TObject);
    procedure imgStockMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure imgStockMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
  protected
    fFrameDataGraph: TFrameDataStockGraph;
    procedure DrawChart(AStockChartPrice: PStockChartPrice);

    procedure imgStockPaintStage(Sender: TObject; Buffer: TBitmap32; StageNum: Cardinal);
    procedure imgStockMouseMove(Sender: TObject; Shift: TShiftState;
        X, Y: Integer; Layer: TCustomLayer); 
    procedure imgStockResize(Sender: TObject);
    procedure DrawStockChart(AStockChartData: PStockChartPrice);
    procedure AddBackgroundGraph;
    
    procedure AddMouseMoveLine;
    procedure UpdateMouseMoveLine(X, Y: Integer);

    procedure AddMouseMoveRect;
    procedure UpdateMouseMoveRect(X, Y: Integer);

    procedure ResizeStockChart(AStockChartData: PStockChartPrice);  
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;   
    procedure NotifyDealItem(ADealItem: PRT_DealItem); override;
  end;

implementation

{$R *.dfm}

uses
  IniFiles,
  UtilsLog,
  StockMinuteData_Load,
  StockChart_Price,
  StockChart_Boll,     
  StockChart_1272,     
  StockChart_CYHT,   
  BaseStockFormApp;
             
{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDFrameGraphM.pas';
{$ENDIF}

{ TfmeStockGraphM }
constructor TfmeStockGraphM.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fFrameDataGraph, SizeOf(fFrameDataGraph), 0);
  imgStock.OnInitStages := imgStockInitStages;
  //imgStock.OnPaintStage := imgStockPaintStage;
  imgStock.OnMouseMove := imgStockMouseMove;
  imgStock.OnMouseDown := imgStockMouseDown;
  imgStock.OnMouseUp := imgStockMouseUp;
  imgStock.OnResize := imgStockResize;

  fFrameDataGraph.ChartPrice.ChartItemWidth := 3;
  fFrameDataGraph.ChartPrice.ChartItemWidthHalf := fFrameDataGraph.ChartPrice.ChartItemWidth div 2;
  fFrameDataGraph.ChartPrice.ChartItemGap := 1;
  
  ResizeStockChart(@fFrameDataGraph.ChartPrice);
  AddBackgroundGraph;
end;

procedure TfmeStockGraphM.NotifyDealItem(ADealItem: PRT_DealItem);
begin                                        
  if fFrameDataGraph.DealItem = ADealItem then
    exit;
  if fFrameDataGraph.DealItem <> ADealItem then
  begin
    fFrameDataGraph.DealItem := ADealItem;
    if nil <> fFrameDataGraph.MinuteData then
      FreeAndNil(fFrameDataGraph.MinuteData);
  end;
  if nil <> fFrameDataGraph.DealItem then
  begin
    fFrameDataGraph.MinuteData := TStockMinuteDataAccess.Create(fFrameDataGraph.DealItem, src_tongdaxin, weightNone);
    fFrameDataGraph.MinuteData.Minute := 60;
    LoadStockMinuteData(App, fFrameDataGraph.MinuteData);
                                            
    if nil <> fFrameDataGraph.Strategy_Boll then
      FreeAndNil(fFrameDataGraph.Strategy_Boll);
    fFrameDataGraph.Strategy_Boll := TStrategy_BOLL.Create(App);
    fFrameDataGraph.Strategy_Boll.DealItem := fFrameDataGraph.DealItem;
    fFrameDataGraph.Strategy_Boll.MinuteData := fFrameDataGraph.MinuteData;
    
//    if nil <> fFrameDataGraph.Strategy_1272 then
//      FreeAndNil(fFrameDataGraph.Strategy_1272);
//    fFrameDataGraph.Strategy_1272 := TStrategy_1272.Create(App);
//    fFrameDataGraph.Strategy_1272.DealItem := fFrameDataGraph.DealItem;
//    fFrameDataGraph.Strategy_1272.DayData := fFrameDataGraph.DayData;
//
    if nil <> fFrameDataGraph.Strategy_CYHT then
      FreeAndNil(fFrameDataGraph.Strategy_CYHT);      
    fFrameDataGraph.Strategy_CYHT := TStrategy_CYHT.Create(App);
    fFrameDataGraph.Strategy_CYHT.DealItem := fFrameDataGraph.DealItem;
    fFrameDataGraph.Strategy_CYHT.MinuteData := fFrameDataGraph.MinuteData;

//    if nil <> fFrameDataGraph.Strategy_BDZX then
//      FreeAndNil(fFrameDataGraph.Strategy_BDZX);
//    fFrameDataGraph.Strategy_BDZX := TStrategy_BDZX.Create(App);
//    fFrameDataGraph.Strategy_BDZX.DealItem := fFrameDataGraph.DealItem;
//    fFrameDataGraph.Strategy_BDZX.DayData := fFrameDataGraph.DayData;

    fFrameDataGraph.ChartPrice.ChartItemWidth := 3;
    fFrameDataGraph.ChartPrice.ChartItemWidthHalf := fFrameDataGraph.ChartPrice.ChartItemWidth div 2;
    
    DrawStockChart(@fFrameDataGraph.ChartPrice);
    if 0 < fFrameDataGraph.MinuteData.RecordCount then
    begin
      imgStock.Invalidate;
    end;
  end;
end;

procedure TfmeStockGraphM.AddBackgroundGraph;
begin
  if nil = fFrameDataGraph.ChartPrice.BackgroundBitmap then
  begin
    fFrameDataGraph.ChartPrice.BackgroundBitmap := TBitmap32.Create();
    //fFrameDataGraph.BackgroundBitmap.DrawMode := dmOpaque;//
    fFrameDataGraph.ChartPrice.BackgroundBitmap.DrawMode := dmBlend;
    //fFrameDataGraph.BackgroundBitmap.MasterAlpha := 100;
    fFrameDataGraph.ChartPrice.BackgroundBitmap.MasterAlpha := 255;
  end;
  if (fFrameDataGraph.ChartPrice.BackgroundBitmap.Width <> imgStock.Width) or
     (fFrameDataGraph.ChartPrice.BackgroundBitmap.Height <> imgStock.Height) then
  begin
    fFrameDataGraph.ChartPrice.BackgroundBitmap.Width := imgStock.Width;
    fFrameDataGraph.ChartPrice.BackgroundBitmap.Height := imgStock.Height;
  end;                                
  ResizeStockChart(@fFrameDataGraph.ChartPrice);
  DrawStockChart(@fFrameDataGraph.ChartPrice);
  
  //if nil = fFrameDataGraph.Background then
  begin
    imgStock.BeginUpdate;

//    fFrameDataGraph.Background := TBitmapLayer.Create(imgStock.Layers);
//    fFrameDataGraph.Background.Bitmap := fFrameDataGraph.ChartPrice.BackgroundBitmap;
//
//    tmpRect.Left := 0;
//    tmpRect.Top := 0;
//    tmpRect.Right := tmpRect.Left + imgStock.Width;
//    tmpRect.Bottom := tmpRect.Top + imgStock.Height;
//    fFrameDataGraph.Background.Location := tmpRect;
    imgStock.EndUpdate;
    imgStock.Changed;
  end;
end;

procedure TfmeStockGraphM.imgStockResize(Sender: TObject);
var
  tmpRect: TFloatRect;     
begin
  inherited;       
  ResizeStockChart(@fFrameDataGraph.ChartPrice);            
  if nil <> fFrameDataGraph.ChartPrice.BackgroundBitmap then
  begin
    imgStock.BeginUpdate;
    try
      fFrameDataGraph.ChartPrice.BackgroundBitmap.Width := imgStock.Width;
      fFrameDataGraph.ChartPrice.BackgroundBitmap.Height := imgStock.Height;
      DrawStockChart(@fFrameDataGraph.ChartPrice);
//      tmpRect.Left := 0;
//      tmpRect.Top := 0;
//      tmpRect.Right := imgStock.Width;
//      tmpRect.Bottom := imgStock.Height;
//      fFrameDataGraph.Background.Location := tmpRect;

      if nil <> fFrameDataGraph.MouseMoveLine then
      begin
        tmpRect.Left := fFrameDataGraph.MouseMoveLine.Location.Left;
        tmpRect.Right := fFrameDataGraph.MouseMoveLine.Location.Right;
        tmpRect.Top := fFrameDataGraph.MouseMoveLine.Location.Top;
        tmpRect.Bottom := fFrameDataGraph.ChartPrice.Rect_PriceChart.Bottom;
        fFrameDataGraph.MouseMoveLine.Location := tmpRect;
      end;
    finally
      imgStock.EndUpdate;
      imgStock.Changed;
    end;
  end;
end;

procedure TfmeStockGraphM.AddMouseMoveRect;
begin
  if nil = fFrameDataGraph.MouseMoveRect then
  begin
    imgStock.BeginUpdate;
    fFrameDataGraph.MouseMoveRectBitmap := TBitmap32.Create();
    fFrameDataGraph.MouseMoveRectBitmap.DrawMode := dmBlend;
    fFrameDataGraph.MouseMoveRectBitmap.CombineMode := cmBlend; 
    fFrameDataGraph.MouseMoveRect := TBitmapLayer.Create(imgStock.Layers);
    fFrameDataGraph.MouseMoveRect.Bitmap := fFrameDataGraph.MouseMoveRectBitmap;
    imgStock.EndUpdate;
    imgStock.Changed;
  end;
end;

procedure TfmeStockGraphM.UpdateMouseMoveRect(X, Y: Integer);
var
  tmpMoveRect: TFloatRect;
begin
  imgStock.BeginUpdate;
  try
    fFrameDataGraph.MouseMoveRectBitmap.Width := Abs(X - fFrameDataGraph.ChartPrice.MouseDownPoint.X);
    fFrameDataGraph.MouseMoveRectBitmap.Height := Abs(Y - fFrameDataGraph.ChartPrice.MouseDownPoint.Y);
    fFrameDataGraph.MouseMoveRectBitmap.Clear($0000000);
    fFrameDataGraph.MouseMoveRectBitmap.FrameRectS(0, 0,
      fFrameDataGraph.MouseMoveRectBitmap.Width,
      fFrameDataGraph.MouseMoveRectBitmap.Height,
      clWhite32);
    if (X > fFrameDataGraph.ChartPrice.MouseDownPoint.X) then
    begin
      tmpMoveRect.Left := fFrameDataGraph.ChartPrice.MouseDownPoint.X;                                     
    end else
    begin
      tmpMoveRect.Left := X;
    end;
    if (Y > fFrameDataGraph.ChartPrice.MouseDownPoint.Y) then
    begin
      tmpMoveRect.Top := fFrameDataGraph.ChartPrice.MouseDownPoint.Y;
    end else
    begin
      tmpMoveRect.Top := Y;
    end;
    tmpMoveRect.Right := tmpMoveRect.Left + fFrameDataGraph.MouseMoveRectBitmap.Width;
    tmpMoveRect.Bottom := tmpMoveRect.Top + fFrameDataGraph.MouseMoveRectBitmap.Height;
    fFrameDataGraph.MouseMoveRect.Bitmap := fFrameDataGraph.MouseMoveRectBitmap;
    fFrameDataGraph.MouseMoveRect.Location := tmpMoveRect;
  finally
    imgStock.EndUpdate;
    imgStock.Changed;
  end;
end;

procedure TfmeStockGraphM.AddMouseMoveLine;
begin
  if nil = fFrameDataGraph.MouseMoveLine then
  begin
    imgStock.BeginUpdate;
    fFrameDataGraph.MouseMoveLineBitmap := TBitmap32.Create();    
    fFrameDataGraph.MouseMoveLine := TBitmapLayer.Create(imgStock.Layers);
    fFrameDataGraph.MouseMoveLine.Bitmap := fFrameDataGraph.MouseMoveLineBitmap;
    imgStock.EndUpdate;
    imgStock.Changed;
  end;
end;

procedure TfmeStockGraphM.UpdateMouseMoveLine(X, Y: Integer);
var
  tmpMoveLineRect: TFloatRect;
begin
  if nil <> fFrameDataGraph.MouseMoveLine then
  begin
    imgStock.BeginUpdate;
    try                                            
      if fFrameDataGraph.MouseMoveLineBitmap.Height <> imgStock.Height then
      begin
        fFrameDataGraph.MouseMoveLineBitmap.Width := 1;
        fFrameDataGraph.MouseMoveLineBitmap.Height := imgStock.Height;
        fFrameDataGraph.MouseMoveLineBitmap.DrawMode := dmBlend;
        fFrameDataGraph.MouseMoveLineBitmap.MasterAlpha := 255;
        fFrameDataGraph.MouseMoveLineBitmap.FillRect(0, 0,
            fFrameDataGraph.MouseMoveLineBitmap.Width,
            fFrameDataGraph.MouseMoveLineBitmap.Height, clWhite32);
      end;
      tmpMoveLineRect.Left := X;                                     
      tmpMoveLineRect.Right := tmpMoveLineRect.Left + fFrameDataGraph.MouseMoveLineBitmap.Width;
      tmpMoveLineRect.Top := fFrameDataGraph.ChartPrice.Rect_PriceChart.Top + 1;
      tmpMoveLineRect.Bottom := fFrameDataGraph.ChartPrice.Rect_PriceChart.Bottom - 1;
      
      fFrameDataGraph.MouseMoveLine.Bitmap := fFrameDataGraph.MouseMoveLineBitmap;
      fFrameDataGraph.MouseMoveLine.Location := tmpMoveLineRect;
    finally
      imgStock.EndUpdate;
      imgStock.Changed;
    end;
  end;
end;

procedure TfmeStockGraphM.ResizeStockChart(AStockChartData: PStockChartPrice);
begin
  AStockChartData.Rect_Chart.Left := 10;
  AStockChartData.Rect_Chart.Right := imgStock.Width - 10;
  AStockChartData.Rect_Chart.Top := 10;
  //tmpRect.Bottom := Buffer.Height - 50;
  //AStockChartData.Rect_Chart.Bottom := AStockChartData.BackgroundBitmap.Height - 10;
  AStockChartData.Rect_Chart.Bottom := imgStock.Height - 10;

  AStockChartData.Rect_PriceChart.Left := AStockChartData.Rect_Chart.Left;
  AStockChartData.Rect_PriceChart.Right := AStockChartData.Rect_Chart.Right - 55;
  AStockChartData.Rect_PriceChart.Top := AStockChartData.Rect_Chart.Top;
  AStockChartData.Rect_PriceChart.Bottom := AStockChartData.Rect_Chart.Bottom - 100;
end;

procedure TfmeStockGraphM.DrawStockChart(AStockChartData: PStockChartPrice);
begin
  if nil <> AStockChartData.BackgroundBitmap then
  begin
    imgStock.BeginUpdate;
    try
      DrawChart(AStockChartData);
      imgStock.Bitmap := AStockChartData.BackgroundBitmap;
    finally
      imgStock.EndUpdate;
      imgStock.Changed;
    end;
  end;
end;

procedure TfmeStockGraphM.imgStockInitStages(Sender: TObject);
var
  tmpStage: PPaintStage;
begin
  inherited;        
//  tmpStage := imgStock.PaintStages.Add;
//  tmpStage.Stage := PST_DRAW_LAYERS;
//  tmpStage.Parameter := 0;
       
  tmpStage := imgStock.PaintStages.Add;
  tmpStage.Stage := PST_CUSTOM;
  tmpStage.Parameter := 0;
end;

procedure TfmeStockGraphM.imgStockMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
begin
  inherited;
  fFrameDataGraph.ChartPrice.MouseDownStatus := 1;
  fFrameDataGraph.ChartPrice.MouseDownPoint.X := X;
  fFrameDataGraph.ChartPrice.MouseDownPoint.Y := Y;
  if nil <> fFrameDataGraph.MouseMoveRect then
  begin
    fFrameDataGraph.MouseMoveRect.Visible := true;
    UpdateMouseMoveRect(X + 1, Y + 1);
  end;
end;
                 
procedure TfmeStockGraphM.imgStockMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
begin
  inherited;                                 
  fFrameDataGraph.ChartPrice.MouseDownStatus := 0;
  if nil <> fFrameDataGraph.MouseMoveRect then
  begin
    fFrameDataGraph.MouseMoveRect.Visible := false;
    if (fFrameDataGraph.ChartPrice.MouseDownPoint.X > X) then
    begin
      if (Abs(fFrameDataGraph.ChartPrice.MouseDownPoint.X - X) > 5) and
         (Abs(fFrameDataGraph.ChartPrice.MouseDownPoint.Y - Y) > 5) then
      begin               
        if 2 < fFrameDataGraph.ChartPrice.ChartItemWidth then
        begin
          fFrameDataGraph.ChartPrice.ChartItemWidth := fFrameDataGraph.ChartPrice.ChartItemWidth - 2;
          if 1 = fFrameDataGraph.ChartPrice.ChartItemWidth then
            fFrameDataGraph.ChartPrice.ChartItemWidth := 2;
          fFrameDataGraph.ChartPrice.ChartItemWidthHalf := fFrameDataGraph.ChartPrice.ChartItemWidth div 2;
          DrawStockChart(@fFrameDataGraph.ChartPrice);
        end else
        begin
          if 2 = fFrameDataGraph.ChartPrice.ChartItemWidth then
          begin
            fFrameDataGraph.ChartPrice.ChartItemWidth := 1;  
            fFrameDataGraph.ChartPrice.ChartItemWidthHalf := 0;
            DrawStockChart(@fFrameDataGraph.ChartPrice);
          end;
        end;
      end;
    end else
    begin
      if (Abs(fFrameDataGraph.ChartPrice.MouseDownPoint.X - X) > 5) and
         (Abs(fFrameDataGraph.ChartPrice.MouseDownPoint.Y - Y) > 5) then
      begin
        if 2 = fFrameDataGraph.ChartPrice.ChartItemWidth then
        begin
          fFrameDataGraph.ChartPrice.ChartItemWidth := 3
        end else
        begin
          fFrameDataGraph.ChartPrice.ChartItemWidth := fFrameDataGraph.ChartPrice.ChartItemWidth + 2;
        end;
        fFrameDataGraph.ChartPrice.ChartItemWidthHalf := fFrameDataGraph.ChartPrice.ChartItemWidth div 2;
        DrawStockChart(@fFrameDataGraph.ChartPrice);
      end;
    end;
  end;
end;

procedure TfmeStockGraphM.imgStockMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer; Layer: TCustomLayer);
begin
  if nil <> fFrameDataGraph.MinuteData  then
  begin
    if 0 < fFrameDataGraph.MinuteData.RecordCount then
    begin
    end;
  end;
  
  if 1 = fFrameDataGraph.ChartPrice.MouseDownStatus then
  begin
    AddMouseMoveRect;
    UpdateMouseMoveRect(X, Y);
  end;
  //AddMouseMoveLine;
  //UpdateMouseMoveLine(X, Y);
end;

procedure TfmeStockGraphM.DrawChart(AStockChartPrice: PStockChartPrice);

  procedure UpdatePriceCoord(ACoord: TChartCoord; APriceMinute: PRT_Quote_Minute);
  begin
    if ACoord.IsInitValue then
    begin
      if ACoord.MaxValue < APriceMinute.PriceRange.PriceHigh.Value then
        ACoord.MaxValue := APriceMinute.PriceRange.PriceHigh.Value;
      if ACoord.MinValue > APriceMinute.PriceRange.PriceLow.Value then
      begin
        if 0 < APriceMinute.PriceRange.PriceLow.Value then
          ACoord.MinValue := APriceMinute.PriceRange.PriceLow.Value;
      end;
    end else
    begin
      ACoord.IsInitValue := true;
      ACoord.MaxValue := APriceMinute.PriceRange.PriceHigh.Value;
      ACoord.MinValue := APriceMinute.PriceRange.PriceLow.Value;
    end;
  end;

var
  tmpItemIndex: integer;
  i: integer;
  tmpChartBoll: TChartData_Boll;
  //tmpChart1272: TChartData_1272;   
begin               
  AStockChartPrice.BackgroundBitmap.FillRect(0, 0, AStockChartPrice.BackgroundBitmap.Width, AStockChartPrice.BackgroundBitmap.Height, Def_Chart_Bg_Color);
  AStockChartPrice.BackgroundBitmap.FrameRectS(AStockChartPrice.Rect_Chart, clRed32);

  AStockChartPrice.ChartItemCount := (AStockChartPrice.Rect_PriceChart.Right - AStockChartPrice.Rect_PriceChart.Left - 2) div (AStockChartPrice.ChartItemWidth + AStockChartPrice.ChartItemGap);
  (*   
  Buffer.FrameRectS(tmpRect_PriceChart, clRed32);
  //*)

  AStockChartPrice.BackgroundBitmap.Line(
      AStockChartPrice.Rect_Chart.Left,
      AStockChartPrice.Rect_PriceChart.Bottom,
      AStockChartPrice.Rect_Chart.Right,
      AStockChartPrice.Rect_PriceChart.Bottom, clRed32);
  AStockChartPrice.BackgroundBitmap.Line(
      AStockChartPrice.Rect_PriceChart.Right,
      AStockChartPrice.Rect_Chart.Top, AStockChartPrice.Rect_PriceChart.Right,
      AStockChartPrice.Rect_Chart.Bottom, clRed32);

  if nil = AStockChartPrice.StockChartCoord then
    AStockChartPrice.StockChartCoord := TChartCoord.Create;
  AStockChartPrice.StockChartCoord.Top := AStockChartPrice.Rect_PriceChart.Top;
  AStockChartPrice.StockChartCoord.Bottom := AStockChartPrice.Rect_PriceChart.Bottom;

  if nil <> fFrameDataGraph.MinuteData  then
  begin
    if 0 < fFrameDataGraph.MinuteData.RecordCount then
    begin  
      tmpItemIndex := 0;   
      AStockChartPrice.StockChartCoord.MaxValue := 0;
      AStockChartPrice.StockChartCoord.MinValue := 0;
      AStockChartPrice.StockChartCoord.IsInitValue := false;
      if AStockChartPrice.ChartItemCount >= fFrameDataGraph.MinuteData.RecordCount then
      begin               
        for i := 0 to fFrameDataGraph.MinuteData.RecordCount - 1 do
        begin
          UpdatePriceCoord(AStockChartPrice.StockChartCoord, fFrameDataGraph.MinuteData.MinuteDataByIndex(i));
        end;
        AStockChartPrice.ChartItemCount := fFrameDataGraph.MinuteData.RecordCount;
      end else
      begin
        for i := 1 to AStockChartPrice.ChartItemCount do
        begin
          tmpItemIndex := fFrameDataGraph.MinuteData.RecordCount - i;    
          UpdatePriceCoord(AStockChartPrice.StockChartCoord, fFrameDataGraph.MinuteData.MinuteDataByIndex(tmpItemIndex));
        end;
      end;

      AStockChartPrice.StockChartCoord.MaxValue := Round(AStockChartPrice.StockChartCoord.MaxValue / 1000 * 1010);
      AStockChartPrice.StockChartCoord.MinValue := Round(AStockChartPrice.StockChartCoord.MinValue / 1000 * 990);

      AStockChartPrice.RT_ChartItem.ItemRect.Left := AStockChartPrice.Rect_PriceChart.Left + 2;
      AStockChartPrice.RT_ChartItem.ChartPrice := AStockChartPrice;

      FillChar(tmpChartBoll, SizeOf(tmpChartBoll), 0);
      tmpChartBoll.Rule_Data := fFrameDataGraph.Strategy_Boll.Rule_Boll;

//      FillChar(tmpChart1272, SizeOf(tmpChart1272), 0);
//      tmpChart1272.Rule_MA_12 := fFrameDataGraph.Strategy_1272.MA_12;
//      tmpChart1272.Rule_MA_72 := fFrameDataGraph.Strategy_1272.MA_72;

      fFrameDataGraph.Strategy_CYHT.Chart.ChartCoord.Top := AStockChartPrice.Rect_PriceChart.Bottom;
      fFrameDataGraph.Strategy_CYHT.Chart.ChartCoord.Bottom := AStockChartPrice.Rect_Chart.Bottom;
      fFrameDataGraph.Strategy_CYHT.Chart.ChartCoord.MaxValue := 120;
      fFrameDataGraph.Strategy_CYHT.Chart.ChartCoord.MinValue := 0;

//      fFrameDataGraph.Strategy_BDZX.Chart.ChartCoord.Top := AStockChartPrice.Rect_PriceChart.Bottom;
//      fFrameDataGraph.Strategy_BDZX.Chart.ChartCoord.Bottom := AStockChartPrice.Rect_Chart.Bottom;
//      fFrameDataGraph.Strategy_BDZX.Chart.ChartCoord.MaxValue := 150;
//      fFrameDataGraph.Strategy_BDZX.Chart.ChartCoord.MinValue := -50;
      fFrameDataGraph.Strategy_CYHT.Chart.BeginDraw;
      for i := 0 to AStockChartPrice.ChartItemCount - 1 do
      begin
        AStockChartPrice.RT_ChartItem.ItemRect.Right := AStockChartPrice.RT_ChartItem.ItemRect.Left + AStockChartPrice.ChartItemWidth;
        AStockChartPrice.RT_ChartItem.ItemIndex := tmpItemIndex + i;
        DrawChart_Item_Price_Minute(@AStockChartPrice.RT_ChartItem, fFrameDataGraph.MinuteData);
        // ��ͼ����
        DrawChart_Item_Price_Boll(@AStockChartPrice.RT_ChartItem, @tmpChartBoll, AStockChartPrice.StockChartCoord);
        //DrawChart_Item_Price_1272(@AStockChartPrice.RT_ChartItem, @tmpChart1272);

        fFrameDataGraph.Strategy_CYHT.Chart.DrawChartItem(@AStockChartPrice.RT_ChartItem);
        //fFrameDataGraph.Strategy_BDZX.Chart.DrawChart(@AStockChartPrice.RT_ChartItem);
        //------------------------------------------------------------------
        AStockChartPrice.RT_ChartItem.ItemRect.Left := AStockChartPrice.RT_ChartItem.ItemRect.Right + AStockChartPrice.ChartItemGap;
      end;
    end;
  end;
end;

procedure TfmeStockGraphM.imgStockPaintStage(Sender: TObject; Buffer: TBitmap32; StageNum: Cardinal);
begin
  inherited;
  //
  //Buffer.Draw();  
  Buffer.DrawMode := dmBlend;
  fFrameDataGraph.ChartPrice.BackgroundBitmap := Buffer;
  try
    //ResizeStockChart(@fFrameDataGraph.Chart);
    DrawChart(@fFrameDataGraph.ChartPrice);
  finally
    fFrameDataGraph.ChartPrice.BackgroundBitmap := nil;
  end;
end;

end.
