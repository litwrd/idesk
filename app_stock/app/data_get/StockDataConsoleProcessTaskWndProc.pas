unit StockDataConsoleProcessTaskWndProc;

interface

uses
  Windows;      

  function ProcessTaskHostCommandWndProcA_Task1_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task1_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task1_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;

  function ProcessTaskHostCommandWndProcA_Task2_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task2_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task2_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;

  function ProcessTaskHostCommandWndProcA_Task3_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task3_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task3_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;

  function ProcessTaskHostCommandWndProcA_Task4_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task4_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
  function ProcessTaskHostCommandWndProcA_Task4_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;

implementation

uses               
  windef_msg,
  define_datasrc,
  define_dealitem,
  StockDataConsoleTask,
  StockDataTaskConsole,
  define_StockDataApp;
  
function BusinessCommandWndProcA(AProcessTask: PDownloadProcessTask; AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT;  
begin
  Result := 0;
  if nil <> AProcessTask then
  begin
    case AMsg of         
      WM_RequestHostCommandWnd: begin
        Result := AProcessTask.DownloadProcess.HostCmdWnd.CmdWndHandle;
      end;
      WM_RegisterClientCommandWnd: begin
        if IsWindow(lParam) then
        begin
          AProcessTask.DownloadProcess.Core.AppCmdWnd := lParam;
          if nil <> AProcessTask.DealItem then
          begin
            PostMessage(AProcessTask.DownloadProcess.HostCmdWnd.CmdWndHandle, WM_ConsoleProcess_Command_Download, 0, 0);
          end;
        end;
      end;
      WM_ConsoleProcess_Command_Download: begin
          //GlobalBaseStockApp.RunStart;
        if nil <> G_StockTaskConsole then
        begin
          AProcessTask.TaskStatus := taskRunning;
          if nil <> AProcessTask.OwnerTask then
          begin
            AProcessTask.OwnerTask.TaskStatus := taskRunning;
          end;
          if not G_StockTaskConsole.Console_CheckDownloaderProcess(AProcessTask) then
          begin                                                            
            PostMessage(AWnd, WM_ConsoleProcess_Command_Download, wParam, lParam);
            exit;
          end;
          if not IsWindow(AProcessTask.DownloadProcess.Core.AppCmdWnd) then
          begin
            PostMessage(AWnd, WM_ConsoleProcess_Command_Download, wParam, lParam);
            exit;
          end;
          G_StockTaskConsole.Console_NotifyDownloadData(AProcessTask);
        end;
      end;
      WM_Downloader2Console_Command_DownloadResult: begin
        if nil <> G_StockTaskConsole then
        begin
          if 0 = lParam then
          begin
              // 下载成功
            AProcessTask.DealItem := G_StockTaskConsole.Console_GetNextDownloadDealItem(AProcessTask);
            if nil <> AProcessTask.DealItem then
            begin
              PostMessage(AWnd, WM_ConsoleProcess_Command_Download, 0, 0);
            end else
            begin
              // 都下载完了 ???
              AProcessTask.TaskStatus := TaskEnd;
              G_StockTaskConsole.Console_NotifyDownloaderShutdown(AProcessTask);
              //PostMessage(AWnd, WM_AppRequestEnd, 0, 0);
            end;
          end else
          begin
              // 下载失败
            PostMessage(AWnd, WM_ConsoleProcess_Command_Download, 0, 0);
          end;
        end;
      end;
      else
        Result := DefWindowProcA(AWnd, AMsg, wParam, lParam);
    end;
  end;                                           
end;
                 
function ProcessTaskHostCommandWndProcA_Task1_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin            
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[0].DownProcessTaskArray[0], AWnd, AMsg, wParam, lParam);
end;
                
function ProcessTaskHostCommandWndProcA_Task1_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[0].DownProcessTaskArray[1], AWnd, AMsg, wParam, lParam);
end;
                       
function ProcessTaskHostCommandWndProcA_Task1_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[0].DownProcessTaskArray[2], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task2_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[1].DownProcessTaskArray[0], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task2_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[1].DownProcessTaskArray[1], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task2_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[1].DownProcessTaskArray[2], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task3_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[2].DownProcessTaskArray[0], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task3_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[2].DownProcessTaskArray[1], AWnd, AMsg, wParam, lParam);
end;
                      
function ProcessTaskHostCommandWndProcA_Task3_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[2].DownProcessTaskArray[2], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task4_Sub1(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[3].DownProcessTaskArray[0], AWnd, AMsg, wParam, lParam);
end;

function ProcessTaskHostCommandWndProcA_Task4_Sub2(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[3].DownProcessTaskArray[1], AWnd, AMsg, wParam, lParam);
end;
                                         
function ProcessTaskHostCommandWndProcA_Task4_Sub3(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := BusinessCommandWndProcA(@GlobalDownloadTasks[3].DownProcessTaskArray[2], AWnd, AMsg, wParam, lParam);
end;

end.
