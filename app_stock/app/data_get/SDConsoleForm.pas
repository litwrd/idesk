unit SDConsoleForm;

interface

uses
  Windows, Forms, BaseForm, Classes, Controls,
  StdCtrls, Sysutils, ExtCtrls, Dialogs, VirtualTrees,
  define_dealitem, define_dealmarket, define_datasrc, define_price, define_datetime,
  StockDataConsoleApp, StockDataConsoleTask, DealItemsTreeView, StockDayDataAccess,
  SDFrameDealItemViewer,
  SDFrameDataViewer,
  SDFrameDataRepair,
  SDFrameStockInstant,
  SDFrameStockInfo,
  {$IFDEF DEALCLASS}
  SDFrameStockClass,
  {$ENDIF}
  SDFrameTDXData,
  SDFrameWindData,
  ComCtrls, Tabs;

type
  TFormSDConsoleData = record
    AppStartTimer: TTimer;       
    DealItemTree: TDealItemTreeCtrl;
    FrameDealItemViewer: TfmeDealItemViewer;     
    FrameDataViewer: TfmeDataViewer;
    FrameDataRepair: TfmeDataRepair;   
    {$IFDEF DEALCLASS}
    FrameStockClass: TfmeStockClass;
    {$ENDIF}
    FrameStockInfo: TfmeStockInfo;
    {$IFDEF TDX}
    FrameTDXData: TfmeTDXData;
    {$ENDIF}                
    {$IFDEF WIND}
    FrameWindData: TfmeWindData;
    {$ENDIF}                
    {$IFDEF DEALINSTANT}
    FrameStockInstant: TfmeStockInstant;
    {$ENDIF}                  
  end;

  TfrmSDConsole = class(TfrmBase)
    pnlBottom: TPanel;
    pnlTop: TPanel;
    pnlMain: TPanel;
    pnlLeft: TPanel;
    splLeft: TSplitter;
    tsfunc: TTabSet;
    procedure tsfuncChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
  private
    { Private declarations }
    fFormSDConsoleData: TFormSDConsoleData;
    procedure tmrAppStartTimer(Sender: TObject);     
    procedure DoNotifyDealItemChange(ADealItem: PRT_DealItem);
  protected
    procedure FormCreate(Sender: TObject);
  public
    constructor Create(Owner: TComponent); override;
  end;

implementation

{$R *.dfm}

uses            
  define_StockDataApp,
  define_stock_quotes,
  define_dealstore_file,
  windef_msg,
  win.shutdown,   
  win.iobuffer,
  ShellAPI,
  UtilsHttp,
  BaseFrame,
  {$IFDEF LOG}
  UtilsLog,
  {$ENDIF}
  HTMLParserAll3,
  BaseStockFormApp,
  StockDayData_Load,
  BaseWinApp,
  db_dealItem_LoadIni,
  db_dealitem_load,
  db_dealitem_save;
                
           
{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDConsoleForm.pas'; 
{$ENDIF}
    
constructor TfrmSDConsole.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fFormSDConsoleData, SizeOf(fFormSDConsoleData), 0);   
  fFormSDConsoleData.AppStartTimer := TTimer.Create(Application);
  fFormSDConsoleData.AppStartTimer.Interval := 100;
  fFormSDConsoleData.AppStartTimer.OnTimer := tmrAppStartTimer;
  fFormSDConsoleData.AppStartTimer.Enabled := true;
  Self.OnCreate := FormCreate;
end;
                          
procedure TfrmSDConsole.FormCreate(Sender: TObject);
begin
  DragAcceptFiles(Handle, True) ;
//
end;
     
procedure TfrmSDConsole.tmrAppStartTimer(Sender: TObject);
begin
  {$IFDEF LOG}
  LOG(LOGTAG, 'TfrmSDConsole.tmrAppStartTimer begin');
  {$ENDIF}
  if nil <> Sender then
  begin
    if Sender is TTimer then
    begin
      TTimer(Sender).Enabled := false;
      TTimer(Sender).OnTimer := nil;
    end;
    if Sender = fFormSDConsoleData.AppStartTimer then
    begin
      fFormSDConsoleData.AppStartTimer.Enabled := false;
      fFormSDConsoleData.AppStartTimer.OnTimer := nil;
    end;
  end;
  // initialize
  fFormSDConsoleData.FrameDealItemViewer := TfmeDealItemViewer.Create(Self);
  fFormSDConsoleData.FrameDealItemViewer.App := Self.App;
  fFormSDConsoleData.FrameDealItemViewer.Parent := Self.pnlLeft;
  fFormSDConsoleData.FrameDealItemViewer.Align := alClient;
  fFormSDConsoleData.FrameDealItemViewer.Visible := True;
  fFormSDConsoleData.FrameDealItemViewer.Initialize;
  fFormSDConsoleData.FrameDealItemViewer.OnDealItemChange := DoNotifyDealItemChange;

  tsfunc.Tabs.Clear;

  fFormSDConsoleData.FrameDataViewer := TfmeDataViewer.Create(Self);
  fFormSDConsoleData.FrameDataViewer.App := Self.App;
  fFormSDConsoleData.FrameDataViewer.Initialize;
  tsfunc.Tabs.AddObject('DayDataView', fFormSDConsoleData.FrameDataViewer);                                               
  {$IFDEF WIND} 
  fFormSDConsoleData.FrameWindData := TfmeWindData.Create(Self);
  fFormSDConsoleData.FrameWindData.App := Self.App;
  fFormSDConsoleData.FrameWindData.Initialize;
  tsfunc.Tabs.AddObject('WINDData', fFormSDConsoleData.FrameWindData);  
  {$ENDIF}
  {$IFDEF TDX} 
  fFormSDConsoleData.FrameTDXData := TfmeTDXData.Create(Self);
  fFormSDConsoleData.FrameTDXData.App := Self.App;
  fFormSDConsoleData.FrameTDXData.Initialize;
  tsfunc.Tabs.AddObject('TDXData', fFormSDConsoleData.FrameTDXData);
  {$ENDIF}
  {$IFDEF DEALINSTANT}
  fFormSDConsoleData.FrameStockInstant := TfmeStockInstant.Create(Self);
  fFormSDConsoleData.FrameStockInstant.App := Self.App;
  fFormSDConsoleData.FrameStockInstant.Initialize;
  tsfunc.Tabs.AddObject('StockInstant', fFormSDConsoleData.FrameStockInstant);
  {$ENDIF}
  fFormSDConsoleData.FrameDataRepair := TfmeDataRepair.Create(Self);
  fFormSDConsoleData.FrameDataRepair.App := Self.App;
  fFormSDConsoleData.FrameDataRepair.Initialize;
  tsfunc.Tabs.AddObject('DataRepair', fFormSDConsoleData.FrameDataRepair);
                              
  {$IFDEF DEALCLASS}
  fFormSDConsoleData.FrameStockClass := TfmeStockClass.Create(Self);
  fFormSDConsoleData.FrameStockClass.App := Self.App;
  //fFormSDConsoleData.FrameStockClass.Initialize;
  tsfunc.Tabs.AddObject('StockClass', fFormSDConsoleData.FrameStockClass);
  {$ENDIF}

  fFormSDConsoleData.FrameStockInfo := TfmeStockInfo.Create(Self);
  fFormSDConsoleData.FrameStockInfo.App := Self.App;
  //fFormSDConsoleData.FrameStockInfo.Initialize;
  tsfunc.Tabs.AddObject('StockInfo', fFormSDConsoleData.FrameStockInfo);


  tsfunc.TabIndex := 0;
  {$IFDEF LOG}
  LOG(LOGTAG, 'TfrmSDConsole.tmrAppStartTimer end');
  {$ENDIF}
end;

procedure TfrmSDConsole.tsfuncChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
var
  tmpFrame: TfmeBase;  
begin
  inherited;
  {$IFDEF LOG}
  LOG(LOGTAG, 'TfrmSDConsole.tsfuncChange begin');
  {$ENDIF}
  if 0 <= tsfunc.TabIndex then
  begin
    tmpFrame := TfmeBase(tsfunc.Tabs.Objects[tsfunc.TabIndex]);
    tmpFrame.CallDeactivate;
    tmpFrame.Visible := false;
  end;
  tmpFrame := TfmeBase(tsfunc.Tabs.Objects[NewTab]);            
  tmpFrame.Parent := Self.pnlMain;
  tmpFrame.Align := alClient;
  tmpFrame.Visible := true;
  tmpFrame.CallActivate;
  {$IFDEF LOG}
  LOG(LOGTAG, 'TfrmSDConsole.tsfuncChange end');
  {$ENDIF}
end;

procedure TfrmSDConsole.DoNotifyDealItemChange(ADealItem: PRT_DealItem);   
var
  tmpFrame: TfmeBase;
begin
  {$IFDEF LOG}
  //LOG(LOGTAG, 'TfrmSDConsole.DoNotifyDealItemChange begin');
  {$ENDIF}
  if 0 <= tsfunc.TabIndex then
  begin
    tmpFrame := TfmeBase(tsfunc.Tabs.Objects[tsfunc.TabIndex]);
    if tmpFrame = fFormSDConsoleData.FrameDataViewer then
    begin
      fFormSDConsoleData.FrameDataViewer.NotifyDealItem(ADealItem);
      exit;
    end;
    if tmpFrame = fFormSDConsoleData.FrameDataRepair then
    begin
      fFormSDConsoleData.FrameDataRepair.NotifyDealItem(ADealItem);
      Exit;
    end;
    {$IFDEF DEALCLASS}
    if tmpFrame = fFormSDConsoleData.FrameStockClass then
    begin
      fFormSDConsoleData.FrameStockClass.NotifyDealItem(ADealItem);
      exit;
    end;
    {$ENDIF}
    if tmpFrame = fFormSDConsoleData.FrameStockInfo then
    begin
      fFormSDConsoleData.FrameStockInfo.NotifyDealItem(ADealItem);
      exit;
    end;
    {$IFDEF TDX}
    if tmpFrame = fFormSDConsoleData.FrameTDXData then
    begin
      fFormSDConsoleData.FrameTDXData.NotifyDealItem(ADealItem);
      exit;
    end;
    {$ENDIF}
  end;
  {$IFDEF LOG}
  //LOG(LOGTAG, 'TfrmSDConsole.DoNotifyDealItemChange end');
  {$ENDIF}
end;

end.
