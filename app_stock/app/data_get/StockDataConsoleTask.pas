unit StockDataConsoleTask;

interface

uses
  Windows,        
  define_datasrc,
  define_dealItem,
  win.process;

type                 
  TRT_TaskType = (
    taskUnknown,
    taskDayData,
    taskDetailData);
    
  TRT_TaskStatus = (
    // ---------------------
    taskUnAssigned, // 未分配
    taskAssigned,   // 已分配 未开始
    taskInit,       // 初始化
    taskReady,      // 初始化完成 未开始         
    // ---------------------
    taskPause,     // 运行中 暂停
    taskRunning,   // 运行中
    taskError,     // 运行中 出错    
    // ---------------------
    taskEnd,     // 结束 成功
    taskFail     // 结束 出错
    // ---------------------
  );  
//const
//  TaskStatus_UnAssigned = 1;   // 未分配
//  TaskStatus_Assigned   = 100; // 已分配 未开始
//  TaskStatus_Active     = 101; // 开始
//  TaskStatus_Suspend    = 102; // 暂停
//  TaskStatus_Fail       = 103; // 失败
//  TaskStatus_End        = 104; // 结束

  PDownloadProcessTask= ^TDownloadProcessTask;   
  PDownloadTask       = ^TDownloadTask;
  
  TDownloadProcessTask= packed record  
    DownloadProcess   : TRT_OwnedProcess;
    CommandWndProc    : TFNWndProc; 
    OwnerTask         : PDownloadTask;    
    TaskStatus        : TRT_TaskStatus;
    TaskIncrement     : Byte;    
    // monitor if task process is crashed
    MonitorTick       : DWORD;
    MonitorDealItem   : PRT_DealItem;

    DealItemIndex     : Integer;
    DealItem          : PRT_DealItem;
    ExecuteCount      : integer;
  end;
  
  TDownloadTask       = packed record
    TaskID            : Integer;
    TaskDataSrc       : TDealDataSource; 
    TaskDealItemCode  : Integer;
    TaskDataDB        : Integer;    
    TaskDataType      : TRT_TaskType;
    TaskStatus        : TRT_TaskStatus;

    DownProcessTaskArray: array[0..2] of TDownloadProcessTask;
                              
    CurrentDataDB     : Integer;
  end;
  
implementation

(*
  任何 事情 都有 状态点
    Active

    BeforeEnter-Active // 请求准备进入状态点
    AfterEnter-Active  // 进入状态点
    Status-Active
    BeforeExit-Active  // 请求准备退出状态点
    AfterExit-Active   // 退出状态点
*)
end.
