unit StockDataAutoDownloadApp;

interface

uses
  Sysutils, Forms, BaseApp,
  define_datasrc,
  StockDataConsoleTask,
  StockDataTaskConsole;
  
type
  TConsoleAppData = record   
    DownloadAllTasks: array[TDealDataSource] of PDownloadTask;
    StockTaskConsole: TStockTaskConsole;
  end;
  
  TStockDataAutoDownloadConsoleApp = class(BaseApp.TBaseAppAgent)
  protected                       
    fConsoleAppData: TConsoleAppData;   
    procedure RunDownload;
    procedure RunDownloadTask(ADataSrc: TDealDataSource);
  public
    constructor Create(AHostApp: TBaseApp); override;
    destructor Destroy; override;     
    function Initialize: Boolean; override;
    procedure Run; override;     
    function CreateAppCommandWindow: Boolean;  
    property TaskConsole: TStockTaskConsole read fConsoleAppData.StockTaskConsole write fConsoleAppData.StockTaskConsole;
  end;
  
implementation

uses
  Windows,    
  {$IFDEF LOG}
  UtilsLog,
  {$ENDIF}
  windef_msg,
  BaseWinApp,
  define_stockapp,
  define_StockDataApp,
  BaseStockFormApp;
           
{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDataAutoDownloadApp.pas';
{$ENDIF}
var
  G_StockAutoConsoleApp: TStockDataAutoDownloadConsoleApp = nil;
  
function AppCommandWndProcW(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
var
  tmpProcessTask: PDownloadProcessTask;
  tmpDownloadTask: PDownloadTask;
  tmpTaskDataType: TRT_TaskType;
  tmpStockCode: integer;
  tmpDataSrc: integer;
begin
  Result := 0;
  case AMsg of
    WM_AppStart: begin
      if nil <> G_StockAutoConsoleApp then
      begin
        G_StockAutoConsoleApp.RunDownload;
      end;
    end;
    WM_AppRequestEnd: begin
      if nil <> GlobalBaseStockApp then
      begin
        GlobalBaseStockApp.Terminate;
      end;
    end;
    WM_Console_Command_Download: begin
       tmpStockCode := 0;
       tmpDataSrc := 0;
       tmpTaskDataType := taskUnknown;
       if 0 = wParam then
       begin
         exit;
       end;
       if 1 = wParam then
       begin
         tmpDownloadTask := PDownloadTask(lParam);
         tmpTaskDataType := tmpDownloadTask.TaskDataType;  
         tmpDataSrc := GetDealDataSourceCode(tmpDownloadTask.TaskDataSrc);
         tmpStockCode := tmpDownloadTask.TaskDealItemCode;
       end;
       if 2 = wParam then
       begin
         tmpProcessTask := PDownloadProcessTask(lparam);
         tmpDownloadTask := tmpProcessTask.OwnerTask;
         if nil <> tmpDownloadTask then
         begin
           tmpTaskDataType := tmpDownloadTask.TaskDataType;
           tmpDataSrc := GetDealDataSourceCode(tmpDownloadTask.TaskDataSrc);
         end;
         if nil <> tmpProcessTask.DealItem then
           tmpStockCode := tmpProcessTask.DealItem.iCode;
       end;
       if nil <> G_StockAutoConsoleApp then
       begin
         if (nil <> G_StockAutoConsoleApp.TaskConsole) then
         begin
           tmpDownloadTask := G_StockAutoConsoleApp.TaskConsole.CheckOutDownloadTask(tmpTaskDataType, GetDealDataSource(tmpDataSrc), tmpStockCode);
           if TaskAssigned = tmpDownloadTask.TaskStatus then
           begin
             G_StockAutoConsoleApp.TaskConsole.RunTask(tmpDownloadTask);
           end;
         end;
       end;
    end;
    WM_RequestHostCommandWnd: begin
      tmpProcessTask := G_StockAutoConsoleApp.TaskConsole.GetDownloadTaskByProcessID(wParam);
      if nil <> tmpProcessTask then
      begin
        Result := tmpProcessTask.DownloadProcess.HostCmdWnd.CmdWndHandle;
      end;
    end;
    WM_ConsoleProcess2Console_Download_Finished: begin   
      tmpDownloadTask := PDownloadTask(wParam);
      if src_163 = tmpDownloadTask.TaskDataSrc then
      begin
        if taskEnd =  tmpDownloadTask.TaskStatus then
        begin
          if nil <> G_StockAutoConsoleApp then
          begin
            G_StockAutoConsoleApp.RunDownloadTask(src_sina);
          end;
        end;
      end;        
      if src_sina = tmpDownloadTask.TaskDataSrc then
      begin
        if taskEnd =  tmpDownloadTask.TaskStatus then
        begin
          PostMessage(AWnd, WM_AppRequestEnd, 0, 0);
        end;
      end;
    end;
    WM_AppNotifyOS: begin
    end;
    else    
      Result := DefWindowProcW(AWnd, AMsg, wParam, lParam);
      //Result :=
      //BusinessCommandWndProcA(nil, AWnd, AMsg, wParam, lParam);
  end;
end;
                
{ TStockDataAutoDownloadConsoleApp }

constructor TStockDataAutoDownloadConsoleApp.Create(AHostApp: TBaseApp);
begin
  inherited;   
  G_StockAutoConsoleApp := Self;
  FillChar(fConsoleAppData, SizeOf(fConsoleAppData), 0);
  AHostApp.AppAgent := Self;
end;

destructor TStockDataAutoDownloadConsoleApp.Destroy;
begin
  if G_StockAutoConsoleApp = Self then
  begin
    G_StockAutoConsoleApp := nil;
  end;
  if nil <> fConsoleAppData.StockTaskConsole then
  begin
    FreeAndNil(fConsoleAppData.StockTaskConsole);
  end;
  inherited;
end;

function TStockDataAutoDownloadConsoleApp.Initialize: Boolean;
begin
  Result := inherited Initialize;     
  {$IFDEF LOG}
  LOG(LOGTAG, 'TStockDataConsoleApp.Initialize');
  {$ENDIF}
  if Result then
  begin
    Application.Initialize;  
    Result := CreateAppCommandWindow;
    if nil = fConsoleAppData.StockTaskConsole then
    begin
      fConsoleAppData.StockTaskConsole := TStockTaskConsole.Create;
      fConsoleAppData.StockTaskConsole.StockItemDB := TBaseStockApp(fBaseAppAgentData.HostApp).StockItemDB;
      fConsoleAppData.StockTaskConsole.StockIndexDB := TBaseStockApp(fBaseAppAgentData.HostApp).StockIndexDB;   
      fConsoleAppData.StockTaskConsole.AppWindow := TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow;         
    end;
  end;                    
  {$IFDEF LOG}
  LOG(LOGTAG, 'TStockDataConsoleApp.Initialize end');
  {$ENDIF}
end;

procedure TStockDataAutoDownloadConsoleApp.RunDownloadTask(ADataSrc: TDealDataSource);
begin
  if nil = fConsoleAppData.DownloadAllTasks[ADataSrc] then
  begin
    fConsoleAppData.DownloadAllTasks[ADataSrc] := fConsoleAppData.StockTaskConsole.NewDownloadTask(taskDayData, ADataSrc, 0);
    if IsWindow(TBaseWinApp(HostApp).AppWindow) then
      PostMessage(TBaseWinApp(HostApp).AppWindow, WM_Console_Command_Download, 1, Integer(fConsoleAppData.DownloadAllTasks[ADataSrc]));
  end;
end;

procedure TStockDataAutoDownloadConsoleApp.RunDownload;

  function CheckTask(ADownloadTask: PDownloadTask): integer;
  var
    exitcode_process: DWORD;
    tick_gap: DWORD;
    waittime: DWORD;
  begin
    Result := 0;
    if nil = ADownloadTask then
      exit;
    Result := 1;
    if TaskEnd = ADownloadTask.TaskStatus then
    begin
      Result := 0;
    end else
    begin
      Windows.GetExitCodeProcess(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle, exitcode_process);
      if Windows.STILL_ACTIVE <> exitcode_process then
      begin
        ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle := 0;
        ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessId := 0;
        ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd := 0;
        // 这里下载进程 shutdown 了
        if taskRunning = ADownloadTask.TaskStatus then
        begin
          if nil = ADownloadTask.DownProcessTaskArray[0].DealItem then
          begin
            ADownloadTask.DownProcessTaskArray[0].DealItem := fConsoleAppData.StockTaskConsole.Console_GetNextDownloadDealItem(@ADownloadTask.DownProcessTaskArray[0]);
          end;
          if nil <> ADownloadTask.DownProcessTaskArray[0].DealItem then
          begin                   
            fConsoleAppData.StockTaskConsole.RunProcessTask(@ADownloadTask.DownProcessTaskArray[0]);
          end;
        end;
      end else
      begin        
        if ADownloadTask.DownProcessTaskArray[0].MonitorDealItem <> ADownloadTask.DownProcessTaskArray[0].DealItem then
        begin
          ADownloadTask.DownProcessTaskArray[0].MonitorDealItem := ADownloadTask.DownProcessTaskArray[0].DealItem;
          ADownloadTask.DownProcessTaskArray[0].MonitorTick := GetTickCount;
        end else
        begin               
          //Log('SDConsoleForm.pas', 'tmrRefreshDownloadTaskTimer CheckTask TerminateProcess Wnd :' +
          //        IntToStr(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd));
                  
          if 0 = ADownloadTask.DownProcessTaskArray[0].MonitorTick then
          begin
            ADownloadTask.DownProcessTaskArray[0].MonitorTick := GetTickCount;
          end else
          begin
            tick_gap := GetTickCount - ADownloadTask.DownProcessTaskArray[0].MonitorTick;
            waittime := 10 * 1000;
            case ADownloadTask.TaskDataSrc of
              src_all : waittime := 30 * 1000;
              src_ctp: waittime := 10 * 1000;
              src_offical: waittime := 10 * 1000;
              src_tongdaxin: waittime := 10 * 1000;
              src_tonghuasun: waittime := 10 * 1000;
              src_dazhihui: waittime := 10 * 1000;
              src_sina: waittime := 3 * 60 * 1000;
              src_163: waittime := 25 * 1000;
              src_qq: waittime := 20 * 1000;
              src_xq: waittime := 20 * 1000;
            end;
            if tick_gap > waittime then
            begin                    
              //Log('SDConsoleForm.pas', 'tmrRefreshDownloadTaskTimer CheckTask TerminateProcess Wnd :' +
              //    IntToStr(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd));
              TerminateProcess(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle, 0);
              ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle := 0;
              ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessId := 0;
              ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd := 0;
            end;
          end;
        end;
      end;
    end;
  end;

begin
  RunDownloadTask(Src_163);
//  Sleep(1000);
//  if nil = fConsoleAppData.DownloadAllTasks[Src_Sina] then
//  begin
//    fConsoleAppData.DownloadAllTasks[Src_Sina] := fConsoleAppData.StockTaskConsole.NewDownloadTask(taskDayData, Src_Sina, 0);
//    if IsWindow(TBaseWinApp(HostApp).AppWindow) then
//      PostMessage(TBaseWinApp(HostApp).AppWindow, WM_Console_Command_Download, 1, Integer(@fConsoleAppData.DownloadAllTasks[Src_Sina]));
//  end;
end;

procedure TStockDataAutoDownloadConsoleApp.Run;
begin
  PostMessage(TBaseWinApp(fBaseAppAgentData.HostApp).AppWindow, WM_AppStart, 0, 0);
  TBaseWinApp(fBaseAppAgentData.HostApp).RunAppMsgLoop;
end;

function TStockDataAutoDownloadConsoleApp.CreateAppCommandWindow: Boolean;
var
  tmpRegWinClass: TWndClassW;
  tmpGetWinClass: TWndClassW;
  tmpIsReged: Boolean;
begin
  Result := false;          
  FillChar(tmpRegWinClass, SizeOf(tmpRegWinClass), 0);
  tmpRegWinClass.hInstance := HInstance;
  tmpRegWinClass.lpfnWndProc := @AppCommandWndProcW;
  tmpRegWinClass.lpszClassName := AppCmdWndClassName_StockDataDownloader_Console;
  tmpIsReged := GetClassInfoW(HInstance, tmpRegWinClass.lpszClassName, tmpGetWinClass);
  if tmpIsReged then
  begin
    if (tmpGetWinClass.lpfnWndProc <> tmpRegWinClass.lpfnWndProc) then
    begin                           
      UnregisterClassW(tmpRegWinClass.lpszClassName, HInstance);
      tmpIsReged := false;
    end;
  end;
  if not tmpIsReged then
  begin
    if 0 = RegisterClassW(tmpRegWinClass) then
      exit;
  end;
  TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow := CreateWindowExW(
    WS_EX_TOOLWINDOW
    //or WS_EX_APPWINDOW
    //or WS_EX_TOPMOST
    ,
    tmpRegWinClass.lpszClassName,
    '', WS_POPUP {+ 0},
    0, 0, 0, 0,
    HWND_MESSAGE, 0, HInstance, nil);
  Result := Windows.IsWindow(TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow);
end;

end.
