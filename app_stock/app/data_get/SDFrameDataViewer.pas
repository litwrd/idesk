unit SDFrameDataViewer;

interface

uses
  Windows, Messages, Forms, BaseFrame, StdCtrls, Controls, ComCtrls, Classes, ExtCtrls,
  SysUtils, VirtualTrees, DealItemsTreeView,
  define_datasrc, define_StockDataApp, define_price, define_datetime, define_dealitem,
  define_stock_quotes, define_dealstore_file,
  BaseWinApp, StockDayDataAccess, StockDetailDataAccess, StockDataConsoleTask;

type
  TfmeDataViewerData = record
    DownloadAllTasks: array[TDealDataSource] of PDownloadTask;
    DayDataAccess: StockDayDataAccess.TStockDayDataAccess;
    DetailDataAccess: TStockDetailDataAccess;  
    DayDataSrc: TDealDataSource;
    WeightMode: TRT_WeightMode;
    DealItem: PRT_DealItem;
    OnGetDealItem: TOnDealItemFunc;
  end;
  
  TfmeDataViewer = class(TfmeBase)
    pnlRight: TPanel;
    mmo1: TMemo;
    pnlMiddle: TPanel;
    vtDayDatas: TVirtualStringTree;
    spl1: TSplitter;
    tmrRefreshDownloadTask: TTimer;
    vtTasks: TVirtualStringTree;
    vtDetailDatas: TVirtualStringTree;
    pnlBottom: TPanel;
    Label1: TLabel;
    cmbDataSrc: TComboBoxEx;
    btnDownload: TButton;
    chkShutDown: TCheckBox;
    cmbDataType: TComboBoxEx;
    cmbDetailDataSrc: TComboBoxEx;
    procedure btnDownloadClick(Sender: TObject);
    procedure tmrRefreshDownloadTaskTimer(Sender: TObject);
    procedure cmbDataSrcChange(Sender: TObject);
  protected
    fDataViewerData: TfmeDataViewerData;                 
    procedure SetDayDataSrc(AValue: TDealDataSource);
    function GetDataSrc(ADataSrc: string = ''): TDealDataSource;
    function GetStockCode: integer;
    function NewDownloadAllTask(ATaskDataType: TRT_TaskType; ADataSrc: TDealDataSource; ADownloadTask: PDownloadTask): PDownloadTask;
    procedure RequestDownloadStockData(ADownloadTask: PDownloadTask);
    procedure RequestDownloadProcessTaskStockData(AProcessTask: PDownloadProcessTask);

    procedure LoadDayDataTreeView(AStockItem: PRT_DealItem);         
    procedure DayDataTreeViewInitialize;
    procedure vtDayDatasGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);  
    procedure vtDayDatasChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
      
    procedure TaskTreeViewInitialize;
    procedure AddTaskNode(ADownloadTask: PDownloadTask); 
    procedure vtTasksGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
                               
    procedure DetailDataTreeViewInitialize;
    procedure vtDetailDatasGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure LoadDetailTreeViewData;     
  public
    constructor Create(Owner: TComponent); override;
    procedure Initialize; override;
    procedure NotifyDealItem(ADealItem: PRT_DealItem);
    property OnGetDealItem: TOnDealItemFunc read fDataViewerData.OnGetDealItem write fDataViewerData.OnGetDealItem;
  end;
        
implementation

{$R *.dfm}

uses
  win.shutdown,
  define_deal,
  //UtilsLog,
  BaseStockFormApp,
  StockDataConsoleApp,
  StockDetailData_Load,
  StockDayData_Load;
              
{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDFrameDataViewer.pas';
{$ENDIF}

type
  PStockDayDataNode = ^TStockDayDataNode;
  TStockDayDataNode = record
    DayIndex: integer;
    QuoteData: PRT_Quote_Day;
  end;
       
  PStockDetailDataNode = ^TStockDetailDataNode;
  TStockDetailDataNode = record
    QuoteData: PRT_Quote_Detail;
  end;
                     
  PTaskDataNode = ^TTaskDataNode;
  TTaskDataNode = record
    DownloadTask: PDownloadTask;
    DownProcessTask: PDownloadProcessTask;
  end;

constructor TfmeDataViewer.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fDataViewerData, SizeOf(fDataViewerData), 0);
end;

procedure TfmeDataViewer.Initialize;
begin
  cmbDataSrc.Clear;
  cmbDataSrc.Items.Add(GetDataSrcName(src_163));
  cmbDataSrc.Items.Add(GetDataSrcName(src_sina));
  cmbDataSrc.Items.Add(GetDataSrcName(src_all));
  cmbDataSrc.ItemIndex := 0;

  cmbDetailDataSrc.Clear;     
  cmbDetailDataSrc.Items.Add(GetDataSrcName(src_163));
  cmbDetailDataSrc.Items.Add(GetDataSrcName(src_sina));
  cmbDetailDataSrc.ItemIndex := 0;

  cmbDataType.Clear;
  cmbDataType.Items.Add('Day Data');
  cmbDataType.Items.Add('Detail Data');
  cmbDataType.ItemIndex := 0;

  DayDataTreeViewInitialize;
  TaskTreeViewInitialize;
  DetailDataTreeViewInitialize;
end;

procedure TfmeDataViewer.NotifyDealItem(ADealItem: PRT_DealItem);
begin
  LoadDayDataTreeView(ADealItem);
end;

function TfmeDataViewer.GetStockCode: integer;
var
  tmpCode: string;
  tmpPos: integer;
begin
  Result := 0;
  tmpCode := Trim(cmbDataSrc.Text);
  tmpPos := Pos(':', tmpCode);
  if 0 < tmpPos then
  begin
    tmpCode := Copy(tmpCode, tmpPos + 1, maxint);
    Result := StrToIntDef(tmpCode, 0);
  end;
end;

function TfmeDataViewer.GetDataSrc(ADataSrc: string = ''): TDealDataSource;
var
  s: string;     
  tmpName: string;
  tmpSrc: TDealDataSource;
begin
  Result := src_unknown;
  s := ADataSrc;
  if '' = s then
    s := lowercase(Trim(cmbDataSrc.Text));  
  if '' = s then
  begin
    if 0 < cmbDataSrc.ItemIndex then
      s := Trim(cmbDataSrc.Items[cmbDataSrc.ItemIndex]);
  end;
  if '' <> s then
  begin
    for tmpSrc := Low(TDealDataSource) to High(TDealDataSource) do
    begin             
      tmpName := lowercase(GetDataSrcName(tmpSrc));
      if '' <> tmpName then
      begin
        if 0 < Pos(tmpName, s) then
        begin
          Result := tmpSrc;
          Break;
        end;
      end;
    end;
  end;
end;

procedure TfmeDataViewer.RequestDownloadProcessTaskStockData(AProcessTask: PDownloadProcessTask);
begin
  TStockDataConsoleApp(App.AppAgent).TaskConsole.RunProcessTask(AProcessTask);
end;

type   
  TDayColumns = (
    colIndex,
    colDate, colOpen, colClose, colHigh, colLow,
    colDayVolume, colDayAmount,
    colWeight
  );
                  
const  
  DayColumnsText: array[TDayColumns] of String = (
    'Index', '日期',
    '开盘', '收盘', '最高', '最低',
    '成交量', '成交金额', '权重'
  );
              
  DayColumnsWidth: array[TDayColumns] of integer = (
    60, 0,
    0, 0, 0, 0,
    0, 0, 0
  );
                                              
procedure TfmeDataViewer.TaskTreeViewInitialize;
var   
  tmpCol: TVirtualTreeColumn;
begin
  vtTasks.NodeDataSize := SizeOf(TTaskDataNode);   
  vtTasks.Header.Options := [hoColumnResize, hoAutoResize, hoVisible];     
  vtTasks.Header.Columns.Clear;
  vtTasks.OnGetText := vtTasksGetText;
  
  tmpCol := vtTasks.Header.Columns.Add;
  tmpCol.Text := '下载任务';
end;

procedure TfmeDataViewer.DayDataTreeViewInitialize;
var
  col_day: TDayColumns;
  tmpCol: TVirtualTreeColumn;
begin
  vtDayDatas.NodeDataSize := SizeOf(TStockDayDataNode);  
  vtDayDatas.Header.Options := [hoColumnResize, hoVisible];
  vtDayDatas.Header.Columns.Clear;

  vtDayDatas.TreeOptions.SelectionOptions := [toFullRowSelect];
  vtDayDatas.TreeOptions.AnimationOptions := [];
  vtDayDatas.TreeOptions.MiscOptions := [toAcceptOLEDrop,toFullRepaintOnResize,toInitOnSave,toToggleOnDblClick,toWheelPanning,toEditOnClick];
  vtDayDatas.TreeOptions.PaintOptions := [toShowButtons,toShowDropmark,{toShowRoot} toShowTreeLines,toThemeAware,toUseBlendedImages];
  vtDayDatas.TreeOptions.StringOptions := [toSaveCaptions,toAutoAcceptEditChange];

  for col_day := low(TDayColumns) to high(TDayColumns) do
  begin
    tmpCol := vtDayDatas.Header.Columns.Add;
    tmpCol.Text := DayColumnsText[col_day];
    case col_day of                           
      colIndex: tmpCol.Width := vtDayDatas.Canvas.TextWidth('2000');
      colDate: tmpCol.Width := vtDayDatas.Canvas.TextWidth('2016-12-12');
      colOpen: tmpCol.Width := vtDayDatas.Canvas.TextWidth('300000');
      colClose: tmpCol.Width := vtDayDatas.Canvas.TextWidth('300000');
      colHigh: tmpCol.Width := vtDayDatas.Canvas.TextWidth('300000');
      colLow: tmpCol.Width := vtDayDatas.Canvas.TextWidth('300000');
      colDayVolume: ;
      colDayAmount: ;
      colWeight: tmpCol.Width := vtDayDatas.Canvas.TextWidth('300000');
    end;
    if 0 <> tmpCol.Width then
    begin
      tmpCol.Width := tmpCol.Width + vtDayDatas.TextMargin + vtDayDatas.Indent;
    end else
    begin
      if 0 = DayColumnsWidth[col_day] then
      begin
        tmpCol.Width := 120;
      end else
      begin
        tmpCol.Width := DayColumnsWidth[col_day];
      end;
    end;
  end; 
  vtDayDatas.OnGetText := vtDayDatasGetText;
  vtDayDatas.OnChange := vtDayDatasChange;
  //vtDayDatas.OnChange := vtDayDatasChange;
end;

procedure TfmeDataViewer.vtDayDatasChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var      
  tmpNodeData: PStockDayDataNode;
  tmpDataSrc: TDealDataSource;
  tmpFileUrl: string;
begin
  inherited;
  //Log(LOGTAG, 'vtDayDatasChange:');
  if nil = Node then
    exit;
  tmpNodeData := Sender.GetNodeData(Node);
  if nil = tmpNodeData then
    exit;
  if nil = tmpNodeData.QuoteData then
    exit;
  tmpDataSrc := GetDataSrc(cmbDetailDataSrc.Text);
  if src_unknown = tmpDataSrc then
    exit;
  tmpFileUrl := TBaseStockApp(App).StockAppPath.GetFileUrl(
      fDataViewerData.DayDataAccess.StockItem.DBType,
      TStockDetailDataAccess.DataTypeDefine,
      GetDealDataSourceCode(tmpDataSrc),      
      tmpNodeData.QuoteData.DealDate.Value,
      fDataViewerData.DayDataAccess.StockItem);
      
  //Log(LOGTAG, 'vtDayDatasChange:' + tmpFileUrl);
  if not FileExists(tmpFileUrl) then
  begin
    tmpFileUrl := ChangeFileExt(tmpFileUrl, '.sdet');
  end;
  //tmpFileUrl := 'E:\StockApp\sdata\sdetsina\600000\600000_20151125.sdet';
  FreeAndNil(fDataViewerData.DetailDataAccess);
  if FileExists(tmpFileUrl) then
  begin                    
    //Log(LOGTAG, 'vtDayDatasChange:' + tmpFileUrl);
              
    if nil <> fDataViewerData.DetailDataAccess then
      FreeAndNil(fDataViewerData.DetailDataAccess);
    if nil = fDataViewerData.DetailDataAccess then
      fDataViewerData.DetailDataAccess := TStockDetailDataAccess.Create(fDataViewerData.DayDataAccess.StockItem, tmpDataSrc);

    fDataViewerData.DetailDataAccess.Clear;
    fDataViewerData.DetailDataAccess.StockItem := fDataViewerData.DayDataAccess.StockItem;
    fDataViewerData.DetailDataAccess.FirstDealDate := tmpNodeData.QuoteData.DealDate.Value;
  
    LoadStockDetailData(
      App,
      fDataViewerData.DetailDataAccess,
      tmpFileUrl);
  end;            
  LoadDetailTreeViewData;
end;

procedure TfmeDataViewer.vtDayDatasGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  tmpNodeData: PStockDayDataNode;
begin     
  CellText := '';    
  tmpNodeData := Sender.GetNodeData(Node);
  if nil <> tmpNodeData then
  begin
    if nil <> tmpNodeData.QuoteData then
    begin
      if Integer(colIndex) = Column then
      begin
        CellText := IntToStr(Node.Index);
        exit;
      end;
      if Integer(colDate) = Column then
      begin
        CellText := FormatDateTime('yyyymmdd', tmpNodeData.QuoteData.DealDate.Value);
        exit;
      end;
      if Integer(colOpen) = Column then
      begin         
        CellText := IntToStr(tmpNodeData.QuoteData.PriceRange.PriceOpen.Value);
        exit;
      end;
      if Integer(colClose) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.PriceRange.PriceClose.Value);
        exit;
      end;
      if Integer(colHigh) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.PriceRange.PriceHigh.Value);
        exit;
      end;
      if Integer(colLow) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.PriceRange.PriceLow.Value);
        exit;
      end;
      if Integer(colDayVolume) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.DealVolume);
        exit;
      end;
      if Integer(colDayAmount) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.DealAmount);
        exit;
      end;
      if Integer(colWeight) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.Weight.Value);
        exit;
      end;
    end;
  end;
end;
                      
procedure TfmeDataViewer.LoadDayDataTreeView(AStockItem: PRT_DealItem);
var
  i: integer;       
  tmpStockDataNode: PStockDayDataNode;
  tmpStockData: PRT_Quote_Day;
  tmpNode: PVirtualNode;
  tmpDataSrc: TDealDataSource;
begin
  if nil = AStockItem then
    exit;
  vtDayDatas.BeginUpdate;
  try
    vtDayDatas.Clear;
    if nil = AStockItem then
      exit;
    //fFormSDConsoleData.DayDataAccess := AStockItem.StockDayDataAccess;
    tmpDataSrc := GetDataSrc;
    if src_unknown = tmpDataSrc then
      exit;
    if nil <> fDataViewerData.DayDataAccess then
    begin
      FreeAndNil(fDataViewerData.DayDataAccess);
    end;
    if nil = fDataViewerData.DayDataAccess then
    begin
      if src_163 = tmpDataSrc then
        fDataViewerData.WeightMode := weightNone;
      if src_sina = tmpDataSrc then
        fDataViewerData.WeightMode := weightBackward;
      fDataViewerData.DayDataAccess := TStockDayDataAccess.Create(AStockItem, tmpDataSrc, fDataViewerData.WeightMode);
      LoadStockDayData(App, fDataViewerData.DayDataAccess);
    end;
    //fDataViewerData.Rule_BDZX_Price := AStockItem.Rule_BDZX_Price;
    //fDataViewerData.Rule_CYHT_Price := AStockItem.Rule_CYHT_Price;

    for i := fDataViewerData.DayDataAccess.RecordCount - 1 downto 0 do
    begin
      tmpStockData := fDataViewerData.DayDataAccess.RecordItem[i];
      tmpNode := vtDayDatas.AddChild(nil);
      tmpStockDataNode := vtDayDatas.GetNodeData(tmpNode);
      tmpStockDataNode.QuoteData := tmpStockData;
      tmpStockDataNode.DayIndex := i;
    end;
  finally
    vtDayDatas.EndUpdate;
  end;
end;

procedure TfmeDataViewer.RequestDownloadStockData(ADownloadTask: PDownloadTask);
begin
  if IsWindow(TBaseWinApp(App).AppWindow) then
  begin
    PostMessage(TBaseWinApp(App).AppWindow, WM_Console_Command_Download, 1, Integer(ADownloadTask));
  end;                        
  if not tmrRefreshDownloadTask.Enabled then
    tmrRefreshDownloadTask.Enabled := True;
end;
  
procedure TfmeDataViewer.tmrRefreshDownloadTaskTimer(Sender: TObject);

  function CheckTask(ADownloadTask: PDownloadTask): integer;
  var
    exitcode_process: DWORD;
    tick_gap: DWORD;
    waittime: DWORD;
  begin
    Result := 0;
    if nil = ADownloadTask then
      exit;
    Result := 1;
    if TaskEnd = ADownloadTask.TaskStatus then
    begin
      Result := 0;
    end else
    begin
      Windows.GetExitCodeProcess(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle, exitcode_process);
      if Windows.STILL_ACTIVE <> exitcode_process then
      begin
        ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle := 0;
        ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessId := 0;
        ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd := 0;
        // 这里下载进程 shutdown 了
        if taskRunning = ADownloadTask.TaskStatus then
        begin
          if nil = ADownloadTask.DownProcessTaskArray[0].DealItem then
          begin
            ADownloadTask.DownProcessTaskArray[0].DealItem := TStockDataConsoleApp(App.AppAgent).TaskConsole.Console_GetNextDownloadDealItem(@ADownloadTask.DownProcessTaskArray[0]);
          end;
          if nil <> ADownloadTask.DownProcessTaskArray[0].DealItem then
          begin
            RequestDownloadProcessTaskStockData(@ADownloadTask.DownProcessTaskArray[0]);
          end;
        end;
      end else
      begin        
        if ADownloadTask.DownProcessTaskArray[0].MonitorDealItem <> ADownloadTask.DownProcessTaskArray[0].DealItem then
        begin
          ADownloadTask.DownProcessTaskArray[0].MonitorDealItem := ADownloadTask.DownProcessTaskArray[0].DealItem;
          ADownloadTask.DownProcessTaskArray[0].MonitorTick := GetTickCount;
        end else
        begin               
          //Log('SDConsoleForm.pas', 'tmrRefreshDownloadTaskTimer CheckTask TerminateProcess Wnd :' +
          //        IntToStr(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd));
                  
          if 0 = ADownloadTask.DownProcessTaskArray[0].MonitorTick then
          begin
            ADownloadTask.DownProcessTaskArray[0].MonitorTick := GetTickCount;
          end else
          begin
            tick_gap := GetTickCount - ADownloadTask.DownProcessTaskArray[0].MonitorTick;
            waittime := 10 * 1000;
            case ADownloadTask.TaskDataSrc of
              src_all : waittime := 30 * 1000;
              src_ctp: waittime := 10 * 1000;
              src_offical: waittime := 10 * 1000;
              src_tongdaxin: waittime := 10 * 1000;
              src_tonghuasun: waittime := 10 * 1000;
              src_dazhihui: waittime := 10 * 1000;
              src_sina: waittime := 3 * 60 * 1000;
              src_163: waittime := 25 * 1000;
              src_qq: waittime := 20 * 1000;
              src_xq: waittime := 20 * 1000;
            end;
            if tick_gap > waittime then
            begin                    
              //Log('SDConsoleForm.pas', 'tmrRefreshDownloadTaskTimer CheckTask TerminateProcess Wnd :' +
              //    IntToStr(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd));
              TerminateProcess(ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle, 0);
              ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessHandle := 0;
              ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.ProcessId := 0;
              ADownloadTask.DownProcessTaskArray[0].DownloadProcess.Core.AppCmdWnd := 0;
            end;
          end;
        end;
      end;
    end;
  end;
  
var
  tmpActiveCount: integer;
begin
  inherited;
  tmpActiveCount := CheckTask(fDataViewerData.DownloadAllTasks[src_163]);
  tmpActiveCount := tmpActiveCount + CheckTask(fDataViewerData.DownloadAllTasks[src_sina]);
  vtTasks.Invalidate;
//  tmpActiveCount := tmpActiveCount + CheckTask(fFormSDConsoleData.DownloadQQAllTask, edtStockQQ);
//  tmpActiveCount := tmpActiveCount + CheckTask(fFormSDConsoleData.DownloadXQAllTask, edtStockXQ);
  if 0 = tmpActiveCount then
  begin
    if chkShutDown.Checked then
      win.shutdown.ShutDown;
  end;
end;

procedure TfmeDataViewer.btnDownloadClick(Sender: TObject);
var
  tmpCode: integer;
  tmpSrc: TDealDataSource;
  tmpTask: TDownloadTask;
  tmpTaskType: TRT_TaskType;
begin    
  if 0 > cmbDataType.ItemIndex then
    exit;
  tmpTaskType := taskUnknown;    
  if 0 = cmbDataType.ItemIndex then
    tmpTaskType := taskDayData;
  if 1 = cmbDataType.ItemIndex then
    tmpTaskType := taskDetailData;
  if taskUnknown = tmpTaskType then
    exit;
    
  tmpCode := GetStockCode;
  tmpSrc := GetDataSrc;
  if src_Unknown <> tmpSrc then
  begin
    if 0 <> tmpCode then
    begin
      FillChar(tmpTask, SizeOf(tmpTask), 0);
      tmpTask.TaskDataSrc := tmpSrc;
      tmpTask.TaskDealItemCode := tmpCode;
      tmpTask.TaskDataType := tmpTaskType;
      RequestDownloadStockData(@tmpTask);
    end else
    begin
      if nil = fDataViewerData.DownloadAllTasks[tmpSrc] then
      begin                                      
        fDataViewerData.DownloadAllTasks[tmpSrc] := NewDownloadAllTask(tmpTaskType, tmpSrc, fDataViewerData.DownloadAllTasks[tmpSrc]);
        AddTaskNode(fDataViewerData.DownloadAllTasks[tmpSrc]);
      end;
    end;
  end;
end;

procedure TfmeDataViewer.AddTaskNode(ADownloadTask: PDownloadTask);
var
  tmpVNode: PVirtualNode;
  tmpVNodeData: PTaskDataNode; 
begin
  tmpVNode := vtTasks.AddChild(nil);
  if nil = tmpVNode then
    exit;
  tmpVNodeData := vtTasks.GetNodeData(tmpVNode);
  if nil = tmpVNodeData then
    exit;
  tmpVNodeData.DownloadTask := ADownloadTask;
end;

procedure TfmeDataViewer.vtTasksGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  tmpVNodeData: PTaskDataNode;
begin
  CellText := '';          
  tmpVNodeData := Sender.GetNodeData(Node);
  if nil <> tmpVNodeData then
  begin
    if nil <> tmpVNodeData.DownloadTask then
    begin
      if nil <> tmpVNodeData.DownloadTask.DownProcessTaskArray[0].DealItem then
        CellText := tmpVNodeData.DownloadTask.DownProcessTaskArray[0].DealItem.sCode;
    end;
  end;
end;

function TfmeDataViewer.NewDownloadAllTask(ATaskDataType: TRT_TaskType; ADataSrc: TDealDataSource; ADownloadTask: PDownloadTask): PDownloadTask;
begin
  Result := ADownloadTask;
  if nil = Result then
  begin
    Result := TStockDataConsoleApp(App.AppAgent).TaskConsole.GetDownloadTask(ATaskDataType, ADataSrc, 0);
    if nil = Result then
    begin
      Result := TStockDataConsoleApp(App.AppAgent).TaskConsole.NewDownloadTask(ATaskDataType, ADataSrc, 0);
    end;
    RequestDownloadStockData(Result);
  end;
end;

procedure TfmeDataViewer.SetDayDataSrc(AValue: TDealDataSource);
begin
end;

procedure TfmeDataViewer.cmbDataSrcChange(Sender: TObject);
begin
  inherited;
  if '' = Trim(cmbDataSrc.Text) then
    exit;
//
end;

type        
  TDetailColumns = (
    colTime, colPrice, 
    colDetailVolume, colDetailAmount, colType
    //, colBoll, colBollUP, colBollLP
    //, colCYHT_SK, colCYHT_SD
    //, colBDZX_AK, colBDZX_AD1, colBDZX_AJ
  );
          
const
  DetailColumnsText: array[TDetailColumns] of String = (
    '时间',
    '价格',
    '成交量',
    '成交金额',
    '性质'
  );
           
  DetailWidthText: array[TDetailColumns] of String = (
    '15:00:00',
    '300.00',
    '成交量',
    '成交金额',
    '性质'
  );
        
procedure TfmeDataViewer.DetailDataTreeViewInitialize;
var    
  col_detail: TDetailColumns;
  tmpCol: TVirtualTreeColumn;
begin
  vtDetailDatas.NodeDataSize := SizeOf(TStockDetailDataNode);
  vtDetailDatas.OnGetText := vtDetailDatasGetText;
  vtDetailDatas.Header.Options := [hoColumnResize, hoVisible];
  vtDetailDatas.Header.Columns.Clear;  
  for col_detail := low(TDetailColumns) to high(TDetailColumns) do
  begin
    tmpCol := vtDetailDatas.Header.Columns.Add;
    tmpCol.Text := DetailColumnsText[col_detail];
    tmpCol.Width := vtDetailDatas.Canvas.TextWidth('XXX' + DetailWidthText[col_detail]);
  end;
end;

procedure TfmeDataViewer.vtDetailDatasGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  tmpNodeData: PStockDetailDataNode;
begin
  CellText := '';    
  tmpNodeData := Sender.GetNodeData(Node);
  if nil <> tmpNodeData then
  begin
    if nil <> tmpNodeData.QuoteData then
    begin
      if Integer(colTime) = Column then
      begin
        CellText := GetTimeText(@tmpNodeData.QuoteData.DealDateTime.Time);
        exit;
      end;
      if Integer(colPrice) = Column then
      begin         
        CellText := IntToStr(tmpNodeData.QuoteData.Price.Value);
        exit;
      end;
      if Integer(colDetailVolume) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.DealVolume);
        exit;
      end;
      if Integer(colDetailAmount) = Column then
      begin
        CellText := IntToStr(tmpNodeData.QuoteData.DealAmount);
        exit;
      end;   
      if Integer(colType) = Column then
      begin             
        CellText := 'U';
        if DealType_Buy = tmpNodeData.QuoteData.DealType then
        begin
          CellText := '买';
        end;
        if DealType_Sale = tmpNodeData.QuoteData.DealType then
        begin
          CellText := '卖';
        end;
        if DealType_Neutral = tmpNodeData.QuoteData.DealType then
        begin
          CellText := '中';
        end;
        exit;
      end;
    end;
  end;
end;

procedure TfmeDataViewer.LoadDetailTreeViewData;     
var
  i: integer;
  tmpNodeData: PStockDetailDataNode;
  tmpDetailData: PRT_Quote_Detail;   
  tmpNode: PVirtualNode;
begin
  vtDetailDatas.Clear;
  vtDetailDatas.BeginUpdate;
  try
    if nil <> fDataViewerData.DetailDataAccess then
    begin
      for i := 0 to fDataViewerData.DetailDataAccess.RecordCount - 1 do
      begin
        tmpDetailData := fDataViewerData.DetailDataAccess.RecordItem[i];  
        tmpNode := vtDetailDatas.AddChild(nil);
        tmpNodeData := vtDetailDatas.GetNodeData(tmpNode);
        tmpNodeData.QuoteData := tmpDetailData;
      end;
    end;      
  finally
    vtDetailDatas.EndUpdate;
  end;
end;

end.
