unit SDFrameStockInfo;

interface

uses
  Windows, Messages, Classes, Forms, BaseFrame, define_dealitem;

type                   
  TfmeStockInfoData = record
    OnGetDealItem: TOnDealItemFunc;
  end;
  
  TfmeStockInfo = class(TfmeBase)
  private
    { Private declarations }   
    fStockInfoData: TfmeStockInfoData; 
  public
    constructor Create(Owner: TComponent); override;
    procedure NotifyDealItem(ADealItem: PRT_DealItem);
    property OnGetDealItem: TOnDealItemFunc read fStockInfoData.OnGetDealItem write fStockInfoData.OnGetDealItem;
  end;

implementation

{$R *.dfm}

{ TfmeStockInfo }

constructor TfmeStockInfo.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fStockInfoData, SizeOf(fStockInfoData), 0);
end;

procedure TfmeStockInfo.NotifyDealItem(ADealItem: PRT_DealItem);
begin

end;

end.
