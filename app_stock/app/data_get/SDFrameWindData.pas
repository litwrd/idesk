unit SDFrameWindData;

interface

{$IFDEF WIND}
uses
  Windows, Messages, Classes, Forms, BaseFrame, StdCtrls,
  Controls, ComCtrls, ExtCtrls, SysUtils, Dialogs, VirtualTrees,
  define_price, define_dealstore_file, define_message, define_dealitem,
  define_stockapp, define_datasrc, define_stock_quotes, define_datetime,
  StockMinuteDataAccess;

type          
  PImportWindSession = ^TImportWindSession;
  TImportWindSession = record
    Active: Byte;
  end;
                   
  TExportWindSession = record
    Active: Byte;  
  end;
  
  TfmeWindDataData = record
    ImportSession: TImportWindSession;
    ExportSession: TExportWindSession;
    DealItem: PRT_DealItem;
    OnGetDealItem: TOnDealItemFunc;
    MinuteDataAccess: TStockMinuteDataAccess;
    
    ColDateTime: TVirtualTreeColumn;
    ColPriceOpen: TVirtualTreeColumn;
    ColPriceHigh: TVirtualTreeColumn;
    ColPriceLow: TVirtualTreeColumn;
    ColPriceClose: TVirtualTreeColumn;
    ColVolume: TVirtualTreeColumn;
  end;
             
  PMinuteNodeData = ^TMinuteNodeData;
  TMinuteNodeData = record
    MinuteData: PRT_Quote_Minute;
  end;

  TfmeWindData = class(TfmeBase)
    pnlRight: TPanel;
    lbl1: TLabel;
    mmo1: TMemo;
    pnlMiddle: TPanel;
    pnlDayDataTop: TPanel;
    vtMinuteData: TVirtualStringTree;
    spl1: TSplitter;
    edtRootPath: TEdit;
    lblMinute: TLabel;
    cmbMinute: TComboBoxEx;
    btnExportDataM60: TButton;
    edtExportDownClick: TEdit;
    lblStockCode: TLabel;
    btnImportDataM60: TButton;
    Label1: TLabel;
    btnExportList: TButton;
    Label2: TLabel;
    edt1: TEdit;
    procedure btnExportDataM60Click(Sender: TObject);
    procedure btnImportDataM60Click(Sender: TObject);
    procedure btnLoadTxtClick(Sender: TObject);
    procedure btnSaveMinDataClick(Sender: TObject);
    procedure btnLoadMinDataClick(Sender: TObject);
    procedure btnExportInfoF10Click(Sender: TObject);
    procedure btnExportListClick(Sender: TObject);
  private
    { Private declarations } 
    fWindFormData: TfmeWindDataData;   
    function EnsureAgent: HWND; 
    procedure InitVirtualTreeView;
    procedure LoadMinuteData(ADealItem: PRT_DealItem);
    procedure LoadStockData_Minute2View; 
    procedure MinuteDataViewGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
        TextType: TVSTTextType; var CellText: WideString);
  public
    { Public declarations }     
    constructor Create(Owner: TComponent); override;
    procedure NotifyDealItem(ADealItem: PRT_DealItem);
    property OnGetDealItem: TOnDealItemFunc read fWindFormData.OnGetDealItem write fWindFormData.OnGetDealItem;
  end;
{$ENDIF}

implementation

{$IFDEF WIND}
{$R *.dfm}

uses
  IniFiles,
  {$IFDEF LOG}
  //UtilsLog,
  {$ENDIF}
  StockMinuteData_Load,
  StockMinuteData_Save,
  StockData_Import_tdx,
  BaseStockFormApp;
             
{$IFNDEF RELEASE}  
const
  LOGTAG = 'SDFrameTDXData.pas';
{$ENDIF}

{ TfmeTDXData }
constructor TfmeWindData.Create(Owner: TComponent);
var 
  tmpIni: TIniFile;
  tmpRootPath: string;
begin
  inherited;
  FillChar(fWindFormData, SizeOf(fWindFormData), 0);  
  tmpIni := TIniFile.Create(IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'StockApp.ini');
  try
    tmpRootPath := tmpIni.ReadString('WIND', 'RootPath', '');
    if '' = tmpRootPath then
    begin
      tmpIni.WriteString('WIND', 'RootPath', edtRootPath.Text);
    end else
    begin
      edtRootPath.Text := tmpRootPath;
    end;
  finally
    tmpIni.Free;
  end;
                        
  cmbMinute.Items.BeginUpdate;
  try
    cmbMinute.Items.Clear;
    cmbMinute.Items.Add('60');
    cmbMinute.Items.Add('10');
    cmbMinute.Items.Add('5');
    cmbMinute.Items.Add('1');            
  finally
    cmbMinute.Items.EndUpdate;
  end;
  cmbMinute.ItemIndex := 0;

  InitVirtualTreeView;
end;

procedure TfmeWindData.NotifyDealItem(ADealItem: PRT_DealItem);
begin
  fWindFormData.DealItem := ADealItem;
  LoadMinuteData(ADealItem);
end;
                           
procedure TfmeWindData.InitVirtualTreeView;
begin
  vtMinuteData.NodeDataSize := SizeOf(TMinuteNodeData);
  vtMinuteData.Header.Options := [hoColumnResize, hoVisible];
  vtMinuteData.Indent := 1;
  vtMinuteData.OnGetText := MinuteDataViewGetText;
  fWindFormData.ColDateTime := vtMinuteData.Header.Columns.Add;
  fWindFormData.ColDateTime.Text := 'DateTime';
  fWindFormData.ColDateTime.Width := vtMinuteData.Canvas.TextWidth('XXXyyyy-mm-dd hh:nn:ss');
  
  fWindFormData.ColPriceOpen := vtMinuteData.Header.Columns.Add;
  fWindFormData.ColPriceOpen.Text := 'Open';
  fWindFormData.ColPriceOpen.Width := vtMinuteData.Canvas.TextWidth('XXXXXXYYY');
  
  fWindFormData.ColPriceHigh := vtMinuteData.Header.Columns.Add;
  fWindFormData.ColPriceHigh.Text := 'High';
  fWindFormData.ColPriceHigh.Width := fWindFormData.ColPriceOpen.Width;
  
  fWindFormData.ColPriceLow := vtMinuteData.Header.Columns.Add;
  fWindFormData.ColPriceLow.Text := 'Low';
  fWindFormData.ColPriceLow.Width := fWindFormData.ColPriceOpen.Width;

  fWindFormData.ColPriceClose := vtMinuteData.Header.Columns.Add;
  fWindFormData.ColPriceClose.Text := 'Close';
  fWindFormData.ColPriceClose.Width := fWindFormData.ColPriceOpen.Width;

  fWindFormData.ColVolume := vtMinuteData.Header.Columns.Add;
  fWindFormData.ColVolume.Text := 'Volume';
  fWindFormData.ColVolume.Width := vtMinuteData.Canvas.TextWidth('xxxxxxxxxxx');
end;
                       
procedure TfmeWindData.MinuteDataViewGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);

  function IsCell(AColumn: TVirtualTreeColumn): Boolean;
  begin
    Result := false;
    if nil = AColumn then
      exit;     
    Result := Column = AColumn.Index;
  end;
  
var    
  tmpVNodeData: PMinuteNodeData;
begin
  inherited;
  CellText := '';   
  tmpVNodeData := Sender.GetNodeData(Node);
  if nil <> tmpVNodeData then
  begin
    if IsCell(fWindFormData.ColDateTime) then
      CellText := FormatDateTime('yyyy-mm-dd hh:nn:ss', define_datetime.DateTimeStock2DateTime(@tmpVNodeData.MinuteData.DealDateTime));
    if IsCell(fWindFormData.ColPriceOpen) then
      CellText := IntToStr(tmpVNodeData.MinuteData.PriceRange.PriceOpen.Value);
    if IsCell(fWindFormData.ColPriceHigh) then
      CellText := IntToStr(tmpVNodeData.MinuteData.PriceRange.PriceHigh.Value);
    if IsCell(fWindFormData.ColPriceLow) then 
      CellText := IntToStr(tmpVNodeData.MinuteData.PriceRange.PriceLow.Value);
    if IsCell(fWindFormData.ColPriceClose) then
      CellText := IntToStr(tmpVNodeData.MinuteData.PriceRange.PriceClose.Value);
    if IsCell(fWindFormData.ColVolume) then
      CellText := IntToStr(tmpVNodeData.MinuteData.DealVolume);
  end;
end;

procedure TfmeWindData.LoadMinuteData(ADealItem: PRT_DealItem);
var
  i: integer;
  tmpVNode: PVirtualNode;
  tmpVNodeData: PMinuteNodeData;
begin                 
  vtMinuteData.BeginUpdate;
  try
    vtMinuteData.Clear;
    if nil <> fWindFormData.MinuteDataAccess then
      FreeAndNil(fWindFormData.MinuteDataAccess);
    if nil = ADealItem then
      exit;
    fWindFormData.MinuteDataAccess := TStockMinuteDataAccess.Create(ADealItem, src_tongdaxin, weightNone);    
    fWindFormData.MinuteDataAccess.Minute := StrToIntDef(cmbMinute.Text, 60);
    if LoadStockMinuteData(App, fWindFormData.MinuteDataAccess) then
    begin            
      for i := fWindFormData.MinuteDataAccess.RecordCount - 1 downto 0 do
      begin
        tmpVNode := vtMinuteData.AddChild(nil);
        tmpVNodeData := vtMinuteData.GetNodeData(tmpVNode);
        if nil <> tmpVNodeData then
        begin
          tmpVNodeData.MinuteData := fWindFormData.MinuteDataAccess.RecordItem[i];
        end;
      end;
    end;
  finally
    vtMinuteData.EndUpdate;
  end;
end;

procedure TfmeWindData.LoadStockData_Minute2View;
var
  i: integer;          
  tmpVNode: PVirtualNode;
  tmpVNodeData: PMinuteNodeData;
begin
  vtMinuteData.BeginUpdate;
  try
    vtMinuteData.Clear;
    if nil = fWindFormData.MinuteDataAccess then
      exit;
    for i := 0 to fWindFormData.MinuteDataAccess.RecordCount - 1 do
    begin
      tmpVNode := vtMinuteData.AddChild(nil);
      tmpVNodeData := vtMinuteData.GetNodeData(tmpVNode);
      if nil <> tmpVNodeData then
      begin
        tmpVNodeData.MinuteData := fWindFormData.MinuteDataAccess.RecordItem[i];
      end;
    end;
  finally
    vtMinuteData.EndUpdate;
  end;
end;

function TfmeWindData.EnsureAgent: HWND;
var 
  tmpRootPath: string;       
  tmpProgFileUrl: AnsiString;
  tmpProgFilePath: AnsiString;
  tmpStartInfo: TStartupInfoA;
  tmpProcInfo: TProcessInformation;
  i: integer;
begin
  Result := 0;   
  tmpRootPath := IncludeTrailingPathDelimiter(Trim(edtRootPath.Text));
  if not DirectoryExists(tmpRootPath) then
    exit;    
  Result := FindWindow(define_stockapp.AppCmdWndClassName_StockDealAgent_ZSZQ, '');
  if not IsWindow(Result) then
  begin
    if FileExists('zszq_agent.exe') then
    begin
      FillChar(tmpStartInfo, SizeOf(tmpStartInfo), 0);
      FillChar(tmpProcInfo, SizeOf(tmpProcInfo), 0);
      tmpStartInfo.cb := SizeOf(tmpStartInfo);
      tmpProgFilePath := ExtractFilePath(ParamStr(0));
      tmpProgFileUrl := tmpProgFilePath + 'zszq_agent.exe';
      Windows.CreateProcessA(PAnsiChar(@tmpProgFileUrl[1]),
          nil, nil, nil, false, 0, nil, @tmpProgFilePath[1],
          tmpStartInfo,
          tmpProcInfo);
      for i := 0 to 10 * 50 do
      begin
        Sleep(20);
        Application.ProcessMessages;
        Result := FindWindow(define_stockapp.AppCmdWndClassName_StockDealAgent_ZSZQ, '');
        if IsWindow(Result) then
          Break;
      end;
    end else
      exit;
  end;
end;
                 
procedure TfmeWindData.btnExportInfoF10Click(Sender: TObject);
begin
  inherited;
//
end;

procedure TfmeWindData.btnExportListClick(Sender: TObject);
var
  tmpWnd: HWND;
  tmpFile: string;
  tmpFileName: string; 
  tmpRootPath: string;
  tmpFilePath: string;
  tmpSearch: TSearchRec;
  tmpFileAttr: Integer;
begin
  inherited;
  tmpWnd := EnsureAgent;  
  if not IsWindow(tmpWnd) then
  begin
    exit;
  end;   
  SendMessage(tmpWnd, WM_Export_FirstListInfo, 0, 0);
  exit;
  tmpRootPath := IncludeTrailingPathDelimiter(Trim(edtRootPath.Text));  
  tmpFilePath := tmpRootPath + 'T0002\export\';                
  tmpFileName := FormatDateTime('yyyymmdd', now) + '.txt';
  FillChar(tmpSearch, SizeOf(tmpSearch), 0);
  tmpFileAttr := faArchive;
  if 0 = SysUtils.FindFirst(tmpFilePath + '*' + tmpFileName, tmpFileAttr, tmpSearch) then
  begin
    repeat
      if (tmpSearch.Attr and tmpFileAttr) = tmpSearch.Attr then
      begin
        if (tmpSearch.Name <> '.') and (tmpSearch.Name <> '..') then
        begin
          SysUtils.DeleteFile(tmpFilePath + tmpSearch.Name);
        end;
      end;
    until FindNext(tmpSearch) <> 0;     
    FindClose(tmpSearch);
  end;
end;

procedure TfmeWindData.btnExportDataM60Click(Sender: TObject);
var
  i: integer;
  tmpDealItem: PRT_DealItem;    
  tmpWnd: HWND;
  j: integer;
  tmpRet: integer;
  tmpStart: integer;
  tmpRootPath: string;
  tmpFile: string;
  tmpCode: string;
  tmpMinute: integer;
  tmpStr: string;
begin
  inherited;          
  if 0 <> fWindFormData.ExportSession.Active then
  begin
    fWindFormData.ExportSession.Active := 0;
    exit;
  end;
  tmpWnd := EnsureAgent;
  if not IsWindow(tmpWnd) then
  begin
    exit;
  end;
  tmpRootPath := IncludeTrailingPathDelimiter(Trim(edtRootPath.Text));
  tmpMinute := StrToIntDef(cmbMinute.Text, 60);
  if 1 > tmpMinute then
    tmpMinute := 60;
  if 60 < tmpMinute then
    tmpMinute := 60;

  //Log(LOGTAG, 'btnExportTxtClick:' + IntToStr(tmpMinute));
  fWindFormData.ExportSession.Active := 1;
  try
    //Log(LOGTAG, 'btnExportTxtClick begin');
    if IsWindow(tmpWnd) then
    begin
      tmpRet := SendMessage(tmpWnd, WM_Export_SetConfig, Config_Export_DownClick, StrToIntDef(edtExportDownClick.text, 0));
      if 1 <> tmpRet then
      begin
        //Log(LOGTAG, 'btnExportTxtClick update config fail:' + IntToStr(tmpRet));
        exit;
      end;
      SendMessage(tmpWnd, WM_Export_SetConfig, Config_Export_MinuteData, tmpMinute);
      //Log(LOGTAG, 'btnExportTxtClick update config');
      tmpStr := Trim(cmbMinute.text);
      tmpStart := StrToIntDef(tmpStr, 0);
      for i := 0 to TBaseStockApp(App).StockIndexDB.RecordCount - 1 do
      begin
        if Application.Terminated then
          Break;      
        tmpDealItem := TBaseStockApp(App).StockIndexDB.RecordItem[i];
        lblStockCode.Caption := tmpDealItem.sCode;   
        if IsWindow(tmpWnd) then
        begin
          SendMessage(tmpWnd, WM_Export_Data, tmpDealItem.iCode, tmpMinute);
          for j := 1 to 10 do
          begin
            if Application.Terminated then
              Break;
            Application.ProcessMessages;
            Sleep(10);
          end;
        end else
        begin
          Break;
        end;
      end; 
      for i := 0 to TBaseStockApp(App).StockItemDB.RecordCount - 1 do
      begin
        if 1 <> fWindFormData.ExportSession.Active then
          Break;
        if Application.Terminated then
          Break;
        tmpDealItem := TBaseStockApp(App).StockItemDB.RecordItem[i];
        lblStockCode.Caption := tmpDealItem.sCode; 
        //if 2 = i then
        //  Break;
        if 0 = tmpDealItem.FirstDealDate then
          Continue;
        if 0 < tmpDealItem.EndDealDate then
          Continue;
        begin
          if 0 <> tmpStart then
          begin
            if tmpStart = tmpDealItem.iCode then
            begin
              tmpStart := 0;
            end else
              Continue;
          end;
        
          tmpFile := tmpRootPath + 'T0002\export\';
          tmpCode := tmpDealItem.sCode;
          tmpFile := tmpFile + tmpCode + '.txt';
          //Log('zsAgentConsoleForm.pas', 'Check File 1:' + tmpFile);
          if FileExists(tmpFile) then
          begin
            Continue;
          end;
          //tmpFile := App.Path.GetFileUrl(tmpDealItem.DBType, DataType_MinuteData, DataSrc_Tongdaxin, tmpMinute, tmpDealItem);
          //Log('zsAgentConsoleForm.pas', 'Check File 2:' + tmpFile);
          //if FileExists(tmpFile) then
          //begin
          //  Continue;
          //end;
          if IsWindow(tmpWnd) then
          begin
            SendMessage(tmpWnd, WM_Export_Data, tmpDealItem.iCode, tmpMinute);
            for j := 1 to 20 do
            begin
              if Application.Terminated then
                Break;
              Application.ProcessMessages;
              Sleep(10);
            end;
          end else
          begin
            Break;
          end;
        end;
      end;
    end;
  finally
    fWindFormData.ExportSession.Active := 0;
  end;
end;

procedure TfmeWindData.btnImportDataM60Click(Sender: TObject);

  procedure ImportTDXMinuteData(AMinuteData: TStockMinuteDataAccess; ASaveFileUrl, ATxtFileUrl: string);
  begin
    if FileExists(ASaveFileUrl) then
      LoadStockMinuteData(App, AMinuteData, ASaveFileUrl);
    if 0 < ImportStockData_Minute_TDX_FromFile(AMinuteData, ATxtFileUrl) then
    //Log('SDTdxForm.pas', 'Save File ' + tmpCode + ' minute:' + IntToStr(tmpMinuteData.Minute));
      SaveStockMinuteData(App, AMinuteData);
  end;

var
  tmpTxtFilePath: string;
                            
  procedure ImportTDXMinuteItemData(ADealItem: PRT_DealItem);
  var
    tmpTxtFile: string;   
    tmpMinuteData: TStockMinuteDataAccess;
  begin
    tmpTxtFile := tmpTxtFilePath + ADealItem.sCode + '.txt';
    lblStockCode.Caption := ADealItem.sCode;
    //Log('SDTdxForm.pas', 'Import:' + tmpFile);
    if FileExists(tmpTxtFile) then
    begin                            
      //Log('SDTdxForm.pas', 'Import File Begin' + tmpTxtFile);
      try
        tmpMinuteData := TStockMinuteDataAccess.Create(ADealItem, src_tongdaxin, weightNone);
        try
          ImportTDXMinuteData(tmpMinuteData, App.Path.GetFileUrl(
            ADealItem.DBType,
            DataType_MinuteData,
            DataSrc_tongdaxin, 60, ADealItem), tmpTxtFile);          
        finally
          tmpMinuteData.Free;
        end;
      finally
        SysUtils.DeleteFile(tmpTxtFile);
      end;
    end;
  end;
  
var
  i: integer;
  tmpDealItem: PRT_DealItem;
  tmpRootPath: string;
  tmpIni: TIniFile;
begin
  inherited;
  if 0 <> fWindFormData.ImportSession.Active then
  begin
    fWindFormData.ImportSession.Active := 0;
    exit;
  end;
  tmpRootPath := IncludeTrailingPathDelimiter(Trim(edtRootPath.Text));
  if '' = tmpRootPath then
  begin
    tmpIni := TIniFile.Create(ChangeFileExt(paramStr(0), '.ini'));
    try
      tmpRootPath := tmpIni.ReadString('TDX', 'RootPath', '');
      if '' <> tmpRootPath then
      begin
        tmpIni.WriteString('TDX', 'RootPath', tmpRootPath);
      end;
    finally
      tmpIni.Free;
    end;
  end;
  if not DirectoryExists(tmpRootPath) then
    exit;      
  tmpTxtFilePath := tmpRootPath + 'T0002\export\';
        
  fWindFormData.ImportSession.Active := 1;
  try                                                     
    for i := 0 to TBaseStockApp(App).StockIndexDB.RecordCount - 1 do
    begin
      tmpDealItem := TBaseStockApp(App).StockIndexDB.Items[i];
      ImportTDXMinuteItemData(tmpDealItem);
    end;
    for i := 0 to TBaseStockApp(App).StockItemDB.RecordCount - 1 do
    begin
      if Application.Terminated then
        Break;
      if 1 <> fWindFormData.ImportSession.Active then
        Break;
      tmpDealItem := TBaseStockApp(App).StockItemDB.Items[i];
      if 0 <> tmpDealItem.EndDealDate then
        Continue;
      ImportTDXMinuteItemData(tmpDealItem);

      Sleep(10);
      Application.ProcessMessages;
      //if 2 = i then
      //  Break;
    end;     
  finally
    fWindFormData.ImportSession.Active := 0;
  end;
end;

procedure TfmeWindData.btnLoadTxtClick(Sender: TObject);
var        
  tmpFileUrl: string;
begin
  //tmpFileUrl := 'E:\StockApp\sdata\999999.txt';
  //tmpFileUrl := 'E:\StockApp\sdata\002414.txt';
  //tmpFileUrl := 'E:\StockApp\sdata\000050.txt';
  //tmpFileUrl := 'E:\StockApp\sdata\600050.txt';
  //fMinuteDataAccess := TStockMinuteDataAccess.Create(DBType_Item_China, nil, src_tongdaxin, weightUnknown);
  with TOpenDialog.Create(Self) do try
    tmpFileUrl := Trim(edtRootPath.Text);
    if '' <> tmpFileUrl then
    begin
      if DirectoryExists(tmpFileUrl) then
      begin
        InitialDir := IncludeTrailingPathDelimiter(tmpFileUrl) + 'T0002\export\'; //D:\stock\zd_zszq\T0002\export;
      end;
    end;
    if not Execute then
      exit;
    tmpFileUrl := FileName;
  finally
    Free;
  end;
  if FileExists(tmpFileUrl) then
  begin
    if nil <> fWindFormData.MinuteDataAccess then
      FreeAndNil(fWindFormData.MinuteDataAccess);
    if nil = fWindFormData.MinuteDataAccess then
      fWindFormData.MinuteDataAccess := TStockMinuteDataAccess.Create(nil, src_tongdaxin, weightUnknown);
    ImportStockData_Minute_TDX_FromFile(fWindFormData.MinuteDataAccess, tmpFileUrl);
    LoadStockData_Minute2View;
  end;
end;
                    
procedure TfmeWindData.btnLoadMinDataClick(Sender: TObject);
var
  tmpDealItem: PRT_DealItem;
begin
  inherited;
  tmpDealItem := fWindFormData.DealItem;
  //tmpDealItem := TBaseStockApp(App).StockItemDB.FindDealItemByCode(edStock.Text);
  if nil = tmpDealItem then
    exit;
  if nil <> fWindFormData.MinuteDataAccess then
    fWindFormData.MinuteDataAccess.Free;
  fWindFormData.MinuteDataAccess := TStockMinuteDataAccess.Create(tmpDealItem, src_tongdaxin, weightUnknown);    
  //LoadStockMinuteData(App, fMinuteDataAccess, 'e:\minute.m60');
  //fMinuteDataAccess.StockCode := '000050';   
  if nil = fWindFormData.MinuteDataAccess.StockItem then
  begin
    if '' <> fWindFormData.MinuteDataAccess.StockCode then
    begin
      fWindFormData.MinuteDataAccess.StockItem := TBaseStockApp(App).StockItemDB.FindDealItemByCode(fWindFormData.MinuteDataAccess.StockCode);
    end;
  end;
  fWindFormData.MinuteDataAccess.Minute := 60;
  LoadStockMinuteData(App, fWindFormData.MinuteDataAccess);  
  LoadStockData_Minute2View;
end;

procedure TfmeWindData.btnSaveMinDataClick(Sender: TObject);
begin
  if nil = fWindFormData.MinuteDataAccess then
    exit;
  if 1 > fWindFormData.MinuteDataAccess.RecordCount then
    exit;
      //SaveStockMinuteData(App, fMinuteDataAccess, 'e:\minute.m60');
  if nil = fWindFormData.MinuteDataAccess.StockItem then
  begin
    if '' <> fWindFormData.MinuteDataAccess.StockCode then
      fWindFormData.MinuteDataAccess.StockItem := TBaseStockApp(App).StockItemDB.FindDealItemByCode(fWindFormData.MinuteDataAccess.StockCode);
  end;
  if nil <> fWindFormData.MinuteDataAccess.StockItem then
    SaveStockMinuteData(App, fWindFormData.MinuteDataAccess);
end;
{$ENDIF}

end.
