unit SDFrameStockInstant;

interface

uses
  Windows, Messages, Classes, Forms, SysUtils,
  BaseFrame, define_dealitem,
  StockInstantData_Get_Sina, Controls, VirtualTrees;

type
  PfmeStockInstantData = ^TfmeStockInstantData;
  TfmeStockInstantData = record
    OnGetDealItem: TOnDealItemFunc;
    {$IFDEF DEALINSTANT}
    StockQuoteInstants: TInstantArray;
    {$ENDIF}
    RefreshThreadHandle: THandle;
    RefreshThreadId: DWORD;
  end;
  
  TfmeStockInstant = class(TfmeBase)
    vtInstant: TVirtualStringTree;
  private
    { Private declarations } 
    fStockInstantData: TfmeStockInstantData;   
    procedure InitVirtualTreeView;        
    procedure InstantDataViewGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
    procedure BuildInstantTreeView;
  public
    { Public declarations }     
    constructor Create(Owner: TComponent); override;
    procedure Initialize; override;
    procedure NotifyDealItem(ADealItem: PRT_DealItem);
    property OnGetDealItem: TOnDealItemFunc read fStockInstantData.OnGetDealItem write fStockInstantData.OnGetDealItem;
  end;

implementation

uses
  BaseApp, BaseStockFormApp, UtilsHttp, define_stock_quotes_instant;

{$R *.dfm}

{ TfmeStockClass }

type
  PInstantNodeData = ^TInstantNodeData;
  TInstantNodeData = record
    StockItem: PRT_DealItem;
    {$IFDEF DEALINSTANT}
    InstantQuote: PRT_InstantQuote;
    {$ENDIF}
  end;
  
constructor TfmeStockInstant.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fStockInstantData, SizeOf(fStockInstantData), 0);  
  InitVirtualTreeView;
end;
               
procedure TfmeStockInstant.InitVirtualTreeView;
begin
  vtInstant.NodeDataSize := SizeOf(TInstantNodeData);
  vtInstant.Header.Options := [hoColumnResize, hoVisible];
  vtInstant.Indent := 1;
  vtInstant.OnGetText := InstantDataViewGetText;
end;

procedure TfmeStockInstant.Initialize;
begin
  inherited;   
  BuildInstantTreeView;
end;

procedure TfmeStockInstant.InstantDataViewGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
var
  tmpVNodeData: PInstantNodeData;
  tmpStr: string;
begin
  CellText := '';    
  tmpVNodeData := Sender.GetNodeData(Node);
  if nil <> tmpVNodeData then
  begin
    if nil <> tmpVNodeData.StockItem then
    begin
      tmpStr := '';
      {$IFDEF DEALINSTANT}
      if nil <> tmpVNodeData.InstantQuote then
      begin
        tmpStr := FormatFloat('0.00', tmpVNodeData.InstantQuote.PriceRange.PriceClose.Value / 1000);
        //tmpStr := tmpVNodeData.InstantQuote.PriceRange.PriceClose.Value;
      end;
      {$ENDIF}
      CellText := tmpVNodeData.StockItem.sCode + '(' + tmpVNodeData.StockItem.Name + ')' + tmpStr;
    end;
  end;
end;

{$IFDEF DEALINSTANT}                    
function CheckOutInstantQuote: PRT_InstantQuote;
begin
  Result := System.New(PRT_InstantQuote);
  FillChar(Result^, SizeOf(TRT_InstantQuote), 0);
end;
         
function CheckQuoteItem(AStockItem: PRT_DealItem): PRT_InstantQuote;
begin
  Result := nil;
  if nil <> AStockItem then
  begin
    Result := CheckOutInstantQuote;
    Result.Item := AStockItem;
    Result.Item.sCode := AStockItem.sCode;
  end;
end;

function ThreadProc_RefreshData(AParam: PfmeStockInstantData): HResult; stdcall;
var
  tmpHttpSession: UtilsHttp.THttpClientSession;
begin
  Result := 0;
  if nil = AParam then
    exit;
  FillChar(tmpHttpSession, SizeOf(tmpHttpSession), 0);
  while True do
  begin
    Sleep(20);
    DataGet_InstantArray_Sina(nil, @AParam.StockQuoteInstants, @tmpHttpSession, nil);
  end;
  ExitThread(Result);
end;     
{$ENDIF}

procedure TfmeStockInstant.BuildInstantTreeView;
var
  tmpIndex: integer;

  procedure AddStockItemNode(AStockCode: string);   
  var
    tmpStockItem: PRT_DealItem;
    tmpVNode: PVirtualNode;
    tmpVNodeData: PInstantNodeData;
  begin
    tmpStockItem := TBaseStockApp(App).StockItemDB.FindDealItemByCode(AStockCode);
    if nil <> tmpStockItem then
    begin
      tmpVNode := vtInstant.AddChild(nil);
      tmpVNodeData := vtInstant.GetNodeData(tmpVNode);
      tmpVNodeData.StockItem := tmpStockItem;
      
      {$IFDEF DEALINSTANT}
      tmpVNodeData.InstantQuote := CheckOutInstantQuote;
      tmpVNodeData.InstantQuote.Item := tmpStockItem;
      fStockInstantData.StockQuoteInstants.Data[tmpIndex] := tmpVNodeData.InstantQuote;
      {$ENDIF}
      Inc(tmpIndex);
    end;
  end;

begin
  tmpIndex := 0;
  AddStockItemNode('600000');
  AddStockItemNode('002414');
  AddStockItemNode('600170');
  
  {$IFDEF DEALINSTANT}
  fStockInstantData.RefreshThreadHandle := Windows.CreateThread(nil, 0, @ThreadProc_RefreshData,
      @fStockInstantData,
      Create_Suspended,
      fStockInstantData.RefreshThreadId);
  Windows.ResumeThread(fStockInstantData.RefreshThreadHandle);
  {$ENDIF}
end;
         
procedure TfmeStockInstant.NotifyDealItem(ADealItem: PRT_DealItem);
begin
end;

end.
