inherited fmeDataViewer: TfmeDataViewer
  Width = 753
  Height = 443
  ParentFont = False
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 753
    Height = 395
    Align = alClient
    BevelOuter = bvNone
    object spl1: TSplitter
      Left = 450
      Top = 0
      Height = 395
      Align = alRight
    end
    object pnlRight: TPanel
      Left = 453
      Top = 0
      Width = 300
      Height = 395
      Align = alRight
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      object mmo1: TMemo
        Left = 0
        Top = 313
        Width = 298
        Height = 80
        Align = alBottom
        Lines.Strings = (
          'mmo1')
        ScrollBars = ssVertical
      end
      object vtDetailDatas: TVirtualStringTree
        Left = 0
        Top = 0
        Width = 298
        Height = 313
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Header.AutoSizeIndex = 0
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.MainColumn = -1
        Columns = <>
      end
    end
    object pnlMiddle: TPanel
      Left = 0
      Top = 0
      Width = 450
      Height = 395
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Ctl3D = False
      ParentCtl3D = False
      object vtDayDatas: TVirtualStringTree
        Left = 0
        Top = 0
        Width = 448
        Height = 313
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Header.AutoSizeIndex = 0
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.MainColumn = -1
        TreeOptions.SelectionOptions = [toFullRowSelect]
        Columns = <>
      end
      object vtTasks: TVirtualStringTree
        Left = 0
        Top = 313
        Width = 448
        Height = 80
        Align = alBottom
        BevelInner = bvNone
        BevelOuter = bvNone
        Header.AutoSizeIndex = 0
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.MainColumn = -1
        TreeOptions.SelectionOptions = [toFullRowSelect]
        Columns = <>
      end
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 395
    Width = 753
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    FullRepaint = False
    object Label1: TLabel
      Left = 13
      Top = 15
      Width = 59
      Height = 13
      Caption = 'Data Source'
    end
    object cmbDataSrc: TComboBoxEx
      Left = 86
      Top = 12
      Width = 163
      Height = 22
      ItemsEx = <>
      ItemHeight = 16
      OnChange = cmbDataSrcChange
    end
    object btnDownload: TButton
      Left = 356
      Top = 10
      Width = 110
      Height = 25
      Caption = 'Download'
      OnClick = btnDownloadClick
    end
    object chkShutDown: TCheckBox
      Left = 649
      Top = 16
      Width = 102
      Height = 17
      Anchors = [akRight, akBottom]
      Caption = 'ShutDown After'
    end
    object cmbDataType: TComboBoxEx
      Left = 255
      Top = 12
      Width = 74
      Height = 22
      ItemsEx = <>
      Style = csExDropDownList
      ItemHeight = 16
      OnChange = cmbDataSrcChange
    end
    object cmbDetailDataSrc: TComboBoxEx
      Left = 560
      Top = 12
      Width = 83
      Height = 22
      ItemsEx = <>
      Anchors = [akTop, akRight]
      ItemHeight = 16
      OnChange = cmbDataSrcChange
    end
  end
  object tmrRefreshDownloadTask: TTimer
    Enabled = False
    OnTimer = tmrRefreshDownloadTaskTimer
    Left = 80
    Top = 232
  end
end
