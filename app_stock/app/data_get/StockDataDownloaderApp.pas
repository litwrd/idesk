unit StockDataDownloaderApp;

interface

uses           
  Sysutils, Windows,   
  UtilsHttp,
  BaseApp,
  define_datasrc,      
  define_price,
  define_dealItem,
  define_stockapp,
  define_StockDataApp,
  win.iobuffer,
  win.process;

type
  PDownloaderAppData = ^TDownloaderAppData;
  TDownloaderAppData = record
    Console_Process: TRT_ExProcess;
    HttpClientSession: THttpClientSession;
    HttpData: win.iobuffer.PIOBuffer;
  end;
               
  TStockDataDownloaderApp = class(BaseApp.TBaseAppAgent)
  protected         
    fDownloaderAppData: TDownloaderAppData;
  public               
    constructor Create(AHostApp: TBaseApp); override;
    destructor Destroy; override;    
    function Initialize: Boolean; override;
    procedure Run; override;  
    function CreateAppCommandWindow: Boolean;  
    function Downloader_Download_Day(AStockCode, ADataSrc: integer): Boolean; overload;
    function Downloader_Download_Day(ADownloaderApp: PDownloaderAppData; AStockCode, ADataSrc: integer): Boolean; overload;

    function Downloader_Download_Detail(AStockCode, ADataSrc: integer): Boolean; overload;
    function Downloader_Download_Detail(ADownloaderApp: PDownloaderAppData; AStockCode, ADataSrc: integer): Boolean; overload;
    
    function Downloader_CheckConsoleProcess(ADownloaderApp: PDownloaderAppData): Boolean;
  end;
  
implementation
            
uses
  windef_msg,
  win.thread,
  BaseWinApp,
  StockDayDataAccess,
  StockDayData_Load,
  {$IFDEF LOG}
  UtilsLog,
  {$ENDIF}
  UtilsParentProcess,
  //BaseStockApp,
  BaseStockFormApp,
  StockDetailData_Get_Sina,
  StockDetailData_Get_163,
  StockDayData_Get_163,
  StockDayData_Get_Sina;

{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDataDownloaderApp.pas';
{$ENDIF}

var
  G_StockDataDownloaderApp: TStockDataDownloaderApp = nil;

function AppCommandWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): LRESULT; stdcall;
begin
  Result := 0;
  case AMsg of
    WM_AppStart: begin
      //Log(LOGTAG, 'WM_AppStart');
      if nil <> G_StockDataDownloaderApp then
      begin
        //G_StockDataDownloaderApp.HostApp.RunMode := RunMode_Debug;
        //Log(LOGTAG, 'WM_AppStart Check Console Process');
        if G_StockDataDownloaderApp.Downloader_CheckConsoleProcess(@G_StockDataDownloaderApp.fDownloaderAppData) then
        begin
          //Log(LOGTAG, 'WM_AppStart Register Self:' +
          //  IntToStr(G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core.AppCmdWnd) + '/' +
          //  IntToStr(AWnd) + '/' +
          //  IntToStr(TBaseStockApp(G_StockDataDownloaderApp.fBaseAppAgentData.HostApp).AppWindow)
          //  );
          // ע���Լ�
          SendMessageA(G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core.AppCmdWnd,
            WM_RegisterClientCommandWnd,
            GetCurrentProcessId,
            TBaseStockApp(G_StockDataDownloaderApp.fBaseAppAgentData.HostApp).AppWindow);
          Windows.GetWindowThreadProcessId(
              G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core.AppCmdWnd,
              G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core.ProcessId);
          if (0 <> G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core.ProcessId) and
               (INVALID_HANDLE_VALUE <> G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core.ProcessId) then
          begin
            CreateParentProcessMonitorThread(@G_StockDataDownloaderApp.fDownloaderAppData.Console_Process.Core);
          end;
        end else
        begin
          if RunMode_Debug <> G_StockDataDownloaderApp.HostApp.RunMode then
          begin
            G_StockDataDownloaderApp.fBaseAppAgentData.HostApp.Terminate;
          end else
          begin
            // ��������
            //PostMessage(AWnd, WM_Console2Downloader_Command_Download_Day, 1002525, DataSrc_163);   
            //PostMessage(AWnd, WM_Console2Downloader_Command_Download_Day, 1002525, DataSrc_Sina);
          end;
        end;
      end else
      begin

      end;
    end;
    WM_AppRequestEnd: begin
      // query if should end      
      //Log(LOGTAG, 'WM_AppRequestEnd');
      if nil <> GlobalBaseStockApp then
      begin
        GlobalBaseStockApp.Terminate;
      end;
      PostMessage(AWnd, WM_AppNotifyEnd, 0, 0);
      //StartShutDownMonitorThread;   
    end;
    WM_AppNotifyEnd: begin
      if nil <> GlobalBaseStockApp then
      begin
        //GlobalBaseStockApp.Terminate;
      end;              
      //Log(LOGTAG, 'WM_AppNotifyEnd PostQuitMessage0');
      PostQuitMessage(0);
      //Log(LOGTAG, 'WM_AppNotifyEnd TerminateProcess');      
      //Windows.TerminateProcess(Windows.GetCurrentProcess, 0);
    end;
    WM_Console2Downloader_Command_Download_Day: begin
      {$IFDEF LOG}
      //Log(LOGTAG, 'WM_Console2Downloader_Command_Download_Day:' + IntToStr(wParam) + '/' + IntToStr(lParam));
      {$ENDIF}
      PostMessage(AWnd, WM_Downloader_Command_Download_Day, wParam, lParam)
    end;
    WM_Console2Downloader_Command_Download_Detail: begin
      {$IFDEF LOG}
      //Log(LOGTAG, 'WM_Console2Downloader_Command_Download_Detail:' + IntToStr(wParam) + '/' + IntToStr(lParam));
      {$ENDIF}
      PostMessage(AWnd, WM_Downloader_Command_Download_Detail, wParam, lParam)
    end;
    WM_Downloader_Command_Download_Day: begin
      {$IFDEF LOG}
      //Log(LOGTAG, 'WM_Downloader_Command_Download_Day');
      {$ENDIF}
      if nil <> G_StockDataDownloaderApp then
      begin
        if nil <> GlobalBaseStockApp then
        begin
          {$IFDEF LOG}
          //Log(LOGTAG, 'WM_Downloader_Command_Download_Day 2');
          {$ENDIF}
          if RunStatus_Active = GlobalBaseStockApp.RunStatus then
          begin
            {$IFDEF LOG}
            //Log(LOGTAG, 'WM_Downloader_Command_Download_Day start:' + IntToStr(wParam) + '/' + IntTostr(lParam));
            {$ENDIF}
            G_StockDataDownloaderApp.Downloader_Download_Day(wParam, lParam);
          end else
          begin
            {$IFDEF LOG}
            //Log(LOGTAG, 'WM_Downloader_Command_Download_Day not active');
            {$ENDIF}
          end;
        end;
      end;
    end;        
    WM_Downloader_Command_Download_Detail: begin
      {$IFDEF LOG}    
      //Log(LOGTAG, 'WM_Downloader_Command_Download_Detail');
      {$ENDIF}
      if nil <> G_StockDataDownloaderApp then
      begin
        if nil <> GlobalBaseStockApp then
        begin
          {$IFDEF LOG}
          //Log(LOGTAG, 'WM_Downloader_Command_Download_Detail 2');
          {$ENDIF}        
          if RunStatus_Active = GlobalBaseStockApp.RunStatus then
          begin
            {$IFDEF LOG}
            //Log(LOGTAG, 'WM_Downloader_Command_Download_Detail start:' + IntToStr(wParam) + '/' + IntTostr(lParam));
            {$ENDIF}
            G_StockDataDownloaderApp.Downloader_Download_Detail(wParam, lParam);
          end else
          begin
            {$IFDEF LOG}
            //Log(LOGTAG, 'WM_Downloader_Command_Download_Detail not active');
            {$ENDIF}
          end;
        end;
      end;
    end;
  end;
  Result := DefWindowProcA(AWnd, AMsg, wParam, lParam);
end;
               
constructor TStockDataDownloaderApp.Create(AHostApp: TBaseApp);
begin
  inherited;
  FillChar(fDownloaderAppData, SizeOf(fDownloaderAppData), 0);
  G_StockDataDownloaderApp := Self;
end;

destructor TStockDataDownloaderApp.Destroy;
begin
  if G_StockDataDownloaderApp = Self then
  begin
    G_StockDataDownloaderApp := nil;
  end;
  inherited;
end;
                  
function TStockDataDownloaderApp.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin                    
    Result := CreateAppCommandWindow;
  end;
  if Result then
  begin
    {$IFDEF LOG}
    UtilsLog.CloseLogFiles;
    UtilsLog.G_LogFile.FileName := ChangeFileExt(ParamStr(0), '.down.' + IntToStr(Windows.GetCurrentProcessId) + '.log');
    //UtilsLog.SDLog(LOGTAG, 'init mode downloader');
    {$ENDIF}
  end;
end;

procedure TStockDataDownloaderApp.Run;
begin
  // this inherited can not be removed
  inherited;
  PostMessage(TBaseWinApp(fBaseAppAgentData.HostApp).AppWindow, WM_AppStart, 0, 0);
  TBaseWinApp(fBaseAppAgentData.HostApp).RunAppMsgLoop;
end;

function TStockDataDownloaderApp.CreateAppCommandWindow: Boolean;
var
  tmpRegWinClass: TWndClassA;  
  tmpGetWinClass: TWndClassA;
  tmpIsReged: Boolean;
  tmpWindowName: AnsiString;
  tmpWndClassName: AnsiString;   
  tmpGuid: TGuid;
begin
  Result := false;          
  FillChar(tmpRegWinClass, SizeOf(tmpRegWinClass), 0);
  tmpRegWinClass.hInstance := HInstance;
  tmpRegWinClass.lpfnWndProc := @AppCommandWndProcA;
  //tmpWndClassName := AppCmdWndClassName_StockDataDownloader;
  
  CreateGuid(tmpGuid);
  tmpWndClassName := GuidToString(tmpGuid);
  
  tmpRegWinClass.lpszClassName := PAnsiChar(@tmpWndClassName[1]);
  tmpIsReged := GetClassInfoA(HInstance, tmpRegWinClass.lpszClassName, tmpGetWinClass);
  if tmpIsReged then
  begin
    if (tmpGetWinClass.lpfnWndProc <> tmpRegWinClass.lpfnWndProc) then
    begin                           
      UnregisterClassA(tmpRegWinClass.lpszClassName, HInstance);
      tmpIsReged := false;
    end;
  end;
  if not tmpIsReged then
  begin
    if 0 = RegisterClassA(tmpRegWinClass) then
      exit;
  end;
  tmpWindowName := IntToStr(GetCurrentProcessId);
  TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow := CreateWindowExA(
    WS_EX_TOOLWINDOW
    //or WS_EX_APPWINDOW
    //or WS_EX_TOPMOST
    ,
    tmpRegWinClass.lpszClassName,
    PAnsiChar(tmpWindowName), WS_POPUP {+ 0},
    0, 0, 0, 0,
    HWND_MESSAGE, 0, HInstance, nil);
  Result := Windows.IsWindow(TBaseStockApp(fBaseAppAgentData.HostApp).AppWindow);
end;

function TStockDataDownloaderApp.Downloader_Download_Detail(AStockCode, ADataSrc: integer): Boolean;
begin
  Result := Downloader_Download_Detail(@fDownloaderAppData, AStockCode, ADataSrc);
end;

function TStockDataDownloaderApp.Downloader_Download_Detail(ADownloaderApp: PDownloaderAppData; AStockCode, ADataSrc: integer): Boolean;
var
  tmpStockItem: PRT_DealItem;
  tmpStockItemRec: TRT_DealItem;
  tmpDayData: TStockDayDataAccess;
begin                      
  {$IFDEF LOG}    
  //Log(LOGTAG, 'Downloader_Download_Detail stock:' + IntToStr(AStockCode) + '/' + IntTostr(ADataSrc));
  {$ENDIF}   
  Result := false; 
  tmpStockItem := nil;

  if nil <> TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockIndexDB then
  begin
    tmpStockItem := TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockIndexDB.FindDealItemByCode(IntToStr(AStockCode));
  end;
  if nil = tmpStockItem then
  begin
    if nil <> TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockItemDB then
    begin
      tmpStockItem := TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockItemDB.FindDealItemByCode(IntToStr(AStockCode));
    end else
    begin
      FillChar(tmpStockItemRec, SizeOf(tmpStockItemRec), 0);
      tmpStockItemRec.iCode := AStockCode;
      tmpStockItemRec.DBType := DBType_Item_China;
      tmpStockItemRec.sCode := getSimpleStockCodeByPackCode(AStockCode);
      tmpStockItem := @tmpStockItemRec;
    end;
  end;
  if nil = tmpStockItem then
  begin                         
    {$IFDEF LOG}
    //Log(LOGTAG, 'Downloader_Download_Detail stock nil');
    {$ENDIF}
  end else
  begin
    if DataSrc_163 = ADataSrc then
    begin
      if tmpStockItem.DBType = DBType_Item_China then
      begin
        tmpDayData := TStockDayDataAccess.Create(tmpStockItem, Src_163, weightNone);
        try
          try
            if LoadStockDayData(TBaseStockApp(Self.fBaseAppAgentData.HostApp), tmpDayData) then
            begin            
              {$IFDEF LOG}  
              //Log(LOGTAG, 'Dowload Stock Detail:' + tmpStockItem.sCode);
              {$ENDIF}
              if GetStockDataDetail_163(TBaseStockApp(Self.fBaseAppAgentData.HostApp), tmpDayData, @fDownloaderAppData.HttpClientSession) then
              begin         
                {$IFDEF LOG}  
                //Log(LOGTAG, 'Dowload Stock Detail ok:' + tmpStockItem.sCode);
                {$ENDIF}
              end else
              begin     
                {$IFDEF LOG}  
                //Log(LOGTAG, 'Dowload Stock Detail fail:' + tmpStockItem.sCode);
                {$ENDIF}
              end;
              Sleep(100);
            end;
          except
            {$IFDEF LOG}  
            //Log(LOGTAG, 'Dowload Stock Detail error:' + tmpStockItem.sCode);
            {$ENDIF}
          end;
        finally
          tmpDayData.Free;
        end;
      end;
    end;
    if DataSrc_Sina = ADataSrc then
    begin         
      if tmpStockItem.DBType = DBType_Item_China then
      begin
        tmpDayData := TStockDayDataAccess.Create(tmpStockItem, Src_163, weightNone);
        try
          try
            if LoadStockDayData(TBaseStockApp(Self.fBaseAppAgentData.HostApp), tmpDayData) then
            begin
              {$IFDEF LOG}
              //Log(LOGTAG, 'Dowload Stock Detail:' + tmpStockItem.sCode);
              {$ENDIF}
              if GetStockDataDetail_Sina(TBaseStockApp(Self.fBaseAppAgentData.HostApp), tmpDayData, @fDownloaderAppData.HttpClientSession) then
              begin
                {$IFDEF LOG}
                //Log(LOGTAG, 'Dowload Stock Detail ok:' + tmpStockItem.sCode);
                {$ENDIF}
                Sleep(100);
              end;
            end;
          except
            {$IFDEF LOG}
            //Log(LOGTAG, 'Dowload Stock Detail error:' + tmpStockItem.sCode);
            {$ENDIF}
          end;
        finally
          tmpDayData.Free;
        end;
      end;
    end;
    if Downloader_CheckConsoleProcess(ADownloaderApp) then
    begin
      {$IFDEF LOG}  
      //SDLog(LOGTAG, 'Downloader_Downloaded:' + IntToStr(AStockCode));
      {$ENDIF}
      PostMessage(ADownloaderApp.Console_Process.Core.AppCmdWnd, WM_Downloader2Console_Command_DownloadResult, Windows.GetCurrentProcessId, 0);
    end else
    begin
      PostMessage(ADownloaderApp.Console_Process.Core.AppCmdWnd, WM_Downloader2Console_Command_DownloadResult, Windows.GetCurrentProcessId, 1001);
    end;
  end;
end;

function TStockDataDownloaderApp.Downloader_Download_Day(AStockCode, ADataSrc: integer): Boolean;
begin
  Result := Downloader_Download_Day(@fDownloaderAppData, AStockCode, ADataSrc);
end;

function TStockDataDownloaderApp.Downloader_Download_Day(ADownloaderApp: PDownloaderAppData; AStockCode, ADataSrc: integer): Boolean;
var
  tmpStockItem: PRT_DealItem;
  tmpStockItemRec: TRT_DealItem;  
begin
  {$IFDEF LOG}                        
  //Log(LOGTAG, 'Downloader_Download stock:' + IntToStr(AStockCode) + '/' + IntTostr(ADataSrc));
  {$ENDIF}   
  Result := false; 
  tmpStockItem := nil;

  if nil <> TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockIndexDB then
  begin
    tmpStockItem := TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockIndexDB.FindDealItemByCode(IntToStr(AStockCode));
    //if nil <> tmpStockItem then
    //begin             
      //Log(LOGTAG, 'Downloader_Download stock index');
    //end;
  end;
  if nil = tmpStockItem then
  begin
    if nil <> TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockItemDB then
    begin
      tmpStockItem := TBaseStockApp(Self.fBaseAppAgentData.HostApp).StockItemDB.FindDealItemByCode(IntToStr(AStockCode));
    end else
    begin
      FillChar(tmpStockItemRec, SizeOf(tmpStockItemRec), 0);
      tmpStockItemRec.iCode := AStockCode;
      tmpStockItemRec.DBType := DBType_Item_China;
      tmpStockItemRec.sCode := getSimpleStockCodeByPackCode(AStockCode);
      tmpStockItem := @tmpStockItemRec;
    end;
  end;
  if nil <> tmpStockItem then
  begin
    if DataSrc_163 = ADataSrc then
    begin
      {$IFDEF LOG}
      //Log(LOGTAG, 'Downloader_Download 163:' + IntToStr(AStockCode));
      {$ENDIF}
      GetStockDataDay_163(fBaseAppAgentData.HostApp, tmpStockItem, false, @ADownloaderApp.HttpClientSession, fDownloaderAppData.HttpData);
    end;
    if DataSrc_Sina = ADataSrc then
    begin
      if nil = fDownloaderAppData.HttpData then
      begin
        fDownloaderAppData.HttpData := CheckOutIOBuffer(SizeMode_512k);
      end;
      {$IFDEF LOG}
      //SDLog(LOGTAG, 'Downloader_Download Sina:' + IntToStr(AStockCode));
      {$ENDIF}
      if DBType_Index_China = tmpStockItem.DBType then
      begin
        GetStockDataDay_Sina(fBaseAppAgentData.HostApp, tmpStockItem, weightNone, @ADownloaderApp.HttpClientSession, fDownloaderAppData.HttpData);
      end;          
      if DBType_Item_China = tmpStockItem.DBType then
      begin
        GetStockDataDay_Sina(fBaseAppAgentData.HostApp, tmpStockItem, weightBackward, @ADownloaderApp.HttpClientSession, fDownloaderAppData.HttpData);
      end;
      {$IFDEF LOG}
      //SDLog(LOGTAG, 'Downloader_Download Sina:' + IntToStr(AStockCode));
      {$ENDIF}
    end;
    if Downloader_CheckConsoleProcess(ADownloaderApp) then
    begin
      {$IFDEF LOG}
      //SDLog(LOGTAG, 'Downloader_Downloaded:' + IntToStr(AStockCode));
      {$ENDIF}
      PostMessage(ADownloaderApp.Console_Process.Core.AppCmdWnd, WM_Downloader2Console_Command_DownloadResult, Windows.GetCurrentProcessId, 0);
    end else
    begin
      // 
      PostMessage(ADownloaderApp.Console_Process.Core.AppCmdWnd, WM_Downloader2Console_Command_DownloadResult, Windows.GetCurrentProcessId, 1001);
    end;
  end else
  begin
    {$IFDEF LOG}
    //SDLog(LOGTAG, 'Downloader_Download can not find stock:' + IntToStr(AStockCode));
    {$ENDIF}
  end;
end;
                       
function TStockDataDownloaderApp.Downloader_CheckConsoleProcess(ADownloaderApp: PDownloaderAppData): Boolean;
var
  tmpWnd: HWND;
begin
  Result := IsWindow(ADownloaderApp.Console_Process.Core.AppCmdWnd);
  if not Result then
  begin
    //Log(LOGTAG, 'Downloader_CheckConsoleProcess:' + IntToStr(ADownloaderApp.Console_Process.Core.AppCmdWnd));
    ADownloaderApp.Console_Process.Core.AppCmdWnd := Windows.FindWindowA(AppCmdWndClassName_StockDataDownloader_Console, nil);    
    Result := IsWindow(ADownloaderApp.Console_Process.Core.AppCmdWnd);         
    //Log(LOGTAG, 'Downloader_CheckConsoleProcess Command Wnd:' + IntToStr(ADownloaderApp.Console_Process.Core.AppCmdWnd));
    if Result then
    begin
      //Log(LOGTAG, 'Downloader_CheckConsoleProcess WM_RequestHostCommandWnd');
      tmpWnd := SendMessageA(ADownloaderApp.Console_Process.Core.AppCmdWnd,
        WM_RequestHostCommandWnd, GetCurrentProcessId, 0);
      //Log(LOGTAG, 'Downloader_CheckConsoleProcess WM_RequestHostCommandWnd:' + IntToStr(tmpWnd));
      if 0 <> tmpWnd then
      begin
        if IsWindow(tmpWnd) then
        begin             
          ADownloaderApp.Console_Process.Core.AppCmdWnd := tmpWnd;
          //Log(LOGTAG, 'Downloader_CheckConsoleProcess set Host Command Wnd:' + IntToStr(ADownloaderApp.Console_Process.Core.AppCmdWnd));          
        end;
      end;   
    end else
    begin
    
    end;
  end;
end;

end.
