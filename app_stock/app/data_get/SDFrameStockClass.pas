unit SDFrameStockClass;

interface
          
{$IFDEF DEALCLASS}
uses
  Windows, Messages, Classes, Controls, ExtCtrls, Forms, StdCtrls, Sysutils,
  VirtualTrees, QuickList_Int,
  BaseFrame, define_dealitem, define_dealclass, define_datasrc,
  win.iobuffer;

type
  TfmeStockClassData = record
    OnGetDealItem: TOnDealItemFunc;
    RootNode_Sina: PVirtualNode;
  end;
  
  TfmeStockClass = class(TfmeBase)
    pnlleft: TPanel;
    pnlLeftBottom: TPanel;
    btnSaveClass: TButton;
    btnAddClass: TButton;
    mmo1: TMemo;
    spl1: TSplitter;
    vtClassDef: TVirtualStringTree;
    spl2: TSplitter;
    pnlMain: TPanel;
    pnl1: TPanel;
    btnSaveStock: TButton;
    btnAddStock: TButton;
    mmo2: TMemo;
    vtClassStock: TVirtualStringTree;
    btnRefresh: TButton;
    procedure btnAddClassClick(Sender: TObject);
    procedure vtClassDefGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
    procedure btnSaveClassClick(Sender: TObject);
    procedure vtClassDefChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure btnSaveStockClick(Sender: TObject);
    procedure btnAddStockClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
  private
    { Private declarations }
    fStockClassData: TfmeStockClassData;
    procedure vtStockClassGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure InitializeClassDefVirtualTree;
    procedure BuildClassDefVirtualTree;
    procedure InitializeStockClassVirtualTree;
    procedure BuildStockClassVirtualTree(ADefClass: PRT_DefClass);
  public
    { Public declarations }     
    constructor Create(Owner: TComponent); override;
    procedure Initialize; override;
    procedure NotifyDealItem(ADealItem: PRT_DealItem);
    property OnGetDealItem: TOnDealItemFunc read fStockClassData.OnGetDealItem write fStockClassData.OnGetDealItem;
  end;
{$ENDIF}

implementation
              
{$IFDEF DEALCLASS}

{$R *.dfm}
uses
  UtilsHttp,
  db_dealclass,
  db_dealclass_load,
  db_dealclass_save,  
  BaseStockFormApp;

type
  PClassDefNode = ^TClassDefNode;
  TClassDefNode = record
    Caption     : string;
    DataSrc     : integer;
    DefClass    : PRT_DefClass;
  end;
  
  PClassStockNode = ^TClassStockNode;
  TClassStockNode = record
    Caption     : string;
    DataSrc     : integer;     
    DealItem    : PRT_DealItem;
  end;
(*
http://vip.stock.finance.sina.com.cn/mkt/#sw_qgzz

gn_jght ���� ��������
http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/
Market_Center.getHQNodeStockCount?node=gn_jght
http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/
Market_Center.getHQNodeData?page=3&num=40&sort=symbol&asc=1&node=gn_jght&symbol=&_s_r_a=init
*)
{ TfmeStockClass }
constructor TfmeStockClass.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fStockClassData, SizeOf(fStockClassData), 0);
end;

procedure TfmeStockClass.Initialize;
begin
  inherited;
  if nil <> App then
  begin
    if nil = TBaseStockApp(App).StockDB.StockClass then
    begin
      TBaseStockApp(App).StockDB.StockClass := TDBDealClassDictionary.Create(DBType_Item_China);
      LoadDBStockClassDic(App, TBaseStockApp(App).StockDB.StockClass);
    end;
  end;
  InitializeClassDefVirtualTree;
  BuildClassDefVirtualTree;
  
  InitializeStockClassVirtualTree;
end;

procedure TfmeStockClass.InitializeClassDefVirtualTree;
begin
  vtClassDef.NodeDataSize := SizeOf(TClassDefNode);
  vtClassDef.OnGetText := vtClassDefGetText;
end;
        
procedure TfmeStockClass.InitializeStockClassVirtualTree;
begin
  vtClassStock.NodeDataSize := SizeOf(TClassStockNode);
  vtClassStock.OnGetText := vtStockClassGetText;
end;
                                                  
procedure TfmeStockClass.BuildClassDefVirtualTree;    
type
  PTmpNodeCache = ^TTmpNodeCache;
  TTmpNodeCache = record
    DefClass: PRT_DefClass;
    VNode: PVirtualNode;
  end;

var
  tmpNodeList: TALIntegerList;
  
  procedure AddClassNode(ADefClass: PRT_DefClass);
  var
    tmpParentNode: PVirtualNode;  
    tmpNode: PVirtualNode;    
    tmpNodeData: PClassDefNode;
    tmpIndex: Integer;   
    tmpCacheNode: PTmpNodeCache;
  begin
    tmpParentNode := nil;
    if DataSrc_Sina = ADefClass.DataSrc then
       tmpParentNode := fStockClassData.RootNode_Sina;
    if 0 <> ADefClass.ParentId then
    begin
      tmpIndex := tmpNodeList.IndexOf(ADefClass.ParentId);
      if 0 <= tmpIndex then
      begin
        tmpCacheNode := PTmpNodeCache(tmpNodeList.Objects[tmpIndex]);
        if nil = tmpCacheNode.VNode then
        begin
          AddClassNode(ADefClass.Parent);
        end;
        tmpParentNode := tmpCacheNode.VNode;
      end;
    end;
    if nil <> tmpParentNode then
    begin
      tmpNode := vtClassDef.AddChild(tmpParentNode);
      tmpNodeData := vtClassDef.GetNodeData(tmpNode);
      tmpNodeData.DataSrc := ADefClass.DataSrc;
      tmpNodeData.DefClass := ADefClass;

      tmpIndex := tmpNodeList.IndexOf(ADefClass.AutoId); 
      if 0 <= tmpIndex then
      begin
        tmpCacheNode := PTmpNodeCache(tmpNodeList.Objects[tmpIndex]);
        tmpCacheNode.VNode := tmpNode;
      end;
    end;
  end;

var
  tmpNodeData: PClassDefNode;
  tmpDefClass: PRT_DefClass;
  i, tmpIndex: integer;
  tmpCacheNode: PTmpNodeCache;
begin
  vtClassDef.BeginUpdate;
  try
    vtClassDef.Clear;
    fStockClassData.RootNode_Sina := vtClassDef.AddChild(nil);
    if nil <> fStockClassData.RootNode_Sina then
    begin
      tmpNodeData := vtClassDef.GetNodeData(fStockClassData.RootNode_Sina);
      tmpNodeData.Caption := 'Sina';
      tmpNodeData.DataSrc := DataSrc_Sina;
      tmpNodeData.DefClass := nil;
    end;
    tmpNodeList := TALIntegerList.Create;
    try
      for i := 0 to TBaseStockApp(App).StockDB.StockClass.RecordCount - 1 do
      begin
        tmpDefClass := TBaseStockApp(App).StockDB.StockClass.Items[i];
        tmpCacheNode := System.New(PTmpNodeCache);
        tmpCacheNode.DefClass := tmpDefClass;
        tmpCacheNode.VNode := nil;
        tmpNodeList.AddObject(tmpDefClass.AutoId, TObject(tmpCacheNode));
      end;      
      for i := 0 to TBaseStockApp(App).StockDB.StockClass.RecordCount - 1 do
      begin
        tmpDefClass := TBaseStockApp(App).StockDB.StockClass.Items[i];
        if 0 <> tmpDefClass.ParentId then
        begin
          tmpIndex := tmpNodeList.IndexOf(tmpDefClass.ParentId);
          if 0 <= tmpIndex then
          begin
            tmpCacheNode := PTmpNodeCache(tmpNodeList.Objects[tmpIndex]);
            tmpDefClass.Parent := tmpCacheNode.DefClass;
          end;
        end;
      end;      
      for i := 0 to TBaseStockApp(App).StockDB.StockClass.RecordCount - 1 do
      begin                       
        tmpDefClass := TBaseStockApp(App).StockDB.StockClass.Items[i];
        AddClassNode(tmpDefClass);
      end;
      for i := tmpNodeList.Count - 1 downto 0 do
      begin
        tmpCacheNode := PTmpNodeCache(tmpNodeList.Objects[i]);
        FreeMem(tmpCacheNode);
      end;
      tmpNodeList.Clear;
    finally
      tmpNodeList.Free;
    end;
  finally
    vtClassDef.EndUpdate;
  end;
end;
             
procedure TfmeStockClass.BuildStockClassVirtualTree(ADefClass: PRT_DefClass);
begin
  vtClassStock.BeginUpdate;
  try
    vtClassStock.Clear;
  finally
    vtClassStock.EndUpdate;
  end;
end;

procedure TfmeStockClass.vtStockClassGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);    
var
  tmpNodeData: PClassStockNode;
begin
  CellText := '';
  tmpNodeData := Sender.GetNodeData(Node);
  if nil <> tmpNodeData then
  begin
    if nil = tmpNodeData.DealItem then
    begin
      CellText := IntToStr(Node.Index) + ':' + tmpNodeData.Caption;
    end else
    begin
      CellText := IntToStr(Node.Index) + ':' + tmpNodeData.DealItem.sCode + '[' + tmpNodeData.DealItem.Name + ']';
    end;
  end;
end;

procedure TfmeStockClass.vtClassDefChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  tmpNodeData: PClassDefNode;
begin
  inherited;
  if nil = Node then
    exit;
  tmpNodeData := vtClassDef.GetNodeData(Node);
  if nil = tmpNodeData then
    exit;
  if nil <> tmpNodeData.DefClass then
    exit;
  BuildStockClassVirtualTree(tmpNodeData.DefClass);
end;

procedure TfmeStockClass.vtClassDefGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  tmpNodeData: PClassDefNode;
begin
  CellText := '';
  tmpNodeData := Sender.GetNodeData(Node);
  if nil <> tmpNodeData then
  begin
    if nil = tmpNodeData.DefClass then
    begin
      CellText := tmpNodeData.Caption;
    end else
    begin
      CellText := tmpNodeData.DefClass.Name;
    end;
  end;
end;

procedure TfmeStockClass.NotifyDealItem(ADealItem: PRT_DealItem);
begin
end;

procedure TfmeStockClass.btnAddClassClick(Sender: TObject);
var
  tmpNode: PVirtualNode;  
  tmpNodeData: PClassDefNode;
  tmpParent: PRT_DefClass;
  tmpDataSrc: integer;
  tmpName: string;
  i: integer;
begin
  inherited;
  if '' = Trim(mmo1.Lines.Text) then
    exit;
    
  tmpParent := nil;
  tmpDataSrc := 0;                                  
  tmpNodeData := vtClassDef.GetNodeData(vtClassDef.FocusedNode);
  if nil <> tmpNodeData then
  begin
    tmpParent := tmpNodeData.DefClass;
    tmpDataSrc := tmpNodeData.DataSrc;
  end;
  for i := 0 to mmo1.Lines.Count - 1 do
  begin
    tmpName := Trim(mmo1.Lines[i]);
    if '' = tmpName then
      Continue;
    tmpNode := vtClassDef.AddChild(vtClassDef.FocusedNode);
    if nil <> tmpNode then
    begin
      tmpNodeData := vtClassDef.GetNodeData(tmpNode);
      if nil <> tmpNodeData then
      begin
        tmpNodeData.DefClass := TBaseStockApp(App).StockDB.StockClass.CheckOutDealClass(tmpDataSrc, 0, tmpName);
        tmpNodeData.DefClass.Parent := tmpParent;
        if nil <> tmpNodeData.DefClass.Parent then
          tmpNodeData.DefClass.ParentId := tmpNodeData.DefClass.Parent.AutoId;
        tmpNodeData.DefClass.Name := tmpName;
        tmpNodeData.DefClass.DataSrc := tmpDataSrc;
        vtClassDef.FullExpand(vtClassDef.FocusedNode);
      end;
    end;
  end;
//
end;
    
procedure TfmeStockClass.btnAddStockClick(Sender: TObject);
begin
  inherited;
//
end;

procedure TfmeStockClass.btnSaveClassClick(Sender: TObject);
begin
  inherited;
  SaveDBStockClassDic(App, TBaseStockApp(App).StockDB.StockClass);
end;

procedure TfmeStockClass.btnSaveStockClick(Sender: TObject);
begin
  inherited;
//
end;
                
type                            
  PParseRecord = ^TParseRecord;
  TParseRecord = record        
    HeadParse: THttpHeadParseSession;
    Stocks: TStringList;
  end;
  
procedure TfmeStockClass.btnRefreshClick(Sender: TObject); 
var
  tmpUrl: string;       
  tmpHttpSession: THttpClientSession;
  tmpHttpData: PIOBuffer;
  tmpParseRec: TParseRecord;
  tmpStr: string;
  tmpPos: integer;
  i: integer;
  tmpTotalCount: integer;
  tmpPage: integer;
  tmpPageCount: integer;
  tmpNodeCode: string;
  tmpValue: string;
  tmpVNode: PVirtualNode;  
  tmpVNodeData: PClassStockNode;
begin
  inherited;
  FillChar(tmpParseRec, SizeOf(tmpParseRec), 0);
  tmpParseRec.Stocks := TStringList.Create;
  tmpParseRec.Stocks.Clear;

  tmpNodeCode := 'gn_jght';
  tmpUrl := 'http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/';
  tmpUrl := tmpUrl + 'Market_Center.getHQNodeStockCount?node=' + tmpNodeCode;
  FillChar(tmpHttpSession, SizeOf(tmpHttpSession), 0);
  tmpHttpData := nil;
  tmpHttpData := GetHttpUrlData(tmpUrl, @tmpHttpSession, tmpHttpData);
  if nil <> tmpHttpData then
  begin
    HttpBufferHeader_Parser(tmpHttpData, @tmpParseRec.HeadParse);
    if (199 < tmpParseRec.HeadParse.RetCode) and (300 > tmpParseRec.HeadParse.RetCode)then
    begin
      tmpStr := AnsiString(PAnsiChar(@tmpHttpData.Data[tmpParseRec.HeadParse.HeadEndPos + 1]));
      if '' <> tmpStr then
      begin
        tmpPos := Pos('(', tmpStr);
        if 0 < tmpPos then
          tmpStr := Copy(tmpStr, tmpPos + 1, maxint);
        tmpPos := Pos('(', tmpStr);
        if 0 < tmpPos then
          tmpStr := Copy(tmpStr, tmpPos + 1, maxint);
        tmpStr := StringReplace(tmpStr, ')', '', [rfReplaceAll]);
        tmpStr := StringReplace(tmpStr, '"', '', [rfReplaceAll]);
        tmpStr := StringReplace(tmpStr, '''', '', [rfReplaceAll]);        
        tmpTotalCount := StrToIntDef(tmpStr, 0);
        if 0 < tmpTotalCount then
        begin
          tmpPage := 1;
          //tmpPageCount := 20;
          tmpPageCount := 40;          
          while 0 < tmpTotalCount do
          begin
            tmpUrl := 'http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/';
            tmpUrl := tmpUrl + 'Market_Center.getHQNodeData?page=' + inttostr(tmpPage) + '&num=' + IntToStr(tmpPageCount) + '&sort=symbol&asc=1&node=' + tmpNodeCode +'&symbol=&_s_r_a=init';
            tmpHttpData := GetHttpUrlData(tmpUrl, @tmpHttpSession, tmpHttpData);
            if nil <> tmpHttpData then
            begin
              FillChar(tmpParseRec.HeadParse, SizeOf(tmpParseRec.HeadParse), 0);     
              HttpBufferHeader_Parser(tmpHttpData, @tmpParseRec.HeadParse);
              if (199 < tmpParseRec.HeadParse.RetCode) and (300 > tmpParseRec.HeadParse.RetCode)then
              begin
                tmpStr := AnsiString(PAnsiChar(@tmpHttpData.Data[tmpParseRec.HeadParse.HeadEndPos + 1]));
                if '' <> tmpStr then
                begin
                  tmpPos := Pos('symbol:"', tmpStr);
                  while 0 < tmpPos do
                  begin
                    tmpStr := Copy(tmpStr, tmpPos + 8, MaxInt);  
                    tmpPos := Pos('"', tmpStr);
                    if 0 < tmpPos then
                    begin
                      tmpValue := Copy(tmpStr, 1, tmpPos - 1);
                      tmpParseRec.Stocks.Add(tmpValue);
                    end;
                    tmpPos := Pos('symbol:"', tmpStr);
                  end;
                end;
              end;
            end else
            begin
              Break;
            end;
            tmpTotalCount := tmpTotalCount - tmpPageCount;
            tmpPage := tmpPage + 1;
          end;
        end;
      end;
    end;
  end;
  vtClassStock.BeginUpdate;
  try
    vtClassStock.Clear;
    for i := 0 to tmpParseRec.Stocks.Count - 1 do
    begin
      tmpVNode := vtClassStock.AddChild(nil);
      tmpVNodeData := vtClassStock.GetNodeData(tmpVNode);
      tmpVNodeData.Caption := tmpParseRec.Stocks[i];
    end;
  finally
    vtClassStock.EndUpdate;
  end;
end;
{$ENDIF}

end.
