inherited fmeStockClass: TfmeStockClass
  Width = 697
  Height = 509
  object spl2: TSplitter
    Left = 257
    Top = 0
    Height = 509
  end
  object pnlleft: TPanel
    Left = 0
    Top = 0
    Width = 257
    Height = 509
    Align = alLeft
    BevelOuter = bvNone
    object spl1: TSplitter
      Left = 0
      Top = 356
      Width = 257
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    object pnlLeftBottom: TPanel
      Left = 0
      Top = 359
      Width = 257
      Height = 150
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      Ctl3D = False
      ParentCtl3D = False
      DesignSize = (
        257
        150)
      object btnSaveClass: TButton
        Left = 93
        Top = 117
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Save Class'
        OnClick = btnSaveClassClick
      end
      object btnAddClass: TButton
        Left = 12
        Top = 117
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Add Class'
        OnClick = btnAddClassClick
      end
      object mmo1: TMemo
        Left = 1
        Top = 1
        Width = 255
        Height = 110
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
    end
    object vtClassDef: TVirtualStringTree
      Align = alClient
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      OnChange = vtClassDefChange
      OnGetText = vtClassDefGetText
      Columns = <>
    end
  end
  object pnlMain: TPanel
    Align = alClient
    BevelOuter = bvNone
    object pnl1: TPanel
      Left = 0
      Top = 359
      Width = 437
      Height = 150
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      Ctl3D = False
      ParentCtl3D = False
      DesignSize = (
        437
        150)
      object btnSaveStock: TButton
        Left = 93
        Top = 117
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Save'
        OnClick = btnSaveStockClick
      end
      object btnAddStock: TButton
        Left = 12
        Top = 117
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'btnAdd'
        OnClick = btnAddStockClick
      end
      object mmo2: TMemo
        Left = 0
        Top = 0
        Width = 437
        Height = 111
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object btnRefresh: TButton
        Left = 200
        Top = 117
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Refresh'
        OnClick = btnRefreshClick
      end
    end
    object vtClassStock: TVirtualStringTree
      Left = 0
      Top = 0
      Width = 437
      Height = 359
      Align = alClient
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      OnGetText = vtClassDefGetText
      Columns = <>
    end
  end
end
