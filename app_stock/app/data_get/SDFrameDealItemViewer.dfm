inherited fmeDealItemViewer: TfmeDealItemViewer
  Width = 372
  Height = 353
  ParentFont = False
  object pnlLeftBottom: TPanel
    Left = 0
    Top = 282
    Width = 372
    Height = 71
    Align = alBottom
    object btnSaveDic: TButton
      Left = 105
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Save Dic'
    end
    object btnOpen: TButton
      Left = 24
      Top = 37
      Width = 75
      Height = 25
      Caption = 'Open'
    end
    object btnClear: TButton
      Left = 24
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Clear'
    end
    object btnSaveIni: TButton
      Left = 105
      Top = 37
      Width = 75
      Height = 25
      Caption = 'Save Ini'
    end
    object btnRefresh: TButton
      Left = 204
      Top = 37
      Width = 75
      Height = 25
      Caption = 'Refresh'
    end
    object btnCheckEnd: TButton
      Left = 285
      Top = 6
      Width = 75
      Height = 25
      Caption = 'CheckEnd'
    end
    object btnSaveTxt: TButton
      Left = 204
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Save Txt'
    end
  end
end
