unit StockDataTaskConsole;

interface

uses
  Windows, Messages, SysUtils,
  define_dealitem,
  define_datasrc,
  db_dealitem,               
  win.process,
  StockDataConsoleTask;
               
type
  TStockTaskConsole = class
  protected            
    fStockItemDB: TDBDealItem;
    fStockIndexDB: TDBDealItem;
    fAppCmdWnd: HWND;       
    function CreateProcessTaskHostCommandWindow(AProcess: POwnedProcess; AWndProc: TFNWndProc): Boolean;
  public               
    constructor Create;
    destructor Destroy; override;   
    function CheckOutDownloadTask(ATaskDataType: TRT_TaskType; ATaskDataSrc: TDealDataSource; ATaskDealItemCode: integer): PDownloadTask;   
    function NewDownloadTask(ATaskDataType: TRT_TaskType; ATaskDataSrc: TDealDataSource; ATaskDealItemCode: integer): PDownloadTask;
    function GetDownloadTask(ATaskDataType: TRT_TaskType; ATaskDataSrc: TDealDataSource; ATaskDealItemCode: integer): PDownloadTask;
    function GetDownloadTaskByProcessID(AProcessId: integer): PDownloadProcessTask;                    

    procedure RunTask(ATask: PDownloadTask);
    procedure RunProcessTask(AProcessTask: PDownloadProcessTask);
    function Console_GetNextDownloadDealItem(AProcessTask: PDownloadProcessTask): PRT_DealItem; overload;
    function Console_GetNextDownloadDealItem_DB(AProcessTask: PDownloadProcessTask; ADealItemDB: TDBDealItem): PRT_DealItem; overload;
    function Console_NotifyDownloadData(AProcessTask: PDownloadProcessTask): Boolean;  
    function Console_CheckDownloaderProcess(AProcessTask: PDownloadProcessTask): Boolean;
    procedure Console_NotifyDownloaderShutdown(AProcessTask: PDownloadProcessTask);    
    property StockItemDB: TDBDealItem read fStockItemDB write fStockItemDB;
    property StockIndexDB: TDBDealItem read fStockIndexDB write fStockIndexDB;  
    property AppWindow: HWND read fAppCmdWnd write fAppCmdWnd;
  end;
  
var
  G_StockTaskConsole: TStockTaskConsole = nil;
                    
var
  // 最多有 4 个下载任务同时 进行 不允许更多
  GlobalDownloadTasks: array[0..3] of TDownloadTask;

implementation

uses
  windef_msg,
  define_dealstore_file,
  define_StockDataApp,
  StockDataConsoleProcessTaskWndProc;
  
{ TStockTaskConsole }
                 
constructor TStockTaskConsole.Create;
begin
  inherited;  
  fStockItemDB := nil;
  fStockIndexDB := nil;
  fAppCmdWnd := 0;
  G_StockTaskConsole := Self;
end;

destructor TStockTaskConsole.Destroy;
begin
  if Self = G_StockTaskConsole then
    G_StockTaskConsole := nil;
  inherited;
end;

function TStockTaskConsole.GetDownloadTask(ATaskDataType: TRT_TaskType; ATaskDataSrc: TDealDataSource; ATaskDealItemCode: integer): PDownloadTask;   
var
  i: Integer;   
begin
  Result := nil;      
  for i := Low(GlobalDownloadTasks) to High(GlobalDownloadTasks) do
  begin
    if (TaskAssigned = GlobalDownloadTasks[i].TaskStatus) or
       (taskRunning = GlobalDownloadTasks[i].TaskStatus) then
    begin
      if (GlobalDownloadTasks[i].TaskDataType = ATaskDataType) and
         (GlobalDownloadTasks[i].TaskDataSrc = ATaskDataSrc) and
         (GlobalDownloadTasks[i].TaskDealItemCode = ATaskDealItemCode) then
      begin
        Result := @GlobalDownloadTasks[i];
        Break;
      end;
    end;
  end;
end;

function TStockTaskConsole.GetDownloadTaskByProcessID(AProcessId: integer): PDownloadProcessTask;
var
  i, j: Integer;   
begin
  Result := nil;    
  for i := Low(GlobalDownloadTasks) to High(GlobalDownloadTasks) do
  begin
    if (TaskAssigned = GlobalDownloadTasks[i].TaskStatus) or
       (taskRunning = GlobalDownloadTasks[i].TaskStatus) then
    begin
      for j := Low(GlobalDownloadTasks[i].DownProcessTaskArray) to High(GlobalDownloadTasks[i].DownProcessTaskArray) do
      begin
        if AProcessId = GlobalDownloadTasks[i].DownProcessTaskArray[j].DownloadProcess.Core.ProcessId then
        begin
          Result := @GlobalDownloadTasks[i].DownProcessTaskArray[j];
          Break;
        end;
      end;
    end;
  end;
end;
              
function TStockTaskConsole.NewDownloadTask(ATaskDataType: TRT_TaskType; ATaskDataSrc: TDealDataSource; ATaskDealItemCode: integer): PDownloadTask;
var
  i: integer;
begin
  Result := nil;
  for i := Low(GlobalDownloadTasks) to High(GlobalDownloadTasks) do
  begin
    if (TaskAssigned <> GlobalDownloadTasks[i].TaskStatus) and
       (taskRunning <> GlobalDownloadTasks[i].TaskStatus) then
    begin
      Result := @GlobalDownloadTasks[i];
      Result.TaskDataType := ATaskDataType;
      Result.TaskStatus := TaskAssigned;
      Break;
    end;
  end;
  if nil <> Result then
  begin
    Result.TaskDataSrc := ATaskDataSrc;    
    Result.TaskDataType := ATaskDataType;
    Result.TaskDealItemCode := ATaskDealItemCode;
  end;
  //CreateProcessHostCommandWindow(@Result.DownProcessTaskArray[0].DownloadProcess);
end;
                       
function TStockTaskConsole.CheckOutDownloadTask(ATaskDataType: TRT_TaskType; ATaskDataSrc: TDealDataSource; ATaskDealItemCode: integer): PDownloadTask;
begin
  Result := GetDownloadTask(ATaskDataType, ATaskDataSrc, ATaskDealItemCode);
  if nil = Result then
    Result := NewDownloadTask(ATaskDataType, ATaskDataSrc, ATaskDealItemCode);
end;
                             
procedure TStockTaskConsole.RunTask(ATask: PDownloadTask);

  procedure StartProcessTask(AProcessTask: PDownloadProcessTask);
  begin
    if CreateProcessTaskHostCommandWindow(@AProcessTask.DownloadProcess, AProcessTask.CommandWndProc) then
    begin
      if nil = AProcessTask.DealItem then
        AProcessTask.DealItem := Console_GetNextDownloadDealItem(AProcessTask);
      if nil <> AProcessTask.DealItem then
      begin
        ATask.TaskStatus := taskRunning;
        if not Console_CheckDownloaderProcess(AProcessTask) then
          ATask.TaskStatus := TaskFail;
      end;
    end;
  end;

var
  i: integer;
  tmpTaskIndex: integer;
begin
  if nil = ATask then
    exit;
  if not Assigned(ATask.DownProcessTaskArray[0].CommandWndProc) then
    exit;
  if TaskAssigned = ATask.TaskStatus then
  begin
    if taskDetailData = ATask.TaskDataType then
    begin
      tmpTaskIndex := 0;
      for i := Low(ATask.DownProcessTaskArray) to High(ATask.DownProcessTaskArray) do
      begin
        ATask.DownProcessTaskArray[i].TaskIncrement := High(ATask.DownProcessTaskArray) - Low(ATask.DownProcessTaskArray) + 1;
        ATask.DownProcessTaskArray[i].DealItemIndex := tmpTaskIndex;
        Inc(tmpTaskIndex);
      end;            
      for i := Low(ATask.DownProcessTaskArray) to High(ATask.DownProcessTaskArray) do
      begin
        StartProcessTask(@ATask.DownProcessTaskArray[i]);
      end;
    end else
    begin
      ATask.DownProcessTaskArray[0].TaskIncrement := 1;
      StartProcessTask(@ATask.DownProcessTaskArray[0]);
    end;
  end;
end;

procedure TStockTaskConsole.RunProcessTask(AProcessTask: PDownloadProcessTask);
begin
  if nil = AProcessTask then
    exit;                        
  if taskRunning = AProcessTask.OwnerTask.TaskStatus then
  begin
    if 0 <> AProcessTask.DownloadProcess.Core.AppCmdWnd then
    begin
      if not IsWindow(AProcessTask.DownloadProcess.Core.AppCmdWnd) then
        AProcessTask.DownloadProcess.Core.AppCmdWnd := 0;
    end;
    Console_NotifyDownloadData(AProcessTask);
  end;
end;

function TStockTaskConsole.CreateProcessTaskHostCommandWindow(AProcess: POwnedProcess; AWndProc: TFNWndProc): Boolean;
var
  tmpRegWinClass: TWndClassA;  
  tmpGetWinClass: TWndClassA;
  tmpIsReged: Boolean;    
  tmpWndClassName: AnsiString;
  tmpGuid: TGuid;
begin
  //Log('StockDataConsoleApp.pas', 'CreateProcessHostCommandWindow');
  Result := false;          
  FillChar(tmpRegWinClass, SizeOf(tmpRegWinClass), 0);
  tmpRegWinClass.hInstance := HInstance;
  tmpRegWinClass.lpfnWndProc := AWndProc;
  CreateGuid(tmpGuid);
  tmpWndClassName := GuidToString(tmpGuid);
  tmpRegWinClass.lpszClassName := PAnsiChar(tmpWndClassName);
  tmpIsReged := GetClassInfoA(HInstance, tmpRegWinClass.lpszClassName, tmpGetWinClass);
  if tmpIsReged then
  begin
    if (tmpGetWinClass.lpfnWndProc <> tmpRegWinClass.lpfnWndProc) then
    begin                           
      UnregisterClassA(tmpRegWinClass.lpszClassName, HInstance);
      tmpIsReged := false;
    end;
  end;
  if not tmpIsReged then
  begin
    if 0 = RegisterClassA(tmpRegWinClass) then
      exit;
  end;
  AProcess.HostCmdWnd.CmdWndHandle := CreateWindowExA(
    WS_EX_TOOLWINDOW
    //or WS_EX_APPWINDOW
    //or WS_EX_TOPMOST
    ,
    tmpRegWinClass.lpszClassName,
    '', WS_POPUP {+ 0},
    0, 0, 0, 0,
    HWND_MESSAGE, 0, HInstance, nil);
  Result := Windows.IsWindow(AProcess.HostCmdWnd.CmdWndHandle);   
  //Log('StockDataConsoleApp.pas', 'CreateProcessHostCommandWindow:' + IntToStr(AProcess.HostCmdWnd.CmdWndHandle));
end;

function TStockTaskConsole.Console_NotifyDownloadData(AProcessTask: PDownloadProcessTask): Boolean;
var
  tmpRetCode: DWORD;
begin
  Result := false;
  if nil = AProcessTask then
    exit;
  if nil = AProcessTask.DealItem then
    AProcessTask.DealItem := Console_GetNextDownloadDealItem(AProcessTask);
  if nil = AProcessTask.DealItem then
  begin   
    PostMessage(AProcessTask.DownloadProcess.Core.AppCmdWnd, WM_AppRequestEnd, 0, 0);
    exit;
  end;
  // 子进程每执行 30 个任务 就 shutdown
  if 30 < AProcessTask.ExecuteCount then
  begin
    if 0 <> AProcessTask.DownloadProcess.Core.ProcessHandle then
    begin
      if Windows.GetExitCodeProcess(AProcessTask.DownloadProcess.Core.ProcessHandle, tmpRetCode) then
      begin
        if Windows.STILL_ACTIVE = tmpRetCode then
        begin
          Console_NotifyDownloaderShutdown(AProcessTask);
          AProcessTask.ExecuteCount := 0;
          Sleep(200);
          Console_NotifyDownloadData(AProcessTask);
          exit;
        end;
      end;
    end;  
  end;
  if Console_CheckDownloaderProcess(AProcessTask) then
  begin
    if taskDayData = AProcessTask.OwnerTask.TaskDataType then
    begin
      Inc(AProcessTask.ExecuteCount);
      PostMessage(AProcessTask.DownloadProcess.Core.AppCmdWnd,
        WM_Console2Downloader_Command_Download_Day,
        AProcessTask.DealItem.iCode, GetDealDataSourceCode(AProcessTask.OwnerTask.TaskDataSrc));
      Result := true;
    end;
    if taskDetailData = AProcessTask.OwnerTask.TaskDataType then
    begin
      Inc(AProcessTask.ExecuteCount);
      PostMessage(AProcessTask.DownloadProcess.Core.AppCmdWnd,
        WM_Console2Downloader_Command_Download_Detail,
        AProcessTask.DealItem.iCode, GetDealDataSourceCode(AProcessTask.OwnerTask.TaskDataSrc));
      Result := true;
    end;
  end;
end;

function TStockTaskConsole.Console_GetNextDownloadDealItem(AProcessTask: PDownloadProcessTask): PRT_DealItem;
begin
  Result := nil;
  if nil = AProcessTask then
    exit;
  if 0 = AProcessTask.OwnerTask.TaskDataDB then
  begin                                  
    if 0 = AProcessTask.OwnerTask.CurrentDataDB then
    begin
      if src_sina = AProcessTask.OwnerTask.TaskDataSrc then
      begin
        if taskDayData = AProcessTask.OwnerTask.TaskDataType then
        begin
          AProcessTask.OwnerTask.CurrentDataDB  := DBType_Item_China;
          AProcessTask.DealItemIndex := -1;
        end;
      end;
    end;
    if 0 = AProcessTask.OwnerTask.CurrentDataDB then
    begin
      AProcessTask.OwnerTask.CurrentDataDB  := DBType_Index_China;
      AProcessTask.DealItemIndex := 0;      
    end;
  end else
  begin
    AProcessTask.OwnerTask.CurrentDataDB  := AProcessTask.OwnerTask.TaskDataDB;
  end;      
  if DBType_Index_China = AProcessTask.OwnerTask.CurrentDataDB then
    Result := Console_GetNextDownloadDealItem_DB(AProcessTask, fStockIndexDB);
  if DBType_Item_China = AProcessTask.OwnerTask.CurrentDataDB then
    Result := Console_GetNextDownloadDealItem_DB(AProcessTask, fStockItemDB);
  if nil = Result then
  begin
    if 0 = AProcessTask.OwnerTask.TaskDataDB then
    begin
      if DBType_Index_China = AProcessTask.OwnerTask.CurrentDataDB then
      begin
        AProcessTask.OwnerTask.CurrentDataDB  := DBType_Item_China;
        AProcessTask.DealItemIndex := -1;
        AProcessTask.DealItem := nil;            
        Result := Console_GetNextDownloadDealItem_DB(AProcessTask, fStockItemDB);
      end;
    end;         
  end;
end;

function TStockTaskConsole.Console_GetNextDownloadDealItem_DB(AProcessTask: PDownloadProcessTask; ADealItemDB: TDBDealItem): PRT_DealItem; 
var
  tmpDealItem: PRT_DealItem;
  tmpDayOfWeek: Word;
begin
  Result := nil;
  if nil = AProcessTask then
    exit;
  if nil = ADealItemDB then
    exit;
  if 1 > ADealItemDB.RecordCount then
    exit;
  if 0 > AProcessTask.DealItemIndex then
    AProcessTask.DealItemIndex := 0;
  if ADealItemDB.RecordCount <= AProcessTask.DealItemIndex then
    exit;
  if 0 <> AProcessTask.OwnerTask.TaskDealItemCode then
  begin                                       
    if nil = AProcessTask.DealItem then
    begin
      AProcessTask.DealItem := ADealItemDB.FindDealItemByCode(IntToStr(AProcessTask.OwnerTask.TaskDealItemCode));
      Result := AProcessTask.DealItem;
    end;
    if nil <> AProcessTask.DealItem then
    begin
      if AProcessTask.DealItem.iCode = AProcessTask.OwnerTask.TaskDealItemCode then
      begin
      end;
    end;   
    exit;
  end;          
  if nil = AProcessTask.DealItem then
  begin
    AProcessTask.DealItemIndex := 0;
  end;
  if nil = AProcessTask.DealItem  then
  begin
    while (nil = Result) do
    begin
      if AProcessTask.DealItemIndex >= ADealItemDB.RecordCount then
        Break;
      //if (2525 = ADealItemDB.Items[AProcessTask.DealItemIndex].iCode) or
      //   (1002525 = ADealItemDB.Items[AProcessTask.DealItemIndex].iCode) then
      begin
        Result := ADealItemDB.Items[AProcessTask.DealItemIndex];
      end;
      if (nil = Result) then
      begin
        if (0 < AProcessTask.TaskIncrement) then
        begin
          AProcessTask.DealItemIndex := AProcessTask.DealItemIndex + AProcessTask.TaskIncrement;
        end else
        begin
          Inc(AProcessTask.DealItemIndex);
        end;
      end;
    end;
    AProcessTask.DealItem := Result;
  end else
  begin
    tmpDealItem := ADealItemDB.Items[AProcessTask.DealItemIndex];
    if AProcessTask.DealItem.iCode = tmpDealItem.iCode then
    begin
      while nil = Result do
      begin
        Inc(AProcessTask.DealItemIndex);
        if ADealItemDB.RecordCount > AProcessTask.DealItemIndex then
        begin
          tmpDealItem := ADealItemDB.Items[AProcessTask.DealItemIndex];
          if 0 = tmpDealItem.EndDealDate then
          begin
            if 0 = tmpDealItem.FirstDealDate then
            begin
              if src_163 = AProcessTask.OwnerTask.TaskDataSrc then
              begin
                tmpDayOfWeek := DayOfWeek(now);
                if (1 = tmpDayOfWeek) or (6 = tmpDayOfWeek) or (7 = tmpDayOfWeek) then
                begin
                  Result := tmpDealItem;
                  AProcessTask.DealItem := Result;
                end;
              end;
            end else
            begin
              Result := tmpDealItem;
              AProcessTask.DealItem := Result;
            end;
          end;
        end else
        begin
          Break;
        end;
      end;
    end;
  end;
end;
                           
function TStockTaskConsole.Console_CheckDownloaderProcess(AProcessTask: PDownloadProcessTask): Boolean;
var
  tmpRetCode: DWORD;
begin
  // 这里主要 检查 下载进程是否有效
  //Log('StockDataConsoleApp.pas', 'Console_CheckDownloaderProcess');
  Result := false;
  if nil = AProcessTask then
    exit;
  Result := IsWindow(AProcessTask.DownloadProcess.Core.AppCmdWnd);
  if not Result then
  begin
    if 0 <> AProcessTask.DownloadProcess.Core.ProcessHandle then
    begin
      if Windows.GetExitCodeProcess(AProcessTask.DownloadProcess.Core.ProcessHandle, tmpRetCode) then
      begin
        if Windows.STILL_ACTIVE <> tmpRetCode then
          AProcessTask.DownloadProcess.Core.ProcessId := 0;
      end;
    end else
    begin
      AProcessTask.DownloadProcess.Core.ProcessId := 0;
    end;
    if 0 = AProcessTask.DownloadProcess.Core.ProcessId then
    begin                                     
      //Log('StockDataConsoleApp.pas', 'Console_CheckDownloaderProcess RunProcessA begin');
      RunProcessA(@AProcessTask.DownloadProcess, ParamStr(0));
      Sleep(1);
      //Log('StockDataConsoleApp.pas', 'Console_CheckDownloaderProcess RunProcessA end');
    end;
    if (0 = AProcessTask.DownloadProcess.Core.ProcessHandle) or
       (INVALID_HANDLE_VALUE = AProcessTask.DownloadProcess.Core.ProcessHandle) then
      exit;
    (*//
    tmpWindowName := IntToStr(ADownloadTask.DownloadProcess.Core.ProcessId);
    for i := 0 to 100 do
    begin
      if IsWindow(ADownloadTask.DownloadProcess.Core.AppCmdWnd) then
        Break;                   
      Sleep(10);
      ADownloadTask.DownloadProcess.Core.AppCmdWnd := Windows.FindWindowA(AppCmdWndClassName_StockDataDownloader, PAnsiChar(tmpWindowName));
      Log('StockDataConsoleApp.pas', 'Console_CheckDownloaderProcess Command Wnd:' + IntToStr(ADownloadTask.DownloadProcess.Core.AppCmdWnd));
    end;
    //*)     
    //Result := IsWindow(ADownloadTask.DownloadProcess.Core.AppCmdWnd);
    if Windows.GetExitCodeProcess(AProcessTask.DownloadProcess.Core.ProcessHandle, tmpRetCode) then
    begin
      Result := Windows.STILL_ACTIVE = tmpRetCode;
    end;
  end;
end;

procedure TStockTaskConsole.Console_NotifyDownloaderShutdown(AProcessTask: PDownloadProcessTask);
var
  i: integer;
  tmpTaskStatus: TRT_TaskStatus;
begin
  if nil = AProcessTask then
    exit;
  if taskEnd = AProcessTask.TaskStatus then
  begin
    if nil <> AProcessTask.OwnerTask then
    begin
      tmpTaskStatus := taskEnd;
      for i := Low(AProcessTask.OwnerTask.DownProcessTaskArray) to High(AProcessTask.OwnerTask.DownProcessTaskArray) do
      begin
        if AProcessTask.OwnerTask.DownProcessTaskArray[i].TaskStatus = taskRunning then
        begin
          tmpTaskStatus := taskRunning;
        end;
      end;         
      AProcessTask.OwnerTask.TaskStatus := tmpTaskStatus;
    end;
  end;
  if IsWindow(AProcessTask.DownloadProcess.Core.AppCmdWnd) then
  begin
    PostMessage(AProcessTask.DownloadProcess.Core.AppCmdWnd, WM_AppRequestEnd, 0, 0);
  end;
  if IsWindow(fAppCmdWnd) then
  begin     
    PostMessage(fAppCmdWnd, WM_ConsoleProcess2Console_Download_Finished, Integer(AProcessTask.OwnerTask), Integer(AProcessTask));
  end;
end;

initialization
  FillChar(GlobalDownloadTasks, SizeOf(GlobalDownloadTasks), 0);
  GlobalDownloadTasks[0].DownProcessTaskArray[0].OwnerTask := @GlobalDownloadTasks[0];
  GlobalDownloadTasks[0].DownProcessTaskArray[0].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task1_Sub1;
  GlobalDownloadTasks[0].DownProcessTaskArray[1].OwnerTask := @GlobalDownloadTasks[0];
  GlobalDownloadTasks[0].DownProcessTaskArray[1].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task1_Sub2;
  GlobalDownloadTasks[0].DownProcessTaskArray[2].OwnerTask := @GlobalDownloadTasks[0];
  GlobalDownloadTasks[0].DownProcessTaskArray[2].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task1_Sub3;

  GlobalDownloadTasks[1].DownProcessTaskArray[0].OwnerTask := @GlobalDownloadTasks[1];
  GlobalDownloadTasks[1].DownProcessTaskArray[0].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task2_Sub1;
  GlobalDownloadTasks[1].DownProcessTaskArray[1].OwnerTask := @GlobalDownloadTasks[1];
  GlobalDownloadTasks[1].DownProcessTaskArray[1].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task2_Sub2; 
  GlobalDownloadTasks[1].DownProcessTaskArray[2].OwnerTask := @GlobalDownloadTasks[1];
  GlobalDownloadTasks[1].DownProcessTaskArray[2].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task2_Sub3;

  GlobalDownloadTasks[2].DownProcessTaskArray[0].OwnerTask := @GlobalDownloadTasks[2];
  GlobalDownloadTasks[2].DownProcessTaskArray[0].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task3_Sub1;
  GlobalDownloadTasks[2].DownProcessTaskArray[1].OwnerTask := @GlobalDownloadTasks[2];
  GlobalDownloadTasks[2].DownProcessTaskArray[1].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task3_Sub2;   
  GlobalDownloadTasks[2].DownProcessTaskArray[2].OwnerTask := @GlobalDownloadTasks[2];
  GlobalDownloadTasks[2].DownProcessTaskArray[2].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task3_Sub3;


  GlobalDownloadTasks[3].DownProcessTaskArray[0].OwnerTask := @GlobalDownloadTasks[3];
  GlobalDownloadTasks[3].DownProcessTaskArray[0].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task4_Sub1;
  GlobalDownloadTasks[3].DownProcessTaskArray[1].OwnerTask := @GlobalDownloadTasks[3];  
  GlobalDownloadTasks[3].DownProcessTaskArray[1].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task4_Sub2; 
  GlobalDownloadTasks[3].DownProcessTaskArray[2].OwnerTask := @GlobalDownloadTasks[3];  
  GlobalDownloadTasks[3].DownProcessTaskArray[2].CommandWndProc := @ProcessTaskHostCommandWndProcA_Task4_Sub3;
  
end.
