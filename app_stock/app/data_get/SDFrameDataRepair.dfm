inherited fmeDataRepair: TfmeDataRepair
  Width = 761
  Height = 475
  object pnlDayDataTop: TPanel
    Left = 0
    Top = 427
    Width = 761
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
  end
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 761
    Height = 427
    Align = alClient
    BevelOuter = bvNone
    FullRepaint = False
    object spl1: TSplitter
      Left = 541
      Top = 0
      Height = 427
      Align = alRight
    end
    object vtDayData: TVirtualStringTree
      Left = 0
      Top = 0
      Width = 541
      Height = 427
      Margins.Left = 1
      Margins.Top = 1
      Margins.Right = 1
      Margins.Bottom = 1
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      Ctl3D = False
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      ParentCtl3D = False
      Columns = <>
    end
    object pnl1: TPanel
      Left = 544
      Top = 0
      Width = 217
      Height = 427
      Align = alRight
      BevelOuter = bvNone
      object lstDayDataRecords: TListBox
        Left = 0
        Top = 0
        Width = 217
        Height = 427
        Align = alClient
        ItemHeight = 13
      end
    end
  end
end
