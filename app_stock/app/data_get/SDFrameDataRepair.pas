unit SDFrameDataRepair;

interface

uses
  Windows, Messages, Forms, BaseFrame, Classes, Controls, Graphics, Sysutils,
  StdCtrls, ExtCtrls, VirtualTrees, VirtualTree_Editor,     
  define_price, define_dealitem, define_data_sina, define_datasrc, define_stock_quotes,
  define_dealstore_file,
  DealItemsTreeView, StockDayDataAccess;

type            
  PStockDayDataColumns = ^TStockDayDataColumns;
  TStockDayDataColumns = record
    Col_PriceOpen: TVirtualTreeColumn;  // 开盘价
    Col_PriceHigh: TVirtualTreeColumn;  // 最高价
    Col_PriceLow: TVirtualTreeColumn;   // 最低价
    Col_PriceClose: TVirtualTreeColumn; // 收盘价   
    Col_Weight: TVirtualTreeColumn;     // 权重
  end;

  TStockDataTreeCtrlData = record       
    Col_Date: TVirtualTreeColumn;
    Cols_DataSrc_163: TStockDayDataColumns;
    Cols_DataSrc_Sina: TStockDayDataColumns;   
    Col_WeightPriceOffset: TVirtualTreeColumn;     // 权重
    
    DayDataAccess_163: TStockDayDataAccess;
    DayDataAccess_Sina: TStockDayDataAccess;    
  end;
  
  TfmeDataRepairData = record
    FocusDealItem: PRT_DealItem;
    OnGetDealItem: TOnDealItemFunc;
    StockListTreeCtrl: TDealItemTreeCtrl;
    StockDataTreeCtrlData: TStockDataTreeCtrlData;
  end;

  TfmeDataRepair = class(TfmeBase)
    pnlDayDataTop: TPanel;
    pnlMain: TPanel;
    vtDayData: TVirtualStringTree;
    pnl1: TPanel;
    spl1: TSplitter;
    lstDayDataRecords: TListBox;
  private
    { Private declarations }  
    fDataRepairData: TfmeDataRepairData;      
    procedure vtDayDataGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);     
    procedure vtDayDataCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure vtDayDataEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);     
    procedure InitializeStockDayDataListView(ATreeView: TVirtualStringTree);
    procedure ClearDayData;                  
    procedure BuildStocksDayDataVirtualTree();

    function vtDayDataGetEditDataType(ATree: TBaseVirtualTree; ANode: PVirtualNode; AColumn: TColumnIndex): TEditorValueType;      
    function vtDayDataGetEditText(ATree: TBaseVirtualTree; ANode: PVirtualNode; AColumn: TColumnIndex): WideString;
    procedure vtDayDataGetEditUpdateData(ATree: TBaseVirtualTree; ANode: PVirtualNode; AColumn: TColumnIndex; AData: WideString);
    procedure vtDayDataBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode;
        Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);  
    procedure vtDayDataAfterItemPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect);
    procedure vtDayDataPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType);
  public
    { Public declarations }    
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Initialize; override;           
    procedure CallDeactivate; override;
    procedure CallActivate; override; 
    procedure NotifyDealItem(ADealItem: PRT_DealItem);
    property OnGetDealItem: TOnDealItemFunc read fDataRepairData.OnGetDealItem write fDataRepairData.OnGetDealItem;
  end;

implementation

{$R *.dfm}

uses                   
  StockDayData_Load,
  StockDayData_Save;

{ TfmeDataRepair }
                   
type
  PStockDayDataNode = ^TStockDayDataNode;
  TStockDayDataNode = record
    Date: Word;
    QuoteData_163: PRT_Quote_Day;
    QuoteData_Sina: PRT_Quote_Day;
    ErrorCheckStatus: Word; 
  end;

constructor TfmeDataRepair.Create(Owner: TComponent);
begin
  inherited;
  FillChar(fDataRepairData, SizeOf(fDataRepairData), 0);
end;

destructor TfmeDataRepair.Destroy;
begin
  ClearDayData;
  inherited;
end;

procedure TfmeDataRepair.Initialize;
begin
  inherited;
  InitializeStockDayDataListView(vtDayData);
end;

procedure TfmeDataRepair.CallDeactivate;
begin
end;

procedure TfmeDataRepair.CallActivate; 
begin
end;

procedure TfmeDataRepair.NotifyDealItem(ADealItem: PRT_DealItem);
begin
    vtDayData.BeginUpdate;
    try
      vtDayData.Clear;
      ClearDayData;
        if nil <> ADealItem then
        begin
          //edtstock.Text := ADealItem.sCode;

          fDataRepairData.FocusDealItem := ADealItem;
          fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163 := TStockDayDataAccess.Create(ADealItem, Src_163, weightNone);
          LoadStockDayData(App, fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163);
          fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina := TStockDayDataAccess.Create(ADealItem, Src_Sina, weightBackward);
          LoadStockDayData(App, fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina);

          BuildStocksDayDataVirtualTree();
        end;
    finally
      vtDayData.EndUpdate;
    end;
end;
                         
procedure TfmeDataRepair.ClearDayData;
begin
  if nil <> fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163 then
  begin
    FreeAndNil(fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163);
  end;             
  if nil <> fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina then
  begin
    FreeAndNil(fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina);
  end;                
end;

procedure TfmeDataRepair.BuildStocksDayDataVirtualTree();
var                        
  tmpVNode: PVirtualNode;
  tmpVNodeData: PStockDayDataNode;
  tmpDayData_163: PRT_Quote_Day;
  tmpDayData_Sina: PRT_Quote_Day;  
  tmpIndex1: integer;
  tmpIndex2: integer;
  tmpint64: int64;  
  tmpOffset: double;
begin
  //mmoLogs.Lines.BeginUpdate;
  lstDayDataRecords.Items.BeginUpdate;
  vtDayData.BeginUpdate;
  try
    //mmoLogs.Lines.Clear;              
    lstDayDataRecords.Items.Clear;                               
    vtDayData.Clear;
    tmpIndex1 := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163.RecordCount - 1;
    tmpIndex2 := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina.RecordCount - 1;
    while (tmpIndex1 > 0) and (tmpIndex2 > 0) do
    begin
      tmpDayData_163 := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163.RecordItem[tmpIndex1];
      tmpDayData_Sina := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina.RecordItem[tmpIndex2];
      tmpVNode := vtDayData.AddChild(nil);
      tmpVNodeData := vtDayData.GetNodeData(tmpVNode);
      if tmpDayData_163.DealDate.Value = tmpDayData_Sina.DealDate.Value then
      begin
        tmpVNodeData.QuoteData_163 := tmpDayData_163;
        tmpVNodeData.Date := tmpDayData_163.DealDate.Value;
        tmpVNodeData.QuoteData_Sina := tmpDayData_Sina;
        Dec(tmpIndex1);
        Dec(tmpIndex2);
        if 0 = tmpDayData_Sina.Weight.Value then
        begin                
          lstDayDataRecords.Items.AddObject('sina0:' + FormatDateTime('yyyy-mm-dd', tmpDayData_Sina.DealDate.Value), TObject(tmpDayData_Sina.DealDate.Value));
        end else
        begin
          if (0 = tmpDayData_Sina.PriceRange.PriceOpen.Value) or
             (0 = tmpDayData_Sina.PriceRange.PriceClose.Value) or
             (0 = tmpDayData_Sina.PriceRange.PriceLow.Value) or
             (0 = tmpDayData_Sina.PriceRange.PriceHigh.Value) then
          begin                        
            lstDayDataRecords.Items.AddObject('sina err:' + FormatDateTime('yyyy-mm-dd', tmpDayData_Sina.DealDate.Value), TObject(tmpDayData_Sina.DealDate.Value));
          end else
          begin
            tmpint64 := int64(tmpDayData_163.PriceRange.PriceOpen.Value) * Int64(tmpDayData_Sina.Weight.Value);
            tmpOffset := tmpint64 / 1000;
            tmpOffset := Abs(tmpOffset - tmpDayData_Sina.PriceRange.PriceOpen.Value);
            if 120 < tmpOffset then
            begin
              lstDayDataRecords.Items.AddObject('sina weight err:' + FormatDateTime('yyyy-mm-dd', tmpDayData_Sina.DealDate.Value), TObject(tmpDayData_Sina.DealDate.Value));
            end;
          end;
        end;
      end else
      begin
        if tmpDayData_163.DealDate.Value > tmpDayData_Sina.DealDate.Value then
        begin
          tmpVNodeData.QuoteData_163 := tmpDayData_163;
          tmpVNodeData.Date := tmpDayData_163.DealDate.Value;     
          Dec(tmpIndex1);
          lstDayDataRecords.Items.AddObject('163:' + FormatDateTime('yyyy-mm-dd', tmpDayData_163.DealDate.Value), TObject(tmpDayData_163.DealDate.Value));
        end else
        begin
          tmpVNodeData.QuoteData_Sina := tmpDayData_Sina;
          tmpVNodeData.Date := tmpDayData_Sina.DealDate.Value; 
          Dec(tmpIndex2);     
          lstDayDataRecords.Items.AddObject('sina:' + FormatDateTime('yyyy-mm-dd', tmpDayData_Sina.DealDate.Value), TObject(tmpDayData_Sina.DealDate.Value));
        end;
      end;
    end;   
    while (tmpIndex1 > 0) do
    begin
      tmpVNode := vtDayData.AddChild(nil);
      tmpVNodeData := vtDayData.GetNodeData(tmpVNode);   
      tmpDayData_163 := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163.RecordItem[tmpIndex1]; 
      tmpVNodeData.QuoteData_163 := tmpDayData_163;
      tmpVNodeData.Date := tmpDayData_163.DealDate.Value;
      lstDayDataRecords.Items.AddObject('163:' + FormatDateTime('yyyy-mm-dd', tmpDayData_163.DealDate.Value), TObject(tmpDayData_163.DealDate.Value));
      Dec(tmpIndex1);
    end; 
    while (tmpIndex2 > 0) do
    begin
      tmpVNode := vtDayData.AddChild(nil);
      tmpVNodeData := vtDayData.GetNodeData(tmpVNode);   
      tmpDayData_Sina := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina.RecordItem[tmpIndex2]; 
      tmpVNodeData.QuoteData_Sina := tmpDayData_Sina;
      tmpVNodeData.Date := tmpDayData_Sina.DealDate.Value;     
      lstDayDataRecords.Items.AddObject('sina:' + FormatDateTime('yyyy-mm-dd', tmpDayData_Sina.DealDate.Value), TObject(tmpDayData_Sina.DealDate.Value));
      Dec(tmpIndex2);
    end;  
  finally
    vtDayData.EndUpdate;       
    lstDayDataRecords.Items.EndUpdate;
    //mmoLogs.Lines.EndUpdate;
  end;
end;

procedure TfmeDataRepair.vtDayDataGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);

  function GetDayDataCellText(ADayData: PRT_Quote_Day; ADayDataCols: PStockDayDataColumns): Boolean;
  begin
    Result := false;
    if nil = ADayData then
      exit;     
    if nil <> ADayDataCols.Col_PriceOpen then
    begin
      if Column = ADayDataCols.Col_PriceOpen.Index then
      begin
        CellText := IntToStr(ADayData.PriceRange.PriceOpen.Value);
        Result := true;
        exit;
      end;
    end;   
    if nil <> ADayDataCols.Col_PriceHigh then
    begin
      if Column = ADayDataCols.Col_PriceHigh.Index then
      begin
        CellText := IntToStr(ADayData.PriceRange.PriceHigh.Value);  
        Result := true;
        exit;
      end;
    end;
    if nil <> ADayDataCols.Col_PriceLow then
    begin
      if Column = ADayDataCols.Col_PriceLow.Index then
      begin
        CellText := IntToStr(ADayData.PriceRange.PriceLow.Value); 
        Result := true;
        exit;
      end;
    end;
    if nil <> ADayDataCols.Col_PriceClose then
    begin
      if Column = ADayDataCols.Col_PriceClose.Index then
      begin
        CellText := IntToStr(ADayData.PriceRange.PriceClose.Value); 
        Result := true;
        exit;
      end;
    end;        
    if nil <> ADayDataCols.Col_Weight then
    begin
      if Column = ADayDataCols.Col_Weight.Index then
      begin
        CellText := IntToStr(ADayData.Weight.Value); 
        Result := true;
        exit;
      end;
    end;
  end;
  
var
  tmpVData: PStockDayDataNode;
  tmpOffset: double;
  tmpInt64: int64;
begin
  inherited;
  CellText := '';
  tmpVData := Sender.GetNodeData(Node);
  if nil <> tmpVData then
  begin
    if nil <> fDataRepairData.StockDataTreeCtrlData.Col_Date then
    begin
      if Column = fDataRepairData.StockDataTreeCtrlData.Col_Date.Index then
      begin
        CellText := FormatDateTime('yyyy-mm-dd', tmpVData.Date);
        exit;        
      end;
    end;
    if nil <> fDataRepairData.StockDataTreeCtrlData.Col_WeightPriceOffset then
    begin
      if Column = fDataRepairData.StockDataTreeCtrlData.Col_WeightPriceOffset.Index then
      begin
        CellText := '0';   
        if (nil <> tmpVData.QuoteData_163) and (nil <> tmpVData.QuoteData_Sina) then
        begin
          if tmpVData.QuoteData_Sina.Weight.Value > 1000 then
          begin
            tmpInt64 := Int64(tmpVData.QuoteData_163.PriceRange.PriceOpen.Value) * Int64(tmpVData.QuoteData_Sina.Weight.Value);
            tmpOffset := tmpInt64 / 1000;
            tmpOffset := tmpOffset - tmpVData.QuoteData_Sina.PriceRange.PriceOpen.Value;
            //tmpOffset := Abs(tmpOffset);
            CellText := FloatToStr(tmpOffset);   
          end;
        end;
        exit;        
      end;
    end;
    if GetDayDataCellText(tmpVData.QuoteData_163, @fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_163) then
      exit;
    GetDayDataCellText(tmpVData.QuoteData_Sina, @fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina);
  end;           
end;



procedure TfmeDataRepair.vtDayDataAfterItemPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect);
begin
  if nil = Node then
    exit;
  if vsSelected in node.States then
  begin
    if Sender.FocusedNode = Node then
    begin
      TargetCanvas.Brush.Color := clGreen;
      TargetCanvas.FrameRect(ItemRect);
    end else
    begin
      TargetCanvas.Brush.Color := clBlue;
      TargetCanvas.FrameRect(ItemRect);
    end;
  end;
end;
procedure TfmeDataRepair.vtDayDataBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode;
    Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);   
var
  tmpVData: PStockDayDataNode;
  tmpIsErrorMode: Integer;
begin
  tmpIsErrorMode := 0;                 
  tmpVData := Sender.GetNodeData(Node);
  if nil <> tmpVData then
  begin
    if (nil = tmpVData.QuoteData_163) or (nil = tmpVData.QuoteData_Sina) then
    begin
    
    end else
    begin
      if 0 = tmpVData.QuoteData_Sina.PriceRange.PriceOpen.Value then
      begin
        if nil <> fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceOpen then
        begin
          if Column = fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceOpen.Index then
          begin
            tmpIsErrorMode := 1;
          end;
        end;
      end;        
      if 0 = tmpVData.QuoteData_Sina.PriceRange.PriceClose.Value then
      begin       
        if nil <> fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceClose then
        begin
          if Column = fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceClose.Index then
          begin                  
            tmpIsErrorMode := 1;
          end;
        end;
      end;
      if 0 = tmpVData.QuoteData_Sina.PriceRange.PriceHigh.Value then
      begin            
        if nil <> fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceHigh then
        begin
          if Column = fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceHigh.Index then
          begin                  
            tmpIsErrorMode := 1;
          end;
        end;
      end;
      if 0 = tmpVData.QuoteData_Sina.PriceRange.PriceLow.Value then
      begin
        if nil <> fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceLow then
        begin
          if Column = fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina.Col_PriceLow.Index then
          begin
            tmpIsErrorMode := 1;
          end;
        end;
      end;
    end; 
    if 1 = tmpIsErrorMode then
    begin
      TargetCanvas.Brush.Color := clRed;
      TargetCanvas.FillRect(CellRect);
    end;
  end;
end;

procedure TfmeDataRepair.vtDayDataPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType); 
var
  tmpVData: PStockDayDataNode;
  tmpOffset: double;
  tmpint64: int64;
begin
  inherited;           
  tmpVData := Sender.GetNodeData(Node);
  if nil <> tmpVData then
  begin
    if (nil = tmpVData.QuoteData_163) or (nil = tmpVData.QuoteData_Sina) then
    begin
      TargetCanvas.Font.Color := clRed;
    end else
    begin
      if (tmpVData.QuoteData_Sina.Weight.Value > 200) then
      begin
        tmpint64 := int64(tmpVData.QuoteData_163.PriceRange.PriceOpen.Value) * Int64(tmpVData.QuoteData_Sina.Weight.Value);
        tmpOffset := tmpint64 / 1000;
        tmpOffset := Abs(tmpOffset - tmpVData.QuoteData_Sina.PriceRange.PriceOpen.Value);
        if tmpOffset > 40 then
        begin
          TargetCanvas.Font.Color := clRed;
          TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsStrikeOut];
        end else
        begin
          if tmpOffset > 20 then
          begin
            TargetCanvas.Font.Color := clRed;      
          end else
          begin
            if tmpOffset > 10 then
            begin
              TargetCanvas.Font.Color := clBlue;
            end;     
          end;
        end;
      end else
      begin
        // 600083 weight < 1000 995 
        if tmpVData.QuoteData_Sina.Weight.Value < 100 then
        begin
          TargetCanvas.Font.Color := clRed;
        end;
      end;
    end;
  end;
end;

procedure TfmeDataRepair.vtDayDataGetEditUpdateData(ATree: TBaseVirtualTree; ANode: PVirtualNode; AColumn: TColumnIndex; AData: WideString);

  function IsInColumns(ADayDataCols: PStockDayDataColumns): Boolean;
  begin
    Result := false;
    if nil = ADayDataCols then
      exit;
    if nil <> ADayDataCols.Col_PriceOpen then
      Result := AColumn = ADayDataCols.Col_PriceOpen.Index;
    if Result then exit;
    if nil <> ADayDataCols.Col_PriceHigh then
      Result := AColumn = ADayDataCols.Col_PriceHigh.Index;
    if Result then exit;  
    if nil <> ADayDataCols.Col_PriceLow then
      Result := AColumn = ADayDataCols.Col_PriceLow.Index;
    if Result then exit;     
    if nil <> ADayDataCols.Col_PriceClose then
      Result := AColumn = ADayDataCols.Col_PriceClose.Index;
    if Result then exit;
  end;

  function UpdateDayDataCellText(ADayData: PRT_Quote_Day; ADayDataCols: PStockDayDataColumns): Boolean;
  var
    intvalue: integer;
  begin
    Result := false;  
    if nil = ADayData then
      exit;     
    if nil <> ADayDataCols.Col_PriceOpen then
    begin          
      if AColumn = ADayDataCols.Col_PriceOpen.Index then
      begin      
        intvalue := StrToIntDef(AData, 0);
        if 0 < intvalue then
        begin
          if ADayData.PriceRange.PriceOpen.Value <> intvalue then
          begin
            ADayData.PriceRange.PriceOpen.Value := intvalue;
          end;
        end;
      end;
    end;
    if nil <> ADayDataCols.Col_PriceHigh then
    begin      
      if AColumn = ADayDataCols.Col_PriceHigh.Index then
      begin             
        intvalue := StrToIntDef(AData, 0);
        if 0 < intvalue then
        begin
          if ADayData.PriceRange.PriceHigh.Value <> intvalue then
          begin
            ADayData.PriceRange.PriceHigh.Value := intvalue;
          end;
        end;
      end;
    end;
    if nil <> ADayDataCols.Col_PriceLow then
    begin
      if AColumn = ADayDataCols.Col_PriceLow.Index then
      begin              
        intvalue := StrToIntDef(AData, 0);
        if 0 < intvalue then
        begin
          if ADayData.PriceRange.PriceLow.Value <> intvalue then
          begin
            ADayData.PriceRange.PriceLow.Value := intvalue;
          end;
        end;
      end;
    end;
    if nil <> ADayDataCols.Col_PriceClose then
    begin
      if AColumn = ADayDataCols.Col_PriceClose.Index then
      begin                        
        intvalue := StrToIntDef(AData, 0);
        if 0 < intvalue then
        begin
          if ADayData.PriceRange.PriceClose.Value <> intvalue then
          begin
            ADayData.PriceRange.PriceClose.Value := intvalue;
          end;
        end;
      end;
    end;
    if nil <> ADayDataCols.Col_Weight then
    begin      
      if AColumn = ADayDataCols.Col_Weight.Index then
      begin
        intvalue := StrToIntDef(AData, 0);
        if 0 < intvalue then
        begin
          if ADayData.Weight.Value <> intvalue then
          begin
            ADayData.Weight.Value := intvalue;
          end;
        end;
      end;
    end;
  end;
  
var
  tmpVData: PStockDayDataNode;
begin                    
  tmpVData := ATree.GetNodeData(ANode);
  if nil <> tmpVData then
  begin
    if nil = tmpVData.QuoteData_Sina then
    begin
      if nil <> tmpVData.QuoteData_163 then
      begin
        tmpVData.QuoteData_Sina := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_Sina.CheckOutRecord(tmpVData.QuoteData_163.DealDate.Value);
      end;
    end;          
    if nil = tmpVData.QuoteData_163 then
    begin
      if IsInColumns(@fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_163) then
      begin
        if 0 < tmpVData.Date then
        begin
          tmpVData.QuoteData_163 := fDataRepairData.StockDataTreeCtrlData.DayDataAccess_163.CheckOutRecord(tmpVData.Date);
        end;
      end;
    end;
    UpdateDayDataCellText(tmpVData.QuoteData_163, @fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_163);    
    UpdateDayDataCellText(tmpVData.QuoteData_Sina, @fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina);
    if (0 = tmpVData.QuoteData_Sina.PriceRange.PriceOpen.Value) and
       (0 = tmpVData.QuoteData_Sina.PriceRange.PriceClose.Value) and
       (0 = tmpVData.QuoteData_Sina.PriceRange.PriceHigh.Value) and
       (0 = tmpVData.QuoteData_Sina.PriceRange.PriceLow.Value) and
       (0 < tmpVData.QuoteData_Sina.Weight.Value) then
    begin      
      if nil <> tmpVData.QuoteData_163 then
      begin
        tmpVData.QuoteData_Sina.PriceRange.PriceOpen.Value := Trunc((tmpVData.QuoteData_163.PriceRange.PriceOpen.Value * tmpVData.QuoteData_Sina.Weight.Value) / 1000);
        tmpVData.QuoteData_Sina.PriceRange.PriceLow.Value := Trunc((tmpVData.QuoteData_163.PriceRange.PriceLow.Value * tmpVData.QuoteData_Sina.Weight.Value) / 1000);
        tmpVData.QuoteData_Sina.PriceRange.PriceHigh.Value := Trunc((tmpVData.QuoteData_163.PriceRange.PriceHigh.Value * tmpVData.QuoteData_Sina.Weight.Value) / 1000);
        tmpVData.QuoteData_Sina.PriceRange.PriceClose.Value := Trunc((tmpVData.QuoteData_163.PriceRange.PriceClose.Value * tmpVData.QuoteData_Sina.Weight.Value) / 1000);
      end;
    end;
  end;
end;

procedure TfmeDataRepair.vtDayDataCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
begin
  EditLink := TPropertyEditLink.Create(vtDayDataGetEditDataType, vtDayDataGetEditText, vtDayDataGetEditUpdateData);
end;
        
procedure TfmeDataRepair.vtDayDataEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := true;
end;
procedure TfmeDataRepair.InitializeStockDayDataListView(ATreeView: TVirtualStringTree);
var
  tmpCol: TVirtualTreeColumn;
  
  procedure InitializeDayDataColumns(ADayDataColumns: PStockDayDataColumns; ATag: string);
  begin
    tmpCol := ATreeView.Header.Columns.Add;  
    tmpCol.Text := 'Open_' + ATag;            
    tmpCol.Width := ATreeView.Canvas.TextWidth(tmpCol.Text + '00');
    tmpCol.Width := tmpCol.Width + ATreeView.TextMargin + ATreeView.Indent;
    ADayDataColumns.Col_PriceOpen := tmpCol;

    tmpCol := ATreeView.Header.Columns.Add;
    tmpCol.Text := 'High_' + ATag;           
    tmpCol.Width := ATreeView.Canvas.TextWidth(tmpCol.Text + '00');
    tmpCol.Width := tmpCol.Width + ATreeView.TextMargin + ATreeView.Indent;
    ADayDataColumns.Col_PriceHigh := tmpCol;

    tmpCol := ATreeView.Header.Columns.Add;
    tmpCol.Text := 'Low_' + ATag;           
    tmpCol.Width := ATreeView.Canvas.TextWidth(tmpCol.Text + '00');
    tmpCol.Width := tmpCol.Width + ATreeView.TextMargin + ATreeView.Indent;
    ADayDataColumns.Col_PriceLow := tmpCol;

    tmpCol := ATreeView.Header.Columns.Add;
    tmpCol.Text := 'Close_' + ATag;         
    tmpCol.Width := ATreeView.Canvas.TextWidth(tmpCol.Text + '00');
    tmpCol.Width := tmpCol.Width + ATreeView.TextMargin + ATreeView.Indent;
    ADayDataColumns.Col_PriceClose := tmpCol;

    tmpCol := ATreeView.Header.Columns.Add;
    tmpCol.Text := 'Weight_' + ATag;      
    tmpCol.Width := ATreeView.Canvas.TextWidth(tmpCol.Text + '00');
    tmpCol.Width := tmpCol.Width + ATreeView.TextMargin + ATreeView.Indent;
    ADayDataColumns.Col_Weight := tmpCol;
  end;

begin      
  ATreeView.NodeDataSize := SizeOf(TStockDayDataNode);
  ATreeView.Header.Options := [hoVisible, hoColumnResize];
  ATreeView.OnGetText := vtDayDataGetText;
  ATreeView.OnPaintText := vtDayDataPaintText;
  ATreeView.OnAfterItemPaint := vtDayDataAfterItemPaint;
  ATreeView.OnBeforeCellPaint := vtDayDataBeforeCellPaint;
  ATreeView.OnCreateEditor := vtDayDataCreateEditor;
  ATreeView.OnEditing := vtDayDataEditing;

  ATreeView.Indent := 4;
  ATreeView.TreeOptions.AnimationOptions := [];
  ATreeView.TreeOptions.SelectionOptions := [toExtendedFocus,toFullRowSelect];
  ATreeView.TreeOptions.SelectionOptions := [toExtendedFocus, toMultiSelect];  
  ATreeView.TreeOptions.AutoOptions := [];
  //ATreeView.TreeOptions.PaintOptions := [];
  
  tmpCol := ATreeView.Header.Columns.Add;
  tmpCol.Text := 'Date';
  tmpCol.Width := ATreeView.Canvas.TextWidth('2000-12-12');
  tmpCol.Width := tmpCol.Width + ATreeView.TextMargin * 2 + ATreeView.Indent * 2;
  tmpCol.Options := tmpCol.Options + [coFixed];
  fDataRepairData.StockDataTreeCtrlData.Col_Date := tmpCol;
                                                    
  tmpCol := ATreeView.Header.Columns.Add;
  tmpCol.Text := 'WeightPrice Offset';
  tmpCol.Width := ATreeView.Canvas.TextWidth(tmpCol.Text + '00');
  tmpCol.Width := tmpCol.Width + ATreeView.TextMargin * 2 + ATreeView.Indent;
  fDataRepairData.StockDataTreeCtrlData.Col_WeightPriceOffset := tmpCol;
  
  InitializeDayDataColumns(@fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_163, '163');
  InitializeDayDataColumns(@fDataRepairData.StockDataTreeCtrlData.Cols_DataSrc_Sina, 'sina');
end;

function TfmeDataRepair.vtDayDataGetEditDataType(ATree: TBaseVirtualTree; ANode: PVirtualNode; AColumn: TColumnIndex): TEditorValueType;
begin
  Result := editString;
end;

function TfmeDataRepair.vtDayDataGetEditText(ATree: TBaseVirtualTree; ANode: PVirtualNode; AColumn: TColumnIndex): WideString;
begin
  vtDayDataGetText(ATree, ANode, AColumn, ttNormal, Result);
end;
                                         
end.
