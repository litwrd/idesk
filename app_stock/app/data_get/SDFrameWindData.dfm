inherited fmeWindData: TfmeWindData
  Width = 712
  Height = 403
  object spl1: TSplitter
    Left = 479
    Top = 0
    Height = 403
    Align = alRight
  end
  object pnlRight: TPanel
    Left = 482
    Top = 0
    Width = 230
    Height = 403
    Align = alRight
    object lbl1: TLabel
      Left = 10
      Top = 10
      Width = 49
      Height = 13
      Caption = 'Wind Path'
    end
    object lblMinute: TLabel
      Left = 10
      Top = 56
      Width = 32
      Height = 13
      Caption = 'Minute'
    end
    object lblStockCode: TLabel
      Left = 159
      Top = 105
      Width = 61
      Height = 13
      Caption = 'lblStockCode'
    end
    object Label1: TLabel
      Left = 10
      Top = 80
      Width = 48
      Height = 13
      Caption = 'DownClick'
    end
    object Label2: TLabel
      Left = 10
      Top = 105
      Width = 51
      Height = 13
      Caption = 'StockCode'
    end
    object mmo1: TMemo
      Left = 1
      Top = 222
      Width = 228
      Height = 180
      Align = alBottom
    end
    object edtRootPath: TEdit
      Left = 10
      Top = 26
      Width = 210
      Height = 21
      Text = 'E:\Wind\Wind.NET.Client\WindNET'
    end
    object cmbMinute: TComboBoxEx
      Left = 71
      Top = 51
      Width = 149
      Height = 22
      ItemsEx = <>
      Style = csExDropDownList
      ItemHeight = 16
    end
    object btnExportDataM60: TButton
      Left = 10
      Top = 129
      Width = 100
      Height = 25
      Caption = 'Export Data M60'
      OnClick = btnExportDataM60Click
    end
    object edtExportDownClick: TEdit
      Left = 71
      Top = 77
      Width = 66
      Height = 21
      Text = '1'
    end
    object btnImportDataM60: TButton
      Left = 120
      Top = 129
      Width = 100
      Height = 25
      Caption = 'Import Data M60'
      OnClick = btnImportDataM60Click
    end
    object btnExportList: TButton
      Left = 10
      Top = 160
      Width = 100
      Height = 25
      Caption = 'Export List'
      OnClick = btnExportListClick
    end
    object edt1: TEdit
      Left = 71
      Top = 102
      Width = 66
      Height = 21
      Text = '1'
    end
  end
  object pnlMiddle: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Ctl3D = False
    ParentCtl3D = False
    object pnlDayDataTop: TPanel
      Left = 0
      Top = 353
      Width = 477
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
    end
    object vtMinuteData: TVirtualStringTree
      Left = 0
      Top = 0
      Width = 477
      Height = 353
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      TreeOptions.SelectionOptions = [toFullRowSelect]
      Columns = <>
    end
  end
end
