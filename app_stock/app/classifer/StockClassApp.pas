unit StockClassApp;

interface

uses
  define_dealitem,
  BaseForm,
  BaseStockFormApp,
  DBStockClass;
  
type
  TStockClassAppData = record  
    ConsoleForm: BaseForm.TfrmBase;
  end;
  
  TStockClassApp = class(TBaseStockApp)
  protected        
    fAppData: TStockClassAppData;
    function InitializeAppDB: Boolean;
  public              
    constructor Create(AppClassId: AnsiString); override;
    destructor Destroy; override;    
    function Initialize: Boolean; override;   
    procedure Finalize; override;
    procedure Run; override;      
  end;
         
var
  GlobalApp: TStockClassApp = nil;
  
implementation

uses
  Forms,
  Windows,
  Sysutils,
  StockClassConsoleForm;
  
constructor TStockClassApp.Create(AppClassId: AnsiString);
begin
  inherited;
  FillChar(fAppData, SizeOf(fAppData), 0);
end;

destructor TStockClassApp.Destroy;
begin
  inherited;
end;

function TStockClassApp.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin                   
    Self.InitializeDBStockItem();
    if 1 > StockItemDB.RecordCount then
    begin
      Result := false;
    end;
    if Result then
    begin
      Result := InitializeAppDB;
    end;
  end;
end;

procedure TStockClassApp.Finalize;
begin
  if nil <> fBaseStockAppData.StockDB.StockClassDB then
  begin
    fBaseStockAppData.StockDB.StockClassDB.Free;
    fBaseStockAppData.StockDB.StockClassDB := nil;
  end;
end;

function TStockClassApp.InitializeAppDB: Boolean;
begin
  if nil = fBaseStockAppData.StockDB.StockClassDB then
  begin
    fBaseStockAppData.StockDB.StockClassDB := TDBStockClass.Create;
    fBaseStockAppData.StockDB.StockClassDB.Initialize;    
  end;
  Result := true;
end;

procedure TStockClassApp.Run;
begin
  inherited;  
  Application.CreateForm(TfrmStockClassConsole, fAppData.ConsoleForm);
  fAppData.ConsoleForm.Initialize(Self);
  Application.Run;    
end;

(*//
function AppCmdWndProcA(AWnd: HWND; AMsg: UINT; wParam: WPARAM; lParam: LPARAM): HRESULT; stdcall;
begin
  case AMsg of
    WM_AppStart: begin
      if nil <> GlobalApp then
      begin
      end;
    end;
  end;
  Result := DefWindowProcA(AWnd, AMsg, wParam, lParam);
end;
//*)
(*//
function AppStartDelayRunProc(AParam: Pointer): HResult; stdcall;
begin
  Result := 0;
  Sleep(1 * 200);             
  if nil <> GlobalApp then
  begin
    PostMessage(GlobalApp.fBaseWinAppData.WinAppRecord.AppCmdWnd, windef_msg.WM_AppStart, 0, 0);
  end;
  Windows.ExitThread(Result);
end;
//*)
end.
