unit StockClassConsoleForm;

interface

uses
  Windows, Forms, Classes, Controls, Sysutils, StdCtrls, ExtCtrls,
  VirtualTrees, BaseForm, define_stockclass, define_datasrc, BaseApp;

type
  TClassConsoleData = record
    VNode_Sina: PVirtualNode;
  end;
  
  TfrmStockClassConsole = class(TfrmBase)
    vtClass: TVirtualStringTree;
    pnl1: TPanel;
    btn1: TButton;
    mmo1: TMemo;
    procedure btn1Click(Sender: TObject);
    procedure vtClassGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
    procedure vtClassNodeDblClick(Sender: TBaseVirtualTree;
      const HitInfo: THitInfo);
  protected
    fClassConsoleData: TClassConsoleData;
    procedure BuildClassDefineTree;   
    function CheckOutDataSrcRootNode(ADataSrc: integer): PVirtualNode;
    procedure GetStockClassOnline(AClassCode: string); overload;
  public
    constructor Create(Owner: TComponent); override;
    procedure Initialize(App: TBaseApp); override;
  end;


implementation

{$R *.dfm}

uses
  win.iobuffer,
  UtilsPinyin,
  UtilsHttp,
  StockClassApp;

type
  PClassNode  = ^TClassNode;
  TClassNode  = record
    NodeType  : Word;
    Caption   : string;
    ClassRec  : PRT_ClassDef_Rec;
  end;

constructor TfrmStockClassConsole.Create(Owner: TComponent); 
begin
  inherited;
  vtClass.NodeDataSize := SizeOf(TClassNode);
  FillChar(fClassConsoleData, SizeOf(fClassConsoleData), 0);
end;

procedure TfrmStockClassConsole.Initialize(App: TBaseApp);
begin
  inherited;
  BuildClassDefineTree;
end;


function TfrmStockClassConsole.CheckOutDataSrcRootNode(ADataSrc: integer): PVirtualNode;
var
  tmpVNode: PClassNode;
begin
  Result := nil;
  if DataSrc_Sina = ADataSrc then
  begin
    if nil = fClassConsoleData.VNode_Sina then
    begin
      fClassConsoleData.VNode_Sina := vtClass.AddChild(nil); 
      tmpVNode := vtClass.GetNodeData(fClassConsoleData.VNode_Sina);
      tmpVNode.Caption := 'www.sina.com.cn';
    end;
    Result := fClassConsoleData.VNode_Sina;
  end;
end;

procedure GetStockClassOnline_Output(AClassCode: string; APage, APageNum: integer; AOutput: TStringList);
var
  tmpUrl: AnsiString;
  tmpHttpClientSession: THttpClientSession;
  tmpHttpData: PIOBuffer;
  tmpHttpHead: THttpHeadParseSession;
  tmpAnsiData: AnsiString;
  tmpPos: integer;
  tmpLengthKey: integer;
  tmpValue: AnsiString;
  tmpKey: AnsiString;
begin
  if '' = Trim(AClassCode) then
    exit;
  // http://vip.stock.finance.sina.com.cn/mkt            
  tmpUrl := 'http://vip.stock.finance.sina.com.cn/mkt'; // --> 301
  tmpUrl := 'http://vip.stock.finance.sina.com.cn/mkt/'; // --> 200
  // 新浪行业 > 玻璃行业 new_blhy
  // 申万行业 > 轻工制造 sw_qgzz
  // 地域 > 北京 diyu_1100 上海 diyu_3100
  // 证监会行业 > 农业 hangye_ZA01
  
  tmpUrl := 'http://vip.stock.finance.sina.com.cn/mkt/#sw_qgzz';

  tmpUrl := 'http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/Market_Center.getHQNodeStockCount?node=gn_jght';
  {
    (new String("130"))
  }
  // gn_jght 概念板块 - 军工航天
  tmpUrl := 'http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/Market_Center.getHQNodeData?' +
          'page=' + IntToStr(APage) + '&' +
          'num=' + IntToStr(APageNum) + '&' +
          'sort=symbol' + '&' +
          'asc=1' + '&' +
          'node=' + AClassCode + '&' +
          'symbol=' + '&' +
          '_s_r_a=init';
  FillChar(tmpHttpClientSession, SizeOf(tmpHttpClientSession), 0);
  tmpHttpClientSession.IsKeepAlive := True;
  tmpHttpData := GetHttpUrlData(tmpUrl, @tmpHttpClientSession, nil);
  if nil <> tmpHttpData then
  begin
    try
      HttpBufferHeader_Parser(tmpHttpData, @tmpHttpHead);
      if 200 = tmpHttpHead.RetCode then
      begin
        tmpAnsiData := AnsiString(PAnsiChar(@tmpHttpData.Data[tmpHttpHead.HeadEndPos + 1]));
        if '' <> tmpAnsiData then
        begin          
            tmpKey := 'symbol:';
            tmpLengthKey := Length(tmpKey);
            tmpPos := Pos(tmpKey, tmpAnsiData);
            while 0 < tmpPos do
            begin
              tmpAnsiData := Copy(tmpAnsiData, tmpPos + 1, maxint);
              tmpPos := Pos(',', tmpAnsiData);
              if 0 < tmpPos then
              begin
                tmpValue := Copy(tmpAnsiData, tmpLengthKey + 1, tmpPos - tmpLengthKey - 1);
                tmpValue :=StringReplace(tmpValue, '"', '', [rfReplaceAll]);
                AOutput.Add(tmpValue);
              end;
              tmpPos := Pos(tmpKey, tmpAnsiData);
            end;                           
        end; 
      end;
    finally
      CheckInIOBuffer(tmpHttpData);
    end;
  end;              
end;
        
procedure TfrmStockClassConsole.GetStockClassOnline(AClassCode: string);
var  
  tmpStrs: TStringList;
  tmpPage: integer;
  tmpCount: integer;
begin
  tmpStrs := TStringList.Create;
  try
    tmpPage := 1;
    tmpCount := 1;
    while 0 < tmpCount do
    begin
      tmpStrs.Clear;
      tmpCount := 0;
      GetStockClassOnline_Output(AClassCode, tmpPage, 20, tmpStrs);
      tmpPage := tmpPage + 1;
      tmpCount := tmpStrs.Count;
      mmo1.Lines.Add('----------------------------');
      mmo1.Lines.Add(tmpStrs.Text);
      Sleep(100);
      Application.ProcessMessages;
    end;
  finally
    tmpStrs.Free;
  end;
end;

procedure TfrmStockClassConsole.BuildClassDefineTree;
var
  i: integer;
  tmpNode: PVirtualNode;
  tmpVNode: PClassNode;
  tmpDealClass: PRT_ClassDef_Rec;
  tmpNodeCnt_UnAdd: integer;
begin
  inherited;
  if nil <> vtClass.RootNode then
  begin
    if 0 < vtClass.RootNode.ChildCount then
      exit;
  end;
  if nil <> GlobalApp.StockClassDB then
  begin
    if nil <> GlobalApp.StockClassDB.ClassList then
    begin
      tmpNodeCnt_UnAdd := 1;
      while 0 < tmpNodeCnt_UnAdd do
      begin
        tmpNodeCnt_UnAdd := 0;
        for i := 0 to GlobalApp.StockClassDB.ClassList.Count - 1 do
        begin
          tmpDealClass := GlobalApp.StockClassDB.ClassList.DealClass[i];
          tmpNode := nil;
          if nil = tmpDealClass.ExData then
          begin
            if nil <> tmpDealClass.Parent then
            begin
              if nil <> tmpDealClass.Parent.ExData then
              begin
                tmpNode := vtClass.AddChild(tmpDealClass.Parent.ExData);
                tmpVNode := vtClass.GetNodeData(tmpNode);
                tmpVNode.ClassRec := tmpDealClass;
                tmpDealClass.ExData := tmpNode;
              end else
              begin
                tmpNodeCnt_UnAdd := tmpNodeCnt_UnAdd + 1;
              end;
            end else
            begin
              tmpNode := vtClass.AddChild(CheckOutDataSrcRootNode(tmpDealClass.DataSrc));
              tmpVNode := vtClass.GetNodeData(tmpNode);
              tmpVNode.ClassRec := tmpDealClass;
              tmpDealClass.ExData := tmpNode;
            end;
          end;
        end;
      end;
    end;
  end;
//
end;

procedure TfrmStockClassConsole.vtClassGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var   
  tmpVNode: PClassNode;
begin
  inherited;
  CellText := '';
  if nil <> Node then
  begin
    tmpVNode := Sender.GetNodeData(Node);
    if nil <> tmpVNode then
    begin
      if nil <> tmpVNode.ClassRec then
      begin
        CellText := tmpVNode.ClassRec.Name;
      end else
      begin
        CellText := tmpVNode.Caption;
      end;
    end;
  end;
end;

procedure TfrmStockClassConsole.btn1Click(Sender: TObject);
begin
  inherited;
  //GlobalApp.StockClassDB.AddClass(DataSrc_Sina, edtclassname.Text);
  //BuildClassDefineTree;
  GetStockClassOnline('gn_jght');
end;

procedure TfrmStockClassConsole.vtClassNodeDblClick(Sender: TBaseVirtualTree;
  const HitInfo: THitInfo);
var
  tmpVNode: PClassNode;
  tmpCode: string;
begin
  inherited;
  if nil <> HitInfo.HitNode then
  begin
    tmpVNode := Sender.GetNodeData(HitInfo.HitNode);
    if nil <> tmpVNode then
    begin
      if nil <> tmpVNode.ClassRec then
      begin            
        mmo1.Lines.Clear;
        tmpCode := 'gn_' + LowerCase(GetChinese2PinYinHeader(tmpVNode.ClassRec.Name));
        mmo1.Lines.Add(tmpCode);
        mmo1.Lines.Add('=========================');
        GetStockClassOnline(tmpCode);
      end;
    end;
  end;
end;

end.
