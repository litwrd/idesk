unit RuleEx_BDZX;

interface

uses
  BaseRule,
  StockRule,
  BaseRuleData,             
  Rule_STD,
  Rule_EMA;

type
  TRule_BDZX_Price_Data = record
    ParamN: Word;
    ParamN_AK_EMA: WORD;
    ParamN_VAR6_EMA : WORD;
    ParamN_AD1_EMA : WORD;
    Var2_Float: PArrayDouble;
    Var5_Float: PArrayDouble;
    Var6_Float: PArrayDouble;
    Ret_AK_Float: PArrayDouble;
    Ret_AJ: PArrayDouble;

    Var3_EMA: TRule_EMA_F;
    Var4_STD: TRule_STD;
    Var6_EMA: TRule_EMA_F;
    Ret_VAR_AK_EMA: TRule_EMA_F;
    Ret_AD1_EMA: TRule_EMA_F;
  end;

  TRule_BDZX_Price = class(TBaseStockRule)
  protected
    fBDZX_Price_Data: TRule_BDZX_Price_Data;
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);  
    function OnGetVar2Data(AIndex: integer): double; 
    function OnGetVar5Data(AIndex: integer): double;
    function OnGetVar6Data(AIndex: integer): double;
    function OnGetAD1Data(AIndex: integer): double;
  public
    constructor Create(ADataType: TRuleDataType = dtDouble); override;
    destructor Destroy; override;
    procedure Execute; override;

    property Var2_Float: PArrayDouble read fBDZX_Price_Data.Var2_Float;
    property Var3_EMA: TRule_EMA_F read fBDZX_Price_Data.Var3_EMA;
    property Var4_STD: TRule_STD read fBDZX_Price_Data.Var4_STD;
    property Var5_Float: PArrayDouble read fBDZX_Price_Data.Var5_Float;
    property Var6_EMA: TRule_EMA_F read fBDZX_Price_Data.Var6_EMA;
    property Var6_Float: PArrayDouble read fBDZX_Price_Data.Var6_Float;
    property Ret_VAR_AK_EMA: TRule_EMA_F read fBDZX_Price_Data.Ret_VAR_AK_EMA;
    property Ret_AK_Float: PArrayDouble read fBDZX_Price_Data.Ret_AK_Float;
    property Ret_AD1_EMA: TRule_EMA_F read fBDZX_Price_Data.Ret_AD1_EMA;
    property Ret_AJ: PArrayDouble read fBDZX_Price_Data.Ret_AJ;
  end;

implementation

{ TRule_BDZX }

(*
VAR2:=(HIGH+LOW+CLOSE*2)/4;
VAR3:=EMA(VAR2,21);
VAR4:=STD(VAR2,21);
VAR5:=((VAR2-VAR3)/VAR4*100+200)/4;
VAR6:=(EMA(VAR5,5)-25)*1.56;
AK: EMA(VAR6,2)*1.22;
AD1: EMA(AK,2);
AJ: 3*AK-2*AD1;

org
VAR2:=(HIGH+LOW+CLOSE*2)/4;
VAR3:=EMA(VAR2,21);
VAR4:=STD(VAR2,21);
VAR5:=((VAR2-VAR3)/VAR4*100+200)/4;
VAR6:=(EMA(VAR5,5)-25)*1.56;
AK: EMA(VAR6,2)*1.22;
AD1: EMA(AK,2);
AJ: 3*AK-2*AD1;
AA:100;
BB:0;
CC:80;
���: IF(CROSS(AK,AD1),58,20);
����: IF(CROSS(AD1,AK),58,20);
*)

constructor TRule_BDZX_Price.Create(ADataType: TRuleDataType = dtDouble);
begin
  inherited;
  FillChar(fBDZX_Price_Data, SizeOf(fBDZX_Price_Data), 0);
  fBDZX_Price_Data.ParamN := 21;
  fBDZX_Price_Data.ParamN_VAR6_EMA := 5;
  fBDZX_Price_Data.ParamN_AK_EMA := 2;
  fBDZX_Price_Data.ParamN_AD1_EMA := 2;
  fBDZX_Price_Data.Var3_EMA := TRule_EMA_F.Create;
  fBDZX_Price_Data.Var4_STD := TRule_STD.Create;
  fBDZX_Price_Data.Var6_EMA := TRule_EMA_F.Create;
  fBDZX_Price_Data.Ret_VAR_AK_EMA := TRule_EMA_F.Create;
  fBDZX_Price_Data.Ret_AD1_EMA := TRule_EMA_F.Create;
end;

destructor TRule_BDZX_Price.Destroy;
begin
  CheckInArrayDouble(fBDZX_Price_Data.Var2_Float);
  CheckInArrayDouble(fBDZX_Price_Data.Var5_Float);
  CheckInArrayDouble(fBDZX_Price_Data.Var6_Float);
  CheckInArrayDouble(fBDZX_Price_Data.Ret_AK_Float);  
  CheckInArrayDouble(fBDZX_Price_Data.Ret_AJ);

  fBDZX_Price_Data.Var3_EMA.Free;
  fBDZX_Price_Data.Var4_STD.Free;
  fBDZX_Price_Data.Var6_EMA.Free;
  fBDZX_Price_Data.Ret_VAR_AK_EMA.Free;
  fBDZX_Price_Data.Ret_AD1_EMA.Free;
  inherited;
end;

procedure TRule_BDZX_Price.Execute;
var 
  i: integer;
  tmpDouble_Var2: double;
  tmpDouble_Var5: double;   
  tmpDouble_AK: double;       
  tmpDouble_AJ: double;  
begin
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength; 
    if fBaseRuleData.DataLength > 0 then
    begin
      // -----------------------------------
      if fBDZX_Price_Data.Var2_Float = nil then
        fBDZX_Price_Data.Var2_Float := CheckOutArrayDouble;

      SetArrayDoubleLength(fBDZX_Price_Data.Var2_Float, fBaseRuleData.DataLength);
      // -----------------------------------
      if fBDZX_Price_Data.Var5_Float = nil then
        fBDZX_Price_Data.Var5_Float := CheckOutArrayDouble;
      SetArrayDoubleLength(fBDZX_Price_Data.Var5_Float, fBaseRuleData.DataLength);
      // -----------------------------------
      if fBDZX_Price_Data.Var6_Float = nil then
        fBDZX_Price_Data.Var6_Float := CheckOutArrayDouble;
      SetArrayDoubleLength(fBDZX_Price_Data.Var6_Float, fBaseRuleData.DataLength);
      // -----------------------------------
      if fBDZX_Price_Data.Ret_AK_Float = nil then
        fBDZX_Price_Data.Ret_AK_Float := CheckOutArrayDouble;
      SetArrayDoubleLength(fBDZX_Price_Data.Ret_AK_Float, fBaseRuleData.DataLength);
      // -----------------------------------
      if fBDZX_Price_Data.Ret_AJ = nil then
        fBDZX_Price_Data.Ret_AJ := CheckOutArrayDouble;
      SetArrayDoubleLength(fBDZX_Price_Data.Ret_AJ, fBaseRuleData.DataLength);
      // -----------------------------------
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        // (HIGH+LOW+CLOSE*2)/4;
        tmpDouble_Var2 := 2 * OnGetPriceClose(i);
        tmpDouble_Var2 := tmpDouble_Var2 + OnGetPriceHigh(i);
        tmpDouble_Var2 := tmpDouble_Var2 + OnGetPriceLow(i);
        tmpDouble_Var2 := tmpDouble_Var2 / 4;
        SetArrayDoubleValue(fBDZX_Price_Data.Var2_Float, i, tmpDouble_Var2);
      end;           
      // -----------------------------------    
      fBDZX_Price_Data.Var3_EMA.ParamN := fBDZX_Price_Data.ParamN;
      fBDZX_Price_Data.Var3_EMA.OnGetDataLength := Self.OnGetDataLength;
      fBDZX_Price_Data.Var3_EMA.OnGetDataF := Self.OnGetVar2Data;
      fBDZX_Price_Data.Var3_EMA.Execute;
      // -----------------------------------
      fBDZX_Price_Data.Var4_STD.ParamN := fBDZX_Price_Data.ParamN;
      fBDZX_Price_Data.Var4_STD.OnGetDataLength := Self.OnGetDataLength;
      fBDZX_Price_Data.Var4_STD.OnGetDataF := Self.OnGetVar2Data;
      fBDZX_Price_Data.Var4_STD.Execute;
      // -----------------------------------
      // VAR5:=((VAR2-VAR3)/VAR4*100+200)/4;
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpDouble_Var5 := GetArrayDoubleValue(fBDZX_Price_Data.Var2_Float, i) - fBDZX_Price_Data.Var3_EMA.ValueF[i];
        tmpDouble_Var5 := tmpDouble_Var5 * 100;
        if fBDZX_Price_Data.Var4_STD.ValueF[i] = 0 then
        begin
          tmpDouble_Var5 := tmpDouble_Var5;
        end else
        begin
          tmpDouble_Var5 := tmpDouble_Var5 / fBDZX_Price_Data.Var4_STD.ValueF[i];
        end;
        tmpDouble_Var5 := tmpDouble_Var5 + 200;
        tmpDouble_Var5 := tmpDouble_Var5 / 4;
        SetArrayDoubleValue(fBDZX_Price_Data.Var5_Float, i, tmpDouble_Var5);
      end;
      // -----------------------------------
      //VAR6:=(EMA(VAR5,5)-25)*1.56;
      fBDZX_Price_Data.Var6_EMA.ParamN := 5;
      fBDZX_Price_Data.Var6_EMA.OnGetDataLength := Self.OnGetDataLength;
      fBDZX_Price_Data.Var6_EMA.OnGetDataF := Self.OnGetVar5Data;
      fBDZX_Price_Data.Var6_EMA.Execute;
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        SetArrayDoubleValue(fBDZX_Price_Data.Var6_Float, i, (fBDZX_Price_Data.Var6_EMA.ValueF[i] - 25) * 1.56);
      end;
      // -----------------------------------
      // AK: EMA(VAR6,2)*1.22;
      fBDZX_Price_Data.Ret_VAR_AK_EMA.ParamN := fBDZX_Price_Data.ParamN_AK_EMA;
      fBDZX_Price_Data.Ret_VAR_AK_EMA.OnGetDataLength := Self.OnGetDataLength;
      fBDZX_Price_Data.Ret_VAR_AK_EMA.OnGetDataF := Self.OnGetVar6Data;
      fBDZX_Price_Data.Ret_VAR_AK_EMA.Execute;
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpDouble_AK := fBDZX_Price_Data.Ret_VAR_AK_EMA.valueF[i] * 1.22;
        if 0 = i then
        begin
          fBDZX_Price_Data.Ret_AK_Float.MaxValue := tmpDouble_AK;
          fBDZX_Price_Data.Ret_AK_Float.MinValue := fBDZX_Price_Data.Ret_AK_Float.MaxValue;
        end else
        begin
          if tmpDouble_AK > fBDZX_Price_Data.Ret_AK_Float.MaxValue then
            fBDZX_Price_Data.Ret_AK_Float.MaxValue := tmpDouble_AK;
          if tmpDouble_AK < fBDZX_Price_Data.Ret_AK_Float.MinValue then
            fBDZX_Price_Data.Ret_AK_Float.MinValue := tmpDouble_AK;
        end;
        SetArrayDoubleValue(fBDZX_Price_Data.Ret_AK_Float, i, tmpDouble_AK);
      end;
      // -----------------------------------
      // AD1: EMA(AK,2);
      fBDZX_Price_Data.Ret_AD1_EMA.ParamN := fBDZX_Price_Data.ParamN_AD1_EMA;
      fBDZX_Price_Data.Ret_AD1_EMA.OnGetDataLength := Self.OnGetDataLength;
      fBDZX_Price_Data.Ret_AD1_EMA.OnGetDataF := Self.OnGetAD1Data;
      fBDZX_Price_Data.Ret_AD1_EMA.Execute;
      // -----------------------------------
      // AJ: 3*AK-2*AD1;
      // -----------------------------------
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpDouble_AJ :=
            3 * GetArrayDoubleValue(fBDZX_Price_Data.Ret_AK_Float, i) -
            2 * fBDZX_Price_Data.Ret_AD1_EMA.valueF[i];
        SetArrayDoubleValue(fBDZX_Price_Data.Ret_AJ, i, tmpDouble_AJ);
        if 0 = i then
        begin
          fBDZX_Price_Data.Ret_AJ.MaxValue := tmpDouble_AJ;
          fBDZX_Price_Data.Ret_AJ.MinValue := fBDZX_Price_Data.Ret_AJ.MaxValue;
        end else
        begin
          if tmpDouble_AJ > fBDZX_Price_Data.Ret_AJ.MaxValue then
            fBDZX_Price_Data.Ret_AJ.MaxValue := tmpDouble_AJ;
          if tmpDouble_AJ < fBDZX_Price_Data.Ret_AJ.MinValue then
            fBDZX_Price_Data.Ret_AJ.MinValue := tmpDouble_AJ;
        end;
      end;
    end;
  end;
end;

function TRule_BDZX_Price.GetParamN: Word;
begin
  Result := fBDZX_Price_Data.ParamN;
end;

procedure TRule_BDZX_Price.SetParamN(const Value: Word);
begin
  if Value > 0 then
  begin
    fBDZX_Price_Data.ParamN := Value;
  end;
end;

function TRule_BDZX_Price.OnGetVar2Data(AIndex: integer): double;
begin
  Result := GetArrayDoubleValue(fBDZX_Price_Data.Var2_Float, AIndex);
end;
                        
function TRule_BDZX_Price.OnGetVar5Data(AIndex: integer): double;
begin
  Result := GetArrayDoubleValue(fBDZX_Price_Data.Var5_Float, AIndex);
end;

function TRule_BDZX_Price.OnGetVar6Data(AIndex: integer): double;
begin
  Result := GetArrayDoubleValue(fBDZX_Price_Data.Var6_Float, AIndex);
end;

function TRule_BDZX_Price.OnGetAD1Data(AIndex: integer): double;
begin
  Result := GetArrayDoubleValue(fBDZX_Price_Data.Ret_AK_Float, AIndex);
end;

end.
