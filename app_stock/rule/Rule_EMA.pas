unit Rule_EMA;

interface

uses
  BaseRule,
  BaseRuleData;

(*//
  EMA是指数平滑移动平均线  
//*)

type
  TRule_EMA_F = class(TBaseRuleF)
  protected
    fParamN: Word;
    fFloatRet: PArrayDouble;
    function GetEMAValueF(AIndex: integer): double;
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);
    procedure ComputeFloat;   
  public
    constructor Create; override;
    procedure Execute; override;
    procedure Clear; override;
    property ValueF[AIndex: integer]: double read GetEMAValueF;
    property ParamN: Word read GetParamN write SetParamN;
  end;

  // 分子 numerator 分母 denominator 比 ratio
  TParamFactor  = record { 系数 }
    Numerator   : int64; // 分子
    Denominator : int64; // 分母
    Ratio       : double; // value
  end;

  PEMAParam     = ^TEMAParam;
  TEMAParam     = record
    ParamN      : Word;
    ParamFactor1: TParamFactor; 
    ParamFactor2: TParamFactor;
  end;
                
  procedure InitEMAParam(AParam: PEMAParam; AParamN: word);

implementation

{ TRule_EMA }

constructor TRule_EMA_F.Create;
begin
  inherited;               
  fFloatRet := nil;
  Self.Feature_IsPrevRelated := True;
end;

procedure TRule_EMA_F.Execute;
begin
  Clear;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeFloat;
  end;
end;

procedure TRule_EMA_F.Clear;
begin
  CheckInArrayDouble(fFloatRet);
  fBaseRuleData.DataLength := 0;
end;

(*
  当日指数平均值 = 平滑系数*（当日指数值-昨日指数平均值）+ 昨日指数平均值
  平滑系数 = 2 /（周期单位+1）
  EMA(X,N) = 2*X/(N+1)+(N-1)/(N+1)*昨天的指数收盘平均值

http://blog.csdn.net/wlr_tang/article/details/7243287
2、EMA(X，N)求X的

N日指数平滑移动平均

。算法是：
若Y=EMA(X，N)，则Y=［2*X+(N-1)*Y’］/(N+1)，其中Y’表示上一周期的Y值。
EMA引用函数在计算机上使用递归算法很容易实现，但不容易理解。例举分析说明EMA函数。

X是变量，每天的X值都不同，从远到近地标记，它们分别记为X1，X2，X3，….，Xn
如果N=1，则EMA(X，1)=［2*X1+(1-1)*Y’］/(1+1)=X1
如果N=2，则EMA(X，2)=［2*X2+(2-1)*Y’］/(2+1)=
   (2/3)*X2+(1/3)Y’
如果N=3，则EMA(X，3)=［2*X3+(3-1)*Y’］/(3+1)=   
  ［2*X3+2*Y’］/4=
  ［2*X3+2*((2/3)*X2+(1/3)*Y’)］/4=
  (1/2)*X3+(1/3)*X2+(1/6)*Y’

如果N=4，则EMA(X，4)=［2*X4+(4-1)*Y’］/(4+1)=
  2/5*X4 + 3/5*((1/2)*X3+(1/3)*X2+(1/6)*X1)=
  2/5*X4 + 3/10*X3+3/15*X2+3/30*X1

如果N=5，则EMA(X，5)=
     2/(5+1)*X5+(5-1)/(5+1)(2/5*X4+3/10*X3+3/15*X2+3/30*X1)=
     (1/3)*X5+(4/15)*X4+(3/15)*X3+(2/15)*X2+(1/15)*X1


  EMA(X，3)=［2*X3+(3-1)*Y’］/(3+1)
           =［2*X3+(3-1)* (2*X2+(2-1) * X1)］/(3+1)
*)


procedure InitEMAParam(AParam: PEMAParam; AParamN: word);
begin                                   
  AParam.ParamN := AParamN;
  AParam.ParamFactor1.Numerator := 2;
  AParam.ParamFactor1.Denominator := AParamN + 1; 
  AParam.ParamFactor1.Ratio := AParam.ParamFactor1.Numerator / AParam.ParamFactor1.Denominator;

  AParam.ParamFactor2.Numerator := AParamN - 1;
  AParam.ParamFactor2.Denominator := AParamN + 1; 
  AParam.ParamFactor2.Ratio := AParam.ParamFactor2.Numerator / AParam.ParamFactor2.Denominator;
end;

procedure TRule_EMA_F.ComputeFloat;
var
  tmpFloat_Meta: PArrayDouble;
  i: integer;
  tmpEMAParam: TEMAParam;
  tmpDouble: double;
begin
  if Assigned(OnGetDataF) then
  begin                 
    if fFloatRet = nil then
      fFloatRet := CheckOutArrayDouble;
    tmpFloat_Meta := CheckOutArrayDouble;
    try
      SetArrayDoubleLength(fFloatRet, fBaseRuleData.DataLength);
      SetArrayDoubleLength(tmpFloat_Meta, fBaseRuleData.DataLength);

      FillChar(tmpEMAParam, SizeOf(tmpEMAParam), 0);
      InitEMAParam(@tmpEMAParam, fParamN);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpFloat_Meta.Value[i] := OnGetDataF(i);
        if i = 0 then
        begin
          SetArrayDoubleValue(fFloatRet, i, tmpFloat_Meta.Value[i]);
          fMaxValue := tmpFloat_Meta.Value[i];
          fMinValue := fMaxValue;
          fFloatRet.MaxValue := fMaxValue;
          fFloatRet.MinValue := fMaxValue;
        end else
        begin
          tmpDouble := tmpFloat_Meta.Value[i] * tmpEMAParam.ParamFactor1.Ratio +
              GetArrayDoubleValue(fFloatRet, i - 1) * tmpEMAParam.ParamFactor2.Ratio;
          SetArrayDoubleValue(fFloatRet, i, tmpDouble);
          if fMaxValue < tmpDouble then
          begin
            fMaxValue := tmpDouble; 
            fFloatRet.MaxValue := fMaxValue;
          end;
          if fMinValue > tmpDouble then
          begin
            fMinValue := tmpDouble;
            fFloatRet.MinValue := fMinValue;
          end;
        end;
      end;
    finally
      CheckInArrayDouble(tmpFloat_Meta);
    end;
  end;
end;

function TRule_EMA_F.GetParamN: Word;
begin
  Result := fParamN;
end;

procedure TRule_EMA_F.SetParamN(const Value: Word);
begin
  if Value > 0 then
    fParamN := Value;
end;

function TRule_EMA_F.GetEMAValueF(AIndex: integer): double;
begin
  Result := 0;
  if fBaseRuleData.DataType = dtDouble then
  begin
    if fFloatRet <> nil then
    begin
      Result := GetArrayDoubleValue(fFloatRet, AIndex);
    end;
  end;
end;

end.
