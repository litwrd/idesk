unit StockRule;

interface

uses
  BaseRule,
  BaseDataSet,
  StockDayDataAccess,
  StockMinuteDataAccess;
  
type
  TBaseStockRuleRec = record
    // 开盘价
    OnGetPriceOpen: TOnGetDataF;
    // 收盘价
    OnGetPriceClose: TOnGetDataF;
    // 最高价
    OnGetPriceHigh: TOnGetDataF;
    // 最低价
    OnGetPriceLow: TOnGetDataF;
    // 交易日期
    OnGetDealDay: TOnGetDataI32;
    // 交易金额
    OnGetDealAmount: TOnGetDataI64;
    // 流通股本
    OnGetDealVolume: TOnGetDataI64;
    // 总股本
    OnGetTotalVolume: TOnGetDataI64;
    // 流通市值
    OnGetDealValue: TOnGetDataI64;
    // 总市值
    OnGetTotalValue: TOnGetDataI64;

    DataSet: TBaseDataSetAccess;
  end;
  
  TBaseStockRule = class(TBaseRule)
  protected
    fBaseStockRuleRec: TBaseStockRuleRec;    
    procedure SetDataSet(const Value: TBaseDataSetAccess);
  public               
    constructor Create(ADataType: TRuleDataType = dtNone); override;
    property OnGetPriceOpen: TOnGetDataF read fBaseStockRuleRec.OnGetPriceOpen write fBaseStockRuleRec.OnGetPriceOpen;
    property OnGetPriceClose: TOnGetDataF read fBaseStockRuleRec.OnGetPriceClose write fBaseStockRuleRec.OnGetPriceClose;
    property OnGetPriceHigh: TOnGetDataF read fBaseStockRuleRec.OnGetPriceHigh write fBaseStockRuleRec.OnGetPriceHigh;
    property OnGetPriceLow: TOnGetDataF read fBaseStockRuleRec.OnGetPriceLow write fBaseStockRuleRec.OnGetPriceLow;
    property OnGetDealDay: TOnGetDataI32 read fBaseStockRuleRec.OnGetDealDay write fBaseStockRuleRec.OnGetDealDay;
    property OnGetDealAmount: TOnGetDataI64 read fBaseStockRuleRec.OnGetDealAmount write fBaseStockRuleRec.OnGetDealAmount;
    property OnGetDealVolume: TOnGetDataI64 read fBaseStockRuleRec.OnGetDealVolume write fBaseStockRuleRec.OnGetDealVolume;
    property OnGetTotalVolume: TOnGetDataI64 read fBaseStockRuleRec.OnGetTotalVolume write fBaseStockRuleRec.OnGetTotalVolume;
    property OnGetDealValue: TOnGetDataI64 read fBaseStockRuleRec.OnGetDealValue write fBaseStockRuleRec.OnGetDealValue;
    property OnGetTotalValue: TOnGetDataI64 read fBaseStockRuleRec.OnGetTotalValue write fBaseStockRuleRec.OnGetTotalValue;

    property DataSet: TBaseDataSetAccess read fBaseStockRuleRec.DataSet write SetDataSet;
  end;
  
implementation

{ TBaseStockRule }

constructor TBaseStockRule.Create(ADataType: TRuleDataType);
begin
  inherited;
  FillChar(fBaseStockRuleRec, SizeOf(fBaseStockRuleRec), 0);
end;

procedure TBaseStockRule.SetDataSet(const Value: TBaseDataSetAccess);
begin
  fBaseStockRuleRec.DataSet := Value;
  if nil <> fBaseStockRuleRec.DataSet then
  begin
    if fBaseStockRuleRec.DataSet is TStockDayDataAccess then
    begin                                                      
      OnGetDataLength := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetRecords;
      OnGetPriceOpen := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockOpenPrice;
      OnGetPriceClose := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockClosePrice;
      OnGetPriceHigh := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockHighPrice;
      OnGetPriceLow := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockLowPrice;
      
      //OnGetDealDay := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetRecords;
//      OnGetDealAmount := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockLowPrice;
//      OnGetDealVolume := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockLowPrice;
//      OnGetTotalVolume := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockLowPrice;
//      OnGetDealValue := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetStockLowPrice;
//      OnGetTotalValue := TStockDayDataAccess(fBaseStockRuleRec.DataSet).DoGetRecords;
    end;      
    if fBaseStockRuleRec.DataSet is TStockMinuteDataAccess then
    begin
      OnGetDataLength := TStockMinuteDataAccess(fBaseStockRuleRec.DataSet).DoGetRecords;
      OnGetPriceOpen := TStockMinuteDataAccess(fBaseStockRuleRec.DataSet).DoGetStockOpenPrice;
      OnGetPriceClose := TStockMinuteDataAccess(fBaseStockRuleRec.DataSet).DoGetStockClosePrice;
      OnGetPriceHigh := TStockMinuteDataAccess(fBaseStockRuleRec.DataSet).DoGetStockHighPrice;
      OnGetPriceLow := TStockMinuteDataAccess(fBaseStockRuleRec.DataSet).DoGetStockLowPrice;
    end;
  end;
end;

end.
