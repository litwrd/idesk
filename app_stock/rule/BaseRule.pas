unit BaseRule;

interface

type
  TRuleDataType = (
    dtNone,     
    dtInt32,
    dtInt64,
    dtDouble,
    dtMultiData);

  TOnGetDataLength = function: integer of object;
  TOnGetDataI32 = function (AIndex: integer): integer of object;
  TOnGetDataI64 = function (AIndex: integer): int64 of object;
  TOnGetDataF   = function (AIndex: integer): double of object;
          
  TRuleFeature = (
    RuleFeature_PrevRelated
  );

  TRuleFeatures = set of TRuleFeature;
  
  TBaseRuleData     = packed record
    DataLength      : integer;
    OnGetDataLength : TOnGetDataLength;
    DataType        : TRuleDataType;
    // 规则是否是 向前数据 相关的
    RuleFeatures    : TRuleFeatures;
  end;

  TBaseRule = class
  protected
    fBaseRuleData: TBaseRuleData;
    function GetFeature_PrevRelated: Boolean;
    procedure SetFeature_PrevRelated(const Value: Boolean);
  public
    constructor Create(ADataType: TRuleDataType = dtNone); virtual;
    destructor Destroy; override;
    procedure Execute; virtual;
    procedure Clear; virtual;
    property OnGetDataLength: TOnGetDataLength read fBaseRuleData.OnGetDataLength write fBaseRuleData.OnGetDataLength;
    property DataType: TRuleDataType read fBaseRuleData.DataType write fBaseRuleData.DataType;

    property Feature_IsPrevRelated: Boolean read GetFeature_PrevRelated write SetFeature_PrevRelated;

    property DataLength: integer read fBaseRuleData.DataLength;
  end;
                       
  TBaseRuleI64 = class(TBaseRule)
  protected             
    fMaxValue: int64;
    fMinValue: int64;      
    fOnGetDataI64    : TOnGetDataI64;
    function GetMaxI: int64; virtual;
    function GetMinI: int64; virtual;
  public                 
    constructor Create; reintroduce; virtual;
    property OnGetDataI: TOnGetDataI64 read fOnGetDataI64 write fOnGetDataI64;
    property MaxValue: int64 read GetMaxI;
    property MinValue: int64 read GetMinI;
  end;
              
  TBaseRuleI64_ParamN = class(TBaseRuleI64)
  protected     
    fParamN: Word;     
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);
  public
    property ParamN: Word read GetParamN write SetParamN;   
  end;
  
  TBaseRuleF = class(TBaseRule)
  protected   
    fMaxValue   : double;
    fMinValue   : double;
    fOnGetDataF : TOnGetDataF;
    function GetMaxF: double; virtual;
    function GetMinF: double; virtual;
  public            
    constructor Create; reintroduce; virtual;
    property OnGetDataF: TOnGetDataF read fOnGetDataF write fOnGetDataF;
    property MaxValue: double read GetMaxF;
    property MinValue: double read GetMinF;
  end;
        
  TBaseRuleF_ParamN = class(TBaseRuleF)
  protected     
    fParamN: Word;     
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);
  public
    property ParamN: Word read GetParamN write SetParamN;   
  end;
  
  TBaseRuleMultiRet = class(TBaseRule)
  public            
    constructor Create; reintroduce; virtual;
  end;

  TBaseRuleTest = class
  protected
    fRule: TBaseRule;
  public
    property Rule: TBaseRule read fRule write fRule;
  end;

implementation

{ TBaseRule }

constructor TBaseRule.Create(ADataType: TRuleDataType = dtNone);
begin
  FillChar(fBaseRuleData, SizeOf(fBaseRuleData), 0);
  fBaseRuleData.DataType := ADataType;
end;

destructor TBaseRule.Destroy;
begin
  Clear;
  inherited;
end;

procedure TBaseRule.Execute;
begin  
end;

function TBaseRule.GetFeature_PrevRelated: Boolean;
begin
  Result := RuleFeature_PrevRelated in fBaseRuleData.RuleFeatures; 
end;

procedure TBaseRule.SetFeature_PrevRelated(const Value: Boolean);
begin
  if Value then
  begin
    fBaseRuleData.RuleFeatures := fBaseRuleData.RuleFeatures + [RuleFeature_PrevRelated];
  end else
  begin
    fBaseRuleData.RuleFeatures := fBaseRuleData.RuleFeatures - [RuleFeature_PrevRelated];
  end;
end;

procedure TBaseRule.Clear;
begin
end;

{ TBaseRuleI }
constructor TBaseRuleI64.Create;
begin
  inherited Create(dtInt64);
end;

function TBaseRuleI64.GetMaxI: int64;
begin
  Result := fMaxValue;
end;

function TBaseRuleI64.GetMinI: int64;
begin
  Result := fMinValue;
end;

{ TBaseRuleF }
constructor TBaseRuleF.Create;
begin
  inherited Create(dtDouble);
end;

function TBaseRuleF.GetMaxF: double;
begin
  Result := fMaxValue;
end;

function TBaseRuleF.GetMinF: double;
begin
  Result := fMinValue;
end;

{ TBaseRuleMultiResult }

constructor TBaseRuleMultiRet.Create;
begin
  inherited Create(dtMultiData);
end;
                    
{ TBaseRuleI_ParamN }
function TBaseRuleI64_ParamN.GetParamN: Word;
begin
  Result := fParamN;
end;

procedure TBaseRuleI64_ParamN.SetParamN(const Value: Word);
begin
  if 0 < Value then
    fParamN := Value;
end;

{ TBaseRuleF_ParamN }                   
function TBaseRuleF_ParamN.GetParamN: Word;
begin
  Result := fParamN;
end;

procedure TBaseRuleF_ParamN.SetParamN(const Value: Word);
begin
  if 0 < Value then
    fParamN := Value;
end;

end.
