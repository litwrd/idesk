unit Rule_STD;

interface

uses
  BaseRule,
  BaseRuleData;

(*//
STD
含义：求标准差。
用法：STD(X,N)为X的N日估算标准差。
//*)
type
  TRule_STD = class(TBaseRuleF)
  protected
    FParamN: Word;
    fFloatRet: PArrayDouble;
    function GetStdValueF(AIndex: integer): double;
    procedure SetParamN(const Value: Word);
    procedure ComputeFloat;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Clear; override;
    property ValueF[AIndex: integer]: double read GetStdValueF;
    property ParamN: Word read FParamN write SetParamN;
  end;

implementation

{ TRule_STD }

constructor TRule_STD.Create;
begin
  inherited;
  fParamN := 2;
  fFloatRet := nil;
end;

destructor TRule_STD.Destroy;
begin
  Clear;
  inherited;
end;

procedure TRule_Std.Execute;
begin
  Clear;
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength;
    ComputeFloat;
  end;
end;

procedure TRule_Std.Clear;
begin
  CheckInArrayDouble(fFloatRet);
  fBaseRuleData.DataLength := 0;
end;
(*
  STD(CLOSE,10),
    -------------------------------------
    x := (x[n] + x[n - 1] + x[1]) / n
    -------------------------------------
    v := (x[n]-x)^2 + (x[n - 1]-x)^2 + (x[1]-x)^2
    v := v / (n - 1)
    std = sqrt(v);
    -------------------------------------
    sqrt（4） = 2
*)
procedure TRule_Std.ComputeFloat;
var
  tmpFloat_Meta: PArrayDouble;
  tmpFloat_v1: PArrayDouble;
  i, j, tmpCounter: integer;
  tmpV: double;
  tmpValue: double;
begin
  if Assigned(OnGetDataF) then
  begin
    if fFloatRet = nil then
      fFloatRet := CheckOutArrayDouble;
    tmpFloat_Meta := CheckOutArrayDouble;
    tmpFloat_v1 := CheckOutArrayDouble;
    try
      SetArrayDoubleLength(fFloatRet, fBaseRuleData.DataLength);
      SetArrayDoubleLength(tmpFloat_Meta, fBaseRuleData.DataLength);
      SetArrayDoubleLength(tmpFloat_v1, fBaseRuleData.DataLength);
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        tmpFloat_Meta.Value[i] := OnGetDataF(i);
        // -------------------------------
        tmpFloat_v1.Value[i] := 0;
        tmpCounter := 0;
        for j := 0 to fParamN - 1 do
        begin
          if i >= j then
          begin
            tmpFloat_v1.value[i] := tmpFloat_v1.value[i] + tmpFloat_Meta.value[i - j];
            Inc(tmpCounter);
          end else
          begin
            Break;
          end;
        end;
        tmpFloat_v1.value[i] := tmpFloat_v1.value[i] / tmpCounter;
        // -------------------------------
        tmpValue := 0;
        for j := 0 to tmpCounter - 1 do
        begin
          tmpv := (tmpFloat_Meta.value[i - j] - tmpFloat_v1.value[i]);
          tmpValue := tmpValue + tmpv * tmpv;
        end;
        // -------------------------------
        if tmpCounter > 1 then
        begin
          tmpValue := tmpValue / (tmpCounter - 1);
          tmpValue := Sqrt(tmpValue);
        end else
        begin
          tmpValue := 0;
        end;   
        SetArrayDoubleValue(fFloatRet, i, tmpValue);
        // -------------------------------
      end;
      SetArrayDoubleLength(tmpFloat_Meta, 0);
      SetArrayDoubleLength(tmpFloat_v1, 0);
    finally
      CheckInArrayDouble(tmpFloat_Meta);
      CheckInArrayDouble(tmpFloat_v1);
    end;
  end;
end;

procedure TRule_Std.SetParamN(const Value: Word);
begin
  if Value > 0 then
  begin
    FParamN := Value;
  end;
end;

function TRule_Std.GetStdValueF(AIndex: integer): double;
begin
  Result := 0;
  if fBaseRuleData.DataType = dtDouble then
  begin
    if fFloatRet <> nil then
    begin
      Result := GetArrayDoubleValue(fFloatRet, AIndex);
    end;
  end;
end;

end.
