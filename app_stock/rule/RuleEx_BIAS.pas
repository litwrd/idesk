unit RuleEx_BIAS;

interface

(*//
������
N1  6        2--250
N2  12       2--250
N3  24       2--250
BIAS1 :(CLOSE-MA(CLOSE,N1))/MA(CLOSE,N1)*100;
BIAS2 :(CLOSE-MA(CLOSE,N2))/MA(CLOSE,N2)*100;
BIAS3 :(CLOSE-MA(CLOSE,N3))/MA(CLOSE,N3)*100;
//*)

uses
  BaseRule,
  StockRule,
  BaseRuleData,             
  Rule_MA;

type
  TRule_BIAS_Price_Data = record
    Param_N1: Word;
    Param_N2: Word;
    Param_N3: Word;
    Ret_BIAS1: PArrayDouble;
    Ret_BIAS2: PArrayDouble;
    Ret_BIAS3: PArrayDouble;
  end;

  TRule_BIAS_Price = class(TBaseStockRule)
  protected
    fBIAS_Price_Data: TRule_BIAS_Price_Data;
    function GetParamN: Word;
    procedure SetParamN(const Value: Word);  
  public
    constructor Create(ADataType: TRuleDataType = dtDouble); override;
    destructor Destroy; override;
    procedure Execute; override;

    property Ret_BIAS1: PArrayDouble read fBIAS_Price_Data.Ret_BIAS1;
    property Ret_BIAS2: PArrayDouble read fBIAS_Price_Data.Ret_BIAS2;
    property Ret_BIAS3: PArrayDouble read fBIAS_Price_Data.Ret_BIAS3;
  end;

implementation

{ TRule_BIAS }

constructor TRule_BIAS_Price.Create(ADataType: TRuleDataType = dtDouble);
begin
  inherited;
  FillChar(fBIAS_Price_Data, SizeOf(fBIAS_Price_Data), 0);
  fBIAS_Price_Data.Param_N1 := 6;
  fBIAS_Price_Data.Param_N2 := 12;
  fBIAS_Price_Data.Param_N3 := 24;
end;

destructor TRule_BIAS_Price.Destroy;
begin
  CheckInArrayDouble(fBIAS_Price_Data.Ret_BIAS1);
  CheckInArrayDouble(fBIAS_Price_Data.Ret_BIAS2);
  CheckInArrayDouble(fBIAS_Price_Data.Ret_BIAS3);
  inherited;
end;

procedure TRule_BIAS_Price.Execute;
var 
  i: integer;
begin
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength; 
    if fBaseRuleData.DataLength > 0 then
    begin
      // -----------------------------------
      if fBIAS_Price_Data.Ret_BIAS1 = nil then
        fBIAS_Price_Data.Ret_BIAS1 := CheckOutArrayDouble;
      SetArrayDoubleLength(fBIAS_Price_Data.Ret_BIAS1, fBaseRuleData.DataLength);
      // -----------------------------------
      if fBIAS_Price_Data.Ret_BIAS2 = nil then
        fBIAS_Price_Data.Ret_BIAS2 := CheckOutArrayDouble;
      SetArrayDoubleLength(fBIAS_Price_Data.Ret_BIAS2, fBaseRuleData.DataLength);
      // -----------------------------------
      if fBIAS_Price_Data.Ret_BIAS3 = nil then
        fBIAS_Price_Data.Ret_BIAS3 := CheckOutArrayDouble;
      SetArrayDoubleLength(fBIAS_Price_Data.Ret_BIAS3, fBaseRuleData.DataLength);
      // -----------------------------------
      for i := 0 to fBaseRuleData.DataLength - 1 do
      begin
        // (HIGH+LOW+CLOSE*2)/4;
        SetArrayDoubleValue(fBIAS_Price_Data.Ret_BIAS1, i, 0);
      end;           
      // -----------------------------------
    end;
  end;
end;

function TRule_BIAS_Price.GetParamN: Word;
begin
  Result := fBIAS_Price_Data.Param_N1;
end;

procedure TRule_BIAS_Price.SetParamN(const Value: Word);
begin
  if Value > 0 then
  begin
    fBIAS_Price_Data.Param_N1 := Value;
  end;
end;

end.
