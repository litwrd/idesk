unit RuleEx_MACD;

interface

(*//              
http://blog.sina.com.cn/s/blog_163a2b9700102wnh9.html
  1979年，查拉尔·阿佩尔(Gerald Apple)创造了MACD（Moving Average Convergence and Divergence)指标，
  最开始是一种跟踪股票、黄金运行趋势的技术分析工具，由一快及一慢指数移动平均（EMA）之间的差计算出来
  买进信号：MACD从负数转向正数；卖出信号：MACD从正数转向负数。在应用上MACD应先行计算出短时期EMA和长
  时期EMA的移动平均值，12及26日EMA是最常用的。


MACD(蓝线): 计算12天平均和26天平均的差
Signal (红线): 计算macd9天均值
Histogram (柱): 计算macd与signal的差值

公式算法:
12日EMA = 前一日EMA12 X 11/13 + 今日收盘 X 2/13
26日EMA = 前一日EMA26 X 25/27 + 今日收盘 X 2/27
DIF线   = EMA12 - EMA26
DEA线   = DIF线的M日指数平滑移动平均线，此值为了不与指标原名相混淆又称DEA或DEM
MACD线  = DIF线与DEA线的差

    虽然绝大多数投资者对MACD指标相当熟悉，但是想用的遂心应手却不是一件容易的事情，MACD指标的一般研判标准可以从4个方面分析：
    1.DIF和MACD的值及线的位置；
    2.DIF和MACD的交叉情况；
    3.红柱状的收缩情况；
    4.MACD图形的形态。

参数：一般为12日、26日、9日。
指数加权平滑系数为：
12日EMA平滑系数： 2/(12日EMA+1)
26日EMA平滑系数： 2/(26日EMA+1)
DEA线平滑系数：   2/(9日EMA+1)

策略实现：
买进：DIF从下而上穿过DEA；
卖出：DIF从上往下穿过DEA

Short   12
Long    26
Mid     9
DIF:EMA(CLOSE,SHORT)-EMA(CLOSE,LONG);
DEA:EMA(DIF,MID);
MACD:(DIF-DEA)*2,COLORSTICK;
//*)

uses
  BaseRule,
  StockRule,
  BaseRuleData,    
  Rule_EMA;

type
  TRule_MACD_Price_Data = record
    Param_Short : Word;
    Param_Long  : WORD;
    Param_Mid   : WORD;
    EMA_Short   : TRule_EMA_F;
    EMA_Long    : TRule_EMA_F;
    EMA_DEA     : TRule_EMA_F;
  end;

  TRule_MACD_Price = class(TBaseStockRule)
  protected
    fMACD_Price_Data: TRule_MACD_Price_Data;
    function GetParam_Short: Word;
    procedure SetParam_Short(const Value: Word);  
    function GetParam_Long: Word;
    procedure SetParam_Long(const Value: Word);
    function GetParam_Mid: Word;
    procedure SetParam_Mid(const Value: Word);  
  public
    constructor Create(ADataType: TRuleDataType = dtDouble); override;
    destructor Destroy; override;
    procedure Execute; override;
  end;

implementation

{ TRule_MACD }

constructor TRule_MACD_Price.Create(ADataType: TRuleDataType = dtDouble);
begin
  inherited;
  FillChar(fMACD_Price_Data, SizeOf(fMACD_Price_Data), 0);
  
  fMACD_Price_Data.Param_Short := 12;
  fMACD_Price_Data.Param_Long  := 26;
  fMACD_Price_Data.Param_Mid   := 9;
end;

destructor TRule_MACD_Price.Destroy;
begin
  inherited;
end;

procedure TRule_MACD_Price.Execute;
begin
  if Assigned(OnGetDataLength) then
  begin
    fBaseRuleData.DataLength := OnGetDataLength; 
    if fBaseRuleData.DataLength > 0 then
    begin
    end;
  end;
end;

function TRule_MACD_Price.GetParam_Short: Word;    
begin
  Result := fMACD_Price_Data.Param_Short;
end;

procedure TRule_MACD_Price.SetParam_Short(const Value: Word);    
begin
  if Value > 0 then
    fMACD_Price_Data.Param_Short := Value;
end;

function TRule_MACD_Price.GetParam_Long: Word;   
begin
  Result := fMACD_Price_Data.Param_Long;
end;

procedure TRule_MACD_Price.SetParam_Long(const Value: Word);   
begin
  if Value > 0 then
    fMACD_Price_Data.Param_Long := Value;
end;

function TRule_MACD_Price.GetParam_Mid: Word;  
begin
  Result := fMACD_Price_Data.Param_Mid;
end;

procedure TRule_MACD_Price.SetParam_Mid(const Value: Word);  
begin
  if Value > 0 then
    fMACD_Price_Data.Param_Mid := Value;
end;

end.
