unit utils_zs_agentconsole;

interface

uses
  Windows, Sysutils;

type
  PDealAgent_ZSZQ   = ^TDealAgent_ZSZQ;
  TDealAgent_ZSZQ   = record
    CommandWnd      : HWND;
  end;
  
  function CallAgentBuy(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AStockCode: AnsiString; APrice: Double; ANum: Word): Boolean;
  function CallAgentSale(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AStockCode: AnsiString; APrice: Double; ANum: Word): Boolean;
  function CallAgentExport(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AStockCode: AnsiString): Boolean;

implementation

uses
  define_message,
  define_stockapp;

function SendAgentMessage(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AMsg: UINT; wParam: WPARAM; lparam: LParam): Boolean;
var
  tmpWnd: HWND;
begin              
  {$IFDEF CONSOLE}
  Writeln('SendAgentMessage begin');
  {$ENDIF}
  Result := false;
  tmpWnd := 0;
  if nil <> ADealAgent_ZSZQ then
    tmpWnd := ADealAgent_ZSZQ.CommandWnd;
  if not IsWindow(tmpWnd) then
  begin
    tmpWnd := FindWindow(define_stockapp.AppCmdWndClassName_StockDealAgent_ZSZQ, '');
  end;
  if IsWindow(tmpWnd) then
  begin                  
    if nil <> ADealAgent_ZSZQ then
      ADealAgent_ZSZQ.CommandWnd := tmpWnd;   
    {$IFDEF CONSOLE}
    Writeln('DealAgentWnd:' + IntToStr(tmpWnd));
    {$ENDIF}
    SendMessage(tmpWnd, WM_Deal_Request_Buy, wParam, lparam);
    Result := True;
  end;
  {$IFDEF CONSOLE}
  Writeln('SendAgentMessage end');
  {$ENDIF}
end;

function CallAgentBuy(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AStockCode: AnsiString; APrice: Double; ANum: Word): Boolean;
var
  tmpStockCode: Integer;
begin
  Result := false;
  if '' = AStockCode then
    exit;
  tmpStockCode := StrToIntDef(AStockCode, 0);
  if 0 >= APrice then
    Exit;
  if 100 > ANum then
    exit;
  if 0 = tmpStockCode then
    exit;
  Result := SendAgentMessage(ADealAgent_ZSZQ, WM_Deal_Request_Buy, tmpStockCode, MakeLong(Trunc(APrice * 100), ANum));
end;
                   
function CallAgentSale(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AStockCode: AnsiString; APrice: Double; ANum: Word): Boolean;
var
  tmpStockCode: Integer;
begin
  Result := false;
  if '' = AStockCode then
    exit;
  tmpStockCode := StrToIntDef(AStockCode, 0); 
  if 0 = tmpStockCode then
    exit;     
  if 0 >= APrice then
    Exit;
  if 100 > ANum then
    exit;              
  Result := SendAgentMessage(ADealAgent_ZSZQ, WM_Deal_Request_Sale, tmpStockCode, MakeLong(Trunc(APrice * 100), ANum));
end;

function CallAgentExport(ADealAgent_ZSZQ: PDealAgent_ZSZQ; AStockCode: AnsiString): Boolean;    
var
  tmpStockCode: Integer;
begin
  Result := false;
  if '' = AStockCode then
    exit;
  tmpStockCode := StrToIntDef(AStockCode, 0); 
  if 0 = tmpStockCode then
    exit;      
  Result := SendAgentMessage(ADealAgent_ZSZQ, WM_Export_Data, tmpStockCode, 0);
end;

end.
