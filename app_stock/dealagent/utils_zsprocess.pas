unit utils_zsprocess;

interface

uses
  Windows, SysUtils,
  define_zsprocess;
                                  
  (* 查找招商证券进程 *)
  function FindZSProcess(AProcess: PZsProcess): Boolean;
  procedure LaunchZSProgram(AProcess: PZsProcess);

implementation

uses
  utils_findwnd,
  UtilsWindows,
  win.process,
  utils_zs_login,
  utils_zs_main;

function FindZSProcess(AProcess: PZsProcess): Boolean;
var
  tmpMainWnd: TZsWndMain;
  tmpLoginWnd: TZsWndLogin;
begin
  FillChar(tmpMainWnd, SizeOf(tmpMainWnd), 0);
  AProcess.Process.Core.ProcessId := 0;
  if FindZSMainWnd(AProcess, @tmpMainWnd) then
  begin
    Windows.GetWindowThreadProcessId(tmpMainWnd.Core.WindowHandle, AProcess.Process.Core.ProcessId);
    //AZsProcess.Process.Core.ProcessHandle := Windows.OpenProcess(PROCESS_TERMINATE, FALSE, AZsProcess.Process.Core.ProcessId);
    //AProcess.Process.Core.ProcessHandle := Windows.OpenProcess(PROCESS_ALL_ACCESS, FALSE, AProcess.Process.Core.ProcessId);  
  end else
  begin
    if FindZSLoginWnd(AProcess, @tmpLoginWnd) then
    begin
      Windows.GetWindowThreadProcessId(tmpLoginWnd.Core.WindowHandle, AProcess.Process.Core.ProcessId);
      //AProcess.Process.Core.ProcessHandle := Windows.OpenProcess(PROCESS_ALL_ACCESS, FALSE, AProcess.Process.Core.ProcessId); 
    end;
  end;
  Result := (0 <> AProcess.Process.Core.ProcessId);
//  if Result then
//  begin
//    Windows.GetExitCodeProcess(AProcess.Process.Core.ProcessHandle, tmpExitCode);
//    Result := Windows.STILL_ACTIVE = tmpExitCode;
//  end;
end;

procedure LaunchZSProgram(AProcess: PZsProcess);
var
  tmpAnsi: AnsiString;
begin          
  //ReadZsConfig(AZsDealSession);
  tmpAnsi := AnsiString(PAnsiChar(@AProcess.ZsProgramFileUrl[0]));  
  tmpAnsi := 'F:\StockApp\zd_zszq\';
  tmpAnsi := 'D:\stock\zd_zszq\';
  tmpAnsi := 'F:\Program Files\zd_zszq\';
  CopyMemory(@AProcess.ZsProgramPathUrl[0], @tmpAnsi[1], Length(tmpAnsi));
  tmpAnsi := tmpAnsi +'TdxW.exe';
  CopyMemory(@AProcess.ZsProgramFileUrl[0], @tmpAnsi[1], Length(tmpAnsi));
  if '' <> tmpAnsi then
  begin  
    //Log('', 'LaunchZSProgram:' + tmpAnsi);
    if FileExists(tmpAnsi) then
    begin
      // win10 run as manager how to solve ???    
      RunProcessA(@AProcess.Process, tmpAnsi, nil);
      Sleep(10);
      if 0 <> AProcess.Process.Core.ProcessId then
      begin
      end;
    end;
  end;
end;

end.
