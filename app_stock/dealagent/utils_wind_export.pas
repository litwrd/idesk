unit utils_wind_export;

interface

uses
  Windows, Messages, SysUtils,
  define_windprocess;

  procedure CallExport_QuoteData(AWindProcess: PWindProcess; AWndMain: PWindWndMain; AStock: integer; AMinute: integer);
  procedure CallExport_FirstListInfo(AWindProcess: PWindProcess; AWndMain: PWindWndMain);
  procedure CallExport_InfoF10_1(AWindProcess: PWindProcess; AWndMain: PWindWndMain; AStock: integer);

var 
  Global_Export_DownClickCount: integer = 10;

implementation

uses
  RichEdit,
  UtilsWindows,
  utils_wind_dialog,  
  utils_findwnd,
  utils_wind_main;
            
function FindWindExportDialogWnd(AWindProcess: PWindProcess; AWndDialog: PWindWndDialog): Boolean;
var
  tmpFind: TExWindowEnumFind;
  //tmpWndLogin: TWindWndLogin;
begin
  FillChar(tmpFind, SizeOf(tmpFind), 0);
  tmpFind.NeedWinCount := 1;
  tmpFind.WndClassKey := '#32770';
  tmpFind.WndCaptionKey := '数据导出';
  Windows.EnumWindows(@EnumFindDesktopWindowProc, Integer(@tmpFind));
  Result := tmpFind.FindCount > 0;
  if Result then
  begin
    AWndDialog.Core.WindowHandle := tmpFind.FindWindow[0];
  end;
end;
                    
procedure TraverseCheckExportDialogChildWindowA(AWnd: HWND; AWndDialog: PWindWndDialog);  
var
  tmpWndChild: HWND;
  tmpStr: string;  
  tmpCtrlId: integer;
  tmpIsHandled: Boolean;
begin                
  if not IsWindowVisible(AWnd) then
    exit;             
  if not IsWindowEnabled(AWnd) then
    exit;                       
  tmpStr := GetWndTextName(AWnd); 
  tmpIsHandled := False;
  tmpCtrlId := Windows.GetDlgCtrlID(AWnd);
  //===============================
  if SameText('导出', tmpStr) then
  begin
    AWndDialog.OKButton := AWnd;
  end;
  //===============================                                 
  if not tmpIsHandled then
  begin
    tmpWndChild := Windows.GetWindow(AWnd, GW_CHILD);
    while 0 <> tmpWndChild do
    begin
      TraverseCheckExportDialogChildWindowA(tmpWndChild, AWndDialog);       
      tmpWndChild := Windows.GetWindow(tmpWndChild, GW_HWNDNEXT);
    end;
  end;
end;

procedure SimulateKeyPress(AKeyCode: Byte);
begin
  Windows.keybd_event(AKeyCode, MapVirtualKey(AKeyCode, 0), 0, 0) ;//#a键位码是86
  SleepWait(10);
  Windows.keybd_event(AKeyCode, MapVirtualKey(AKeyCode, 0), KEYEVENTF_KEYUP, 0);
  SleepWait(10);
end;

procedure SimulateExportCommand();
var
  i: integer;    
  tmpCommand: AnsiString; 
begin
  tmpCommand := '34';
  for i := 1 to Length(tmpCommand) do
  begin
    SimulateKeyPress(Byte(tmpCommand[i]));
    SleepWait(100);
  end;     
  SleepWait(200);
  SimulateKeyPress(VK_RETURN);
  SleepWait(10);
end;

function ExportMinuteCommand(AMinute: Word): AnsiString;
begin
  Result := '95';
  if 10 = AMinute then
    Result := '99';
  if 1 = AMinute then
    Result := '91';
  if 5 = AMinute then
    Result := '92';
  if 15 = AMinute then
    Result := '93';
  if 30 = AMinute then
    Result := '94';
  // 60 分钟线
  if 60 = AMinute then
    Result := '95';
  // 日线
  if 240 = AMinute then
    Result := '96';
  // 周线  
  if 240 * 5 = AMinute then
    Result := '97';
  // 月线
  if 240 * 20 = AMinute then
    Result := '98';
  // 45日线
  if 240 * 45  = AMinute then
    Result := '910';
  // 季线
  if 240 * 20 * 3 = AMinute then
    Result := '911';
  // 年线
  if 240 * 20 * 12 = AMinute then
    Result := '912';        
end;

procedure HandleExportDialog(AWindProcess: PWindProcess; AExportFile: AnsiString); forward;

procedure CallExport_QuoteData(AWindProcess: PWindProcess; AWndMain: PWindWndMain; AStock: integer; AMinute: integer);
var
  tmpRect: TRect;
  tmpCommand: AnsiString;
  i: integer;
begin
  if not IsEnsureWindAgent_Main(AWindProcess, AWndMain) then
    exit;
  if not IsWindow(AWndMain.WndTopHeaderPanelRoot) then
  begin
    TraverseCheckMainChildWindowA(AWndMain.Core.WindowHandle, AWndMain);
  end;
  if IsWindow(AWndMain.WndTopHeaderPanelRoot) then
  begin
    if not IsWindow(AWndMain.WndMenuCommandEdit) then
    begin
      TraverseCheckMainTopHeaderPanelChildWindowA(AWndMain.WndTopHeaderPanelRoot, AWndMain);
    end;
    if IsWindow(AWndMain.WndMenuCommandEdit) then
    begin
      GetWindowRect(AWndMain.WndMenuCommandEdit, tmpRect);
      Windows.SetCursorPos(tmpRect.Left + 10, tmpRect.Top + 10);
      SleepWait(20);
      Windows.mouse_event(MOUSEEVENTF_LEFTDOWN or MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
      SleepWait(100);
          
      tmpCommand := inttostr(AStock);
      if 6 > Length(tmpCommand) then
      begin
        tmpCommand := Copy('000000', 1, 6 - Length(tmpCommand)) + tmpCommand;
      end else if 6 < Length(tmpCommand) then
      begin
        tmpCommand := Copy(tmpCommand, Length(tmpCommand) - 6 + 1, maxint);
      end;
      for i := 1 to Length(tmpCommand) do
      begin
        SimulateKeyPress(Byte(tmpCommand[i]));
        SleepWait(100);
      end;   
      SimulateKeyPress(VK_RETURN);
      SleepWait(10); 
      SleepWait(100);

      tmpCommand := ExportMinuteCommand(AMinute);
      if '' <> tmpCommand then
      begin
        for i := 1 to Length(tmpCommand) do
        begin
          SimulateKeyPress(Byte(tmpCommand[i]));
          SleepWait(100);
        end;
        SimulateKeyPress(VK_RETURN);
        SleepWait(10);
      end;   
      
      if 0 < Global_Export_DownClickCount then
      begin
        for i := 1 to Global_Export_DownClickCount do
        begin                  
          SimulateKeyPress(VK_DOWN);
          SleepWait(10);
          SleepWait(200);
        end;
      end else
      begin
        SleepWait(2000);
      end;
      SleepWait(400);
      SimulateExportCommand();  
      SleepWait(200);
      HandleExportDialog(AWindProcess, '');
    end;
  end;
end;

procedure HandleExportDialog(AWindProcess: PWindProcess; AExportFile: AnsiString);
var
  i: integer;  
  tmpWndDialog: TWindWndDialog;
begin
  FillChar(tmpWndDialog, SizeOf(tmpWndDialog), 0);
  for i := 1 to 5 do
  begin
    if FindWindExportDialogWnd(AWindProcess, @tmpWndDialog) then
    begin
      TraverseCheckExportDialogChildWindowA(tmpWndDialog.Core.WindowHandle, @tmpWndDialog);
      SleepWait(10);
      if IsWindow(tmpWndDialog.OKButton) then
      begin
        ClickButtonWnd(tmpWndDialog.OKButton);  
        SleepWait(50); 
        Break;
      end;
    end else
    begin
      SleepWait(100);
    end;
  end;
  SleepWait(200);
  // escape 关闭对话框    
  SimulateKeyPress(VK_ESCAPE);
  SleepWait(10);      
  while CloseWindDialog(AWindProcess, nil) do
  begin
    SleepWait(100);
  end;
end;

procedure CallExport_FirstListInfo(AWindProcess: PWindProcess; AWndMain: PWindWndMain);
var
  i: integer;
begin
  if not IsEnsureWindAgent_Main(AWindProcess, AWndMain) then
    exit;
  for i := 1 to 3 do
  begin
    SleepWait(300);
    // escape 关闭对话框
    SimulateKeyPress(VK_ESCAPE);
    SleepWait(10);
  end;        
  SimulateExportCommand(); 
  SleepWait(500);
  HandleExportDialog(AWindProcess, ''); 
  SimulateKeyPress(VK_NEXT);
end;

(*//
function EditStreamCallBack(dwCookie: Longint; pbBuff: PByte; cb: Longint; var pcb: Longint): Longint; stdcall;
var
  vCopyDataStruct: TCopyDataStruct;
begin
  pcb := cb;
  vCopyDataStruct.dwData := 0;
  vCopyDataStruct.cbData := cb;
  vCopyDataStruct.lpData := pbBuff;
  //vMySendMessage(dwCookie, WM_COPYDATA, 0, Integer(@vCopyDataStruct));
  Result := ERROR_SUCCESS;
end;
//*)

procedure CallExport_InfoF10_1(AWindProcess: PWindProcess; AWndMain: PWindWndMain; AStock: integer);
var
  tmpRichEditHandle: HWND;
  //tmpEditStream: RichEdit.TEditStream;
  tmpProcessID: DWORD;                  
  tmpProcessHandle: DWORD;
  tmpDataPointer: Pointer;

  tmpGetTextEx: RichEdit.TGETTEXTEX;
  tmpGetTextLengthEx: RichEdit.TGetTextLengthEx;
  tmpNumberOfBytesRead: Cardinal;
  tmpRet: Integer;
  tmpData: WideString;
begin
  if not IsEnsureWindAgent_Main(AWindProcess, AWndMain) then
    exit;
  tmpData := '';
  tmpRichEditHandle := $00250BDC;
  if IsWindow(tmpRichEditHandle) then
  begin
    GetWindowThreadProcessId(tmpRichEditHandle, tmpProcessID);
    tmpProcessHandle := OpenProcess(PROCESS_VM_OPERATION or PROCESS_VM_READ or PROCESS_VM_WRITE, False, tmpProcessID);
    try
      tmpRet := 0;
      tmpDataPointer := VirtualAllocEx(tmpProcessHandle, nil, 4096, MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
      try
        tmpGetTextLengthEx.flags := GTL_DEFAULT;
        tmpGetTextLengthEx.codepage := 1200; // Unicode
        WriteProcessMemory(tmpRichEditHandle, tmpDataPointer, @tmpGetTextLengthEx, SizeOf(tmpGetTextLengthEx), tmpNumberOfBytesRead);
        tmpRet := SendMessage(tmpRichEditHandle, EM_GETTEXTLENGTHEX, Integer(tmpDataPointer), 0);
      finally
        VirtualFreeEx(tmpProcessHandle, tmpDataPointer, 0, MEM_RELEASE);
      end;   
      if 0 < tmpRet then
      begin
        tmpDataPointer := VirtualAllocEx(tmpProcessHandle, nil, SizeOf(tmpGetTextEx) + tmpRet * 2 + 2, MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
        try
          SetLength(tmpData, tmpRet);
          tmpGetTextEx.cb := tmpRet * 2 + 2; // 加上结束符号
          tmpGetTextEx.flags := GT_DEFAULT;
          tmpGetTextEx.codepage := 1200; // Unicode
          tmpGetTextEx.lpDefaultChar := nil;
          tmpGetTextEx.lpUsedDefChar := nil;
          WriteProcessMemory(tmpProcessHandle, tmpDataPointer, @tmpGetTextEx, SizeOf(tmpGetTextEx), tmpNumberOfBytesRead);
          SendMessage(tmpRichEditHandle, EM_GETTEXTEX, Integer(tmpDataPointer), Integer(tmpDataPointer) + SizeOf(tmpGetTextEx));
          ReadProcessMemory(tmpProcessHandle, Pointer(Integer(tmpDataPointer) + SizeOf(tmpGetTextEx)), @tmpData[1], tmpRet * 2, tmpNumberOfBytesRead);
        finally
          VirtualFreeEx(tmpProcessHandle, tmpDataPointer, 0, MEM_RELEASE);
        end;
      end;
        //tmpEditStream.dwCookie := 0;
        //tmpEditStream.dwError := 0;
        //tmpEditStream.pfnCallback := EditStreamCallBack;
        //WIndows.SendMessage(tmpRichEditHandle, RichEdit.EM_STREAMOUT, SF_RTF, Longint(@tmpEditStream));
    finally
      CloseHandle(tmpProcessHandle);
    end;
  end;
  // 读取跨进程 RichEdit 数据 
  if '' <> tmpData then
  begin
    // save to file
  end;
end;

end.
