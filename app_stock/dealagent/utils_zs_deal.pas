unit utils_zs_deal;

interface

uses
  Windows, SysUtils,
  define_zsprocess;


  procedure ClickFunctionTreeCancelNode(AWndMain: PZsWndMain);     
  procedure ClickFunctionTreeNode(AWndMain: PZsWndMain; ALeftOffset, ATopOffset: integer);   

  procedure CallBuy(AZsProcess: PZsProcess; AWndMain: PZsWndMain; AStock: integer; APrice: Double; ANum: integer);  
  procedure CallSale(AZsProcess: PZsProcess; AWndMain: PZsWndMain; AStock: integer; APrice: Double; ANum: integer);
  procedure CallCancel(AZsProcess: PZsProcess; AWndMain: PZsWndMain; AOrderNo: AnsiString);
    
implementation

uses
  UtilsWindows,
  utils_zs_login,
  utils_zs_dialog,
  utils_zs_main;
                 
procedure ClickFunctionTreeNode(AWndMain: PZsWndMain; ALeftOffset, ATopOffset: integer);
var
  tmpRect: TRect;
  tmpWnd: HWND;
begin
  tmpWnd := AWndMain.WndFunctionTree;
  if IsWindow(tmpWnd) then
  begin
    GetWindowRect(tmpWnd, tmpRect);
    Windows.SetCursorPos(tmpRect.Left + ALeftOffset, tmpRect.Top + ATopOffset);
    SleepWait(20);
    Windows.mouse_event(MOUSEEVENTF_LEFTDOWN or MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
  end;
end;
          
procedure ClickFunctionTreeBuyNode(AWndMain: PZsWndMain);
begin
  ClickFunctionTreeNode(AWndMain, 30, 10);
end;
             
  
procedure ClickFunctionTreeSaleNode(AWndMain: PZsWndMain);  
begin                    
  ClickFunctionTreeNode(AWndMain, 30, 30); 
end;

procedure ClickFunctionTreeCancelNode(AWndMain: PZsWndMain);
begin
  ClickFunctionTreeNode(AWndMain, 30, 70); 
end;

procedure FillDealInfo(AZsProcess: PZsProcess; AWndMainDeal: PZSWndMainDeal; AStock: integer; APrice: double; ANum: integer);
var
  tmpWndDialogs: TZsWndDialogs;
  tmpStockCode: AnsiString;
begin
  tmpStockCode := IntToStr(AStock);
  tmpStockCode := Copy('000000', 1, 6 - Length(tmpStockCode)) + tmpStockCode;
  if IsWindow(AWndMainDeal.WndAccountCombo) then
  begin
    //InputEditWnd(tmpMainDealWnd.WndStockCodeEdit, '002414');   
    InputEditWnd(AWndMainDeal.WndStockCodeEdit, tmpStockCode);
    SleepWait(20);
    //InputEditWnd(tmpMainDealWnd.WndPriceEdit, FormatFloat('0.00', 25.1));
    InputEditWnd(AWndMainDeal.WndPriceEdit, FormatFloat('0.00', APrice));
    SleepWait(20);
    InputEditWnd(AWndMainDeal.WndNumEdit, IntToStr(ANum));
    SleepWait(20);
    ClickButtonWnd(AWndMainDeal.WndOrderButton);


    FillChar(tmpWndDialogs, SizeOf(tmpWndDialogs), 0);
    if FindZSDialogWnd(AZsProcess, @tmpWndDialogs, 0) then
    begin
      if 1 = tmpWndDialogs.WndCount then
      begin
        ForceBringFrontWindow(tmpWndDialogs.WndArray[0].Core.WindowHandle);
        ClickButtonWnd(tmpWndDialogs.WndArray[0].OKButton);
      end;
    end;
    Sleep(100);
    while CloseZsDialog(AZsProcess, 0) do
    begin
      Sleep(100);
    end;
  end;
end;
               
function EnsureAppOrderStatus(AZsProcess: PZsProcess; AMainWnd: PZsWndMain): Boolean;
begin
  Result := false;
  if not EnsureAppLoginStatus(AZsProcess, AMainWnd) then
    exit;       
  while CloseZsDialog(AZsProcess, 0) do
  begin
    Sleep(100);
  end;
  if not IsWindow(AMainWnd.WndFunctionTree) then
  begin
    if IsWindow(AMainWnd.Core.WindowHandle) then
    begin
      TraverseCheckMainChildWindowA(AMainWnd.Core.WindowHandle, AMainWnd);
    end;
  end;
  Result := IsWindow(AMainWnd.WndFunctionTree);
  //GlobalApp
end;

procedure CallBuy(AZsProcess: PZsProcess; AWndMain: PZsWndMain; AStock: integer; APrice: Double; ANum: integer);
var  
  tmpMainDealWnd: TZSWndMainDeal;
  i: integer;
begin
  if 0 >= APrice then
    exit;
  if 100 > ANum then
    exit;
  if not EnsureAppOrderStatus(AZsProcess, AWndMain) then
    exit;
  ClickFunctionTreeBuyNode(AWndMain);

  if IsWindow(AWndMain.WndFunctionTree) then
  begin
    FillChar(tmpMainDealWnd, SizeOf(tmpMainDealWnd), 0);

    for i := 1 to 10 do
    begin
      SleepWait(20);
      TraverseCheckMainDealPanelChildWindowA(AWndMain.WndDealPanel, AWndMain, @tmpMainDealWnd);
      if IsWindow(tmpMainDealWnd.WndStockCodeEdit) then
        Break;
    end;
    if IsWindow(tmpMainDealWnd.WndStockCodeEdit) then
    begin
      FillDealInfo(AZsProcess, @tmpMainDealWnd, AStock, APrice, ANum);
    end;
  end;
end;
                
procedure CallSale(AZsProcess: PZsProcess; AWndMain: PZsWndMain; AStock: integer; APrice: Double; ANum: integer);
var  
  tmpMainDealWnd: TZSWndMainDeal;
  i: integer;
begin
  if 0 >= APrice then
    exit;
  if 100 > ANum then
    exit;
  if not EnsureAppOrderStatus(AZsProcess, AWndMain) then
    exit;
  ClickFunctionTreeSaleNode(AWndMain);

  if IsWindow(AWndMain.WndFunctionTree) then
  begin
    FillChar(tmpMainDealWnd, SizeOf(tmpMainDealWnd), 0);
    for i := 1 to 10 do
    begin
      SleepWait(20);
      TraverseCheckMainDealPanelChildWindowA(AWndMain.WndDealPanel, AWndMain, @tmpMainDealWnd);
      if IsWindow(tmpMainDealWnd.WndStockCodeEdit) then
        Break;
    end;
    if IsWindow(tmpMainDealWnd.WndStockCodeEdit) then
    begin
      FillDealInfo(AZsProcess, @tmpMainDealWnd, AStock, APrice, ANum);
    end;
  end;
end;

procedure CallCancel(AZsProcess: PZsProcess; AWndMain: PZsWndMain; AOrderNo: AnsiString);
begin
//
end;

end.
