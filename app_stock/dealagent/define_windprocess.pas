unit define_windprocess;

interface

uses
  Windows,
  win.process;
  
type                          
  PWindWndLogin           = ^TWindWndLogin;
  PWindWndMain            = ^TWindWndMain;
  PWindWndDialogs         = ^TWindWndDialogs;
  PWindWndDialog          = ^TWindWndDialog;

  PWindExProcessWnd       = ^TWindExProcessWnd;
  TWindExProcessWnd       = record
    ParentWindow          : HWND;
    WindowHandle          : HWND;
    CloseButton           : HWND;
  end;
             
  PWindProcess            = ^TWindProcess;
  TWindProcess            = record
    IsLaunchedFlag        : Byte;
    Process               : TRT_OwnedProcess;

    
    //Start                 : TStartupInfoA;
    //ProcInfo              : TProcessInformation;

    MainWindow            : PWindWndMain;
    LoginWindow           : PWindWndLogin;
    LoginAccountId        : integer;
    WindProgramFileUrl    : array[0..64 - 1] of AnsiChar;
    WindProgramPathUrl    : array[0..64 - 1] of AnsiChar;
    WindAccount           : array[0..15] of AnsiChar;
    WindPassword          : array[0..15] of AnsiChar;
  end;
       
  TWindWndLogin             = record
    Core                  : TWindExProcessWnd;
    WindProcess           : PWindProcess;
    WndAccountEdit        : HWND;
    WndPasswordEdit       : HWND;
    WndVerifyCodeEdit     : HWND;
    WndLoginButton        : HWND;
  end;

  TWindWndDialog          = record
    Core                  : TWindExProcessWnd;
    WndText               : AnsiString;
    OKButton              : HWND;
    CancelButton          : HWND;
  end;
  
  TWindWndDialogs         = record
    WndCount              : integer;
    WndArray              : array[0..7] of TWindWndDialog;
    WindProcess           : PWindProcess;
  end;  

  TWindWndMain            = record  
    Core                  : TWindExProcessWnd;
    WindProcess             : PWindProcess;
    // 整个头面板
    WndTopHeaderPanelRoot : HWND;
    WndMenuOrderButton    : HWND;
    WndMenuCommandEdit    : HWND;    
    // 整个交易面板
    //     包括买入卖出功能树  
    WndDealPanelRoot      : HWND;    
    WndDealPanel          : HWND; // E81E
    // 买入卖出 ... 功能树
    WndFunctionTree       : HWND;
  end;
          
implementation

end.
