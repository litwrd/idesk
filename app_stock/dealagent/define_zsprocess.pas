unit define_zsprocess;

interface

uses
  Windows,
  win.process;
  
type                          
  PZsWndLogin             = ^TZsWndLogin;
  PZsWndMain              = ^TZsWndMain;    
  PZsWndDialogs           = ^TZsWndDialogs;  
  PZsWndDialog            = ^TZsWndDialog;
  
  PZsExProcessWnd         = ^TZsExProcessWnd;
  TZsExProcessWnd         = record
    ParentWindow          : HWND;
    WindowHandle          : HWND;
    CloseButton           : HWND;
  end;
             
  PZsProcess              = ^TZsProcess;
  TZsProcess              = record
    IsLaunchedFlag        : Byte;
    Process               : TRT_OwnedProcess;

    
    //Start                 : TStartupInfoA;
    //ProcInfo              : TProcessInformation;

    MainWindow            : PZsWndMain;
    LoginWindow           : PZsWndLogin;
    LoginAccountId        : integer;
    ZsProgramFileUrl      : array[0..64 - 1] of AnsiChar;
    ZsProgramPathUrl      : array[0..64 - 1] of AnsiChar;
    ZsAccount             : array[0..15] of AnsiChar;
    ZsPassword            : array[0..15] of AnsiChar;    
  end;
       
  TZsWndLogin             = record
    Core                  : TZsExProcessWnd;
    ZsProcess             : PZsProcess;
    WndAccountEdit        : HWND;
    WndPasswordEdit       : HWND;
    WndVerifyCodeEdit     : HWND;
    WndLoginButton        : HWND;
  end;

  TZsWndDialog            = record
    Core                  : TZsExProcessWnd;
    WndText               : AnsiString;
    OKButton              : HWND;
    CancelButton          : HWND;
  end;
  
  TZsWndDialogs           = record
    WndCount              : integer;
    WndArray              : array[0..7] of TZsWndDialog;
    ZsProcess             : PZsProcess;
  end;  

  TZsWndMain              = record  
    Core                  : TZsExProcessWnd;
    ZsProcess             : PZsProcess;
    // 整个头面板
    WndTopHeaderPanelRoot : HWND;
    WndMenuOrderButton    : HWND;
    WndMenuCommandEdit    : HWND;    
    // 整个交易面板
    //     包括买入卖出功能树  
    WndDealPanelRoot      : HWND;    
    WndDealPanel          : HWND; // E81E
    // 买入卖出 ... 功能树
    WndFunctionTree       : HWND;
  end;
                 
  // 交易面板 买卖
  PZSWndMainDeal          = ^TZSWndMainDeal;
  TZSWndMainDeal          = record
    WndMain               : PZsWndMain;
    WndAccountCombo       : HWND; // 2EEF
    WndStockCodeEdit      : HWND; // 2EE5
    WndPriceEdit          : HWND; // 2EE6
    WndNumEdit            : HWND; // 2EE7
    WndOrderButton        : HWND; // 7DA
    OrderText             : AnsiString;
  end;
           
implementation

end.
