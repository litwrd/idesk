unit utils_windprocess;

interface

uses
  Windows, SysUtils,
  define_windprocess;
                                  
  (* ���ҽ��� *)
  function FindWindProcess(AProcess: PWindProcess): Boolean;
  procedure LaunchWindProgram(AProcess: PWindProcess);

implementation

uses
  utils_findwnd,
  UtilsWindows,
  win.process,
  utils_wind_login,
  utils_wind_main;

function FindWindProcess(AProcess: PWindProcess): Boolean;
var
  tmpMainWnd: TWindWndMain;
  tmpLoginWnd: TWindWndLogin;
begin
  FillChar(tmpMainWnd, SizeOf(tmpMainWnd), 0);
  AProcess.Process.Core.ProcessId := 0;
  if FindWindMainWnd(AProcess, @tmpMainWnd) then
  begin
    Windows.GetWindowThreadProcessId(tmpMainWnd.Core.WindowHandle, AProcess.Process.Core.ProcessId);
    //AWindProcess.Process.Core.ProcessHandle := Windows.OpenProcess(PROCESS_TERMINATE, FALSE, AWindProcess.Process.Core.ProcessId);
    //AProcess.Process.Core.ProcessHandle := Windows.OpenProcess(PROCESS_ALL_ACCESS, FALSE, AProcess.Process.Core.ProcessId);  
  end else
  begin
    if FindWindLoginWnd(AProcess, @tmpLoginWnd) then
    begin
      Windows.GetWindowThreadProcessId(tmpLoginWnd.Core.WindowHandle, AProcess.Process.Core.ProcessId);
      //AProcess.Process.Core.ProcessHandle := Windows.OpenProcess(PROCESS_ALL_ACCESS, FALSE, AProcess.Process.Core.ProcessId); 
    end;
  end;
  Result := (0 <> AProcess.Process.Core.ProcessId);
//  if Result then
//  begin
//    Windows.GetExitCodeProcess(AProcess.Process.Core.ProcessHandle, tmpExitCode);
//    Result := Windows.STILL_ACTIVE = tmpExitCode;
//  end;
end;

procedure LaunchWindProgram(AProcess: PWindProcess);
var
  tmpAnsi: AnsiString;
begin          
  //ReadWindConfig(AWindDealSession);
  tmpAnsi := AnsiString(PAnsiChar(@AProcess.WindProgramFileUrl[0]));  
  CopyMemory(@AProcess.WindProgramPathUrl[0], @tmpAnsi[1], Length(tmpAnsi));
  tmpAnsi := tmpAnsi +'TdxW.exe';
  CopyMemory(@AProcess.WindProgramFileUrl[0], @tmpAnsi[1], Length(tmpAnsi));
  if '' <> tmpAnsi then
  begin  
    //Log('', 'LaunchWindProgram:' + tmpAnsi);
    if FileExists(tmpAnsi) then
    begin
      // win10 run as manager how to solve ???    
      RunProcessA(@AProcess.Process, tmpAnsi, nil);
      Sleep(10);
      if 0 <> AProcess.Process.Core.ProcessId then
      begin
      end;
    end;
  end;
end;

end.
