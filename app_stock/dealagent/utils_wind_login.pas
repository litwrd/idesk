unit utils_wind_login;

interface

uses
  windows,
  Graphics, Sysutils,
  define_windprocess;
                                                 
  function FindWindLoginWnd(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin): Boolean;
  procedure ClickLoginButton(AWndLogin: PWindWndLogin);    
  function CheckWindLoginWndUIElement(ALoginWnd: PWindWndLogin): Boolean;
  function InputUserLoginInfo(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin; APassword: Integer): Boolean;
  function AutoLogin(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin; APassword: Integer): Boolean;
                                                               
  function EnsureAppLoginStatus(AWindProcess: PWindProcess; AMainWnd: PWindWndMain): Boolean;

implementation

uses
  UtilsWindows,
  utils_findwnd,
  utils_windprocess,
  utils_wind_dialog,
  utils_wind_main;
                  
function FuncCheckLoginWnd(AWnd: HWND; AFind: PExWindowEnumFind): Boolean;
var
  tmpChildWnd: HWND;
begin
  Result := true;
  tmpChildWnd := Windows.GetWindow(AWnd, GW_CHILD);
  if 0 = tmpChildWnd then
  begin
    Result := false;
  end;
end;
                   
function FindWindLoginWnd(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin): Boolean;
var
  tmpFind: TExWindowEnumFind;
  //tmpWndLogin: TWindWndLogin;
begin
  FillChar(tmpFind, SizeOf(tmpFind), 0);
  tmpFind.NeedWinCount := 1;
  tmpFind.WndClassKey := '#32770';
  tmpFind.WndCaptionKey := '招商证券智远理财服务平台';
  tmpFind.CheckWndFunc := FuncCheckLoginWnd;
  Windows.EnumWindows(@EnumFindDesktopWindowProc, Integer(@tmpFind));
  Result := tmpFind.FindCount > 0;
  if Result then
  begin
    if nil <> AWndLogin then
    begin
      AWndLogin.Core.WindowHandle := tmpFind.FindWindow[0];
    end;
  end;
end;

type  
  PTraverse_LoginWindow = ^TTraverse_LoginWindow;
  TTraverse_LoginWindow = record
    LoginWindow: PWindWndLogin;
  end;
           
procedure TraverseCheckChildWindowA(AWnd: HWND; ATraverseWindow: PTraverse_LoginWindow);
var
  tmpChildWnd: HWND;
  tmpStr: string;  
  tmpCtrlId: integer;
  tmpRect: TRect;
  tmpIsHandled: Boolean;
begin
  tmpChildWnd := Windows.GetWindow(AWnd, GW_CHILD);
  while 0 <> tmpChildWnd do
  begin          
    tmpStr := GetWndClassName(tmpChildWnd);
    tmpIsHandled := False;
    tmpCtrlId := Windows.GetDlgCtrlID(tmpChildWnd);
    if SameText('SafeEdit', tmpStr) then
    begin
      if tmpCtrlId = $ED then
      begin
        tmpIsHandled := true;
        ATraverseWindow.LoginWindow.WndVerifyCodeEdit := tmpChildWnd;
        Windows.GetWindowRect(ATraverseWindow.LoginWindow.WndVerifyCodeEdit, tmpRect);
        if 0 < tmpRect.Left then
        begin
        
        end;
      end;
    end;
    if not tmpIsHandled then
    begin
      if SameText('Edit', tmpStr) then
      begin
        if tmpCtrlId = $EC then
        begin
          tmpIsHandled := true;
          ATraverseWindow.LoginWindow.WndPasswordEdit := tmpChildWnd;
        end;                  
        if tmpCtrlId = $3E9 then
        begin
          tmpIsHandled := true;
          ATraverseWindow.LoginWindow.WndAccountEdit := tmpChildWnd;
        end;                            
      end;
    end;
    if not tmpIsHandled then
    begin
      if tmpCtrlId = 1 then
      begin
        tmpIsHandled := true;
        ATraverseWindow.LoginWindow.WndLoginButton := tmpChildWnd;
      end;
    end;
    if not tmpIsHandled then
    begin    
    end;
    TraverseCheckChildWindowA(tmpChildWnd, ATraverseWindow);
    tmpChildWnd := Windows.GetWindow(tmpChildWnd, GW_HWNDNEXT);
  end;
end;

function CheckWindLoginWndUIElement(ALoginWnd: PWindWndLogin): Boolean;
var
  tmpTraverse_Window: TTraverse_LoginWindow;
begin
  Result := false;
  if nil = ALoginWnd then
    exit;
  if IsWindow(ALoginWnd.WndVerifyCodeEdit) then
  begin
    exit;
  end;
  FillChar(tmpTraverse_Window, SizeOf(tmpTraverse_Window), 0);
  tmpTraverse_Window.LoginWindow := ALoginWnd;
  TraverseCheckChildWindowA(ALoginWnd.Core.WindowHandle, @tmpTraverse_Window);
  Result := IsWindow(ALoginWnd.WndVerifyCodeEdit);
end;
                  
procedure InputLoginAccount(AWndLogin: PWindWndLogin; Account: AnsiString);
var
  tmpAccount: AnsiString;
  i: integer;
  tmpKeyCode: Byte;
begin
  if nil = AWndLogin then
    exit;
  if '' = Account then
    exit;
  if IsWindow(AWndLogin.WndAccountEdit) then
  begin
    ForceBringFrontWindow(AWndLogin.WndAccountEdit);
    for i := 1 to 10 do
    begin
      SimulateKeyPress(VK_BACK, 20);
      SimulateKeyPress(VK_DELETE, 20);
    end;
    tmpAccount := UpperCase(Account);
    for i := 1 to Length(tmpAccount) do
    begin
      tmpKeyCode := Byte(tmpAccount[i]);
      SimulateKeyPress(tmpKeyCode, 20);
    end;      
    SimulateKeyPress(VK_RETURN, 20); 
    SimulateKeyPress(VK_TAB, 20);
  end;
end;
                     
procedure InputLoginPassword(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin; APassword: AnsiString);

  procedure DoInputPassword;
  var
    tmpKeyCode: Byte;
    i: integer;     
    tmpPassword: AnsiString;
  begin
    for i := 1 to 8 do
    begin
      Windows.keybd_event(VK_BACK, MapVirtualKey(VK_BACK, 0), 0, 0) ;//#a键位码是86
      SleepWait(10);
      Windows.keybd_event(VK_BACK, MapVirtualKey(VK_BACK, 0), KEYEVENTF_KEYUP, 0);
      SleepWait(10);
      SleepWait(10);
                    
      Windows.keybd_event(VK_DELETE, MapVirtualKey(VK_DELETE, 0), 0, 0) ;//#a键位码是86
      SleepWait(10);
      Windows.keybd_event(VK_DELETE, MapVirtualKey(VK_DELETE, 0), KEYEVENTF_KEYUP, 0);
      SleepWait(10);
      SleepWait(10);
    end;
    tmpPassword := UpperCase(APassword);
    for i := 1 to Length(tmpPassword) do
    begin
      tmpKeyCode := Byte(tmpPassword[i]);
      Windows.keybd_event(tmpKeyCode, MapVirtualKey(tmpKeyCode, 0), 0, 0) ;//#a键位码是86
      SleepWait(10);
      Windows.keybd_event(tmpKeyCode, MapVirtualKey(tmpKeyCode, 0), KEYEVENTF_KEYUP, 0);
      SleepWait(10);
      SleepWait(10);
    end;

    Windows.keybd_event(VK_RETURN, MapVirtualKey(VK_RETURN, 0), 0, 0) ;//#a键位码是86
    SleepWait(10);
    Windows.keybd_event(VK_RETURN, MapVirtualKey(VK_RETURN, 0), KEYEVENTF_KEYUP, 0);
    SleepWait(10);
    SleepWait(10);

    Windows.keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0) ;//#a键位码是86
    SleepWait(10);
    Windows.keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), KEYEVENTF_KEYUP, 0);
    SleepWait(10);
    SleepWait(10);
  end;
  
var
  i: integer;    
  processid: DWORD;
  tmpRect: TRect;
begin         
  if nil = AWndLogin then
    exit;
  if '' = APassword then
    exit;
  if IsWindow(AWndLogin.WndPasswordEdit) then
  begin
    GetWindowThreadProcessId(AWndLogin.WndPasswordEdit, processid);
    if 0 <> processid then
    begin
      if AttachThreadInput(processid, GetCurrentThreadId(), TRUE) then
      begin
        try
          for i := 0 to 1 do
          begin
            if Windows.GetFocus <> AWndLogin.WndPasswordEdit then
            begin
              SetFocus(AWndLogin.WndPasswordEdit);
            end;
            SleepWait(10);
          end;            
          if Windows.GetFocus <> AWndLogin.WndPasswordEdit then
          begin
            SleepWait(10);
            exit;
          end;
          DoInputPassword;
        finally
          AttachThreadInput(processid, GetCurrentThreadId(), FALSE);
        end;
      end else
      begin
        GetWindowRect(AWndLogin.WndPasswordEdit, tmpRect);
        Windows.SetCursorPos((tmpRect.Left + tmpRect.Right) div 2, (tmpRect.Top + tmpRect.Bottom) div 2);
        SleepWait(20);
        Windows.mouse_event(MOUSEEVENTF_LEFTDOWN or MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        SleepWait(300);
        CloseWindDialog(AWindProcess, @AWndLogin.Core); 
        DoInputPassword;
      end;
    end;
  end;
end;

procedure InputLoginVerifyCode(AWndLogin: PWindWndLogin; AVerifyCode: AnsiString);
var
  tmpVerifyCode: AnsiString;   
  tmpKeyCode: Byte;
  i: integer;
begin       
  if nil = AWndLogin then
    exit;
  if '' = AVerifyCode then
    exit;
  if IsWindow(AWndLogin.WndVerifyCodeEdit) then
  begin                       
    ForceBringFrontWindow(AWndLogin.WndVerifyCodeEdit);
    for i := 1 to 4 do
    begin
      SimulateKeyPress(VK_BACK, 20);
      SimulateKeyPress(VK_DELETE, 20);
    end;
    tmpVerifyCode := UpperCase(AVerifyCode);
    for i := 1 to Length(tmpVerifyCode) do
    begin
      tmpKeyCode := Byte(tmpVerifyCode[i]);
      SimulateKeyPress(tmpKeyCode, 20);
    end;
  end;
end;
                     
function getLoginVerifyCodeBmp(AWndLogin: PWindWndLogin; ALeft, ATop, AWidth, AHeight: integer): Graphics.TBitmap;
const
  CAPTUREBLT = $40000000;
var
  tmpWnd: HWND;
  tmpForegroundWnd: HWND;
  tmpProcessID: DWORD;
  tmpWinRect: TRect;
  tmpMemDC: HDC;
  tmpDC: HDC;
  tmpMemBmp: HBITMAP;
  tmpOldBmp: HBITMAP;
begin
  Result := nil;       
  tmpWnd := AWndLogin.Core.WindowHandle;
  if IsWindow(tmpWnd) and IsWindowVisible(tmpWnd) then
  begin
    tmpForegroundWnd := GetForegroundWindow;
    GetWindowThreadProcessId(tmpForegroundWnd, tmpProcessID);
    AttachThreadInput(tmpProcessID, GetCurrentThreadId(), TRUE);
    if Windows.GetForegroundWindow <> tmpWnd then
      SetForegroundWindow(tmpWnd);
    if Windows.GetFocus <> tmpWnd then
      SetFocus(tmpWnd);          
    AttachThreadInput(tmpProcessID, GetCurrentThreadId(), FALSE);
    ForceBringFrontWindow(tmpWnd);

    Windows.GetWindowRect(tmpWnd, tmpWinRect);
    Result := Graphics.TBitmap.Create;
    Result.PixelFormat := pf32bit;  
    //tmpVerifyCodeBmp.Width := tmpWinRect.Right - tmpWinRect.Left;
    //tmpVerifyCodeBmp.Height := tmpWinRect.Bottom - tmpWinRect.Top;
    Result.Width := AWidth; //52;
    Result.Height := AHeight; //21;
    
    //tmpDC := Windows.GetDC(tmpWnd);
    tmpDC := Windows.GetDC(0);
    try
      tmpMemDC := Windows.CreateCompatibleDC(tmpDC);
      tmpMemBmp := Windows.CreateCompatibleBitmap(tmpDC, Result.Width, Result.Height);
      tmpOldBmp := Windows.SelectObject(tmpMemDC, tmpMemBmp);
      try                     
        Windows.BitBlt(tmpMemDC,
            0,
            0, //tmpWinRect.Top,
          //Windows.BitBlt(tmpVerifyCodeBmp.Canvas.Handle, 0, 0,
            Result.Width,
            Result.Height,
            tmpDC,
            tmpWinRect.Left + ALeft, //0,
            tmpWinRect.Top + ATop, //0,
            SRCCOPY or CAPTUREBLT);
        Windows.BitBlt(Result.Canvas.Handle, 0, 0,
        //Windows.BitBlt(tmpVerifyCodeBmp.Canvas.Handle, 0, 0,
          Result.Width,
          Result.Height,
          tmpMemDC,
          0, 0, SRCCOPY);
      finally
        Windows.SelectObject(tmpMemDC, tmpOldBmp);
        Windows.DeleteDC(tmpMemDC);
        Windows.DeleteObject(tmpMemBmp);
      end;
    finally
      Windows.ReleaseDC(tmpWnd, tmpDC);
    end;          
  end;
end;
            
function getLoginVerifyCode(AWndLogin: PWindWndLogin): AnsiString;
var
  tmpVerifyCodeBmp: Graphics.TBitmap;
begin
  Result := '';
  tmpVerifyCodeBmp := getLoginVerifyCodeBmp(AWndLogin, 447, 241, 52, 21);
  if nil = tmpVerifyCodeBmp then
    exit;
  try
    Result := '';// GetVerifyCode(tmpVerifyCodeBmp);            
  finally
    tmpVerifyCodeBmp.Free;
  end;
end;
               
procedure ClickLoginButton(AWndLogin: PWindWndLogin);
var
  tmpRect: TRect;
begin
  if IsWindow(AWndLogin.WndLoginButton) then
  begin
    GetWindowRect(AWndLogin.WndLoginButton, tmpRect);
    Windows.SetCursorPos(tmpRect.Left + 10, tmpRect.Top + 10);
    Windows.mouse_event(MOUSEEVENTF_LEFTDOWN or MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    SleepWait(20);
  end;
end;
                   
function InputUserLoginInfo(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin; APassword: Integer): Boolean;
var
  i: integer;                  
  tmpAnsi: AnsiString;
begin
  Result := false;
  if nil = AWndLogin then
    exit;          
  for i := 1 to 5 do
  begin
    if CheckWindLoginWndUIElement(AWndLogin) then
      Break;
    Sleep(100);
  end;
  if not IsWindow(AWndLogin.WndAccountEdit) then
    exit;
  for i := 1 to 2 do
  begin
    ForceBringFrontWindow(AWndLogin.Core.WindowHandle);
    SleepWait(50);
  end;
  CloseWindDialog(AWindProcess, @AWndLogin.Core);
  if IsWindowEnabled(AWndLogin.WndAccountEdit) then
  begin
    //===========================
    InputLoginAccount(AWndLogin, inttostr(AWindProcess.LoginAccountId));
    SleepWait(200);
    //===========================
    tmpAnsi := getLoginVerifyCode(AWndLogin);
    if '' <> tmpAnsi then
    begin
      InputLoginVerifyCode(AWndLogin, tmpAnsi);
    end;
    SleepWait(200);
    //===========================
    tmpAnsi := inttostr(APassword);
    tmpAnsi := Copy('000000', 1, 6 - length(tmpAnsi)) + tmpAnsi;
    InputLoginPassword(AWindProcess, AWndLogin, tmpAnsi);                                                                   
    SleepWait(200);
    //===========================
    Result := True;
  end;
end;

function AutoLogin(AWindProcess: PWindProcess; AWndLogin: PWindWndLogin; APassword: Integer): Boolean;
begin
  Result := InputUserLoginInfo(AWindProcess, AWndLogin, APassword);
  if Result then
  begin
    ClickLoginButton(AWndLogin);
  end;
end;
                
//function InputUserLoginInfo(AWindProcess: PWindProcess; ALoginWnd: PWindWndLogin): Boolean;
//begin
//  Result := AutoLogin(AWindProcess, ALoginWnd, 1808175166, 12177);
//end;
                                         
procedure DoLoginInMainWnd(AWindProcess: PWindProcess; AMainWnd: PWindWndMain); forward;

function EnsureAppLoginStatus(AWindProcess: PWindProcess; AMainWnd: PWindWndMain): Boolean;
var
  tmpLoginWnd: TWindWndLogin; 
  tmpCounter: integer;
  tmpIsLoginMode: integer;
  i: integer;
begin
  Result := false;
  if nil = AMainWnd then
  begin
    exit;
  end;
  if (0 = AWindProcess.Process.Core.ProcessId) then
  begin
    LaunchWindProgram(AWindProcess);
  end else
  begin
    if not IsWindow(AMainWnd.Core.WindowHandle) then
    begin
      if FindWindMainWnd(AWindProcess, AMainWnd) then
      begin
        TraverseCheckMainChildWindowA(AMainWnd.Core.WindowHandle, AMainWnd);
      end;
    end;  
    Result := IsWindow(AMainWnd.WndFunctionTree);
    if Result then
    begin
      exit;
    end;
  end;
  tmpCounter := 0;
  tmpIsLoginMode := 0;
  SleepWait(200);
  FillChar(tmpLoginWnd, SizeOf(tmpLoginWnd), 0);
  FindWindLoginWnd(AWindProcess, @tmpLoginWnd);

  while not IsWindow(AMainWnd.Core.WindowHandle) do
  begin
    if 0 = tmpLoginWnd.Core.WindowHandle then
    begin
      FindWindLoginWnd(AWindProcess, @tmpLoginWnd);
      tmpCounter := tmpCounter + 1;
      if 0 = tmpLoginWnd.Core.WindowHandle then
      begin
        SleepWait(10);
        if 1000 < tmpCounter then
          Break; 
        Continue;
      end;
    end;           
    if not IsWindow(tmpLoginWnd.WndAccountEdit) then
    begin
      CheckWindLoginWndUIElement(@tmpLoginWnd);
    end;         
    if IsWindow(tmpLoginWnd.WndAccountEdit) then
    begin
      if IsWindowEnabled(tmpLoginWnd.WndAccountEdit) then
      begin
        tmpIsLoginMode := 1;
        //InputUserLoginInfo(AWindProcess, @tmpLoginWnd);
        AutoLogin(AWindProcess, @tmpLoginWnd, 12177);
      end else
      begin
        tmpIsLoginMode := 2;
        if IsWindow(tmpLoginWnd.WndLoginButton) then
        begin
          ForceBringFrontWindow(tmpLoginWnd.Core.WindowHandle);
          SleepWait(100);          
          ClickLoginButton(@tmpLoginWnd);
          SleepWait(100);
        end;
      end;
    end;
    FindWindMainWnd(AWindProcess, AMainWnd);
    SleepWait(10);
    tmpCounter := tmpCounter + 1;
    if 1000 < tmpCounter then
      Break;
  end;
  case tmpIsLoginMode of
    0: begin    
      if IsWindow(AMainWnd.Core.WindowHandle) then
      begin
        ForceBringFrontWindow(AMainWnd.Core.WindowHandle);
        Sleep(100);
        i := 0;    
        while CloseWindDialog(AWindProcess, @tmpLoginWnd.Core) do
        begin
          Sleep(100);
          Inc(i);
          if 10 < i then
            Break;
        end;            
        DoLoginInMainWnd(AWindProcess, AMainWnd);
      end;
    end;
    1: begin
    end;
    2: begin
      // 等待主窗体出现 关闭对话框
      SleepWait(1000);   
      i := 0;           
      while CloseWindDialog(AWindProcess, 0) do
      begin
        Sleep(100);  
        Inc(i);
        if 10 < i then
          Break;
      end;
      DoLoginInMainWnd(AWindProcess, AMainWnd);
    end;
  end;
  Result := IsWindow(AMainWnd.WndFunctionTree);
end;

procedure DoLoginInMainWnd(AWindProcess: PWindProcess; AMainWnd: PWindWndMain);
var    
  tmpLoginWnd: TWindWndLogin;
  i: integer;
begin
  if not IsWindow(AMainWnd.WndMenuOrderButton) then
  begin
    TraverseCheckMainChildWindowA(AMainWnd.Core.WindowHandle, AMainWnd);
  end;
  if IsWindow(AMainWnd.WndMenuOrderButton) then
  begin                                                                                                                                      // 点击 交易 Menu 菜单
    ClickButtonWnd(AMainWnd.WndMenuOrderButton);
    Sleep(100);        
    FillChar(tmpLoginWnd, SizeOf(tmpLoginWnd), 0);
    for i := 1 to 20 do
    begin
      FindWindLoginWnd(AWindProcess, @tmpLoginWnd);
      if IsWindow(tmpLoginWnd.Core.WindowHandle) then
        Break;
      Sleep(100);
    end;
    //InputUserLoginInfo(AWindProcess, @tmpLoginWnd);    
    AutoLogin(AWindProcess, @tmpLoginWnd, 12177);
    Sleep(100);
    for i := 1 to 20 do
    begin
      if IsWindow(AMainWnd.WndFunctionTree) then
      begin
        Break;
      end else
      begin
        if IsWindow(AMainWnd.Core.WindowHandle) then
        begin
          TraverseCheckMainChildWindowA(AMainWnd.Core.WindowHandle, AMainWnd);
        end;
      end;
      SleepWait(100);
    end;  
    SleepWait(1000);
    i := 0;
    while CloseWindDialog(AWindProcess, 0) do
    begin
      Sleep(100);
      Inc(i);
      if 10 < i then
        Break;
    end;
  end;
end;

end.
