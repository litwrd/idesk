unit utils_wind_main;

interface

uses
  Windows, Sysutils,
  define_windprocess;
                                            
  function IsEnsureWindAgent_Main(AWindProcess: PWindProcess; AWndMain: PWindWndMain): Boolean;
  function FindWindMainWnd(AWindProcess: PWindProcess; AWndMain: PWindWndMain): Boolean;     
  procedure TraverseCheckMainChildWindowA(AWnd: HWND; AWndMain: PWindWndMain);

  // 头部面板
  procedure TraverseCheckMainTopHeaderPanelChildWindowA(AWnd: HWND; AWndMain: PWindWndMain);

implementation

uses
  UtilsWindows,
  utils_findwnd;

function IsEnsureWindAgent_Main(AWindProcess: PWindProcess; AWndMain: PWindWndMain): Boolean;
begin
  Result := false;   
  if nil = AWindProcess then
    exit;
  if nil = AWndMain then
    exit;        
  if not IsWindow(AWndMain.Core.WindowHandle) then
  begin
    if not FindWindMainWnd(AWindProcess, AWndMain) then
    begin
      exit;
    end;
  end;          
  ForceBringFrontWindow(AWndMain.Core.WindowHandle);
  SleepWait(50);
  Result := true;
end;
        
function FindWindMainWnd(AWindProcess: PWindProcess; AWndMain: PWindWndMain): Boolean;
var
  tmpFind: TExWindowEnumFind;
  //tmpWndLogin: TWindWndLogin;
begin
  FillChar(tmpFind, SizeOf(tmpFind), 0);   
  tmpFind.WndClassKey := 'TdxW_MainFrame_Class';
  tmpFind.WndCaptionKey := '招商证券智远理财服务平台';
  tmpFind.WndCaptionExcludeKey := '';
  tmpFind.CheckWndFunc := nil;
  tmpFind.NeedWinCount := 1;
  tmpFind.FindCount := 0;
  FillChar(tmpFind.FindWindow, SizeOf(tmpFind.FindWindow), 0);
  Windows.EnumWindows(@EnumFindDesktopWindowProc, Integer(@tmpFind));
  Result := tmpFind.FindCount > 0;
  if Result then
  begin
    if nil <> AWndMain then
    begin
      AWndMain.Core.WindowHandle := tmpFind.FindWindow[0];
    end;
  end;
end;

procedure TraverseCheckMainChildWindowA(AWnd: HWND; AWndMain: PWindWndMain);
var
  tmpWndChild: HWND;
  tmpStr: string;  
  tmpCtrlId: integer;
  tmpIsHandled: Boolean;
begin
  if not IsWindow(AWnd) then
    exit;
  tmpStr := GetWndClassName(AWnd);
  tmpIsHandled := False;
  tmpCtrlId := Windows.GetDlgCtrlID(AWnd); 
  if 0 = tmpCtrlId then
  begin
    if 0 = AWndMain.WndDealPanel then
    begin
      tmpStr := GetWndTextName(AWnd);
      if 0 < Pos('通达信网上交易', tmpStr) then
      begin
        AWndMain.WndDealPanel := AWnd;
      end;
    end;
  end;    
  if SameText('SysTreeView32', tmpStr) then
  begin
    if $E900 = tmpCtrlId then
    begin
      tmpIsHandled := true;
      AWndMain.WndFunctionTree := AWnd;
    end;
  end;    
  if not tmpIsHandled then
  begin
    if SameText('AfxWnd42', tmpStr) then
    begin
      if $BC5 = tmpCtrlId then
      begin                   
        tmpIsHandled := true;
        AWndMain.WndMenuOrderButton := AWnd;
      end;
    end;
  end;               
  if not tmpIsHandled then
  begin
    if ($E81E = tmpCtrlId) then
    begin
      if SameText('AfxControlBar42', tmpStr) then
      begin
        AWndMain.WndDealPanelRoot := AWnd;
        //CheckDealPanelSize(AWndMain.MainWindow);
      end;
    end;
  end;         
  if not tmpIsHandled then
  begin
    if ($3001 = tmpCtrlId) then
    begin
      if SameText('#32770', tmpStr) then
      begin
        AWndMain.WndTopHeaderPanelRoot := AWnd;
        //CheckDealPanelSize(AWndMain.MainWindow);
      end;
    end;
  end;
  //=====================================================      
  if not tmpIsHandled then
  begin
    tmpWndChild := Windows.GetWindow(AWnd, GW_CHILD);
    while 0 <> tmpWndChild do
    begin
      TraverseCheckMainChildWindowA(tmpWndChild, AWndMain);       
      tmpWndChild := Windows.GetWindow(tmpWndChild, GW_HWNDNEXT);
    end;
  end;
end;
      
// 头部面板
procedure TraverseCheckMainTopHeaderPanelChildWindowA(AWnd: HWND; AWndMain: PWindWndMain);
var
  tmpWndChild: HWND;
  tmpStr: string;  
  tmpCtrlId: integer;
  tmpIsHandled: Boolean;
begin                
  if not IsWindowVisible(AWnd) then
    exit;             
  if not IsWindowEnabled(AWnd) then
    exit;                       
  tmpStr := GetWndClassName(AWnd); 
  tmpIsHandled := False;
  tmpCtrlId := Windows.GetDlgCtrlID(AWnd);
  //===============================
  if not tmpIsHandled then
  begin
    if SameText('Edit', tmpStr) then
    begin
      tmpIsHandled := true;    
      if $BB8 = tmpCtrlId then
      begin
        AWndMain.WndMenuCommandEdit := AWnd;
      end;
    end;
  end;
  //===============================                                 
  if not tmpIsHandled then
  begin
    tmpWndChild := Windows.GetWindow(AWnd, GW_CHILD);
    while 0 <> tmpWndChild do
    begin
      TraverseCheckMainTopHeaderPanelChildWindowA(tmpWndChild, AWndMain);       
      tmpWndChild := Windows.GetWindow(tmpWndChild, GW_HWNDNEXT);
    end;
  end;
end;

end.
