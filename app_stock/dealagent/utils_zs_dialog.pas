unit utils_zs_dialog;

interface

uses
  Windows, Messages, Sysutils, define_zsprocess;
                                 
  function FindZSDialogWnd(AZsProcess: PZsProcess; AWndDialog: PZsWndDialogs; AExcludeWnd: HWND): Boolean;
  function CloseZsDialog(AZsProcess: PZsProcess; AExcludeWnd: PZsExProcessWnd): Boolean;
  
implementation

uses
  UtilsWindows,
  utils_findwnd;
                 
function FuncCheckDialogWnd(AWnd: HWND; AFind: PZsExWindowEnumFind): Boolean;
var
  tmpWnd: HWND;
  tmpRect: TRect;
  tmpStr: AnsiString;
begin
  Result := true;
  tmpWnd := Windows.GetWindow(AWnd, GW_CHILD);
  if 0 = tmpWnd then
  begin
    Result := false;
  end;
  if result then
  begin
    if 0 <> AFind.ExcludeWnd then
    begin
      if AWnd = AFind.ExcludeWnd then
      begin
        Result := false;
      end;
    end;
  end;
  if Result then
  begin                 
    Result := false;
    Windows.GetWindowRect(AWnd, tmpRect);
    if (0 < tmpRect.Left) or (0 < tmpRect.Right) or
       (0 < tmpRect.top) or (0 < tmpRect.Bottom) then
    begin
      Result := true;
    end;
  end;       
  if Result then
  begin
    if 0 <> AFind.ParentWnd then
    begin
      tmpWnd := Windows.GetParent(AWnd);
      if AFind.ParentWnd <> tmpWnd then
      begin       
        Result := false;
      end;
    end;
  end;    
  if Result then
  begin
    tmpStr := GetWndClassName(AWnd);
    if '' <> tmpStr then
    begin
      tmpStr := GetWndTextName(AWnd);
      if '' <> tmpStr then
      begin
      end;
    end;
  end;
end;

procedure TraverseCheckDialogChildWindowA(AWnd: HWND; AWindow: PZsWndDialog); 
var
  tmpChildWnd: HWND; 
  tmpStr: Widestring;
  tmpIsHandled: Boolean;
begin
  tmpChildWnd := Windows.GetWindow(AWnd, GW_CHILD);
  while 0 <> tmpChildWnd do
  begin
    tmpIsHandled := false;
    tmpStr := Trim(GetWndTextName(tmpChildWnd));
    if '' <> tmpStr then
    begin
      if SameText('关闭', tmpStr) then
      begin
        AWindow.CancelButton := tmpChildWnd;
        tmpIsHandled := true;
      end;          
      if not tmpIsHandled then
      begin
        if SameText('取消', tmpStr) then
        begin
          AWindow.CancelButton := tmpChildWnd;
          tmpIsHandled := true;
        end;
      end;
      if not tmpIsHandled then
      begin
        if Length(tmpStr) < 5 then
        begin
          if Pos('确', tmpStr) > 0 then
          begin
            AWindow.OKButton := tmpChildWnd;
            tmpIsHandled := true;
          end;
        end;
      end;
    end;
    if not tmpIsHandled then
    begin
      TraverseCheckDialogChildWindowA(tmpChildWnd, AWindow);
    end;
    tmpChildWnd := Windows.GetWindow(tmpChildWnd, GW_HWNDNEXT);
  end;
end;
                       
function FindZSDialogWnd(AZsProcess: PZsProcess; AWndDialog: PZsWndDialogs; AExcludeWnd: HWND): Boolean;
var
  tmpFind: TZsExWindowEnumFind;
  i: integer;
begin
  if nil <> AWndDialog then
  begin
    AWndDialog.ZsProcess := AZsProcess;
  end;
  FillChar(tmpFind, SizeOf(tmpFind), 0);
  tmpFind.NeedWinCount := 255;
  tmpFind.WndClassKey := '#32770';
  tmpFind.ProcessId := AZsProcess.Process.Core.ProcessId;
  tmpFind.ExcludeWnd := AExcludeWnd;  
  //tmpFind.WndCaptionKey := '密码确认';
  tmpFind.CheckWndFunc := FuncCheckDialogWnd;
  //Result := FindDesktopWindow(AWindow);
  Windows.EnumWindows(@ZsEnumFindDesktopWindowProc, Integer(@tmpFind));
  Result := tmpFind.FindCount > 0;
  if Result then
  begin
    if nil <> AWndDialog then
    begin
      AWndDialog.WndCount := 0;
      for i := 0 to tmpFind.FindCount - 1 do
      begin                    
        AWndDialog.WndArray[AWndDialog.WndCount].Core.WindowHandle := tmpFind.FindWindow[i];
        AWndDialog.WndArray[AWndDialog.WndCount].WndText := GetWndTextName(AWndDialog.WndArray[AWndDialog.WndCount].Core.WindowHandle);
        TraverseCheckDialogChildWindowA(AWndDialog.WndArray[AWndDialog.WndCount].Core.WindowHandle, @AWndDialog.WndArray[AWndDialog.WndCount]);
        AWndDialog.WndCount := AWndDialog.WndCount + 1;
        if AWndDialog.WndCount = Length(AWndDialog.WndArray) then
          Break;
      end;
    end;
  end;
end;

function CloseZsDialog(AZsProcess: PZsProcess; AExcludeWnd: PZsExProcessWnd): Boolean;
var    
  tmpWndDialogs: TZsWndDialogs;
  tmpExcludeWnd: HWND;
  i: integer;
begin
  Result := false;
  FillChar(tmpWndDialogs, SizeOf(tmpWndDialogs), 0);
  tmpExcludeWnd := 0;
  if nil <> AExcludeWnd then
    tmpExcludeWnd := AExcludeWnd.WindowHandle;
  if FindZSDialogWnd(AZsProcess, @tmpWndDialogs, tmpExcludeWnd) then
  begin
    Result := 0 < tmpWndDialogs.WndCount;
    for i := 0 to tmpWndDialogs.WndCount - 1 do
    begin
      if (0 = tmpWndDialogs.WndArray[i].CancelButton) and
         (0 = tmpWndDialogs.WndArray[i].OKButton) then
      begin
        PostMessage(tmpWndDialogs.WndArray[i].Core.WindowHandle, WM_Close, 0, 0);
        SleepWait(100);
      end else
      begin
        if (0 <> tmpWndDialogs.WndArray[i].CancelButton) then
        begin
          ClickButtonWnd(tmpWndDialogs.WndArray[i].CancelButton);
          SleepWait(100);
        end else
        begin
          ClickButtonWnd(tmpWndDialogs.WndArray[i].OKButton);
          SleepWait(100);
        end;
      end;
    end;
  end;
end;

end.
