unit utils_zs_dealquery;

interface
              
uses
  Windows, SysUtils,
  define_zsprocess;
                                                                 
  procedure ClickFunctionTreeQueryNode(AWndMain: PZsWndMain);
  procedure ClickFunctionTreeQueryDealNode(AWndMain: PZsWndMain);

implementation

uses
  utils_zs_deal;

(* ���� TreeNode View *)  
procedure ClickFunctionTreeQueryNode(AWndMain: PZsWndMain);
begin
  ClickFunctionTreeNode(AWndMain, 30, 90);
end;

procedure ClickFunctionTreeQueryDealNode(AWndMain: PZsWndMain);
begin
  ClickFunctionTreeNode(AWndMain, 80, 150);
end;

end.
