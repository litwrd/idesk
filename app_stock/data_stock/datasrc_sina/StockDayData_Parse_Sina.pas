unit StockDayData_Parse_Sina;

interface

uses
  Sysutils,
  define_stockday_sina,
  define_stock_quotes,
  define_price;
                 
  procedure ParseSinaCellData(AHeadCol: TDealDayDataHeadName_Sina; ADealDayData: PRT_Quote_Day; AStringData: string);

implementation

//uses
//  UtilsLog;
                   
{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDayData_Parse_Sina.pas';
{$ENDIF}
  
function tryGetSinaIntValue(AStringData: string): integer;
var
  tmpPos: integer;
  tmpPrefix: string;
  tmpSuffix: string; 
begin
  Result := 0;
  tmpPos := Pos('.', AStringData);
  if 0 < tmpPos then
  begin
    tmpPrefix := Copy(AStringData, 1, tmpPos - 1);
    tmpSuffix := Copy(AStringData, tmpPos + 1, maxint);
    if 3 = Length(tmpSuffix) then
    begin
      Result := StrToIntDef(tmpPrefix + tmpSuffix, 0);
    end;
  end;
end;

procedure ParseSinaCellData(AHeadCol: TDealDayDataHeadName_Sina; ADealDayData: PRT_Quote_Day; AStringData: string);
var
  tmpPos: integer; 
  tmpDate: TDateTime;
begin
  if AStringData <> '' then
  begin
    case AHeadCol of
      headDay: begin                  
        //Log('headDay', '' + AStringData);
        tmpDate := StrToDateDef(AStringData, 0, DateFormat_Sina);
        ADealDayData.DealDate.Value := Trunc(tmpDate);
      end; // 1 日期,
      headPrice_Open: begin // 7开盘价,
        ADealDayData.PriceRange.PriceOpen.Value := tryGetSinaIntValue(AStringData);
        //if 0 = ADealDayData.PriceRange.PriceOpen.Value then
        //  SetRTPricePack(@ADealDayData.PriceRange.PriceOpen, StrToFloatDef(AStringData, 0.00));
      end;
      headPrice_High: begin // 5最高价,       
        //Log('headPrice_High', '' + AStringData);
        ADealDayData.PriceRange.PriceHigh.Value := tryGetSinaIntValue(AStringData);
        //if 0 = ADealDayData.PriceRange.PriceHigh.Value then
        //  SetRTPricePack(@ADealDayData.PriceRange.PriceHigh, StrToFloatDef(AStringData, 0.00));
      end;
      headPrice_Close: begin // 4收盘价,
        ADealDayData.PriceRange.PriceClose.Value := tryGetSinaIntValue(AStringData);
        //if 0 = ADealDayData.PriceRange.PriceClose.Value then
        //  SetRTPricePack(@ADealDayData.PriceRange.PriceClose, StrToFloatDef(AStringData, 0.00));
      end;
      headPrice_Low: begin // 6最低价,        
        //Log('headPrice_Low', '' + AStringData);
        ADealDayData.PriceRange.PriceLow.Value := tryGetSinaIntValue(AStringData);
        //if 0 = ADealDayData.PriceRange.PriceLow.Value then
        //  SetRTPricePack(@ADealDayData.PriceRange.PriceLow, StrToFloatDef(AStringData, 0.00));
      end;
      headDeal_Volume: begin // 12成交量,
        tmpPos := Sysutils.LastDelimiter('.', AStringData);
        if tmpPos > 0 then
        begin
          AStringData := Copy(AStringData, 1, tmpPos - 1);
        end;
        ADealDayData.DealVolume := StrToInt64Def(AStringData, 0);
      end;
      headDeal_Amount: begin // 13成交金额,
        tmpPos := Sysutils.LastDelimiter('.', AStringData);
        if tmpPos > 0 then
        begin
          AStringData := Copy(AStringData, 1, tmpPos - 1);
        end;
        ADealDayData.DealAmount := StrToInt64Def(AStringData, 0);
      end;
      headDeal_WeightFactor: begin
        ADealDayData.Weight.Value := tryGetSinaIntValue(AStringData);
        if 0 = ADealDayData.Weight.Value then
          ADealDayData.Weight.Value := Trunc(StrToFloatDef(AStringData, 0.00) * 1000);
      end;
    end;
  end;
end;
                
end.
