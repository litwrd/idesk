unit StockDayData_Get_Sina;

interface

uses
  BaseApp,
  Sysutils,
  UtilsHttp,      
  QuickList_Int,  
  win.iobuffer,
  define_price,
  define_dealitem,
  define_stockday_sina,
  StockDayDataAccess;
         
function GetStockDataDay_Sina(App: TBaseApp; AStockItem: PRT_DealItem; AWeightMode: TRT_WeightMode; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): Boolean;        
function DataGet_DayData_SinaNow(App: TBaseApp; ADataAccess: TStockDayDataAccess; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): Boolean; overload;
function DataGet_DayData_Sina(App: TBaseApp; ADataAccess: TStockDayDataAccess; AYear, ASeason: Word; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): Boolean; overload;

function DataGet_DayData_SinaNow(App: TBaseApp; AStockCode_Sina: string; AWeightMode: TRT_WeightMode; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): TALIntegerList; overload;
function DataGet_DayData_Sina(App: TBaseApp; AStockCode_Sina: string; AWeightMode: TRT_WeightMode; AYear, ASeason: Word; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): TALIntegerList; overload;

implementation

uses
  Classes,
  Windows,
  Define_DataSrc,    
  define_data_sina, 
  define_stock_quotes,
  define_dealstore_file,
  UtilsDateTime,
  //StockDayData_Parse_Sina_Html1,
  //StockDayData_Parse_Sina_Html2,  
  StockDayData_Parse_Sina_Html3,
  //UtilsLog,
  StockDayData_Parse_Sina,
  StockDayData_Load,
  StockDayData_Save;
                    
{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDayData_Get_Sina.pas';
{$ENDIF}

function DataGet_DayData_SinaNow(App: TBaseApp; AStockCode_Sina: string; AWeightMode: TRT_WeightMode; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): TALIntegerList; overload;
var
  tmpurl: string;
  tmpHttpData: PIOBuffer;
//  tmpDayDataList: TALIntegerList;
//  tmpDayData: PRT_Quote_Day;
begin
  Result := nil;
  if weightNone <> AWeightMode then
    tmpUrl := BaseSinaDayUrl_weight
  else
    tmpUrl := BaseSinaDayUrl1;
  tmpurl := tmpurl + AStockCode_Sina + '.phtml';
  tmpHttpData := GetHttpUrlData(tmpUrl, ANetSession, AHttpData, SizeMode_512k);
  if nil <> tmpHttpData then
  begin
    try
      //Result := StockDayData_Parse_Sina_Html1.DataParse_DayData_Sina(ADataAccess, tmpHttpData);
      //Result := StockDayData_Parse_Sina_Html2.DataParse_DayData_Sina(ADataAccess, tmpHttpData);      
      Result := StockDayData_Parse_Sina_Html3.DataParse_DayData_Sina(tmpHttpData);
//      if nil <> tmpDayDataList then
//      begin
//        try
//          if 0 < tmpDayDataList.Count then
//          begin
//            for i := 0 to tmpDayDataList.Count - 1 do
//            begin
//              tmpDayData := PRT_Quote_Day(tmpDayDataList.Objects[i]);
//              Result := True;
//            end;
//          end;
//          for i := tmpDayDataList.Count - 1 downto 0 do
//          begin
//            FreeMem(PRT_Quote_Day(tmpDayDataList.Objects[i]));
//          end;
//          tmpDayDataList.Clear;
//        finally
//          tmpDayDataList.Free;
//        end;
//      end;
    finally
      if AHttpData <> tmpHttpData then
      begin
        CheckInIOBuffer(tmpHttpData);
      end;
    end;
  end;    
end;

function DataGet_DayData_SinaNow(App: TBaseApp; ADataAccess: TStockDayDataAccess; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): Boolean; overload;
var
  tmpurl: string;  
  tmpHttpData: PIOBuffer;
  tmpDayDataList: TALIntegerList;
  tmpDayData: PRT_Quote_Day;
  i: integer; 
begin          
  Result := false;
  if weightNone <> ADataAccess.WeightMode then
    tmpUrl := BaseSinaDayUrl_weight
  else
    tmpUrl := BaseSinaDayUrl1;
  tmpurl := tmpurl + ADataAccess.StockItem.sCode + '.phtml';
  tmpHttpData := GetHttpUrlData(tmpUrl, ANetSession, AHttpData, SizeMode_512k);
  if nil <> tmpHttpData then
  begin
    try
      //Result := StockDayData_Parse_Sina_Html1.DataParse_DayData_Sina(ADataAccess, tmpHttpData);
      //Result := StockDayData_Parse_Sina_Html2.DataParse_DayData_Sina(ADataAccess, tmpHttpData);      
      tmpDayDataList := StockDayData_Parse_Sina_Html3.DataParse_DayData_Sina(tmpHttpData);
      if nil <> tmpDayDataList then
      begin
        try
          if 0 < tmpDayDataList.Count then
          begin
            for i := 0 to tmpDayDataList.Count - 1 do
            begin
              tmpDayData := PRT_Quote_Day(tmpDayDataList.Objects[i]);
              AddDealDayData(ADataAccess, tmpDayData);
              Result := True;
            end;
          end;
          for i := tmpDayDataList.Count - 1 downto 0 do
          begin
            FreeMem(PRT_Quote_Day(tmpDayDataList.Objects[i]));
          end;
          tmpDayDataList.Clear;
        finally
          tmpDayDataList.Free;
        end;
      end;
    finally
      if AHttpData <> tmpHttpData then
      begin
        CheckInIOBuffer(tmpHttpData);
      end;
    end;
  end;
  for i := 1 to 10 do
  begin
    if RunStatus_Active <> App.RunStatus then
      exit;
    Sleep(10);
  end;
end;

function DataGet_DayData_Sina(App: TBaseApp; AStockCode_Sina: string; AWeightMode: TRT_WeightMode; AYear, ASeason: Word; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): TALIntegerList; 
var
  tmpUrl: string;
  tmpHttpData: PIOBuffer;
begin
  Result := nil;  
  if weightNone <> AWeightMode then
  begin
    tmpUrl := BaseSinaDayUrl_weight;
    AWeightMode := weightBackward;
  end else
  begin
    tmpUrl := BaseSinaDayUrl1;
  end;
  tmpurl := tmpurl + AStockCode_Sina + '.phtml';
  if (0 <> AYear) and (0 < ASeason) then 
  begin
    tmpurl := tmpurl + '?year=' + inttostr(AYear);
    tmpUrl := tmpUrl + '&' + 'jidu=' + inttostr(ASeason);
  end;
  // parse html data
  ClearIOBuffer(AHttpData);     
  tmpHttpData := GetHttpUrlData(tmpUrl, ANetSession, AHttpData, SizeMode_512k);
  if nil <> tmpHttpData then
  begin
    try
      //Result := StockDayData_Parse_Sina_Html1.DataParse_DayData_Sina(ADataAccess, tmpHttpData);
      //Result := StockDayData_Parse_Sina_Html2.DataParse_DayData_Sina(ADataAccess, tmpHttpData);        
      Result := StockDayData_Parse_Sina_Html3.DataParse_DayData_Sina(tmpHttpData);
//        if nil <> tmpDayDataList then
//        begin
//          try
//            if 0 < tmpDayDataList.Count then
//            begin
//              for i := 0 to tmpDayDataList.Count - 1 do
//              begin
//                tmpDayData := PRT_Quote_Day(tmpDayDataList.Objects[i]);
//                AddDealDayData(ADataAccess, tmpDayData);
//                Result := True;
//              end;
//            end; 
//            for i := tmpDayDataList.Count - 1 downto 0 do
//            begin
//              FreeMem(PRT_Quote_Day(tmpDayDataList.Objects[i]));
//            end;
//            tmpDayDataList.Clear;
//          finally
//            tmpDayDataList.Free;
//          end;
//        end;
    finally
      if AHttpData <> tmpHttpData then
      begin
        CheckInIOBuffer(tmpHttpData);
      end;
    end;
  end;
end;

function DataGet_DayData_Sina(App: TBaseApp; ADataAccess: TStockDayDataAccess; AYear, ASeason: Word; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): Boolean; overload;
var
  tmpUrl: string;
  tmpHttpData: PIOBuffer;
  tmpRepeat: Integer;  
  tmpDayDataList: TALIntegerList;   
  i: integer;
  tmpDayData: PRT_Quote_Day;
  tmpCounter: integer;
begin
  Result := false;
  tmpurl := '';  
  if DBType_Item_China = ADataAccess.StockItem.DBType then
  begin
    tmpurl := ADataAccess.StockItem.sCode + '.phtml';
  end;
  if DBType_Index_China = ADataAccess.StockItem.DBType then
  begin
    if '999999' = ADataAccess.StockItem.sCode then
    begin
      tmpurl := '000001' + '/type/S.phtml';
    end else
    begin
      tmpurl := ADataAccess.StockItem.sCode + '/type/S.phtml';
    end;
  end;
  if '' = tmpUrl then
    exit;

  if weightNone <> ADataAccess.WeightMode then
  begin
    tmpUrl := BaseSinaDayUrl_weight + tmpUrl;
    ADataAccess.WeightMode := weightBackward;
  end else
  begin
    tmpUrl := BaseSinaDayUrl1 + tmpUrl;
  end;    
  if (0 <> AYear) and (0 < ASeason) then 
  begin
    tmpurl := tmpurl + '?year=' + inttostr(AYear);
    tmpUrl := tmpUrl + '&' + 'jidu=' + inttostr(ASeason);
  end;
  // parse html data
  //Log(LOGTAG, 'DataGet_DayData_Sina:' + tmpUrl);
  tmpRepeat := 3;
  while tmpRepeat > 0 do
  begin
    tmpHttpData := GetHttpUrlData(tmpUrl, ANetSession, AHttpData, SizeMode_512k);
    if RunStatus_Active <> App.RunStatus then
      exit;
    if nil = tmpHttpData then
    begin   
      //Log(LOGTAG, 'DataGet_DayData_Sina Error Http Nil');
    end else
    begin
      try
        //Result := StockDayData_Parse_Sina_Html1.DataParse_DayData_Sina(ADataAccess, tmpHttpData);
        //Result := StockDayData_Parse_Sina_Html2.DataParse_DayData_Sina(ADataAccess, tmpHttpData);        
        tmpDayDataList := StockDayData_Parse_Sina_Html3.DataParse_DayData_Sina(tmpHttpData);
        if nil <> tmpDayDataList then
        begin
          try
            if 0 < tmpDayDataList.Count then
            begin                                
              //Log(LOGTAG, 'DataGet_DayData_Sina Count:' + IntToStr(tmpDayDataList.Count));
              for i := 0 to tmpDayDataList.Count - 1 do
              begin
                tmpDayData := PRT_Quote_Day(tmpDayDataList.Objects[i]);
                AddDealDayData(ADataAccess, tmpDayData);
                Result := True;
              end;
            end; 
            for i := tmpDayDataList.Count - 1 downto 0 do
            begin
              FreeMem(PRT_Quote_Day(tmpDayDataList.Objects[i]));
            end;
            tmpDayDataList.Clear;
          finally
            tmpDayDataList.Free;
          end;
        end;
      finally
        if AHttpData <> tmpHttpData then
        begin
          CheckInIOBuffer(tmpHttpData);
        end;
      end;
    end;
    if Result then
      Break;
    Dec(tmpRepeat);
    tmpCounter := 500 * (3 - tmpRepeat);
    if 0 < tmpCounter then
    begin
      while 0 < tmpCounter do
      begin         
        if RunStatus_Active <> App.RunStatus then
          exit;
        Sleep(10);
        tmpCounter := tmpCounter - 10;
      end;
    end;
  end;
end;

function GetStockDataDay_Sina(App: TBaseApp; AStockItem: PRT_DealItem; AWeightMode: TRT_WeightMode; ANetSession: PHttpClientSession; AHttpData: PIOBuffer): Boolean;
var
  tmpStockDataAccess: TStockDayDataAccess; 
  tmpLastDealDate: Word;
  tmpInt: integer; 
  tmpQuoteDay: PRT_Quote_Day;
  tmpFromYear: Word;   
  tmpFromMonth: Word;
  tmpFromDay: Word;
  tmpCurrentYear: Word; 
  tmpCurrentMonth: Word;
  tmpCurrentDay: Word;
  tmpJidu: integer;
begin
  Result := false;
  if nil = AStockItem then
    exit;
  tmpLastDealDate := Trunc(now());
  tmpInt := DayOfWeek(tmpLastDealDate);
  if 1 = tmpInt then
  begin
    tmpLastDealDate := tmpLastDealDate - 2;
  end else if 7 = tmpInt then
  begin
    tmpLastDealDate := tmpLastDealDate - 1;
  end else
  begin
    // 当天数据不下载
    tmpLastDealDate := tmpLastDealDate - 1;
  end;
  tmpStockDataAccess := TStockDayDataAccess.Create(AStockItem, src_Sina, AWeightMode);
  try                                                
    //SDLog('', 'GetStockDataDay_Sina check:' + AStockItem.sCode);
    if CheckNeedLoadStockDayData(App, tmpStockDataAccess, tmpLastDealDate) then
    begin

    end else
      exit;
    //Log(LOGTAG, 'GetStockDataDay_Sina begin:' + AStockItem.sCode);
    (*//
    // 先以 163 必须 获取到数据为前提 ???
    if AStockItem.FirstDealDate < 1 then
    begin
      exit;
    end;
    //*)
    DecodeDate(now, tmpCurrentYear, tmpCurrentMonth, tmpCurrentDay);
    if (0 < tmpStockDataAccess.LastDealDate) and (0 < tmpStockDataAccess.FirstDealDate) then
    begin
      DecodeDate(tmpStockDataAccess.LastDealDate, tmpFromYear, tmpFromMonth, tmpFromDay);
    end else
    begin
      if tmpStockDataAccess.StockItem.FirstDealDate > 0 then
      begin
        DecodeDate(tmpStockDataAccess.StockItem.FirstDealDate, tmpFromYear, tmpFromMonth, tmpFromDay);
      end else
      begin
      end;
    end;
    if tmpFromYear < 1980 then
    begin
      if tmpStockDataAccess.StockItem.FirstDealDate = 0 then
      begin
        tmpFromYear := 2013;
        tmpFromYear := 1990;
        tmpFromMonth := 12;
      end;
    end;
    tmpJidu := SeasonOfMonth(tmpFromMonth);
    while tmpFromYear < tmpCurrentYear do
    begin
      if RunStatus_Active <> App.RunStatus then
        exit;
      while tmpJidu < 5 do
      begin
        if RunStatus_Active <> App.RunStatus then
          exit;
        DataGet_DayData_Sina(App, tmpStockDataAccess, tmpFromYear, tmpJidu, ANetSession, AHttpData);
        Inc(tmpJidu);
      end;
      Inc(tmpFromYear);
      tmpJidu := 1;
    end;
    while tmpJidu < SeasonOfMonth(tmpCurrentMonth) do
    begin
      if RunStatus_Active <> App.RunStatus then
        exit;
      DataGet_DayData_Sina(App, tmpStockDataAccess, tmpCurrentYear, tmpJidu, ANetSession, AHttpData);
      Inc(tmpJidu);
    end;
    if RunStatus_Active <> App.RunStatus then
      exit;
    DataGet_DayData_SinaNow(App, tmpStockDataAccess, ANetSession, AHttpData);

    SaveStockDayData(App, tmpStockDataAccess); 
    if 0 = AStockItem.FirstDealDate then
    begin
      if 0 < tmpStockDataAccess.RecordCount then
      begin
        tmpQuoteDay := tmpStockDataAccess.RecordItem[0];
        AStockItem.FirstDealDate := tmpQuoteDay.DealDate.Value;
        AStockItem.IsDataChange := 1;
      end;
    end;   
  finally
    tmpStockDataAccess.Free;
  end;
end;

end.
