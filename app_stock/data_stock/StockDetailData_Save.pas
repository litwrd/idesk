unit StockDetailData_Save;

interface

uses
  BaseApp,
  StockDetailDataAccess;

  procedure SaveStockDetailData(App: TBaseApp; ADataAccess: TStockDetailDataAccess);   
  procedure SaveStockDetailData2File(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AFileUrl: string);

implementation
        
uses
  Windows,
  Sysutils,
  BaseWinFile,
  //UtilsLog,
  define_datasrc,
  define_dealstore_file,
  define_stock_quotes,
  define_dealstore_header,
  Define_Price;
          
{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDetailData_Save.pas';
{$ENDIF}
               
procedure SaveStockDetailDataToBuffer(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AMemory: pointer); forward;

procedure SaveStockDetailData(App: TBaseApp; ADataAccess: TStockDetailDataAccess);
var
  tmpFileUrl: string;
begin
  tmpFileUrl := App.Path.GetFileUrl(ADataAccess.DBType, ADataAccess.DataType,
    GetDealDataSourceCode(ADataAccess.DataSource), ADataAccess.FirstDealDate, ADataAccess.StockItem);
    //Log(LOGTAG, 'SaveStockDetailData:' + tmpFileUrl);
  SaveStockDetailData2File(App, ADataAccess, tmpFileUrl);
end;

procedure SaveStockDetailData2File(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AFileUrl: string);
var
  tmpWinFile: TWinFile;
  tmpFileMapView: Pointer;
  tmpFileNewSize: integer;
begin
  if '' = AFileUrl then
    exit;
  ForceDirectories(ExtractFilePath(AFileUrl));
  tmpWinFile := TWinFile.Create;
  try
    if tmpWinFile.OpenFile(AFileUrl, true) then
    begin
      tmpFileNewSize := SizeOf(define_dealstore_header.TStore_Quote_M2_Detail_Header_V1Rec) +
          ADataAccess.RecordCount * SizeOf(define_stock_quotes.TStore_Quote32_Detail); //400k
      tmpFileNewSize := ((tmpFileNewSize div (1 * 1024)) + 1) * 1 * 1024;
      tmpWinFile.FileSize := tmpFileNewSize;

      tmpFileMapView := tmpWinFile.OpenFileMap;
      if nil <> tmpFileMapView then
      begin
        SaveStockDetailDataToBuffer(App, ADataAccess, tmpFileMapView);
      end;
    end;
  finally
    tmpWinFile.Free;
  end;
end;

procedure SaveStockDetailDataToBuffer(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AMemory: pointer);
var
  tmpHead: PStore_Quote_M2_Detail_Header_V1Rec;
  tmpStoreDetailData: PStore_Quote32_Detail;
  tmpRTDetailData: PRT_Quote_Detail;
  i: integer;
begin
  tmpHead := AMemory;  
  tmpHead.Header.BaseHeader.CommonHeader.Signature.Signature := 7784; // 6
  tmpHead.Header.BaseHeader.CommonHeader.Signature.DataVer1  := 1;
  tmpHead.Header.BaseHeader.CommonHeader.Signature.DataVer2  := 0;
  tmpHead.Header.BaseHeader.CommonHeader.Signature.DataVer3  := 0;
  // 字节存储顺序 java 和 delphi 不同
  // 00
  // 01
  tmpHead.Header.BaseHeader.CommonHeader.Signature.BytesOrder:= 1;
  tmpHead.Header.BaseHeader.CommonHeader.HeadSize            := SizeOf(TStore_Quote_M2_Detail_Header_V1Rec);             // 1 -- 7
  tmpHead.Header.BaseHeader.CommonHeader.StoreSizeMode.Value := 16;  // 1 -- 8 page size mode
  { 表明是什么数据 }
  tmpHead.Header.BaseHeader.CommonHeader.DataType            := DataType_Stock;             // 2 -- 10
  tmpHead.Header.BaseHeader.CommonHeader.DataMode            := DataMode_DayDetailDataM2;             // 1 -- 11
  tmpHead.Header.BaseHeader.CommonHeader.RecordSizeMode.Value:= 6;  // 1 -- 12
  tmpHead.Header.BaseHeader.CommonHeader.RecordCount         := ADataAccess.RecordCount;          // 4 -- 16
  tmpHead.Header.BaseHeader.CommonHeader.CompressFlag        := 0;             // 1 -- 17
  tmpHead.Header.BaseHeader.CommonHeader.EncryptFlag         := 0;             // 1 -- 18
  tmpHead.Header.BaseHeader.CommonHeader.DataSourceId        := GetDealDataSourceCode(ADataAccess.DataSource);             // 2 -- 20
  CopyMemory(@tmpHead.Header.BaseHeader.Code[0], @ADataAccess.StockItem.sCode[1], Length(ADataAccess.StockItem.sCode));
  //Move(ADataAccess.StockItem.Code, tmpHead.Header.BaseHeader.Code[0], Length(ADataAccess.StockItem.Code)); // 12 - 32
  // ----------------------------------------------------
  tmpHead.Header.BaseHeader.StorePriceFactor    := 1000;             // 2 - 34
                                                
  tmpHead.Header.BaseHeader.FirstDealDate       := ADataAccess.FirstDealDate;             // 2 - 36
  tmpHead.Header.BaseHeader.LastDealDate       := ADataAccess.LastDealDate;             // 2 - 36
  
  Inc(tmpHead);
  tmpStoreDetailData := PStore_Quote32_Detail(tmpHead); 
  ADataAccess.Sort;
  for i := 0 to ADataAccess.RecordCount - 1 do
  begin
    tmpRTDetailData := ADataAccess.RecordItem[i];
    if nil <> tmpRTDetailData then
    begin
      RTPricePack2StorePrice(@tmpStoreDetailData.Quote.Price, @tmpRTDetailData.Price); 
      tmpStoreDetailData.Quote.DealVolume          := tmpRTDetailData.DealVolume;         // 8 - 24 成交量
      tmpStoreDetailData.Quote.DealAmount          := tmpRTDetailData.DealAmount;         // 8 - 32 成交金额
      tmpStoreDetailData.Quote.QuoteDealTime       := tmpRTDetailData.DealDateTime.Time.Value;       // 4 - 36 交易日期
      tmpStoreDetailData.Quote.QuoteDealDate       := tmpRTDetailData.DealDateTime.Date.Value;
      tmpStoreDetailData.Quote.DealType       := tmpRTDetailData.DealType;
      Inc(tmpStoreDetailData);
    end;
  end;
end;

end.
