unit DBStockClass;

interface
         
{$IFDEF DEALCLASS}
uses
  Windows, Sysutils, define_stockclass, define_datasrc,
  QuickList_DealClass,
  SQLite3,
  SQLite3Wrap;

type
  TDBStockClassData = record
    SqlDB: TSQLite3Database;
    ClassList: TDealClassList;
  end;

  TDBStockClass = class
  protected
    fDBStockClassData: TDBStockClassData;
  public
    constructor Create; //override;
    destructor Destroy; override;
    function Initialize: boolean;
    function AddClass(ADataSrc: integer; AClassName: string): PRT_ClassDef_Rec;
    function FindClass(ADataSrc: integer; AClassName: string): PRT_ClassDef_Rec;    
    function CheckOutClass(ADataSrc: integer; AClassName: string): PRT_ClassDef_Rec; 
    property ClassList: TDealClassList read fDBStockClassData.ClassList;
  end;

const
  // b f g h j k l m q r t w x y z
  Field_AutoKeyID         = 'a';
  Field_CreateTime        = 'c'; // CRUD time create
  Field_EndTime           = 'd'; // CRUD time end
  Field_ValidStatus       = 'e'; // end
  Field_KeyID             = 'i'; // id = index 
  Field_Memo              = 'm';
  Field_Name              = 'n'; // name   
  Field_Object            = 'o';
  Field_Parent            = 'p';  
  Field_Relation          = 'r'; // link relation
  Field_DataSrc           = 's'; // src   
  Field_Type              = 't'; // type
  Field_UpdateTime        = 'u'; // CRUD time update
  Field_Value             = 'v';
  Field_Owner             = 'w';

  // d ==> define + object name
  Table_Define_Class      = 'd_cs'; // class
  Table_Class_Link        = 'cs_lk'; // class -- class link
  Table_Stock_Class       = 'sk_cs_lk'; // stock -- class link

  Field_InfoCreateTime    = 'tc'; // 信息采集时间 create time
  Field_InfoBeginTime     = 'tb'; // 信息记录开始时间 begin time
  Field_InfoLastValidTime = 'tl'; // 信息最新持续有效时间 last time
  Field_InfoInvalidTime   = 'te'; // 信息过期时间 end time

  Field_Stock             = 'sk_o'; // stock object
  Field_Stock_Class       = 'sk_cs'; // stock class
  
  Field_StockClass_ClassA = 'cs_a';
  Field_StockClass_ClassB = 'cs_b';
  Field_StockClass_LinkType = 'lk_t';
  
const
  FieldType_AutoIncKey = #32 + 'INTEGER PRIMARY KEY AUTOINCREMENT' + #32;
  FieldType_String = #32 + 'TEXT NOT NULL' + #32;
  FieldType_Double = #32 + 'REAL NOT NULL DEFAULT 0' + #32;        
  FieldType_Int = #32 + 'INT NOT NULL DEFAULT 0' + #32;
{$ENDIF}

implementation
             
{$IFDEF DEALCLASS}
{ TDBStockClass }

constructor TDBStockClass.Create;
begin
  inherited;
  FillChar(fDBStockClassData, SizeOf(fDBStockClassData), 0);    
  fDBStockClassData.ClassList := TDealClassList.Create;
end;

destructor TDBStockClass.Destroy;
begin
  if nil <> fDBStockClassData.SqlDB then
  begin
    fDBStockClassData.SqlDB.Close;
    FreeAndNil(fDBStockClassData.SqlDB);
  end;
  inherited;
end;

// CREATE TABLE ... AS SELECT
// Create Table if not exists
 
function TDBStockClass.Initialize: boolean;
var
  tmpStr: string;
  tmpQuery: TSQLite3Statement;
  tmpClass: PRT_ClassDef_Rec;
  tmpCacheID: integer;
  tmpCacheClass: PRT_ClassDef_Rec;
  i: Integer;
begin
  Result := false;
  if nil = fDBStockClassData.SqlDB then
  begin
    fDBStockClassData.SqlDB := TSQLite3Database.Create;
  end;
  tmpStr := 'sclass.db3'; //ChangeFileExt(ParamStr(0), '.db');
  fDBStockClassData.SqlDB.Open(tmpStr,
      SQLite3.SQLITE_OPEN_CREATE or
      SQLite3.SQLITE_OPEN_READWRITE or
      SQLite3.SQLITE_OPEN_NOMUTEX);
  
  tmpStr := 'Create Table if not exists ' + Table_Define_Class + '(' +
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +
    Field_KeyID       + FieldType_Int + ',' +
    Field_ValidStatus + FieldType_Int + ',' +
    Field_DataSrc     + FieldType_Int + ',' +
    Field_Parent      + FieldType_Int + ',' +
    Field_Name        + FieldType_String +
    ');';
    
  fDBStockClassData.SqlDB.Execute(tmpStr);
                                      
  tmpStr := 'Create Table if not exists ' + Table_Class_Link + '(' +
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +
    Field_ValidStatus + FieldType_Int + ',' +
    Field_StockClass_ClassA      + FieldType_Int + ',' +
    Field_StockClass_ClassB      + FieldType_Int + ',' +
    Field_StockClass_LinkType    + FieldType_Int +
    ');';
  fDBStockClassData.SqlDB.Execute(tmpStr); 
  
  tmpStr := 'Create Table if not exists ' + Table_Stock_Class + '(' +
    Field_AutoKeyID         + FieldType_AutoIncKey + ',' + 
    Field_ValidStatus       + FieldType_Int + ',' +
    Field_DataSrc           + FieldType_Int + ',' +
    Field_Stock             + FieldType_Int + ',' +
    Field_Stock_Class       + FieldType_Int + ',' +
    Field_InfoCreateTime    + FieldType_Int + ',' + // 信息采集时间
    Field_InfoBeginTime     + FieldType_Int + ',' + //= 'itb'; // 信息记录开始时间
    Field_InfoLastValidTime + FieldType_Int + ',' + //= 'itv'; // 信息最新持续有效时间
    Field_InfoInvalidTime   + FieldType_Int + // 'iti'; // 信息过期时间
  ');';

  fDBStockClassData.SqlDB.Execute(tmpStr);

  tmpStr := 'Select ' +
               Field_AutoKeyID + ',' +
               Field_DataSrc + ',' +
               Field_Parent + ',' +
               Field_Name + ' from ' + Table_Define_Class;
  tmpQuery := fDBStockClassData.SqlDB.Prepare(tmpStr);
  while SQLite3.SQLITE_Row = tmpQuery.Step do
  begin
    tmpClass := System.New(PRT_ClassDef_Rec);
    FillChar(tmpClass^, SizeOf(TRT_ClassDef_Rec), 0);

    tmpClass.Id := tmpQuery.ColumnInt(0);
    tmpClass.DataSrc := tmpQuery.ColumnInt(1);
    tmpClass.ParentId := tmpQuery.ColumnInt(2);
    tmpClass.Name := tmpQuery.ColumnText(3);
    fDBStockClassData.ClassList.AddDealClass(tmpClass.Id, tmpClass);
  end;
  tmpCacheID := 0;
  tmpCacheClass := nil;
  for i := 0 to fDBStockClassData.ClassList.Count - 1 do
  begin
    tmpClass := fDBStockClassData.ClassList.DealClass[i];
    if 0 <> tmpClass.ParentId then
    begin
      if tmpCacheID <> tmpClass.ParentId then
      begin
        tmpCacheID := tmpClass.ParentId;
        tmpCacheClass := fDBStockClassData.ClassList.FindDealClass(tmpClass.ParentId);
      end;
      tmpClass.Parent := tmpCacheClass;
    end;
  end;
end;
                        
function TDBStockClass.CheckOutClass(ADataSrc: integer; AClassName: string): PRT_ClassDef_Rec;
begin
  Result := nil;
  if '' = Trim(AClassName) then
    exit;         
  Result := FindClass(ADataSrc, AClassName);
  if nil = Result then
   Result := AddClass(ADataSrc, AClassName);
end;

function TDBStockClass.AddClass(ADataSrc: integer; AClassName: string): PRT_ClassDef_Rec;
var
  tmpStr: string;        
  tmpClass: PRT_ClassDef_Rec;     
  tmpQuery: TSQLite3Statement;
  tmpRet: integer;
begin           
  Result := nil;
  if '' = Trim(AClassName) then
    exit;

  tmpStr := 'insert into ' + Table_Define_Class + '(' +
               Field_DataSrc + ',' +
               Field_Parent + ',' +
               Field_Name +  ') values (' + 
               ':' + Field_DataSrc + ',' +
               ':' + Field_Parent + ',' +
               ':' + Field_Name +  ')';
               
  tmpQuery := fDBStockClassData.SqlDB.Prepare(tmpStr);
  tmpQuery.BindInt(1, ADataSrc);
  tmpQuery.BindInt(2, 0);
  tmpQuery.BindText(3, AClassName);
  tmpRet := tmpQuery.Step;
  if SQLITE_DONE <> tmpRet then
  begin
  end;
  tmpClass := System.New(PRT_ClassDef_Rec);
  FillChar(tmpClass^, SizeOf(TRT_ClassDef_Rec), 0);

  tmpClass.Id := fDBStockClassData.SqlDB.LastInsertRowID;
  tmpClass.DataSrc := ADataSrc;
  tmpClass.ParentId := 0;
  tmpClass.Name := AClassName;

  fDBStockClassData.ClassList.AddDealClass(tmpClass.Id, tmpClass);
end;

function TDBStockClass.FindClass(ADataSrc: integer; AClassName: string): PRT_ClassDef_Rec;
var
  i: integer;
  tmpRec: PRT_ClassDef_Rec;
begin
  Result := nil;   
  if '' = Trim(AClassName) then
    exit;         
  if nil = fDBStockClassData.ClassList then
    exit;
  for i := 0 to fDBStockClassData.ClassList.Count - 1 do
  begin
    tmpRec := fDBStockClassData.ClassList.DealClass[i];
    if SameText(AClassName, tmpRec.Name) then
    begin
      Result := tmpRec;
      Break;
    end;
  end;
end;
{$ENDIF}

end.
