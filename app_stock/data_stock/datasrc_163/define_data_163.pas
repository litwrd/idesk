unit define_data_163;

interface

type
  TDealDayDataHeadName_163 = (
    headNone,
    headDay,
    headCode,
    headName,
    headPrice_Close,
    headPrice_High,
    headPrice_Low,
    headPrice_Open,
    headPrice_PrevClose,
    headPrice_Change,
    headPrice_ChangeRate,
    headDeal_VolumeRate,
    headDeal_Volume,
    headDeal_Amount,
    headTotal_Value,
    headDeal_Value); 
                                       
const
  Base163DayUrl1 = 'http://quotes.money.163.com/service/chddata.html?';

  // 上证指数
  // http://quotes.money.163.com/service/chddata.html?
  // code=0000001&start=19901219&end=20160219&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;VOTURNOVER;VATURNOVER';
  // code=1399300 沪深 300
            
  DealDayDataHeadNames_163: array[TDealDayDataHeadName_163] of string = ('',
    '日期', '股票代码', '名称',
    '收盘价', '最高价', '最低价', '开盘价', '前收盘',
    '涨跌额', '涨跌幅', '换手率', '成交量',
    '成交金额', '总市值', '流通市值');
                  
implementation

end.
