unit DB_Stock;

interface

uses
  define_dealmarket,
  define_dealitem,
  db_dealitem,
  {$IFDEF DEALCLASS}
  //db_dealclass,
  DBStockClass,       
  {$ENDIF}
  {$IFDEF DEALINSTANT}
  db_quote_instant,
  {$ENDIF}
  Sysutils;
  
type
  TDBStockData = record
    StockIndexDB: TDBDealItem;
    StockItemDB: TDBDealItem;
    {$IFDEF DEALCLASS}
    //StockClass: TDBDealClassDictionary;
    StockClassDB: TDBStockClass;
    {$ENDIF}
    {$IFDEF DEALINSTANT}
    QuoteInstantDB: TDBQuoteInstant;
    {$ENDIF}
    IndexItem_SH: PRT_Dealitem; // 上证指数
    IndexItem_SZ: PRT_Dealitem; // 深圳成指
    IndexItem_ZX: PRT_Dealitem; // 中小板指
    IndexItem_CY: PRT_Dealitem; // 创业板指   
  end;
              
  TDBStock = class
  protected
    fDBStockData: TDBStockData;
  public
    constructor Create;
    destructor Destroy; override;    
    property StockIndexDB: TDBDealItem read fDBStockData.StockIndexDB write fDBStockData.StockIndexDB;
    property StockItemDB: TDBDealItem read fDBStockData.StockItemDB write fDBStockData.StockItemDB;
    {$IFDEF DEALCLASS} 
    //property StockClass: TDBDealClassDictionary read fDBStockData.StockClass write fDBStockData.StockClass;
    property StockClassDB: TDBStockClass read fDBStockData.StockClassDB write fDBStockData.StockClassDB;
    {$ENDIF}
    {$IFDEF DEALINSTANT}
    property QuoteInstantDB: TDBQuoteInstant read fDBStockData.QuoteInstantDB write fDBStockData.QuoteInstantDB;   
    {$ENDIF}        

    property IndexItem_SH: PRT_Dealitem read fDBStockData.IndexItem_SH write fDBStockData.IndexItem_SH; // 上证指数
    property IndexItem_SZ: PRT_Dealitem read fDBStockData.IndexItem_SZ write fDBStockData.IndexItem_SZ; // 深圳成指
    property IndexItem_ZX: PRT_Dealitem read fDBStockData.IndexItem_ZX write fDBStockData.IndexItem_ZX; // 中小板指
    property IndexItem_CY: PRT_Dealitem read fDBStockData.IndexItem_CY write fDBStockData.IndexItem_CY; // 创业板指
  end;
  
implementation


{ TDBStock }

constructor TDBStock.Create;
begin
  FillChar(fDBStockData, SizeOf(fDBStockData), 0);
end;

destructor TDBStock.Destroy;
begin
  if (nil <> fDBStockData.StockIndexDB) then
    FreeAndNil(fDBStockData.StockIndexDB);
  if (nil <> fDBStockData.StockItemDB) then
    FreeAndNil(fDBStockData.StockItemDB);  
  {$IFDEF DEALCLASS} 
  if nil <> fDBStockData.StockClassDB then
    FreeAndNil(fDBStockData.StockClassDB);
  {$ENDIF}                   
  {$IFDEF DEALINSTANT}
  if nil <> fDBStockData.QuoteInstantDB then
    FreeAndNil(fDBStockData.QuoteInstantDB);
  {$ENDIF}                   
  inherited;
end;

end.
