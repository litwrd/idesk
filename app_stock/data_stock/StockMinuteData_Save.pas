unit StockMinuteData_Save;

interface

uses
  Classes, Sysutils, BaseApp,
  StockMinuteDataAccess;

  function SaveStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess): Boolean; overload;
  function SaveStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess; AFileUrl: string): Boolean; overload;

implementation
        
uses
  Windows,
  BaseWinFile,   
  //utilsLog,
  define_price,
  define_datetime,
  define_datasrc,
  define_stock_quotes,
  define_dealstore_header,
  define_dealstore_file,
  StockMinuteData_Load;
                       
function SaveStockMinuteDataToBuffer(App: TBaseApp; ADataAccess: TStockMinuteDataAccess; AMemory: pointer): Boolean;
var
  tmpHead: PStore_Quote_M1_Minute_Header_V1Rec;
  tmpQuoteData: PStore64;
  tmpStoreMinuteData: PStore_Quote32_Minute;
  tmpRTMinuteData: PRT_Quote_Minute;
  tmpCode: AnsiString;
  i: integer;
begin
  Result := false;
  if nil = ADataAccess then
    exit;
  if nil = AMemory then
    exit;
  
  tmpHead := AMemory;  
  tmpHead.Header.BaseHeader.CommonHeader.Signature.Signature := 7784; // 6
  tmpHead.Header.BaseHeader.CommonHeader.Signature.DataVer1  := 1;
  tmpHead.Header.BaseHeader.CommonHeader.Signature.DataVer2  := 0;
  tmpHead.Header.BaseHeader.CommonHeader.Signature.DataVer3  := 0;
  // 字节存储顺序 java 和 delphi 不同
  // 00
  // 01
  tmpHead.Header.BaseHeader.CommonHeader.Signature.BytesOrder:= 1;
  tmpHead.Header.BaseHeader.CommonHeader.HeadSize            := SizeOf(TStore_Quote_M1_Minute_Header_V1Rec);             // 1 -- 7
  tmpHead.Header.BaseHeader.CommonHeader.StoreSizeMode.Value := 16;  // 1 -- 8 page size mode
  { 表明是什么数据 }
  tmpHead.Header.BaseHeader.CommonHeader.DataType            := DataType_Stock;             // 2 -- 10
  tmpHead.Header.BaseHeader.CommonHeader.DataMode            := DataMode_MinuteData;             // 1 -- 11
  tmpHead.Header.BaseHeader.CommonHeader.RecordSizeMode.Value:= 6;  // 1 -- 12
  tmpHead.Header.BaseHeader.CommonHeader.RecordCount         := ADataAccess.RecordCount;          // 4 -- 16
  tmpHead.Header.BaseHeader.CommonHeader.CompressFlag        := 0;             // 1 -- 17
  tmpHead.Header.BaseHeader.CommonHeader.EncryptFlag         := 0;             // 1 -- 18
  tmpHead.Header.BaseHeader.CommonHeader.DataSourceId        := GetDealDataSourceCode(ADataAccess.DataSource);             // 2 -- 20
  tmpCode := ADataAccess.StockCode;
  if '' <> tmpCode then
    CopyMemory(@tmpHead.Header.BaseHeader.Code[0], @tmpCode[1], Length(tmpCode));
  //Move(ADataAccess.StockItem.Code, tmpHead.Header.BaseHeader.Code[0], Length(ADataAccess.StockItem.Code)); // 12 - 32
  // ----------------------------------------------------
  tmpHead.Header.BaseHeader.StorePriceFactor    := 1000;             // 2 - 34
                                                
  tmpHead.Header.FirstDealDateTime       := ADataAccess.FirstDealDateTime;             // 2 - 36
  tmpHead.Header.LastDealDateTime        := ADataAccess.LastDealDateTime;             // 2 - 38
  tmpHead.Header.Minute := ADataAccess.Minute;
  
  Inc(tmpHead);
  tmpQuoteData := PStore64(tmpHead);
  ADataAccess.Sort;
  for i := 0 to ADataAccess.RecordCount - 1 do
  begin
    tmpRTMinuteData := ADataAccess.RecordItem[i];
    if nil <> tmpRTMinuteData then
    begin
      tmpStoreMinuteData := PStore_Quote32_Minute(tmpQuoteData);
      RTPricePackRange2StorePriceRange(@tmpStoreMinuteData.PriceRange, @tmpRTMinuteData.PriceRange); 
      tmpStoreMinuteData.DealVolume          := tmpRTMinuteData.DealVolume;         // 8 - 24 成交量
      tmpStoreMinuteData.DealAmount          := tmpRTMinuteData.DealAmount;         // 8 - 32 成交金额
      tmpStoreMinuteData.StockDateTime       := tmpRTMinuteData.DealDateTime;

      //tmpStoreMinuteData.DealDate            := tmpRTMinuteData.DealDate.Value;       // 4 - 36 交易日期
      //tmpStoreMinuteData.Weight.Value        := tmpRTMinuteData.Weight.Value; // 4 - 40 复权权重 * 100
      //tmpStoreMinuteData.TotalValue          := tmpRTMinuteData.TotalValue;         // 8 - 48 总市值
      //tmpStoreMinuteData.DealValue           := tmpRTMinuteData.DealValue;         // 8 - 56 流通市值
      Inc(tmpQuoteData);
    end;
  end;
end;

function SaveStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess): Boolean;
var
  tmpFileUrl: string;
begin               
  //Log('SaveStockMinuteData.pas', 'SaveStockMinuteData 1:' + IntToStr(ADataAccess.Minute));
  Result := false;
  if nil = ADataAccess then
    exit;
  if 0 < ADataAccess.Minute then
  begin                                
    if 240 > ADataAccess.Minute then
    begin
      if nil = ADataAccess.StockItem then
      begin
        if '' <> ADataAccess.StockCode then
        begin
        end;
      end;
      if nil <> ADataAccess.StockItem then
      begin
        if weightNone <> ADataAccess.WeightMode then
        begin
          tmpFileUrl := App.Path.GetFileUrl(ADataAccess.DBType, ADataAccess.DataType, GetDealDataSourceCode(ADataAccess.DataSource), ADataAccess.Minute, ADataAccess.StockItem);
        end else
        begin
          tmpFileUrl := App.Path.GetFileUrl(ADataAccess.DBType, ADataAccess.DataType, GetDealDataSourceCode(ADataAccess.DataSource), ADataAccess.Minute, ADataAccess.StockItem);
        end;
      end;   
      Result := SaveStockMinuteData(App, ADataAccess, tmpFileUrl);
    end;
  end;
end;

function SaveStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess; AFileUrl: string): Boolean;
var
  tmpWinFile: TWinFile;   
  tmpFileMapView: Pointer; 
  tmpFileNewSize: integer;
  tmpMergeDataAccess: TStockMinuteDataAccess;
  tmpIdxMergeSrc, tmpIdxDest: integer;
  tmpMinuteMergeSrc: PRT_Quote_Minute;
  tmpMinuteDest: PRT_Quote_Minute;
  tmpAddList: TList;
  i: integer;
begin
  Result := false;
  if nil = ADataAccess then
    exit;
  if '' = Trim(AFileUrl) then
    exit;
  //Log('SaveStockMinuteData.pas', 'SaveStockMinuteData:' + tmpFileUrl);
  if FileExists(AFileUrl) then
  begin
    // merge data
    tmpMergeDataAccess := TStockMinuteDataAccess.Create(ADataAccess.StockItem,
        GetDealDataSource(ADataAccess.DataSrcID),
        ADataAccess.WeightMode);
    try
      if LoadStockMinuteData(App, tmpMergeDataAccess, AFileUrl) then
      begin
        if tmpMergeDataAccess.Minute = ADataAccess.Minute then
        begin
          tmpAddList := TList.Create;
          try
            ADataAccess.Sort;
            tmpMergeDataAccess.Sort;
            tmpIdxMergeSrc := 0;
            tmpIdxDest := 0;
            while (tmpIdxMergeSrc < tmpMergeDataAccess.RecordCount) and (tmpIdxDest < ADataAccess.RecordCount) do
            begin                    
              tmpMinuteDest := ADataAccess.MinuteDataByIndex(tmpIdxDest);
              tmpMinuteMergeSrc := tmpMergeDataAccess.MinuteDataByIndex(tmpIdxMergeSrc);
              if (tmpMinuteDest.DealDateTime.Date.Value = tmpMinuteMergeSrc.DealDateTime.Date.Value) then
              begin
                if (tmpMinuteDest.DealDateTime.Time.Value = tmpMinuteMergeSrc.DealDateTime.Time.Value) then
                begin
                  Inc(tmpIdxMergeSrc);
                  Inc(tmpIdxDest);
                end else
                begin
                  if (tmpMinuteDest.DealDateTime.Time.Value < tmpMinuteMergeSrc.DealDateTime.Time.Value) then
                  begin      
                    Inc(tmpIdxDest);
                  end else
                  begin
                    tmpAddList.Add(tmpMinuteMergeSrc);
                    Inc(tmpIdxMergeSrc);
                  end;
                end;
              end else
              begin
                if (tmpMinuteDest.DealDateTime.Date.Value < tmpMinuteMergeSrc.DealDateTime.Date.Value) then
                begin           
                  Inc(tmpIdxDest);
                end else
                begin             
                  tmpAddList.Add(tmpMinuteMergeSrc);  
                  Inc(tmpIdxMergeSrc);
                end;
              end;          
            end;           
            while (tmpIdxMergeSrc < tmpMergeDataAccess.RecordCount) do
            begin              
              tmpMinuteMergeSrc := tmpMergeDataAccess.MinuteDataByIndex(tmpIdxMergeSrc);   
              tmpAddList.Add(tmpMinuteMergeSrc);
              Inc(tmpIdxMergeSrc);
            end;
            for i := 0 to tmpAddList.Count - 1 do
            begin           
              tmpMinuteMergeSrc := PRT_Quote_Minute(tmpAddList.Items[i]);   
              tmpMinuteDest := ADataAccess.CheckOutRecord(tmpMinuteMergeSrc.DealDateTime);
              if nil <> tmpMinuteDest then
              begin
                tmpMinuteDest^ := tmpMinuteMergeSrc^;
              end;
            end;
            tmpAddList.Clear;
          finally
            tmpAddList.Free;
          end;
        end;
      end;
    finally
      tmpMergeDataAccess.Free;
    end;
  end;

  tmpWinFile := TWinFile.Create;
  try
    if tmpWinFile.OpenFile(AFileUrl, true) then
    begin
      tmpFileNewSize := SizeOf(TStore_Quote_M1_Minute_Header_V1Rec) + ADataAccess.RecordCount * SizeOf(TStore64); //400k
      tmpFileNewSize := ((tmpFileNewSize div (64 * 1024)) + 1) * 64 * 1024;
      tmpWinFile.FileSize := tmpFileNewSize;

      tmpFileMapView := tmpWinFile.OpenFileMap;
      if nil <> tmpFileMapView then
      begin
        Result := SaveStockMinuteDataToBuffer(App, ADataAccess, tmpFileMapView);
      end;
    end;
  finally
    tmpWinFile.Free;
  end;
end;

end.
