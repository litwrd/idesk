unit StockDetailData_Load;

interface
                           
uses
  BaseApp,
  StockDetailDataAccess;
  
  procedure LoadStockDetailData(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AIsDoLog: Boolean = false); overload;
  procedure LoadStockDetailData(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AFileUrl: AnsiString; AIsDoLog: Boolean = false); overload; 

implementation

uses
  SysUtils,
  //UtilsLog,
  BaseWinFile,
  define_datetime,      
  define_Price,
  define_datasrc,
  define_stock_quotes,
  define_dealstore_header,
  define_dealstore_file;

{$IFNDEF RELEASE}  
const
  LOGTAG = 'StockDetailData_Load.pas';
{$ENDIF}
                            
procedure LoadStockDetailDataFromBuffer(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AMemory: pointer; AIsDoLog: Boolean = false); forward;

procedure LoadStockDetailData(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AIsDoLog: Boolean = false);
begin
  LoadStockDetailData(App, ADataAccess,
      App.Path.GetFileUrl(ADataAccess.DBType, ADataAccess.DataType,
      GetDealDataSourceCode(ADataAccess.DataSource), ADataAccess.FirstDealDate, ADataAccess.StockItem),
      AIsDoLog);
end;
             
procedure LoadStockDetailData(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AFileUrl: AnsiString; AIsDoLog: Boolean = false);
var                   
  tmpWinFile: TWinFile;
  tmpFileMapView: Pointer;
begin
  if AIsDoLog then
  begin     
    //Log(LOGTAG, 'LoadStockDetailData ' + AFileUrl);
  end;
  if App.Path.IsFileExists(AFileUrl) then
  begin
    tmpWinFile := TWinFile.Create;
    try
      if tmpWinFile.OpenFile(AFileUrl, false) then
      begin
        tmpFileMapView := tmpWinFile.OpenFileMap;
        if nil <> tmpFileMapView then
        begin
          LoadStockDetailDataFromBuffer(App, ADataAccess, tmpFileMapView, AIsDoLog);
        end else
        begin
          if AIsDoLog then
          begin
            //Log(LOGTAG, 'LoadStockDetailData map open error ');
          end;
        end;
      end else
      begin
        if AIsDoLog then
        begin
          //Log(LOGTAG, 'LoadStockDetailData file open error ');
        end;
      end;
    finally
      tmpWinFile.Free;
    end;
  end else
  begin      
    if AIsDoLog then
    begin
      //Log(LOGTAG, 'LoadStockDetailData file not exists ' + AFileUrl);
    end;
  end;
end;

procedure LoadStockDetailDataFromBuffer(App: TBaseApp; ADataAccess: TStockDetailDataAccess; AMemory: pointer; AIsDoLog: Boolean = false);
var 
  tmpHead: PStore_Quote_M2_Detail_Header_V1Rec;
  tmpStoreDetailData: PStore_Quote32_Detail;
  
  tmpRTDetailData: PRT_Quote_Detail;
  tmpRecordCount: integer;
  tmpStockDateTime: TDateTimeStock;
  i: integer;
  tmpDateStr: string;
begin             
  if AIsDoLog then
  begin
    //Log(LOGTAG, 'LoadStockDetailData buffer');
  end;
  tmpHead := AMemory;
  tmpStockDateTime.Date.Value := 0;
  tmpStockDateTime.Time.Value := 0;
  if tmpHead.Header.BaseHeader.CommonHeader.HeadSize = SizeOf(TStore_Quote_M2_Detail_Header_V1Rec) then
  begin
    if (tmpHead.Header.BaseHeader.CommonHeader.DataType = DataType_Stock) then
    begin
      if (tmpHead.Header.BaseHeader.CommonHeader.DataMode = DataMode_DayDetailDataM2) then
      begin
        if src_unknown = ADataAccess.DataSource then
          ADataAccess.DataSource  := GetDealDataSource(tmpHead.Header.BaseHeader.CommonHeader.DataSourceId);  
        if AIsDoLog then
        begin
          //Log(LOGTAG, 'ADataAccess.DataSourceId' + IntToStr(GetDealDataSourceCode(ADataAccess.DataSource)) + '/' +
          //  IntToStr(tmpHead.Header.BaseHeader.CommonHeader.DataSourceId));
        end;
        if GetDealDataSourceCode(ADataAccess.DataSource) = tmpHead.Header.BaseHeader.CommonHeader.DataSourceId then
        begin
          tmpRecordCount := tmpHead.Header.BaseHeader.CommonHeader.RecordCount;
          Inc(tmpHead);
          tmpStoreDetailData := PStore_Quote32_Detail(tmpHead);
          for i := 0 to tmpRecordCount - 1 do
          begin
            tmpStockDateTime.Date.Value := tmpStoreDetailData.Quote.QuoteDealDate;
            tmpStockDateTime.Time.Value := tmpStoreDetailData.Quote.QuoteDealTime;
            
            if 0 = tmpStockDateTime.Date.Value then
            begin
              tmpStockDateTime.Date.Value := tmpHead.Header.BaseHeader.FirstDealDate;  
              if 0 = tmpStockDateTime.Date.Value then
                tmpStockDateTime.Date.Value := ADataAccess.FirstDealDate;
              if 0 < tmpStockDateTime.Date.Value then
              begin
                tmpDateStr := FormatDateTime('yyyymmdd', tmpStockDateTime.Date.Value);
                if '' = tmpDateStr then
                begin
                end;
              end;
            end;
            if 0 < tmpStockDateTime.Date.Value then
            begin
              if (0 < tmpStoreDetailData.Quote.DealVolume) and
                 (0 < tmpStoreDetailData.Quote.DealAmount) then
              begin
                tmpRTDetailData := ADataAccess.CheckOutDetail(tmpStockDateTime);
                if nil <> tmpRTDetailData then
                begin
                  StorePrice2RTPricePack(@tmpRTDetailData.Price, @tmpStoreDetailData.Quote.Price);   
                  tmpRTDetailData.DealVolume := tmpStoreDetailData.Quote.DealVolume;
                  tmpRTDetailData.DealAmount := tmpStoreDetailData.Quote.DealAmount;
                  tmpRTDetailData.DealType := tmpStoreDetailData.Quote.DealType;
                end;
              end;
            end else
            begin
              if AIsDoLog then
              begin
                //Log(LOGTAG, 'tmpDate Error' + IntToStr(tmpStockDateTime.Date.Value) + '/' + IntToStr(ADataAccess.FirstDealDate));
              end;
            end;
            Inc(tmpStoreDetailData);
          end;
        end;
      end else
      begin
        if AIsDoLog then
        begin
          //Log(LOGTAG, 'tmpHead.Header.BaseHeader.DataMode Error');
        end;
      end;
    end else
    begin
      if AIsDoLog then
      begin
        //Log(LOGTAG, 'tmpHead.Header.BaseHeader.DataType Error');
      end;
    end;
  end else
  begin
    if AIsDoLog then
    begin
      //Log(LOGTAG, 'tmpHead.Header.BaseHeader.HeadSize Error');
    end;
  end;
end;

end.
