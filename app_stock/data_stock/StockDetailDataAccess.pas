unit StockDetailDataAccess;

interface

uses
  define_DealItem,
  define_datasrc,
  define_datetime,
  BaseDataSet,
  QuickList_DetailData,
  define_stock_quotes;
  
type
  TStockDetailDataAccess = class(TBaseDataSetAccess)
  protected
    fStockItem: PRT_DealItem;  
    fDetailDealData: TDetailDataList;
    fDataSource: TDealDataSource;
    fFirstDealDate: Word;
    fLastDealDate: Word;    
    function GetFirstDealDate: Word;
    procedure SetFirstDealDate(const Value: Word);

    function GetLastDealDate: Word;
    procedure SetLastDealDate(const Value: Word);

    procedure SetStockItem(AStockItem: PRT_DealItem);
    
    function GetRecordItem(AIndex: integer): Pointer; override;
    function GetRecordCount: Integer; override;
  public
    constructor Create(AStockItem: PRT_DealItem; ADataSrc: TDealDataSource); reintroduce;
    destructor Destroy; override;
    
    function FindDetail(AStockDateTime: TDateTimeStock): PRT_Quote_Detail;
    function CheckOutDetail(AStockDateTime: TDateTimeStock): PRT_Quote_Detail;
    function NewDetail(AStockDateTime: TDateTimeStock): PRT_Quote_Detail;
    procedure Sort; override;    
    procedure Clear; override;
    class function DataTypeDefine: integer; override;
    property FirstDealDate: Word read GetFirstDealDate write SetFirstDealDate;
    property LastDealDate: Word read GetLastDealDate write SetLastDealDate;
    property StockItem: PRT_DealItem read fStockItem write SetStockItem;
    property DataSource: TDealDataSource read fDataSource write fDataSource;
  end;
          
implementation

uses
  SysUtils,
  define_dealstore_file,
  QuickSortList;
                       
{ TStockDetailDataAccess }

constructor TStockDetailDataAccess.Create(AStockItem: PRT_DealItem; ADataSrc: TDealDataSource);
begin         
  inherited Create(AStockItem.DBType, GetDealDataSourceCode(ADataSrc));
  fStockItem := AStockItem;   
  fDetailDealData := TDetailDataList.Create;
  fDetailDealData.Duplicates := lstDupAccept; 
  fDataSource := ADataSrc;
end;

destructor TStockDetailDataAccess.Destroy;
begin
  Clear;
  FreeAndNil(fDetailDealData);
  inherited;
end;

class function TStockDetailDataAccess.DataTypeDefine: integer;
begin
  Result := DataType_DetailData;
end;

procedure TStockDetailDataAccess.SetStockItem(AStockItem: PRT_DealItem);
begin
  if nil <> AStockItem then
  begin
    if fStockItem <> AStockItem then
    begin
      Self.Clear;
    end;
  end;
  fStockItem := AStockItem;
  if nil <> fStockItem then
  begin

  end;
end;
                           
function TStockDetailDataAccess.GetFirstDealDate: Word;
begin
  Result := fFirstDealDate;
end;

procedure TStockDetailDataAccess.SetFirstDealDate(const Value: Word);
begin
  fFirstDealDate := Value;
end;
                    
function TStockDetailDataAccess.GetLastDealDate: Word;
begin
  Result := fLastDealDate;
end;
                    
procedure TStockDetailDataAccess.SetLastDealDate(const Value: Word);
begin
  fLastDealDate := Value;
end;

function TStockDetailDataAccess.GetRecordCount: Integer;
begin
  Result := fDetailDealData.Count;
end;

function TStockDetailDataAccess.GetRecordItem(AIndex: integer): Pointer;
begin
  Result := nil;
  if AIndex < fDetailDealData.Count then
  begin
    Result := fDetailDealData.DetailData[AIndex];
  end else
  begin
    if AIndex > 0 then
    begin
    
    end;
  end;
end;

procedure TStockDetailDataAccess.Sort;
begin                   
  if nil <> fDetailDealData then
    fDetailDealData.Sort;
end;

procedure TStockDetailDataAccess.Clear;  
var
  i: integer;
  tmpQuote: PRT_Quote_Detail;
begin
  inherited;
  if nil <> fDetailDealData then
  begin
    for i := fDetailDealData.Count - 1 downto 0 do
    begin
      tmpQuote := fDetailDealData.DetailData[i];
      FreeMem(tmpQuote);
    end;
    fDetailDealData.Clear;
  end;
end;

function TStockDetailDataAccess.NewDetail(AStockDateTime: TDateTimeStock): PRT_Quote_Detail;
begin
  Result := System.New(PRT_Quote_Detail);
  FillChar(Result^, SizeOf(TRT_Quote_Detail), 0);
  Result.DealDateTime := AStockDateTime;
  fDetailDealData.AddDetailData(Result.DealDateTime, Result);
end;

function TStockDetailDataAccess.CheckOutDetail(AStockDateTime: TDateTimeStock): PRT_Quote_Detail;
begin
  Result := FindDetail(AStockDateTime);
  if nil = Result then
  begin
    Result := NewDetail(AStockDateTime);
  end;
end;

function TStockDetailDataAccess.FindDetail(AStockDateTime: TDateTimeStock): PRT_Quote_Detail;
var
  tmpPos: integer;

begin
  Result := nil;
  tmpPos := fDetailDealData.IndexOf(AStockDateTime);
  if 0 <= tmpPos then
    Result := fDetailDealData.DetailData[tmpPos];
end;

end.
