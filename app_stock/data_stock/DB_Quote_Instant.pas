unit DB_Quote_Instant;

interface
          
{$IFDEF DEALINSTANT} 
uses
  BaseDataSet, define_stock_quotes_instant, QuickList_QuoteInstant;
  
type              
  TDBQuoteInstant = class(TBaseDataSetAccess)
  protected
    fInstantQuoteList: TQuoteInstantList;
    function GetRecordCount: Integer; override;  
    function GetRecordItem(AIndex: integer): Pointer; override;
    function GetItem(AIndex: integer): PRT_InstantQuote; overload;
    function GetItem(ACode: string): PRT_InstantQuote; overload;
  public
    constructor Create(ADBType: integer); reintroduce;
    destructor Destroy; override;
    class function DataTypeDefine: integer; override;
    procedure Sort; override;
    procedure Clear; override;
    function AddInstantQuote(ADataSrc: integer; AStockPackCode: integer): PRT_InstantQuote;
    function CheckOutInstantQuote(ADataSrc: integer; AStockPackCode: integer): PRT_InstantQuote;
    function FindRecordByKey(AKey: Integer): Pointer; override;
    function InstantQuoteByIndex(AIndex: integer): PRT_InstantQuote;
    property Items[AIndex: integer]: PRT_InstantQuote read GetItem;       
    property Item[ACode: string]: PRT_InstantQuote read GetItem;
  end;
{$ENDIF}

implementation
         
{$IFDEF DEALINSTANT}
uses
  define_dealstore_file,
  QuickSortList;

constructor TDBQuoteInstant.Create(ADBType: integer); 
begin
  inherited Create(ADBType, 0);
  fInstantQuoteList := TQuoteInstantList.Create;
  fInstantQuoteList.Duplicates := QuickSortList.lstDupIgnore;
end;

destructor TDBQuoteInstant.Destroy;
begin
  Clear;
  fInstantQuoteList.Free;
  inherited;
end;

class function TDBQuoteInstant.DataTypeDefine: integer;
begin
  Result := DataType_InstantData;
end;


function TDBQuoteInstant.GetRecordItem(AIndex: integer): Pointer;
begin
  Result := Pointer(fInstantQuoteList.InstantQuote[AIndex]);
end;

function TDBQuoteInstant.GetItem(AIndex: integer): PRT_InstantQuote;
begin
  Result := GetRecordItem(AIndex);
end;

function TDBQuoteInstant.GetItem(ACode: string): PRT_InstantQuote;
begin
  Result := nil;
end;

function TDBQuoteInstant.GetRecordCount: integer;
begin
  Result := fInstantQuoteList.Count;
end;

procedure TDBQuoteInstant.Sort;
begin
  fInstantQuoteList.Sort;
end;
          
procedure TDBQuoteInstant.Clear;
var
  i: integer;
  tmpInstantQuote: PRT_InstantQuote;
begin
  for i := fInstantQuoteList.Count - 1 downto 0 do
  begin
    tmpInstantQuote := fInstantQuoteList.InstantQuote[i];
    FreeMem(tmpInstantQuote);
  end;
  fInstantQuoteList.Clear;
end;

function TDBQuoteInstant.AddInstantQuote(ADataSrc: integer; AStockPackCode: integer): PRT_InstantQuote;
begin
  Result := System.New(PRT_InstantQuote);
  FillChar(Result^, SizeOf(TRT_InstantQuote), 0);
  if nil <> Result then
  begin
    fInstantQuoteList.AddInstantQuote(AStockPackCode, Result);
  end;
end;

function TDBQuoteInstant.FindRecordByKey(AKey: Integer): Pointer;
var
  tmpIndex: integer;
begin
  Result := nil;
  tmpIndex := fInstantQuoteList.IndexOf(AKey);
  if 0 <= tmpIndex then
    Result := fInstantQuoteList.InstantQuote[tmpIndex];
end;

function TDBQuoteInstant.InstantQuoteByIndex(AIndex: integer): PRT_InstantQuote;
begin
  Result := GetRecordItem(AIndex);
end;

function TDBQuoteInstant.CheckOutInstantQuote(ADataSrc: integer; AStockPackCode: integer): PRT_InstantQuote;
begin
  Result := nil; //FindInstantQuoteByCode(AMarketCode + AStockCode);
  if nil = Result then
  begin
    Result := AddInstantQuote(ADataSrc, AStockPackCode);
  end;
end;
{$ENDIF}

end.
