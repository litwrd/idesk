unit StockData_Import_tdx;

interface
             
{$IFDEF TDX}
uses
  Classes, Sysutils, BaseApp,  
  define_datasrc, define_dealitem, define_price, define_stock_quotes,
  StockMinuteDataAccess;
                     
  function ImportStockData_Minute_TDX_FromFile(AMinuteDataAccess: TStockMinuteDataAccess; AFileUrl: string): integer;
          
{$ENDIF}

implementation

uses
  define_data_tdx,
  define_datetime,
  UtilsDateTime;
               
{$IFDEF TDX}
function ImportStockData_Minute_TDX_FromFile(AMinuteDataAccess: TStockMinuteDataAccess; AFileUrl: string): integer;  
type
  TColumnsIndex = array[TImportDataHeadName_tdx] of Integer;
var    
  tmpFileContent: TStringList;
  tmpRowDatas: TStringList;   
  tmpIsReadHead: Boolean;
  tmpColumnIndex: TColumnsIndex;
  iCol: TImportDataHeadName_tdx;
  i, j: integer;
  tmpPos: integer;
  tmpRowData: string;
  tmpTimeStr: string;
  tmpDateTimeParse: TDateTimeParseRecord;
  tmpDateTimeParse2: TDateTimeParseRecord;
  tmpMinuteParseData: TRT_Quote_Minute;          
  tmpMinuteData: PRT_Quote_Minute;
  tmpMinuteData2: PRT_Quote_Minute;
  tmpStockDateTime: TDateTimeStock;
begin
  Result := 0;
  if not FileExists(AFileUrl) then
    exit;
  if nil = AMinuteDataAccess then
    exit;
  tmpFileContent := TStringList.Create;
  tmpRowDatas := TStringList.Create;
  try
    tmpFileContent.LoadFromFile(AFileUrl);
    tmpIsReadHead := false;
    for iCol := Low(TImportDataHeadName_tdx) to High(TImportDataHeadName_tdx) do
      tmpColumnIndex[iCol] := -1;

      
    for i := 0 to tmpFileContent.Count - 1 do
    begin
      tmpRowData := Trim(tmpFileContent[i]);
      if '' <> tmpRowData then
      begin
        // 上证指数 (999999)
        // '时间'#9'    开盘'#9'    最高'#9'    最低'#9'    收盘'#9'         成交量'#9'BOLL-M.BOLL  '#9'BOLL-M.UB    '#9'BOLL-M.LB    '#9'  VOL.VOLUME'#9'  VOL.MAVOL1'#9'  VOL.MAVOL2'#9' CYHT.高抛  '#9' CYHT.SK    '#9' CYHT.SD    '#9' CYHT.低吸  '#9' CYHT.强弱分界'#9' CYHT.卖出  '#9' CYHT.买进  '#9' BDZX.AK    '#9' BDZX.AD1   '#9' BDZX.AJ    '#9' BDZX.aa    '#9' BDZX.bb    '#9' BDZX.cc    '#9' BDZX.买进  '#9' BDZX.卖出'
        tmpRowDatas.Text := StringReplace(tmpRowData, #9, #13#10, [rfReplaceAll]);
        if not tmpIsReadHead then
        begin
          if 3 > tmpRowDatas.Count then
          begin                    
            if '' = AMinuteDataAccess.StockCode then
            begin
              tmpPos := Pos('(', tmpRowData);
              if 0 < tmpPos then
              begin
                tmpRowData := Trim(Copy(tmpRowData, tmpPos + 1, maxint));
                tmpPos := Pos(')', tmpRowData);
                if 0 < tmpPos then
                begin
                  tmpRowData := Trim(Copy(tmpRowData, 1, tmpPos - 1));
                end;
                if '' <> tmpRowData then
                begin
                  AMinuteDataAccess.StockCode := tmpRowData;
                end;
              end;
            end;
          end else
          begin
            for iCol := Low(TImportDataHeadName_tdx) to High(TImportDataHeadName_tdx) do
            begin
              if '' <> ImportDataHeadName_tdx[iCol] then
              begin
                for j := 0 to tmpRowDatas.Count - 1 do
                begin
                  if SameText(ImportDataHeadName_tdx[iCol], Trim(tmpRowDatas[j])) then
                  begin
                    tmpIsReadHead := True;
                    tmpColumnIndex[iCol] := j;
                  end;
                end;
              end;
            end;
          end;
        end else
        begin
          FillChar(tmpDateTimeParse, SizeOf(tmpDateTimeParse), 0);
          tmpTimeStr := '';
          if 0 <= tmpColumnIndex[headTime] then
          begin
            tmpTimeStr := tmpRowDatas[tmpColumnIndex[headTime]];
            // 2014/06/12-10:30
          end;
          if '' <> tmpTimeStr then
          begin
            ParseDateTime(@tmpDateTimeParse, tmpTimeStr);
          end;
          if (0 < tmpDateTimeParse.Date) then
          begin
            FillChar(tmpMinuteParseData, SizeOf(tmpMinuteParseData), 0);
            DateTime2DateTimeStock(tmpDateTimeParse.Date + tmpDateTimeParse.Time, @tmpStockDateTime);
            if 0 = tmpDateTimeParse.Time then
            begin
              // 60 * 4
              AMinuteDataAccess.Minute := 240;
            end;
            trySetRTPricePack(@tmpMinuteParseData.PriceRange.PriceOpen, tmpRowDatas[tmpColumnIndex[headPrice_Open]]);
            trySetRTPricePack(@tmpMinuteParseData.PriceRange.PriceHigh, tmpRowDatas[tmpColumnIndex[headPrice_High]]);
            trySetRTPricePack(@tmpMinuteParseData.PriceRange.PriceLow, tmpRowDatas[tmpColumnIndex[headPrice_Low]]);
            trySetRTPricePack(@tmpMinuteParseData.PriceRange.PriceClose, tmpRowDatas[tmpColumnIndex[headPrice_Close]]);
            tmpMinuteParseData.DealVolume := StrToIntDef(tmpRowDatas[tmpColumnIndex[headDeal_Volume]], 0);
              
            tmpMinuteData := AMinuteDataAccess.FindRecord(tmpStockDateTime);
            if nil = tmpMinuteData then
            begin
              tmpMinuteData := AMinuteDataAccess.NewRecord(tmpStockDateTime);
              Result := Result + 1;
              if nil <> tmpMinuteData then
              begin
                tmpMinuteData.DealDateTime := tmpStockDateTime; 
                tmpMinuteData.PriceRange := tmpMinuteParseData.PriceRange;
                tmpMinuteData.DealVolume := tmpMinuteParseData.DealVolume;
                tmpMinuteData.DealAmount := tmpMinuteParseData.DealAmount;                
              end;
            end else
            begin
              if (tmpMinuteData.PriceRange.PriceOpen.Value <> tmpMinuteParseData.PriceRange.PriceOpen.Value) or
                 (tmpMinuteData.PriceRange.PriceHigh.Value <> tmpMinuteParseData.PriceRange.PriceHigh.Value) or
                 (tmpMinuteData.PriceRange.PriceLow.Value <> tmpMinuteParseData.PriceRange.PriceLow.Value) or
                 (tmpMinuteData.PriceRange.PriceClose.Value <> tmpMinuteParseData.PriceRange.PriceClose.Value) or
                 (tmpMinuteData.DealVolume <> tmpMinuteParseData.DealVolume) or
                 (tmpMinuteData.DealAmount <> tmpMinuteParseData.DealAmount) then
              begin
                Result := Result + 1;   
                tmpMinuteData.PriceRange := tmpMinuteParseData.PriceRange;
                tmpMinuteData.DealVolume := tmpMinuteParseData.DealVolume;
                tmpMinuteData.DealAmount := tmpMinuteParseData.DealAmount;   
              end;
            end;
          end;
        end;
      end;
    end;
  finally
    tmpRowDatas.Clear;
    tmpRowDatas.Free;
    tmpFileContent.Free;
  end;
  if 0 = AMinuteDataAccess.Minute then
  begin
    if 1 < AMinuteDataAccess.RecordCount then
    begin
      i := 0;
      while i + 1 < AMinuteDataAccess.RecordCount do
      begin                     
        if 0 <> AMinuteDataAccess.Minute then
          Break;
        tmpMinuteData := AMinuteDataAccess.RecordItem[i];
        tmpMinuteData2 := AMinuteDataAccess.RecordItem[i + 1];
        DecodeDate(tmpMinuteData.DealDateTime.Date.Value, tmpDateTimeParse.Year, tmpDateTimeParse.Month, tmpDateTimeParse.Day);
        DecodeDate(tmpMinuteData2.DealDateTime.Date.Value, tmpDateTimeParse2.Year, tmpDateTimeParse2.Month, tmpDateTimeParse2.Day);

        if (tmpDateTimeParse.Year = tmpDateTimeParse2.Year) and
           (tmpDateTimeParse.Month = tmpDateTimeParse2.Month) and
           (tmpDateTimeParse.Day = tmpDateTimeParse2.Day) then
        begin
          DecodeTime(TimeStock2Time(@tmpMinuteData.DealDateTime.Time), tmpDateTimeParse.Hour, tmpDateTimeParse.Minute, tmpDateTimeParse.Second, tmpDateTimeParse.MSecond);
          DecodeTime(TimeStock2Time(@tmpMinuteData2.DealDateTime.Time), tmpDateTimeParse2.Hour, tmpDateTimeParse2.Minute, tmpDateTimeParse2.Second, tmpDateTimeParse2.MSecond);
          if tmpDateTimeParse.Hour = tmpDateTimeParse2.Hour then
          begin
            if tmpDateTimeParse.Minute <> tmpDateTimeParse2.Minute then
            begin
              AMinuteDataAccess.Minute := Abs(tmpDateTimeParse2.Minute - tmpDateTimeParse.Minute);
            end;
          end else
          begin
            if 1 = Abs(tmpDateTimeParse2.Hour - tmpDateTimeParse.Hour) then
            begin
              if tmpDateTimeParse.Minute = tmpDateTimeParse2.Minute then
              begin
                AMinuteDataAccess.Minute := 60;
              end;
            end else
            begin
            end;
          end;
        end;
        i := i + 1;        
      end;
    end;
  end;
end;
{$ENDIF}

end.
