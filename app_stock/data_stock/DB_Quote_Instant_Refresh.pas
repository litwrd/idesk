unit DB_Quote_Instant_Refresh;

interface
         
{$IFDEF DEALINSTANT}
uses
  Windows, StockInstantData_Get_Sina, DB_Quote_Instant;

type
  PDB_Quote_Instant_Refresh = ^TDB_Quote_Instant_Refresh;
  TDB_Quote_Instant_Refresh = record
    ThreadHandle            : THandle;
    ThreadId                : DWORD;
    QuoteInstantDB          : TDBQuoteInstant;
    StockQuoteInstantArray  : TInstantArray;
  end;

  procedure ThreadRunDBQuoteInstantRefresh(ARefresh: PDB_Quote_Instant_Refresh);
{$ENDIF}

implementation
       
{$IFDEF DEALINSTANT}
uses
  UtilsHttp;
          
function ThreadProc_RefreshData(ARefresh: PDB_Quote_Instant_Refresh): HResult; stdcall;
var
  tmpHttpSession: UtilsHttp.THttpClientSession;
  tmpDBItemIndex: integer;
  tmpArrayIndex: integer;
  tmpArraySize: integer;  
begin
  Result := 0;
  if nil <> ARefresh then
  begin
    if nil <> ARefresh.QuoteInstantDB then
    begin
      tmpDBItemIndex := 0;
      FillChar(tmpHttpSession, SizeOf(tmpHttpSession), 0);
      tmpArraySize := Length(ARefresh.StockQuoteInstantArray.Data);
      while True do
      begin
        Sleep(300);
        tmpArrayIndex := 0;
        while tmpArrayIndex < tmpArraySize do
        begin
          ARefresh.StockQuoteInstantArray.Data[tmpArrayIndex] := ARefresh.QuoteInstantDB.Items[tmpDBItemIndex];
          Inc(tmpArrayIndex);
          Inc(tmpDBItemIndex);
        end;
        DataGet_InstantArray_Sina(nil, @ARefresh.StockQuoteInstantArray, @tmpHttpSession, nil);
      end;
    end;
  end;
  ExitThread(Result);
end;

procedure ThreadRunDBQuoteInstantRefresh(ARefresh: PDB_Quote_Instant_Refresh);
begin
  ARefresh.ThreadHandle := Windows.CreateThread(nil, 0, @ThreadProc_RefreshData, ARefresh, Create_Suspended, ARefresh.ThreadId);
  Windows.ResumeThread(ARefresh.ThreadHandle);
end;
{$ENDIF}

end.
