unit StockMinuteData_Load;

interface
                           
uses
  BaseApp,
  StockMinuteDataAccess;
  
  function LoadStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess): Boolean; overload;
  function LoadStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess; AFileUrl: string): Boolean; overload;

implementation

uses
  Sysutils,
  BaseWinFile,          
  define_price,
  define_datasrc,
  define_datetime,
  //UtilsLog,
  define_stock_quotes,
  define_dealstore_header,
  define_dealstore_file;
                          
function LoadStockMinuteDataFromBuffer(ADataAccess: TStockMinuteDataAccess; AMemory: pointer): Boolean; forward;

function LoadStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess): Boolean;
var
  tmpFileUrl: WideString; 
begin
  Result := false;
  if nil = ADataAccess then
    exit;
  if nil = ADataAccess.StockItem then
    exit;
  tmpFileUrl := App.Path.GetFileUrl(ADataAccess.DBType, ADataAccess.DataType, GetDealDataSourceCode(ADataAccess.DataSource), ADataAccess.Minute, ADataAccess.StockItem);
  if not FileExists(tmpFileUrl) then
    tmpFileUrl := '';
  if '' <> tmpFileUrl then
  begin
    Result := LoadStockMinuteData(App, ADataAccess, tmpFileUrl);
  end;
end;

function LoadStockMinuteData(App: TBaseApp; ADataAccess: TStockMinuteDataAccess; AFileUrl: string): Boolean;
var
  tmpWinFile: TWinFile;
  tmpFileMapView: Pointer;
begin
  //Log('LoadStockDayData', 'FileUrl:' + tmpFileUrl);    
  Result := false;
  if App.Path.IsFileExists(AFileUrl) then
  begin                                 
    //Log('LoadStockDayData', 'FileUrl exist');
    tmpWinFile := TWinFile.Create;
    try
      if tmpWinFile.OpenFile(AFileUrl, false) then
      begin
        tmpFileMapView := tmpWinFile.OpenFileMap;
        if nil <> tmpFileMapView then
        begin        
          Result := LoadStockMinuteDataFromBuffer(ADataAccess, tmpFileMapView);
        end else
        begin
          //Log('LoadStockDayData', 'FileUrl map fail');
        end;
      end else
      begin
        //Log('LoadStockDayData', 'FileUrl open fail');
      end;
    finally
      tmpWinFile.Free;
    end;
  end;
end;

function ReadStockMinuteDataHeader(ADataAccess: TStockMinuteDataAccess; AMemory: pointer): PStore_Quote_M1_Minute_Header_V1Rec;
var
  tmpHead: PStore_Quote_M1_Minute_Header_V1Rec;
begin
  Result := nil;
  tmpHead := AMemory;
  if tmpHead.Header.BaseHeader.CommonHeader.HeadSize = SizeOf(TStore_Quote_M1_Minute_Header_V1Rec) then
  begin
    if (tmpHead.Header.BaseHeader.CommonHeader.DataType = DataType_Stock) then
    begin
      if (tmpHead.Header.BaseHeader.CommonHeader.DataMode = DataMode_MinuteData) then
      begin
        if src_unknown = ADataAccess.DataSource then
          ADataAccess.DataSource  := GetDealDataSource(tmpHead.Header.BaseHeader.CommonHeader.DataSourceId);
        if ADataAccess.DataSource = GetDealDataSource(tmpHead.Header.BaseHeader.CommonHeader.DataSourceId) then
        begin
          Result := tmpHead;
          ADataAccess.StockCode := tmpHead.Header.BaseHeader.Code;
          ADataAccess.Minute := tmpHead.Header.Minute;
        end;
      end;
    end;
  end;
end;

function LoadStockMinuteDataFromBuffer(ADataAccess: TStockMinuteDataAccess; AMemory: pointer): Boolean;
var
  tmpHead: PStore_Quote_M1_Minute_Header_V1Rec;
  tmpQuoteData: PStore64;
  tmpStoreMinuteData: PStore_Quote32_Minute;
  tmpRTMinuteData: PRT_Quote_Minute;
  tmpRecordCount: integer; 
  i: integer;
begin
  Result := false; 
  //Log('LoadStockDayData', 'LoadStockDayDataFromBuffer');
  tmpHead := ReadStockMinuteDataHeader(ADataAccess, AMemory);
  if nil <> tmpHead then
  begin
    tmpRecordCount := tmpHead.Header.BaseHeader.CommonHeader.RecordCount;
    //Log('LoadStockDayData', 'LoadStockDayDataFromBuffer record count:' + IntToStr(tmpRecordCount));    
    Inc(tmpHead);
    tmpQuoteData := PStore64(tmpHead);
    for i := 0 to tmpRecordCount - 1 do
    begin            
      Result := true;
      tmpStoreMinuteData := PStore_Quote32_Minute(tmpQuoteData);
      tmpRTMinuteData := ADataAccess.CheckOutRecord(tmpStoreMinuteData.StockDateTime);
      if nil <> tmpRTMinuteData then
      begin
        StorePriceRange2RTPricePackRange(@tmpRTMinuteData.PriceRange, @tmpStoreMinuteData.PriceRange);   
        tmpRTMinuteData.DealVolume := tmpStoreMinuteData.DealVolume;         // 8 - 24 成交量
        tmpRTMinuteData.DealAmount := tmpStoreMinuteData.DealAmount;         // 8 - 32 成交金额
        tmpRTMinuteData.DealDateTime := tmpStoreMinuteData.StockDateTime;        
        //tmpRTMinuteData.Weight.Value := tmpStoreMinuteData.Weight.Value; // 4 - 40 复权权重 * 100
        //tmpRTMinuteData.TotalValue := tmpStoreMinuteData.TotalValue;         // 8 - 48 总市值
        //tmpRTMinuteData.DealValue := tmpStoreMinuteData.DealValue;         // 8 - 56 流通市值
      end;
      Inc(tmpQuoteData);
    end;
    ADataAccess.Sort;
  end;
end;

end.
