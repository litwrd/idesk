unit StockMinuteDataAccess;

interface

uses
  define_dealItem,
  define_datetime,
  BaseDataSet,
  QuickList_MinuteData,
  define_price,
  define_datasrc,
  define_stock_quotes;
  
type
  TRT_StockMinuteData = record
    DealItem          : PRT_DealItem;
    StockCode         : string;
    IsDataChangedStatus: Byte;
    WeightMode        : Byte;
    Minute            : integer;
    MinuteDealData    : TMinuteDataList; 
    FirstDealDateTime : TDateTime;   // 2
    LastDealDateTime  : TDateTime;   // 2 最后记录交易时间
    DataSource        : TDealDataSource;
  end;
  
  TStockMinuteDataAccess = class(TBaseDataSetAccess)
  protected
    fStockMinuteData: TRT_StockMinuteData;   
    function GetStockCode: AnsiString;
    procedure SetStockCode(const Value: AnsiString);
    function GetFirstDealDateTime: TDateTime;
    procedure SetFirstDealDateTime(const Value: TDateTime);

    function GetLastDealDateTime: TDateTime;
    procedure SetLastDealDateTime(const Value: TDateTime);

    procedure SetStockItem(AStockItem: PRT_DealItem);

    function GetWeightMode: TRT_WeightMode;
    procedure SetWeightMode(value: TRT_WeightMode);
        
    function GetRecordItem(AIndex: integer): Pointer; override;
    function GetRecordCount: Integer; override;
  public
    constructor Create(AStockItem: PRT_DealItem; ADataSrc: TDealDataSource; AWeightMode: TRT_WeightMode); reintroduce;
    destructor Destroy; override;         
    class function DataTypeDefine: integer; override;
    
    function FindRecord(AStockDateTime: TDateTimeStock): PRT_Quote_Minute;
    function CheckOutRecord(AStockDateTime: TDateTimeStock): PRT_Quote_Minute;
    function NewRecord(AStockDateTime: TDateTimeStock): PRT_Quote_Minute;
                                  
    function MinuteDataByIndex(AIndex: integer): PRT_Quote_Minute;
    
    function DoGetRecords: integer; 
    function DoGetStockOpenPrice(AIndex: integer): double;
    function DoGetStockClosePrice(AIndex: integer): double;
    function DoGetStockHighPrice(AIndex: integer): double;
    function DoGetStockLowPrice(AIndex: integer): double;

    procedure Sort; override;
    procedure Clear; override;
    property FirstDealDateTime: TDateTime read GetFirstDealDateTime;
    property LastDealDateTime: TDateTime read GetLastDealDateTime;

    property StockItem: PRT_DealItem read fStockMinuteData.DealItem write SetStockItem;
    property StockCode: AnsiString read GetStockCode write SetStockCode;
    property DataSource: TDealDataSource read fStockMinuteData.DataSource write fStockMinuteData.DataSource;
    property WeightMode: TRT_WeightMode read GetWeightMode write SetWeightMode;
    property Minute: integer read fStockMinuteData.Minute write fStockMinuteData.Minute;
  end;
                            
implementation

uses
  QuickSortList,
  define_dealstore_file,
  SysUtils;
  
constructor TStockMinuteDataAccess.Create(AStockItem: PRT_DealItem; ADataSrc: TDealDataSource; AWeightMode: TRT_WeightMode);
begin
  if nil = AStockItem then
  begin
    inherited Create(DBType_Unknown, GetDealDataSourceCode(ADataSrc));
  end else
  begin
    inherited Create(AStockItem.DBType, GetDealDataSourceCode(ADataSrc));
  end;
  FillChar(fStockMinuteData, SizeOf(fStockMinuteData), 0);
  fStockMinuteData.DealItem := AStockItem;
  fStockMinuteData.MinuteDealData := TMinuteDataList.Create;
  fStockMinuteData.MinuteDealData.Clear;
  fStockMinuteData.MinuteDealData.Duplicates := QuickSortList.lstDupIgnore;
  fStockMinuteData.DataSource := ADataSrc;
  fStockMinuteData.WeightMode := Byte(AWeightMode);
end;

destructor TStockMinuteDataAccess.Destroy;
begin
  Clear;
  FreeAndNil(fStockMinuteData.MinuteDealData);
  inherited;
end;
                  
class function TStockMinuteDataAccess.DataTypeDefine: integer;
begin
  Result := DataType_MinuteData;
end;

procedure TStockMinuteDataAccess.Clear;   
var
  i: integer;
  tmpMinuteData: PRT_Quote_Minute;
begin
  if nil <> fStockMinuteData.MinuteDealData then
  begin
    for i := fStockMinuteData.MinuteDealData.Count - 1 downto 0 do
    begin
      tmpMinuteData := fStockMinuteData.MinuteDealData.MinuteData[i];
      FreeMem(tmpMinuteData);
    end;
    fStockMinuteData.MinuteDealData.Clear;
  end;
end;

procedure TStockMinuteDataAccess.SetStockCode(const Value: AnsiString);
begin
  fStockMinuteData.StockCode := Value;
end;

procedure TStockMinuteDataAccess.SetStockItem(AStockItem: PRT_DealItem);
begin
  if nil <> AStockItem then
  begin
    if fStockMinuteData.DealItem <> AStockItem then
    begin
    
    end;
  end;
  fStockMinuteData.DealItem := AStockItem;
  if nil <> fStockMinuteData.DealItem then
  begin

  end;
end;

function TStockMinuteDataAccess.GetFirstDealDateTime: TDateTime;
begin         
  Result := fStockMinuteData.FirstDealDateTime;
end;
                  
function TStockMinuteDataAccess.GetWeightMode: TRT_WeightMode;
begin
  Result := TRT_WeightMode(fStockMinuteData.WeightMode);
end;

procedure TStockMinuteDataAccess.SetWeightMode(value: TRT_WeightMode);
begin
  fStockMinuteData.WeightMode := Byte(value);
end;

procedure TStockMinuteDataAccess.SetFirstDealDateTime(const Value: TDateTime);
begin
  fStockMinuteData.FirstDealDateTime := Value;
end;

function TStockMinuteDataAccess.GetLastDealDateTime: TDateTime;
begin
  Result := fStockMinuteData.LastDealDateTime;
end;
              
procedure TStockMinuteDataAccess.SetLastDealDateTime(const Value: TDateTime);
begin
  fStockMinuteData.LastDealDateTime := Value;
end;

function TStockMinuteDataAccess.GetRecordCount: Integer;
begin
  Result := fStockMinuteData.MinuteDealData.Count;
end;

function TStockMinuteDataAccess.GetRecordItem(AIndex: integer): Pointer;
begin
  Result := fStockMinuteData.MinuteDealData.MinuteData[AIndex];
end;

function TStockMinuteDataAccess.GetStockCode: AnsiString;
begin
  Result := fStockMinuteData.StockCode;
  if '' = Result then
  begin
    if nil <> fStockMinuteData.DealItem then
    begin
      Result := fStockMinuteData.DealItem.sCode;
    end;
  end;
end;

procedure TStockMinuteDataAccess.Sort;
begin
  fStockMinuteData.MinuteDealData.Sort;
end;

function TStockMinuteDataAccess.CheckOutRecord(AStockDateTime: TDateTimeStock): PRT_Quote_Minute;
begin             
  //Result := nil;
  Result := FindRecord(AStockDateTime);
  if nil = Result then
    Result := NewRecord(AStockDateTime);
end;

function TStockMinuteDataAccess.NewRecord(AStockDateTime: TDateTimeStock): PRT_Quote_Minute;
begin
  if fStockMinuteData.FirstDealDateTime = 0 then
    fStockMinuteData.FirstDealDateTime := AStockDateTime.Date.Value;
  if fStockMinuteData.FirstDealDateTime > AStockDateTime.Date.Value then
    fStockMinuteData.FirstDealDateTime := AStockDateTime.Date.Value;
  if fStockMinuteData.LastDealDateTime < AStockDateTime.Date.Value then
    fStockMinuteData.LastDealDateTime := AStockDateTime.Date.Value;
  Result := System.New(PRT_Quote_Minute);
  FillChar(Result^, SizeOf(TRT_Quote_Minute), 0);
    //Result.DealDate.Value := ADate;
  fStockMinuteData.MinuteDealData.AddMinuteData(AStockDateTime, Result);
end;

function TStockMinuteDataAccess.MinuteDataByIndex(AIndex: integer): PRT_Quote_Minute;
begin
  Result := GetRecordItem(AIndex);
end;

function TStockMinuteDataAccess.FindRecord(AStockDateTime: TDateTimeStock): PRT_Quote_Minute;
var
  tmpPos: integer;
begin
  Result := nil;
  tmpPos := fStockMinuteData.MinuteDealData.IndexOf(AStockDateTime);
  if 0 <= tmpPos then
    Result := PRT_Quote_Minute(fStockMinuteData.MinuteDealData.MinuteData[tmpPos]);
end;

function TStockMinuteDataAccess.DoGetStockOpenPrice(AIndex: integer): double;
begin
  Result := MinuteDataByIndex(AIndex).PriceRange.PriceOpen.Value;
end;
                          
function TStockMinuteDataAccess.DoGetStockClosePrice(AIndex: integer): double;
begin
  Result := MinuteDataByIndex(AIndex).PriceRange.PriceClose.Value;
end;

function TStockMinuteDataAccess.DoGetStockHighPrice(AIndex: integer): double;
begin
  Result := MinuteDataByIndex(AIndex).PriceRange.PriceHigh.Value;
end;

function TStockMinuteDataAccess.DoGetStockLowPrice(AIndex: integer): double;
begin
  Result := MinuteDataByIndex(AIndex).PriceRange.PriceLow.Value;
end;


function TStockMinuteDataAccess.DoGetRecords: integer;
begin          
  Result := Self.RecordCount;
end;

end.
