﻿package sysutils
         
import (
//  "os"
  "fmt"
  . "strconv" // 加个. 引用函数 不需要写前面的包名
  "syscall"
)

/*
  info := sysutils.GetCpuInfo();
  fmt.Printf("cpu info %s\n", info);

  info = sysutils.GetOSInfo();
  fmt.Printf("os info %s\n", info);

  info = sysutils.GetDiskInfo();
  fmt.Printf("disk info %s\n", info);
//*/

func GetWindowsLogicalDrives() []string {
    kernel32 := syscall.MustLoadDLL("kernel32.dll")
    GetLogicalDrives := kernel32.MustFindProc("GetLogicalDrives")
    n, _, _ := GetLogicalDrives.Call()
    s := FormatInt(int64(n), 2)
 
    var drives_all = []string{"A:", "B:", "C:", "D:", "E:", "F:", "G:", "H:", "I:", "J:", "K:", "L:", "M:", "N:", "O:", "P：", "Q：", "R：", "S：", "T：", "U：", "V：", "W：", "X：", "Y：", "Z："}
    temp := drives_all[0:len(s)]
 
    var d []string
    for i, v := range s {
 
        if v == 49 {
            l := len(s) - i - 1
            d = append(d, temp[l])
        }
    }
 
    var drives []string
    for i, v := range d {
        drives = append(drives[i:], append([]string{v}, drives[:i]...)...)
    }
    fmt.Println(drives)
    return drives
 
}

func GetDiskInfo() (string) {
  diskinfo := GetWindowsLogicalDrives()
  if nil == diskinfo {
    return "bad"
  } else {
    return "good"
  }
}