﻿package sysutils
         
import (
  "os"    
  "os/exec"
  "path/filepath"
  "runtime"
)

// https://github.com/shirou/gopsutil
// os/exec包通过系统命令来获取相关信息
// gocrawl
// runtime.GOARCH 返回当前的系统架构；runtime.GOOS 返回当前的操作系统

/*
  http://blog.csdn.net/chenbaoke/article/details/42556949
  golang中os/exec包用法
  exec包执行外部命令，它将os.StartProcess进行包装使得它更容易映射到stdin和stdout，并且利用pipe连接i/o

  f, err := exec.LookPath("ls")
  if err != nil {
      fmt.Println(err)
  }
  fmt.Println(f) //  /bin/ls  

  Output()和CombinedOutput()不能够同时使用
  
  cmd := exec.Command("tr", "a-z", "A-Z")
  cmd.Stdin = strings.NewReader("some input")
  var out bytes.Buffer
  cmd.Stdout = &out
  err := cmd.Run()
  if err != nil {
      log.Fatal(err)
  }
  fmt.Printf("in all caps: %q\n", out.String())　　//in all caps: "SOME INPUT"
    
  cmd := exec.Command("ls")
  out, err := cmd.CombinedOutput()
  if err != nil {
      fmt.Println(err)
  }
  fmt.Println(string(out))

  cmd := exec.Command("ls")
  cmd.Stdout = os.Stdout //
  cmd.Run()
  fmt.Println(cmd.Start()) //exec: already started  
//*/

func GetOSInfo() (string) {
  //homeInfo := os.Getenv("HOME")
  //javahomeInfo := os.Getenv("JAVA_HOME")
  //return homeInfo + javahomeInfo;

  // 获取项目当前路径
  filehandle, _ := exec.LookPath(os.Args[0])
  fileurl, _ := filepath.Abs(filehandle)
  println(fileurl)
  
  host, err := os.Hostname()
  if (nil != err) {
  }
  info := runtime.GOARCH + "/" + runtime.GOOS
  if "windows" == runtime.GOOS {
    return "good windows"
  } else {
  }                       
    
  return host + "/" + info
}
