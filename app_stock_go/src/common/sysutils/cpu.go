﻿package sysutils
         
import (
  "strconv"
  "runtime" 
)  
   

func GetCpuInfo() (string) {
  //  通过Itoa方法转换  
  //  str1 := strconv.Itoa(i)
  
  // 通过Sprintf方法转换
  //str2 := fmt.Sprintf("%d", i)
  num := runtime.NumCPU()
  result := "num:" + strconv.Itoa(num)
  return result
}