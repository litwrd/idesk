﻿package datastore
         
import (  
    //"io"
    "bufio"
    //"io/ioutil"
    "fmt"
    "os"  
)

type DetailFileHeader struct {  
}

func LoadDetailDataFromFile(fileurl string) {
    // f, err := os.OpenFile("test.txt", os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModePerm|os.ModeTemporary)
    f, err := os.Open(fileurl)//打开文件
    defer f.Close() //打开文件出错处理
    if nil == err {
        buff := make([]byte, 1024)
        filereader := bufio.NewReader(f) //读入缓存
        rowindex := 0;
        for {
            readsize, _ := filereader.Read(buff)
            if 0 < readsize {
                rowindex++
                fmt.Printf("line%d %d\n", rowindex, readsize)
            } else {
                fmt.Printf("end!")
                break
            }
        }
    }
}  
