﻿package dataget

import (  
    "io"
    "bufio"
    "io/ioutil"
    "fmt"  
    "net/http"  
    "os"  
)  
  
var (  
    //url = "http://quotes.money.163.com/service/chddata.html?code=0600000"
    url = "http://quotes.money.163.com/service/chddata.html?code=0600000&start=20170101&end=20170108&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP"
)

// 需要导出的函数 第一个字母大写
func GetDayData_163_file() {
    resp, err := http.Get(url)  
    if err != nil {  
        panic(err)  
    }  
    f, err := os.Create("0600000.csv")  
    if err != nil {  
        panic(err)  
    }  
    io.Copy(f, resp.Body)   
    body, err := ioutil.ReadAll(resp.Body)   
    fmt.Println(string(body))
}

func GetDayData_163() {
    resp, err := http.Get(url)  
    if nil != err {  
        panic(err)  
    }
    defer resp.Body.Close()    
    fmt.Println("resp code", resp.StatusCode)

    // resp.body是io.ReadCloser接口.当你读完时，它就关闭了.如果想copy resp.body
    // 可以使用 httputil.DumpResponse(resp *http.Response, body bool) 这个函数

    //body, err := ioutil.ReadAll(resp.Body)
    //if nil != err {
        // handle error
    //}
    buff := bufio.NewReader(resp.Body)
    for {
      line, err := buff.ReadString('\n') //以'\n'为结束符读入一行
      if err != nil || io.EOF == err {
        break
      }                 
      fmt.Println("---------------------------\r\n")
      fmt.Print(line)  //可以对一行进行处理
    }
    //fmt.Println(string(body))
}

// 可以中断 Http 下载 ???
// http.DefaultTransport.(*http.Transport).CancelRequest(req)

func GetDayData_163_Step() {                         
    fmt.Print("GetDayData_163\r\n")
    req, _ := http.NewRequest("GET", url, nil)  
    req.Header.Set("Connection", "close")
//    req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
//    req.Header.Set("Cookie", "name=anny")
    resp, err := http.DefaultClient.Do(req)
    defer resp.Body.Close()
    if nil != err {  
        panic(err)  
    }
    fmt.Println("resp code", resp.StatusCode)
}
