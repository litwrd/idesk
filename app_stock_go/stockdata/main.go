﻿package main  

// main包和main函数比较特殊,是程序的入口，其他的包就要和文件夹名字一样
  
import (  
    //"io"
    //"net/http"
    //"os"  
    //"dataget"
    "common/sysutils"
    //"datastore"       
    "common/net"
)  
  
//var (
//    url = "http://quotes.money.163.com/service/chddata.html?code=0600000"
//)

func main() {
    //res, err := http.Get(url)
    //if err != nil {
    //    panic(err)
    //}
    //f, err := os.Create("0600000.csv")
    //if err != nil {
    //    panic(err)
    //}
    //io.Copy(f, res.Body)
    //dataget.GetDayData_163()
    //datastore.LoadDayData();
    //datastore.SaveDayData();
    //datastore.LoadDictionaryDataFromFile();
    println(sysutils.GetOSInfo());
    //net.OpenUdpServer("127.0.0.1", 7788);
    //net.OpenUdpServer("0.0.0.0", 7788);
    //net.OpenUdpClient("127.0.0.1", 7788);
    //net.OpenTcpServer(4040);
    net.OpenTcpClient("127.0.0.1", 4040);
}  