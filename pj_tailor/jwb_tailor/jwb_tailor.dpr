program jwb_tailor;

uses
  Forms,
  VirtualTrees in '..\..\..\devdcomps\virtualtree\VirtualTrees.pas',
  VirtualTree_Editor in '..\..\..\devdcomps\virtualtree\VirtualTree_Editor.pas',
  SQLite3 in '..\..\..\devdcomps\sqlite\SQLite3.pas',
  SQLite3Utils in '..\..\..\devdcomps\sqlite\SQLite3Utils.pas',
  SQLite3Wrap in '..\..\..\devdcomps\sqlite\SQLite3Wrap.pas',
  QuickSortList in '..\..\..\devwintech\comps\list\QuickSortList.pas',
  QuickList_Int in '..\..\..\devwintech\comps\list\QuickList_Int.pas',
  QuickList_double in '..\..\..\devwintech\comps\list\QuickList_double.pas',
  win.app in '..\..\..\devwintech\v0001\rec\win_app\win.app.pas',
  win.thread in '..\..\..\devwintech\v0001\rec\win_sys\win.thread.pas',
  windef_msg in '..\..\..\devwintech\v0001\windef\windef_msg.pas',
  Base.Run in '..\..\..\devwintech\v0001\rec\app_base\Base.Run.pas',
  Base.Thread in '..\..\..\devwintech\v0001\rec\app_base\Base.Thread.pas',
  BaseWinFormApp in '..\..\devwintech\v0001\win_app\BaseWinFormApp.pas',
  BaseWinApp in '..\..\devwintech\v0001\win_app\BaseWinApp.pas',
  BaseForm in '..\..\devwintech\v0001\win_uiform\BaseForm.pas' {frmBase},
  BaseApp in '..\..\devwintech\v0001\app_base\BaseApp.pas',
  BasePath in '..\..\devwintech\v0001\app_base\BasePath.pas',
  jwb_tailor_App in 'jwb_tailor_App.pas',
  jwb_tailor_mainform in 'jwb_tailor_mainform.pas' {frm_jwb_tailor_main},
  jwb_tailor_projectItem in 'jwb_tailor_projectItem.pas' {frm_jwb_tailor_projitem},
  jwb_tailor_editform in 'jwb_tailor_editform.pas' {frm_jwb_tailor_busi},
  jwb_tailor_custform in 'jwb_tailor_custform.pas' {frm_jwb_tailor_customer},
  define_dbfield in 'define_dbfield.pas',
  define_db_obj in 'define_db_obj.pas',
  jwb_tailor_ObjDB in 'jwb_tailor_ObjDB.pas',
  jwb_define_dataobject in 'jwb_define_dataobject.pas',
  jwb_tailor_AppUI in 'jwb_tailor_AppUI.pas',
  jwb_tailor_uibaseform in 'jwb_tailor_uibaseform.pas' {frmAppUIBase},
  jwb_tailor_DB in 'jwb_tailor_DB.pas';

{$R *.res}

begin
  RunApp(TApp_Jwb_Tailor, 'jwb', TBaseApp(GlobalApp));
end.
