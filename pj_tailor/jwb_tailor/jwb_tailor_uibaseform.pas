unit jwb_tailor_uibaseform;

interface

uses
  Forms,
  BaseForm;

type
  TfrmAppUIBase = class(TfrmBase)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }    
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}
            
uses
  jwb_tailor_App;
  
{ TfrmAppUIBase }

destructor TfrmAppUIBase.Destroy;
begin
  if nil <> GlobalApp then
  begin
    if nil <> GlobalApp.UI then
    begin
      GlobalApp.UI.FormOnDestroyNotify(self);
    end;
  end;
  inherited;
end;

procedure TfrmAppUIBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

end.
