unit jwb_tailor_sample;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  BaseForm, StdCtrls, ComCtrls, Buttons, ExtCtrls, VirtualTrees,
  BaseApp;

type       
  TFormData_Sample = record
  end;
  
  Tfrm_jwb_tailor_sample = class(TfrmBase)
  protected     
    fFormData: TFormData_Sample;     
  public         
    constructor Create(AOwner: TComponent); override; 
    procedure Initialize(App: TBaseApp); override;  
  end;

implementation

{$R *.dfm}

uses
  jwb_tailor_App;
  
constructor Tfrm_jwb_tailor_sample.Create(AOwner: TComponent);
begin
  inherited;
  FillChar(fFormData, SizeOf(fFormData), 0);
end;

procedure Tfrm_jwb_tailor_sample.Initialize(App: TBaseApp);
begin
  inherited;
end;

end.
