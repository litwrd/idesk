unit jwb_tailor_AppUI;

interface

uses
  Forms,
  BaseApp,
  jwb_tailor_uibaseform;
  
type
  TAppUIData = record
    MainForm: TfrmAppUIBase;
    CustomerForm: TfrmAppUIBase;
    ProjectForm: TfrmAppUIBase;
  end;
  
  TUIManager = class(TBaseAppObj)
  protected
    fAppUIData: TAppUIData;
  public               
    constructor Create(App: TBaseApp); override; 
    function Initialize: Boolean;
    procedure Finalize;
    procedure Run;

    procedure callCustomerForm;    
    procedure callProjectItemForm;
    procedure FormOnDestroyNotify(AForm: TfrmAppUIBase);
  end;
  
implementation
             
uses
  jwb_tailor_custform,
  jwb_tailor_projectItem,
  jwb_tailor_mainform;
               
{ TUIManager }
constructor TUIManager.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fAppUIData, SizeOf(fAppUIData), 0);
end;

function TUIManager.Initialize: Boolean;
begin
  Result := true;
end;

procedure TUIManager.Finalize;
begin

end;

procedure TUIManager.Run;
begin
  Application.CreateForm(Tfrm_jwb_tailor_main, fAppUIData.MainForm);
  fAppUIData.MainForm.Initialize(App);
  Application.Run;
end;
               
procedure TUIManager.FormOnDestroyNotify(AForm: TfrmAppUIBase);
begin
  if AForm = fAppUIData.CustomerForm then
    fAppUIData.CustomerForm := nil;
  if AForm = fAppUIData.ProjectForm then
    fAppUIData.ProjectForm := nil;   
  if AForm = fAppUIData.MainForm then
    fAppUIData.MainForm := nil;
end;

procedure TUIManager.callCustomerForm;
begin         
  fAppUIData.CustomerForm := Tfrm_jwb_tailor_customer.Create(Application);
  fAppUIData.CustomerForm.Show;
end;

procedure TUIManager.callProjectItemForm;
begin
  fAppUIData.ProjectForm := Tfrm_jwb_tailor_projitem.Create(Application);
  fAppUIData.ProjectForm.Show;
end;

end.
