unit jwb_tailor_custform;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  BaseForm, StdCtrls, ComCtrls, Buttons, ExtCtrls, VirtualTrees,
  BaseApp, jwb_define_dataobject, jwb_tailor_uibaseform;

type       
  TFormData_Edit = record
    Customer: PRT_Customer;
  end;
  
  Tfrm_jwb_tailor_customer = class(TfrmAppUIBase)
    pnl1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    edtUserName: TEdit;
    edtUserClass: TEdit;
    edtContact: TEdit;
    edtBalance: TEdit;
    mmoCustomer: TMemo;
    cmbPhone: TComboBoxEx;
    cmbAddress: TComboBoxEx;
    pnl2: TPanel;
    btnSave: TBitBtn;
    btnCancel: TBitBtn;
    vtCustomerList: TVirtualStringTree;
    btn3: TBitBtn;
    edt7: TEdit;
    btnNewCustomer: TBitBtn;
    procedure btnNewCustomerClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure vtCustomerListGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure edtUserNameChange(Sender: TObject);
    procedure vtCustomerListFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
  protected     
    fFormData: TFormData_Edit;      
    procedure LoadCustomerInfo;
    procedure SaveCustomerInfo;
    procedure BuildCustomerList;   
    procedure InitCustomerListView; 
  public         
    constructor Create(AOwner: TComponent); override; 
    procedure Initialize(App: TBaseApp); override;  
  end;

implementation

{$R *.dfm}

uses
  jwb_tailor_App;
  
{ Tfrm_jwb_tailor_edit }

type
  PVCustomerNode = ^TVCustomerNode;
  TVCustomerNode = record
    Customer: PRT_Customer;
    Caption: string;
  end;

constructor Tfrm_jwb_tailor_customer.Create(AOwner: TComponent);
begin
  inherited;
  FillChar(fFormData, SizeOf(fFormData), 0);   
  InitCustomerListView;
end;

procedure Tfrm_jwb_tailor_customer.Initialize(App: TBaseApp);
begin
  inherited;
  vtCustomerList.NodeDataSize := SizeOf(TVCustomerNode);
  vtCustomerList.OnGetText := vtCustomerListGetText;
  BuildCustomerList;
end;
                  
procedure Tfrm_jwb_tailor_customer.edtUserNameChange(Sender: TObject);
begin
  inherited;
  if nil <> fFormData.Customer then
  begin
    fFormData.Customer.Name := edtUserName.Text;
    vtCustomerList.Invalidate;
  end;
end;

procedure Tfrm_jwb_tailor_customer.vtCustomerListFocusChanged(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);   
var
  tmpVNode: PVCustomerNode;  
begin
  inherited;
  if nil <> Node then
  begin       
    tmpVNode := Sender.GetNodeData(Node);
    fFormData.Customer := tmpVNode.Customer;
    LoadCustomerInfo;
  end;
//
end;

procedure Tfrm_jwb_tailor_customer.vtCustomerListGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  tmpVNode: PVCustomerNode;  
begin
  inherited;
  CellText := '';
  tmpVNode := Sender.GetNodeData(Node);
  if nil <> tmpVNode then
  begin
    if nil <> tmpVNode.Customer then
    begin
      CellText := tmpVNode.Customer.Name;
    end;
  end;
end;
                                 
procedure Tfrm_jwb_tailor_customer.InitCustomerListView;
var
  tmpColumn: TVirtualTreeColumn;
begin
  vtCustomerList.NodeDataSize := SizeOf(TVCustomerNode);
  vtCustomerList.OnGetText := vtCustomerListGetText;
  vtCustomerList.Header.Options := vtCustomerList.Header.Options + [hoVisible];

  tmpColumn := vtCustomerList.Header.Columns.Add;
  tmpColumn.Text := '客户编号';
  tmpColumn.Width := 100;

  tmpColumn := vtCustomerList.Header.Columns.Add;
  tmpColumn.Text := '客户名称';
  tmpColumn.Width := 200;  
end;

procedure Tfrm_jwb_tailor_customer.BuildCustomerList;     
var
  tmpVNode: PVirtualNode;
  tmpVNodeData: PVCustomerNode;
  tmpCustomer: PRT_Customer;
  i: integer;
begin
  vtCustomerList.Clear;
  vtCustomerList.BeginUpdate;
  try
    for i := 0 to GlobalApp.DB.CustomerList.Count - 1 do
    begin
      tmpCustomer := PRT_Customer(GlobalApp.DB.CustomerList.Objects[i]);
      tmpVNode := vtCustomerList.AddChild(nil);
      tmpVNodeData := vtCustomerList.GetNodeData(tmpVNode);
      tmpVNodeData.Customer := tmpCustomer;
    end;
  finally
    vtCustomerList.EndUpdate;
  end;
end;

procedure Tfrm_jwb_tailor_customer.btnCancelClick(Sender: TObject);
begin
  inherited;
//
end;

procedure Tfrm_jwb_tailor_customer.btnNewCustomerClick(Sender: TObject);
begin
  inherited;
  fFormData.Customer := GlobalApp.DB.NewCustomer(0);
  LoadCustomerInfo;
  BuildCustomerList;
//
end;

procedure Tfrm_jwb_tailor_customer.btnSaveClick(Sender: TObject);
begin
  inherited;          
  SaveCustomerInfo;
  GlobalApp.DB.Post;
end;
        
procedure Tfrm_jwb_tailor_customer.LoadCustomerInfo;
begin
  if nil = fFormData.Customer then
  begin
  end else
  begin                                  
    edtUserName.Text := fFormData.Customer.Name;
    if nil <> fFormData.Customer.Info then
    begin
      edtUserClass.Text := fFormData.Customer.Info.ClassInfo;     
      edtContact.Text := fFormData.Customer.Info.Contact;
      cmbPhone.Items.Text := fFormData.Customer.Info.PhoneNum;
      if 0 < cmbPhone.Items.Count then
        cmbPhone.Text := cmbPhone.Items[0];
      edtBalance.Text := FormatFloat('0.00', fFormData.Customer.Info.Balance100/ 100);
      cmbAddress.Items.Text := fFormData.Customer.Info.Address;
      if 0 < cmbAddress.Items.Count then
        cmbAddress.Text := cmbAddress.Items[0];
      mmoCustomer.Lines.Text := fFormData.Customer.Info.Memo;
    end;
  end;
end;

procedure Tfrm_jwb_tailor_customer.SaveCustomerInfo;
var
  tmpStrs: TStringList;
begin
  if nil = fFormData.Customer then
  begin
  end else
  begin                                   
    fFormData.Customer.Name := edtUserName.Text;
    if nil = fFormData.Customer.Info then
      fFormData.Customer.Info := CheckOutCustomerInfo(fFormData.Customer);
    if nil <> fFormData.Customer.Info then
    begin              
      tmpStrs := TStringList.Create;
      try
        tmpStrs.Duplicates := dupIgnore;
        fFormData.Customer.Info.ClassInfo := edtUserClass.Text;     
        fFormData.Customer.Info.Contact := edtContact.Text;

        tmpStrs.Clear;
        if 0 < cmbPhone.Items.Count then
          tmpStrs.AddStrings(cmbPhone.Items);
        if '' <> Trim(cmbPhone.Text) then
          tmpStrs.Add(cmbPhone.Text);
        fFormData.Customer.Info.PhoneNum := tmpStrs.Text;
        
        fFormData.Customer.Info.Balance100 := Trunc(StrToFloatDef(edtBalance.Text, 0) * 100);

        tmpStrs.Clear;
        if 0 < cmbAddress.Items.Count then
          tmpStrs.AddStrings(cmbAddress.Items);
        if '' <> Trim(cmbAddress.Text) then
          tmpStrs.Add(cmbAddress.Text);
        fFormData.Customer.Info.Address := tmpStrs.Text;
        fFormData.Customer.Info.Memo := mmoCustomer.Lines.Text;
      finally
        tmpStrs.Clear;
        tmpStrs.Free;
      end;
    end;
  end;
end;

end.
