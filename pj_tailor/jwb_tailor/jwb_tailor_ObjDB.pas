unit jwb_tailor_ObjDB;

interface

uses
  SysUtils,
  BaseApp,
  SQLite3,
  SQLite3Wrap,
  QuickSortList,
  QuickList_Int;
  
type
  TAppObjDBData = record
    SqlDB: TSQLite3Database;
  end;
  
  TAppObjDB = class(TBaseAppObj)
  protected
    fDBData: TAppObjDBData;
  public            
    constructor Create(App: TBaseApp); override; 
    destructor Destroy; override;
    function Initialize: Boolean;

    procedure Post;
  end;
  
implementation

uses
  define_dbfield, define_db_obj;
  
{ TAppObjDB }

constructor TAppObjDB.Create(App: TBaseApp);
begin
  inherited;
  FillChar(fDBData, SizeOf(fDBData), 0);  
end;

destructor TAppObjDB.Destroy;
begin
  Post;
  inherited;
end;

function TAppObjDB.Initialize: Boolean;
var    
  tmpStr: string;
  tmpSelect: TSQLite3Statement;
  tmpRet: integer;
  tmpKeyId: integer;
begin
  if nil = fDBData.SqlDB then
  begin
    fDBData.SqlDB := TSQLite3Database.Create;
  end;      
  tmpStr := 'objmodeldb.db3'; //ChangeFileExt(ParamStr(0), '.db');
  fDBData.SqlDB.Open(tmpStr,
      SQLite3.SQLITE_OPEN_CREATE or
      SQLite3.SQLITE_OPEN_READWRITE or
      SQLite3.SQLITE_OPEN_NOMUTEX);

  fDBData.SqlDB.Execute(CreateTableSQL_Model);
  fDBData.SqlDB.Execute(CreateTableSQL_ModelObject);
  fDBData.SqlDB.Execute(CreateTableSQL_ModelElement);
  fDBData.SqlDB.Execute(CreateTableSQL_Object);
  fDBData.SqlDB.Execute(CreateTableSQL_Element);
  
  tmpStr := ' select ' +
              Field_AutoKeyID + ',' +
              Field_Name +
            ' from ' + Table_Object;
  tmpSelect := fDBData.SqlDB.Prepare(tmpStr);
  if nil <> tmpSelect then
  begin
    tmpRet := tmpSelect.Step;
    while SQLITE_ROW = tmpRet do
    begin
      tmpKeyId := tmpSelect.ColumnInt(0);
      tmpRet := tmpSelect.Step;
    end;
  end;
  
  Result := true;
end;

procedure TAppObjDB.Post;
var
  i: integer;
  tmpRet: integer;
  tmpInsert: TSQLite3Statement;
  tmpUpdate: TSQLite3Statement;
  tmpStr: string;
begin
  fDBData.SqlDB.BeginTransaction;
  try
    fDBData.SqlDB.Commit;
  except
    fDBData.SqlDB.Rollback;
  end;
end;

end.
