unit jwb_tailor_mainform;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, Buttons, BaseForm, BaseApp, jwb_tailor_uibaseform, Tabs;

type
  TFormData_Main = record
    BusinessForm: TfrmAppUIBase;
    CustomerForm: TfrmAppUIBase;
    ProjectItemDicForm: TfrmAppUIBase;
    ActiveForm: TfrmAppUIBase;
  end;
  
  Tfrm_jwb_tailor_main = class(TfrmAppUIBase)
    TabSet1: TTabSet;
    procedure TabSet1Change(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
  protected
    fFormData: TFormData_Main;
  public       
    constructor Create(AOwner: TComponent); override;    
    procedure Initialize(App: TBaseApp); override;  
  end;

implementation

{$R *.dfm}

uses
  jwb_tailor_custform,
  jwb_tailor_projectItem,
  jwb_tailor_editform;
  
{ Tfrm_jwb_tailor_main }

constructor Tfrm_jwb_tailor_main.Create(AOwner: TComponent);
begin
  inherited;
  FillChar(fFormData, SizeOf(fFormData), 0);
end;

procedure Tfrm_jwb_tailor_main.Initialize(App: TBaseApp);
begin
  inherited;
  TabSet1.Tabs.Clear;
  if nil = fFormData.BusinessForm then
  begin
    fFormData.BusinessForm := Tfrm_jwb_tailor_busi.Create(Self);
    fFormData.BusinessForm.Initialize(App);
  end;    
  TabSet1.Tabs.AddObject('客户项目', fFormData.BusinessForm);
  
  if nil = fFormData.CustomerForm then
  begin
    fFormData.CustomerForm := Tfrm_jwb_tailor_customer.Create(Self);
    fFormData.CustomerForm.Initialize(App);
  end;    
  TabSet1.Tabs.AddObject('客户管理', fFormData.CustomerForm);
  
  if nil = fFormData.ProjectItemDicForm then
  begin
    fFormData.ProjectItemDicForm := Tfrm_jwb_tailor_projitem.Create(Self);
    fFormData.ProjectItemDicForm.Initialize(App);
  end;    
  TabSet1.Tabs.AddObject('项目货品', fFormData.ProjectItemDicForm);
  TabSet1.TabIndex := 0;
end;

procedure Tfrm_jwb_tailor_main.TabSet1Change(Sender: TObject; NewTab: Integer; var AllowChange: Boolean);
begin
  inherited;
  if nil <> fFormData.ActiveForm then
  begin
    fFormData.ActiveForm.Hide;
  end;
  fFormData.ActiveForm := TfrmAppUIBase(TabSet1.Tabs.Objects[NewTab]); 
  fFormData.ActiveForm.Parent := Self;
  fFormData.ActiveForm.BorderStyle := bsNone;
  fFormData.ActiveForm.Align := alClient;
  fFormData.ActiveForm.Visible := True;
end;

end.
