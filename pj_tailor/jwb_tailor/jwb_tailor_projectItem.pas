unit jwb_tailor_projectItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  jwb_tailor_uibaseform, StdCtrls, ComCtrls, Buttons, ExtCtrls, VirtualTrees,
  BaseApp, jwb_define_dataobject, BaseForm;

type       
  TFormData_ProjItem = record
    ProjItem: PRT_ProjectItem;
  end;
  
  Tfrm_jwb_tailor_projitem = class(TfrmAppUIBase)
    pnl2: TPanel;
    vtProjectItemList: TVirtualStringTree;
    btn3: TBitBtn;
    edt7: TEdit;
    pnl1: TPanel;
    Label1: TLabel;
    Label13: TLabel;
    edtProjItemName: TEdit;
    btnSave: TBitBtn;
    btnCancel: TBitBtn;
    btnNewProject: TBitBtn;
    edt9: TEdit;
    procedure btnSaveClick(Sender: TObject);
    procedure vtProjectItemListGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vtProjectItemListFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure btnNewProjectClick(Sender: TObject);
  protected     
    fFormData: TFormData_ProjItem;      
    procedure LoadProjItemInfo;
    procedure SaveProjItemInfo;
    procedure BuildProjItemList;    
  public         
    constructor Create(AOwner: TComponent); override; 
    procedure Initialize(App: TBaseApp); override;  
  end;

implementation

{$R *.dfm}

uses
  jwb_tailor_App;
  
{ Tfrm_jwb_tailor_edit }

type
  PVProjItemNode = ^TVProjItemNode;
  TVProjItemNode = record
    ProjItem: PRT_ProjectItem;
    Caption: string;
  end;

constructor Tfrm_jwb_tailor_projitem.Create(AOwner: TComponent);
begin
  inherited;
  FillChar(fFormData, SizeOf(fFormData), 0);
end;

procedure Tfrm_jwb_tailor_projitem.Initialize(App: TBaseApp);
begin
  inherited;
  btnSave.OnClick := btnSaveClick;
  btnNewProject.OnClick := btnNewProjectClick;
  
  vtProjectItemList.NodeDataSize := SizeOf(TVProjItemNode);
  vtProjectItemList.OnGetText := vtProjectItemListGetText;
  BuildProjItemList;
end;
                  
procedure Tfrm_jwb_tailor_projitem.vtProjectItemListFocusChanged(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);   
var
  tmpVNode: PVProjItemNode;  
begin
  inherited;
  if nil <> Node then
  begin       
    tmpVNode := Sender.GetNodeData(Node);
    fFormData.ProjItem := tmpVNode.ProjItem;
    LoadProjItemInfo;
  end;
//
end;

procedure Tfrm_jwb_tailor_projitem.vtProjectItemListGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  tmpVNode: PVProjItemNode;  
begin
  inherited;
  CellText := '';
  tmpVNode := Sender.GetNodeData(Node);
  if nil <> tmpVNode then
  begin
    if nil <> tmpVNode.ProjItem then
    begin
      CellText := tmpVNode.ProjItem.Name;
    end;
  end;
end;

procedure Tfrm_jwb_tailor_projitem.BuildProjItemList;
var
  tmpVNode: PVirtualNode;
  tmpVNodeData: PVProjItemNode;
  tmpProjItem: PRT_ProjectItem;
  i: integer;
begin
  vtProjectItemList.Clear;
  vtProjectItemList.BeginUpdate;
  try
    for i := 0 to GlobalApp.DB.ProjItemList.Count - 1 do
    begin
      tmpProjItem := PRT_ProjectItem(GlobalApp.DB.ProjItemList.Objects[i]);
      tmpVNode := vtProjectItemList.AddChild(nil);
      tmpVNodeData := vtProjectItemList.GetNodeData(tmpVNode);
      tmpVNodeData.ProjItem := tmpProjItem;
    end;
  finally
    vtProjectItemList.EndUpdate;
  end;
end;

procedure Tfrm_jwb_tailor_projitem.btnNewProjectClick(Sender: TObject);
begin
  inherited;
  fFormData.ProjItem := GlobalApp.DB.NewProjItem(0);
  LoadProjItemInfo;
  BuildProjItemList;
end;

procedure Tfrm_jwb_tailor_projitem.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveProjItemInfo;
  GlobalApp.DB.Post;
end;
        
procedure Tfrm_jwb_tailor_projitem.LoadProjItemInfo;
begin     
  if nil = fFormData.ProjItem then
  begin
  end else
  begin
    edtProjItemName.Text := fFormData.ProjItem.Name;
  end;
end;

procedure Tfrm_jwb_tailor_projitem.SaveProjItemInfo;
begin      
  if nil = fFormData.ProjItem then
  begin
  end else
  begin
    fFormData.ProjItem.Name := edtProjItemName.Text;
  end;
end;

end.
