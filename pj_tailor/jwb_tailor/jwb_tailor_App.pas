unit jwb_tailor_App;

interface

uses
  Forms, Sysutils,
  BaseForm,
  BaseWinFormApp,
  jwb_tailor_AppUI,
  jwb_tailor_ObjDB,
  jwb_tailor_DB;

type            
  TAppData = record
    UI: TUIManager;
    DB: TAppDB;
    ObjDB: TAppObjDB;
  end;
  
  TApp_Jwb_Tailor = class(TBaseWinFormApp)
  protected
    fAppData: TAppData;
  public                  
    constructor Create(AppClassId: AnsiString); override;
    destructor Destroy; override;    
    function Initialize: Boolean; override;   
    procedure Finalize; override;
    procedure Run; override;      
    property DB: TAppDB read fAppData.DB;
    property UI: TUIManager read fAppData.UI;
  end;
                   
var
  GlobalApp: TApp_Jwb_Tailor = nil;
  
implementation

constructor TApp_Jwb_Tailor.Create(AppClassId: AnsiString);
begin
  inherited;
  FillChar(fAppData, SizeOf(fAppData), 0);  
end;

destructor TApp_Jwb_Tailor.Destroy;
begin
  Finalize;
  GlobalApp := nil;
  inherited;
end;
          
function TApp_Jwb_Tailor.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin
    if nil = fAppData.DB then
    begin
      fAppData.DB := TAppDB.Create(Self);
      fAppData.DB.Initialize;
    end;
    if nil = fAppData.ObjDB then
    begin
      fAppData.ObjDB := TAppObjDB.Create(Self);    
      fAppData.ObjDB.Initialize;
    end;
    if nil = fAppData.UI then
    begin
      fAppData.UI := TUIManager.Create(Self);  
      fAppData.UI.Initialize;
    end;
  end;
end;
      

procedure TApp_Jwb_Tailor.Finalize;
begin
  if nil <> fAppData.DB then
  begin
    fAppData.DB.Post;
    FreeAndNil(fAppData.DB);
  end;        
  if nil <> fAppData.ObjDB then
  begin
    fAppData.ObjDB.Post;
    FreeAndNil(fAppData.ObjDB);    
  end;      
  if nil <> fAppData.UI then
  begin
    fAppData.UI.Finalize;
    FreeAndNil(fAppData.UI);    
  end;
end;

procedure TApp_Jwb_Tailor.Run;
begin
  inherited;
  fAppData.UI.Run;
end;

end.
