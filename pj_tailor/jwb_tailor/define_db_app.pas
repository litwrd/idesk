unit define_db_app;

interface

uses
  define_dbfield;

const
  Table_Customer       = 'cust';    
  Table_Customer_Class = 'cust_c';
  Table_Customer_Info  = 'cust_i';

  Table_Project        = 'proj';     
  Table_Project_Item   = 'proj_l'; // child

  Field_Customer_No      = 'c_no';   // 编号
  Field_Customer_Contact = 'c_cont'; // 联系人
  Field_Customer_Phone   = 'c_phon'; // 电话    
  Field_Customer_Address = 'c_addr'; // 地址
  Field_Customer_Balance = 'c_blac'; // 余额

implementation

end.
