unit jwb_tailor_editform;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  BaseForm, StdCtrls, ComCtrls, Buttons, ExtCtrls, VirtualTrees,
  jwb_tailor_uibaseform,
  BaseApp, jwb_define_dataobject;

type       
  TFormData_Edit = record
    Customer: PRT_Customer;
  end;
  
  Tfrm_jwb_tailor_busi = class(TfrmAppUIBase)
    pnl1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    btnExCustomer: TSpeedButton;
    btnExCustomClass: TSpeedButton;
    edtUserName: TEdit;
    edt1: TEdit;
    edt2: TEdit;
    dtp1: TDateTimePicker;
    edt3: TEdit;
    edt4: TEdit;
    edt5: TEdit;
    edt6: TEdit;
    mmo1: TMemo;
    mmo2: TMemo;
    cmbPhone: TComboBoxEx;
    cmb2: TComboBoxEx;
    pnl2: TPanel;
    btnSave: TBitBtn;
    btnCancel: TBitBtn;
    vtCustomerList: TVirtualStringTree;
    btnFindCustomer: TBitBtn;
    edt7: TEdit;
    btnNewProject: TBitBtn;
    btn1: TSpeedButton;
    pnl3: TPanel;
    vtProjectItemDic: TVirtualStringTree;
    btnFindProjectItem: TBitBtn;
    edt8: TEdit;
    Label13: TLabel;
    edt9: TEdit;
    btnCustomer: TBitBtn;
    btnProjectItem: TBitBtn;
    btnPrint: TBitBtn;
    procedure btnNewProjectClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure vtCustomerListGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
      
    procedure vtProjectItemDicGetText(
      Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType; var CellText: WideString);
    procedure edtUserNameChange(Sender: TObject);
    procedure vtCustomerListFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure btnCustomerClick(Sender: TObject);
    procedure btnProjectItemClick(Sender: TObject);
  protected     
    fFormData: TFormData_Edit;      
    procedure LoadCustomerInfo;
    procedure SaveCustomerInfo;
    procedure BuildCustomerList;
    procedure InitCustomerListView;
    procedure BuildProjectItemDicList;
    procedure InitProjectItemDicListView;    
  public         
    constructor Create(AOwner: TComponent); override; 
    procedure Initialize(App: TBaseApp); override;  
  end;

implementation

{$R *.dfm}

uses
  jwb_tailor_App;
  
{ Tfrm_jwb_tailor_edit }

type
  PVCustomerNode = ^TVCustomerNode;
  TVCustomerNode = record
    Customer: PRT_Customer;
    Caption: string;
  end;

  PVProjectItemDicNode = ^TVProjectItemDicNode;
  TVProjectItemDicNode = record  
    ProjectDicItem: PRT_ProjectItem;
    Caption: string;
  end;

constructor Tfrm_jwb_tailor_busi.Create(AOwner: TComponent);
begin
  inherited;
  FillChar(fFormData, SizeOf(fFormData), 0);
  InitCustomerListView;
  InitProjectItemDicListView;
end;

procedure Tfrm_jwb_tailor_busi.Initialize(App: TBaseApp);
begin
  inherited;
  BuildCustomerList;   
  BuildProjectItemDicList;
end;
                  
procedure Tfrm_jwb_tailor_busi.edtUserNameChange(Sender: TObject);
begin
  inherited;
  if nil <> fFormData.Customer then
  begin
    fFormData.Customer.Name := edtUserName.Text;
    vtCustomerList.Invalidate;
  end;
end;

procedure Tfrm_jwb_tailor_busi.vtCustomerListFocusChanged(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);   
var
  tmpVNode: PVCustomerNode;  
begin
  inherited;
  if nil <> Node then
  begin       
    tmpVNode := Sender.GetNodeData(Node);
    fFormData.Customer := tmpVNode.Customer;
    LoadCustomerInfo;
  end;
//
end;

procedure Tfrm_jwb_tailor_busi.vtCustomerListGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  tmpVNode: PVCustomerNode;  
begin
  inherited;
  CellText := '';
  tmpVNode := Sender.GetNodeData(Node);
  if nil <> tmpVNode then
  begin
    if nil <> tmpVNode.Customer then
    begin
      CellText := tmpVNode.Customer.Name;
    end;
  end;
end;
                
procedure Tfrm_jwb_tailor_busi.vtProjectItemDicGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  tmpVNode: PVProjectItemDicNode;  
begin
  inherited;
  CellText := '';
  tmpVNode := Sender.GetNodeData(Node);
  if nil <> tmpVNode then
  begin
    if nil <> tmpVNode.ProjectDicItem then
    begin
      CellText := tmpVNode.ProjectDicItem.Name;
    end;
  end;
end;  

procedure Tfrm_jwb_tailor_busi.InitCustomerListView;
var
  tmpColumn: TVirtualTreeColumn;
begin
  vtCustomerList.NodeDataSize := SizeOf(TVCustomerNode);
  vtCustomerList.OnGetText := vtCustomerListGetText;
  vtCustomerList.Header.Options := vtCustomerList.Header.Options + [hoVisible];

  tmpColumn := vtCustomerList.Header.Columns.Add;
  tmpColumn.Text := '客户编号';
  tmpColumn.Width := 100;

  tmpColumn := vtCustomerList.Header.Columns.Add;
  tmpColumn.Text := '客户名称';
  tmpColumn.Width := 200;  
end;

procedure Tfrm_jwb_tailor_busi.BuildCustomerList;
var
  tmpVNode: PVirtualNode;
  tmpVNodeData: PVCustomerNode;
  tmpCustomer: PRT_Customer;
  i: integer;
begin
  vtCustomerList.Clear;
  vtCustomerList.BeginUpdate;
  try
    for i := 0 to GlobalApp.DB.CustomerList.Count - 1 do
    begin
      tmpCustomer := PRT_Customer(GlobalApp.DB.CustomerList.Objects[i]);
      tmpVNode := vtCustomerList.AddChild(nil);
      tmpVNodeData := vtCustomerList.GetNodeData(tmpVNode);
      tmpVNodeData.Customer := tmpCustomer;
    end;
  finally
    vtCustomerList.EndUpdate;
  end;
end;

procedure Tfrm_jwb_tailor_busi.InitProjectItemDicListView;
var
  tmpColumn: TVirtualTreeColumn;
begin
  vtProjectItemDic.NodeDataSize := SizeOf(TVCustomerNode);
  vtProjectItemDic.OnGetText := vtProjectItemDicGetText;
  vtProjectItemDic.Header.Options := vtProjectItemDic.Header.Options + [hoVisible];

  tmpColumn := vtProjectItemDic.Header.Columns.Add;
  tmpColumn.Text := '货品编号';
  tmpColumn.Width := 60;

  tmpColumn := vtProjectItemDic.Header.Columns.Add;
  tmpColumn.Text := '货品名称';
  tmpColumn.Width := 100;

  tmpColumn := vtProjectItemDic.Header.Columns.Add;
  tmpColumn.Text := '单价';
  tmpColumn.Width := 60;

  tmpColumn := vtProjectItemDic.Header.Columns.Add;
  tmpColumn.Text := '单位';
  tmpColumn.Width := 60;
end;

procedure Tfrm_jwb_tailor_busi.BuildProjectItemDicList;
var
  tmpVNode: PVirtualNode;
  tmpVNodeData: PVCustomerNode;
  tmpCustomer: PRT_Customer;
  i: integer;
begin
  vtCustomerList.Clear;
  vtCustomerList.BeginUpdate;
  try
    for i := 0 to GlobalApp.DB.CustomerList.Count - 1 do
    begin
      tmpCustomer := PRT_Customer(GlobalApp.DB.CustomerList.Objects[i]);
      tmpVNode := vtCustomerList.AddChild(nil);
      tmpVNodeData := vtCustomerList.GetNodeData(tmpVNode);
      tmpVNodeData.Customer := tmpCustomer;
    end;
  finally
    vtCustomerList.EndUpdate;
  end;
end;

procedure Tfrm_jwb_tailor_busi.btnCustomerClick(Sender: TObject);
begin
  inherited;
  GlobalApp.UI.callCustomerForm;
//
end;

procedure Tfrm_jwb_tailor_busi.btnProjectItemClick(Sender: TObject);
begin
  inherited;
  GlobalApp.UI.callProjectItemForm;
//
end;

procedure Tfrm_jwb_tailor_busi.btnCancelClick(Sender: TObject);
begin
  inherited;
//
end;

procedure Tfrm_jwb_tailor_busi.btnNewProjectClick(Sender: TObject);
begin
  inherited;
  fFormData.Customer := GlobalApp.DB.NewCustomer(0);
  LoadCustomerInfo;
  BuildCustomerList;
//
end;

procedure Tfrm_jwb_tailor_busi.btnSaveClick(Sender: TObject);
begin
  inherited;          
  SaveCustomerInfo;
  GlobalApp.DB.Post;
end;
        
procedure Tfrm_jwb_tailor_busi.LoadCustomerInfo;
begin
  if nil = fFormData.Customer then
  begin
  end else
  begin                                  
    edtUserName.Text := fFormData.Customer.Name;
    if nil <> fFormData.Customer.Info then
    begin
    end;
  end;
end;

procedure Tfrm_jwb_tailor_busi.SaveCustomerInfo;
begin
  if nil = fFormData.Customer then
  begin
  end else
  begin                                   
    fFormData.Customer.Name := edtUserName.Text;
    if nil <> fFormData.Customer.Info then
    begin
    end;
  end;
end;

end.
