object frm_jwb_tailor_busi: Tfrm_jwb_tailor_busi
  Left = 256
  Top = 174
  Caption = #23458#25143#39033#30446
  ClientHeight = 573
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 8
    Top = 4
    Width = 420
    Height = 560
    Anchors = [akLeft, akTop, akBottom]
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 63
      Width = 48
      Height = 13
      Caption = #23458#25143#21517#31216
    end
    object Label2: TLabel
      Left = 24
      Top = 93
      Width = 48
      Height = 13
      Caption = #23458#25143#20998#31867
    end
    object Label3: TLabel
      Left = 24
      Top = 138
      Width = 48
      Height = 13
      Caption = #39033'        '#30446
    end
    object Label4: TLabel
      Left = 225
      Top = 193
      Width = 48
      Height = 13
      Caption = #21046#21333#26102#38388
    end
    object Label5: TLabel
      Left = 25
      Top = 166
      Width = 48
      Height = 13
      Caption = #32852'  '#31995'  '#20154
    end
    object Label6: TLabel
      Left = 226
      Top = 165
      Width = 48
      Height = 13
      Caption = #21046#20316#21333#21495
    end
    object Label7: TLabel
      Left = 12
      Top = 223
      Width = 60
      Height = 13
      Caption = #20250#21592#21345#20313#39069
    end
    object Label8: TLabel
      Left = 224
      Top = 223
      Width = 48
      Height = 13
      Caption = #39044'  '#20184'  '#27454
    end
    object Label9: TLabel
      Left = 24
      Top = 193
      Width = 48
      Height = 13
      Caption = #32852#31995#30005#35805
    end
    object Label10: TLabel
      Left = 24
      Top = 256
      Width = 48
      Height = 13
      Caption = #36865#36135#22320#22336
    end
    object Label11: TLabel
      Left = 24
      Top = 289
      Width = 48
      Height = 13
      Caption = #21046#20316#35201#27714
    end
    object Label12: TLabel
      Left = 24
      Top = 404
      Width = 48
      Height = 13
      Caption = #23458#25143#22791#27880
    end
    object btnExCustomer: TSpeedButton
      Left = 323
      Top = 60
      Width = 23
      Height = 22
    end
    object btnExCustomClass: TSpeedButton
      Left = 323
      Top = 91
      Width = 23
      Height = 22
    end
    object btn1: TSpeedButton
      Left = 375
      Top = 135
      Width = 23
      Height = 22
    end
    object Label13: TLabel
      Left = 24
      Top = 33
      Width = 48
      Height = 13
      Caption = #23458#25143#32534#21495
    end
    object edtUserName: TEdit
      Left = 78
      Top = 60
      Width = 239
      Height = 21
      OnChange = edtUserNameChange
    end
    object edt1: TEdit
      Left = 78
      Top = 91
      Width = 239
      Height = 21
    end
    object edt2: TEdit
      Left = 78
      Top = 135
      Width = 291
      Height = 21
    end
    object dtp1: TDateTimePicker
      Left = 278
      Top = 190
      Width = 120
      Height = 22
      Date = 42788.470787974540000000
      Time = 42788.470787974540000000
    end
    object edt3: TEdit
      Left = 78
      Top = 163
      Width = 121
      Height = 21
    end
    object edt4: TEdit
      Left = 278
      Top = 162
      Width = 121
      Height = 21
    end
    object edt5: TEdit
      Left = 78
      Top = 220
      Width = 121
      Height = 21
    end
    object edt6: TEdit
      Left = 277
      Top = 220
      Width = 121
      Height = 21
    end
    object mmo1: TMemo
      Left = 78
      Top = 286
      Width = 320
      Height = 100
      ScrollBars = ssVertical
    end
    object mmo2: TMemo
      Left = 78
      Top = 401
      Width = 320
      Height = 100
      ScrollBars = ssVertical
    end
    object cmbPhone: TComboBoxEx
      Left = 78
      Top = 190
      Width = 121
      Height = 22
      ItemsEx = <>
      ItemHeight = 16
    end
    object cmb2: TComboBoxEx
      Left = 78
      Top = 253
      Width = 320
      Height = 22
      ItemsEx = <>
      ItemHeight = 16
    end
    object btnSave: TBitBtn
      Left = 323
      Top = 520
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = #20445#23384
      Default = True
      TabOrder = 12
      OnClick = btnSaveClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object btnCancel: TBitBtn
      Left = 242
      Top = 520
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Cancel = True
      Caption = #21462#28040
      OnClick = btnCancelClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object btnNewProject: TBitBtn
      Left = 78
      Top = 520
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = #26032#24314
      OnClick = btnNewProjectClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00344446333334
        44433333FFFF333333FFFF33000033AAA43333332A4333338833F33333883F33
        00003332A46333332A4333333383F33333383F3300003332A2433336A6633333
        33833F333383F33300003333AA463362A433333333383F333833F33300003333
        6AA4462A46333333333833FF833F33330000333332AA22246333333333338333
        33F3333300003333336AAA22646333333333383333F8FF33000033444466AA43
        6A43333338FFF8833F383F330000336AA246A2436A43333338833F833F383F33
        000033336A24AA442A433333333833F33FF83F330000333333A2AA2AA4333333
        333383333333F3330000333333322AAA4333333333333833333F333300003333
        333322A4333333333333338333F333330000333333344A433333333333333338
        3F333333000033333336A24333333333333333833F333333000033333336AA43
        33333333333333833F3333330000333333336663333333333333333888333333
        0000}
      NumGlyphs = 2
    end
    object edt9: TEdit
      Left = 78
      Top = 30
      Width = 239
      Height = 21
      OnChange = edtUserNameChange
    end
    object btnPrint: TBitBtn
      Left = 159
      Top = 520
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = #25171#21360
      OnClick = btnCancelClick
      Kind = bkRetry
    end
  end
  object pnl2: TPanel
    Left = 432
    Top = 4
    Width = 334
    Height = 247
    Anchors = [akLeft, akTop, akRight]
    object vtCustomerList: TVirtualStringTree
      Left = 8
      Top = 41
      Width = 318
      Height = 199
      Anchors = [akLeft, akTop, akRight, akBottom]
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      Header.Options = [hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible]
      OnFocusChanged = vtCustomerListFocusChanged
      OnGetText = vtCustomerListGetText
      Columns = <>
    end
    object btnFindCustomer: TBitBtn
      Left = 166
      Top = 10
      Width = 75
      Height = 25
      Caption = #26597#25214
      Kind = bkHelp
    end
    object edt7: TEdit
      Left = 8
      Top = 12
      Width = 152
      Height = 21
    end
    object btnCustomer: TBitBtn
      Left = 245
      Top = 10
      Width = 65
      Height = 25
      Caption = #23458#25143
      OnClick = btnCustomerClick
      Kind = bkHelp
    end
  end
  object pnl3: TPanel
    Left = 432
    Top = 259
    Width = 334
    Height = 305
    Anchors = [akLeft, akTop, akRight, akBottom]
    object vtProjectItemDic: TVirtualStringTree
      Left = 8
      Top = 41
      Width = 318
      Height = 257
      Anchors = [akLeft, akTop, akRight, akBottom]
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      OnFocusChanged = vtCustomerListFocusChanged
      OnGetText = vtCustomerListGetText
      Columns = <>
    end
    object btnFindProjectItem: TBitBtn
      Left = 166
      Top = 10
      Width = 75
      Height = 25
      Caption = #26597#25214
      Kind = bkHelp
    end
    object edt8: TEdit
      Left = 8
      Top = 12
      Width = 152
      Height = 21
    end
    object btnProjectItem: TBitBtn
      Left = 245
      Top = 10
      Width = 65
      Height = 25
      Caption = #39033#30446
      OnClick = btnProjectItemClick
      Kind = bkHelp
    end
  end
end
