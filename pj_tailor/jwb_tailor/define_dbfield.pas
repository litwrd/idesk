unit define_dbfield;

interface
               
const
  // b g h j k q y z
  Field_AutoKeyID         = 'a';
  Field_CreateTime        = 'c'; // CRUD time create
  Field_EndTime           = 'd'; // CRUD time end
  Field_ValidStatus       = 'e'; // end
  Field_Status            = 'f'; // Field_StatusType='';  xxx:123;yyy:234
  Field_KeyID             = 'i'; // id = index
  Field_Child             = 'l'; // parent - child ( sub )
  Field_Memo              = 'm';
  Field_Name              = 'n'; // name
  Field_Object            = 'o';
  Field_Parent            = 'p'; // parent - child 
  Field_Relation          = 'r'; // link relation
  Field_DataSrc           = 's'; // src
  Field_Type              = 't'; // type
  Field_UpdateTime        = 'u'; // CRUD time update
  Field_Value             = 'v';
  Field_Owner             = 'w';
  Field_Extend            = 'x';

  FieldType_AutoIncKey  = #32 + 'INTEGER PRIMARY KEY AUTOINCREMENT' + #32;
  FieldType_IntKey      = #32 + 'INTEGER PRIMARY KEY' + #32;
  FieldType_StringKey   = #32 + 'TEXT PRIMARY KEY' + #32;  
  FieldType_String      = #32 + 'TEXT NOT NULL' + #32;
  FieldType_String_Def  = #32 + 'TEXT NOT NULL DEFAULT ''''' + #32;  
  FieldType_Double      = #32 + 'REAL NOT NULL' + #32;
  FieldType_Double_Def  = #32 + 'REAL NOT NULL DEFAULT 0' + #32;
  FieldType_Int         = #32 + 'INT NOT NULL' + #32;
  FieldType_Int_Def     = #32 + 'INT NOT NULL DEFAULT 0' + #32;

  SQL_CreateTable_Header = 'Create Table if not exists' + #32;
     
implementation

end.
