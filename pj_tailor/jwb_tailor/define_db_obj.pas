unit define_db_obj;

interface

const
  Table_Model          = 'm'; // model
  Table_ModelObject    = 'c'; // model - object define 
  Table_ModelElement   = 'e'; // model - object - element define
  
  Table_Object         = 'o';       
  Table_Object_prefix  = 'o'; // table prefix + object class define id    
  Table_Element        = 'n';
  Table_Element_prefix = 'n'; // table prefix + object class define id

  function CreateTableSQL_Model: string;
  function CreateTableSQL_ModelObject: string;
  function CreateTableSQL_ModelElement: string;
  
  function CreateTableSQL_Object: string;
  function CreateTableSQL_Element: string;
  
implementation

uses
  define_dbfield;
                  
function CreateTableSQL_Model: string;
begin
  Result := SQL_CreateTable_Header + Table_Model + '(' +   
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +     
    Field_KeyID       + FieldType_Int_Def + ',' +   
    Field_ValidStatus + FieldType_Int_Def + ',' +
    
    Field_DataSrc     + FieldType_Int_Def + ',' +    
    Field_Name        + FieldType_String +
  ')';
end;

function CreateTableSQL_ModelObject: string;
begin
  // 对象 是不存在 类型的
  Result := SQL_CreateTable_Header + Table_ModelObject + '(' +
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +   
    Field_KeyID       + FieldType_Int_Def + ',' +   
    Field_ValidStatus + FieldType_Int_Def + ',' +
        
    Field_Owner       + FieldType_Int_Def + ',' +
    Field_Name        + FieldType_String +
  ')';
end;

function CreateTableSQL_ModelElement: string;
begin
  Result := SQL_CreateTable_Header + Table_ModelElement + '(' +
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +   
    Field_KeyID       + FieldType_Int_Def + ',' +         
    Field_ValidStatus + FieldType_Int_Def + ',' +
        
    Field_Owner       + FieldType_Int_Def + ',' + // ==> Model Object
    Field_Name        + FieldType_String +
  ')';
end;

function CreateTableSQL_Object: string;
begin
  Result := SQL_CreateTable_Header + Table_Object + '(' +
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +
    Field_KeyID       + FieldType_Int_Def + ',' +
    Field_ValidStatus + FieldType_Int_Def + ',' +
    Field_DataSrc     + FieldType_Int_Def + ',' +
    Field_Owner       + FieldType_Int_Def + ',' + // ==> Model Object
    Field_Name        + FieldType_String +
    ');';            
end;
           
function CreateTableSQL_Element: string;
begin
  Result := SQL_CreateTable_Header + Table_Element + '(' +
    Field_AutoKeyID   + FieldType_AutoIncKey + ',' +
    Field_KeyID       + FieldType_Int_Def + ',' +
    Field_ValidStatus + FieldType_Int_Def + ',' +
    Field_DataSrc     + FieldType_Int_Def + ',' +
    Field_Owner       + FieldType_Int_Def + ',' + // ==> Model Element
    Field_Name        + FieldType_String +
    ');';            
end;

end.
