unit jwb_define_dataobject;

interface

type
  PRT_Customer      = ^TRT_Customer;  
  PRT_CustomerInfo  = ^TRT_CustomerInfo;  
  PRT_Project       = ^TRT_Project;      
  PRT_ProjectItem   = ^TRT_ProjectItem;
  
  TRT_Customer      = record    
    UpdateVersion   : integer;  // update flag
    AutoKeyId       : integer;
    CustomNo        : string; 
    Name            : string;
    Info            : PRT_CustomerInfo;
  end;

  TRT_CustomerInfo  = record
    UpdateVersion   : integer;  // update flag
    Customer        : PRT_Customer;
    ClassInfo       : string; // 分类
    Contact         : string; // 联系人
    PhoneNum        : string; //
    Balance100      : Integer; // * 100 余额 
    Address         : string;
    Memo            : string;
    CurrentProject  : PRT_Project;  
  end;

  TRT_Project       = record    
    UpdateVersion   : integer;  // update flag
    AutoKeyId       : integer;     
    Name            : string;
  end;
  
  TRT_ProjectItem   = record    
    UpdateVersion   : integer;  // update flag
    AutoKeyId       : integer; 
    Name            : string;
    Memo            : string;    
  end;

  function CheckOutCustomer: PRT_Customer;   
  function CheckOutCustomerInfo(ACustomer: PRT_Customer): PRT_CustomerInfo;   
  function CheckOutProjectItem: PRT_ProjectItem;   

implementation

function CheckOutCustomer: PRT_Customer;
begin
  Result := System.New(PRT_Customer);
  FillChar(Result^, SizeOf(TRT_Customer), 0);
end;

function CheckOutProjectItem: PRT_ProjectItem;
begin
  Result := System.New(PRT_ProjectItem);
  FillChar(Result^, SizeOf(TRT_ProjectItem), 0);
end;

function CheckOutCustomerInfo(ACustomer: PRT_Customer): PRT_CustomerInfo;
begin
  if nil = ACustomer.Info then
  begin
    ACustomer.Info := System.New(PRT_CustomerInfo);
    FillChar(ACustomer.Info^, SizeOf(TRT_CustomerInfo), 0);
  end;
  Result := ACustomer.Info;
end;

end.
