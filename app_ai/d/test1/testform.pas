unit testform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Ann, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    mmo1: TMemo;
    pnl1: TPanel;
    btnTrain: TButton;
    btnCompute: TButton;
    edtX: TEdit;
    edtY: TEdit;
    pnl2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    edt5: TEdit;
    edt6: TEdit;
    edt7: TEdit;
    edt8: TEdit;
    edt9: TEdit;
    edt10: TEdit;
    edt11: TEdit;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    procedure btnTrainClick(Sender: TObject);
    procedure btnComputeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
(*
  神经网络模型库
    程序设计 学习算法  前向神经网络  反馈神经网络  竞争神经网络  随机神经网络

  http://www.cnblogs.com/delphi-xe5/p/5840887.html
*)
var
  bp: TBpDeep = nil;
             
procedure TForm1.FormCreate(Sender: TObject);
begin
  bp:=TBpdeep.Create([2,10,2],0.15,0.8);
end;

procedure TForm1.btnComputeClick(Sender: TObject);
var
  rst:Tdbarr;
  x:array[0..1] of double;
begin
  x[0] := strtofloat(edtX.Text);
  x[1] := strtofloat(edtY.Text);
  rst  := bp.computeout(x);
  mmo1.Lines.Append(floattostr(rst[0])+' '+floattostr(rst[1]));
end;

procedure TForm1.btnTrainClick(Sender: TObject);
var
  i,k:integer;
const
  data:array[0..3] of array[0..1] of double=((1,2),(2,2),(1,1),(2,1));
  tar:array[0..3] of array[0..1] of double=((1,0),(0,1),(0,1),(1,0));
begin
  for k:=0 to 499 do
  begin
    for i:=0 to high(data) do
    begin
      bp.train(data[i], tar[i]);
    end;
  end;
end;

end.
