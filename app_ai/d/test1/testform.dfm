object Form1: TForm1
  Left = 188
  Top = 81
  Caption = 'Form1'
  ClientHeight = 557
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmo1: TMemo
    Left = 8
    Top = 16
    Width = 329
    Height = 209
    Lines.Strings = (
      'mmo1')
  end
  object pnl1: TPanel
    Left = 351
    Top = 16
    Width = 114
    Height = 121
    object btnTrain: TButton
      Left = 16
      Top = 6
      Width = 75
      Height = 25
      Caption = #35757#32451
      OnClick = btnTrainClick
    end
    object btnCompute: TButton
      Left = 16
      Top = 88
      Width = 75
      Height = 25
      Caption = #35745#31639
      OnClick = btnComputeClick
    end
    object edtX: TEdit
      Left = 17
      Top = 37
      Width = 58
      Height = 21
      Text = '3'
    end
    object edtY: TEdit
      Left = 17
      Top = 64
      Width = 58
      Height = 21
      Text = '1'
    end
  end
  object pnl2: TPanel
    Left = 8
    Top = 248
    Width = 401
    Height = 281
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 87
      Height = 13
      Caption = 'BP '#31639#27861#21450#20854#25913#36827
    end
    object Label2: TLabel
      Left = 16
      Top = 32
      Width = 96
      Height = 13
      Caption = #36755#20837#23618#31070#32463#20803#20010#25968
    end
    object Label3: TLabel
      Left = 16
      Top = 56
      Width = 96
      Height = 13
      Caption = #36755#20986#23618#31070#32463#20803#20010#25968
    end
    object Label4: TLabel
      Left = 16
      Top = 80
      Width = 96
      Height = 13
      Caption = #38544#21547#23618#31070#32463#20803#20010#25968
    end
    object Label5: TLabel
      Left = 208
      Top = 32
      Width = 48
      Height = 13
      Caption = #23398#20064#22240#23376
    end
    object Label6: TLabel
      Left = 208
      Top = 56
      Width = 48
      Height = 13
      Caption = #27604#20363#31995#25968
    end
    object Label7: TLabel
      Left = 208
      Top = 80
      Width = 72
      Height = 13
      Caption = #25945#24072#25351#23548#31995#25968
    end
    object Label8: TLabel
      Left = 16
      Top = 107
      Width = 108
      Height = 13
      Caption = #36755#20837#31070#32463#20803#21021#22987#36171#20540
    end
    object Label9: TLabel
      Left = 16
      Top = 131
      Width = 132
      Height = 13
      Caption = #36755#20837#23618#19982#38544#21547#23618#36830#25509#26435#20540
    end
    object Label10: TLabel
      Left = 16
      Top = 155
      Width = 120
      Height = 13
      Caption = #38544#21547#23618#26576#31070#32463#20803#30340#38408#20540
    end
    object Label11: TLabel
      Left = 16
      Top = 179
      Width = 132
      Height = 13
      Caption = #38544#21547#23618#19982#36755#20986#23618#36830#25509#26435#20540
    end
    object Label12: TLabel
      Left = 16
      Top = 203
      Width = 120
      Height = 13
      Caption = #36755#20986#23618#26576#31070#32463#20803#30340#38408#20540
    end
    object edt1: TEdit
      Left = 118
      Top = 29
      Width = 75
      Height = 21
    end
    object edt2: TEdit
      Left = 118
      Top = 53
      Width = 75
      Height = 21
    end
    object edt3: TEdit
      Left = 118
      Top = 77
      Width = 75
      Height = 21
    end
    object edt4: TEdit
      Left = 302
      Top = 29
      Width = 75
      Height = 21
    end
    object edt5: TEdit
      Left = 302
      Top = 53
      Width = 75
      Height = 21
    end
    object edt6: TEdit
      Left = 302
      Top = 77
      Width = 75
      Height = 21
    end
    object edt7: TEdit
      Left = 166
      Top = 104
      Width = 211
      Height = 21
      TabOrder = 6
    end
    object edt8: TEdit
      Left = 166
      Top = 128
      Width = 211
      Height = 21
    end
    object edt9: TEdit
      Left = 166
      Top = 152
      Width = 211
      Height = 21
    end
    object edt10: TEdit
      Left = 166
      Top = 176
      Width = 211
      Height = 21
    end
    object edt11: TEdit
      Left = 166
      Top = 200
      Width = 211
      Height = 21
    end
    object btn1: TButton
      Left = 16
      Top = 238
      Width = 75
      Height = 25
      Caption = #21021#22987#21270
      OnClick = btnTrainClick
    end
    object btn2: TButton
      Left = 97
      Top = 238
      Width = 75
      Height = 25
      Caption = #36171#20540
      OnClick = btnTrainClick
    end
    object btn3: TButton
      Left = 177
      Top = 238
      Width = 75
      Height = 25
      Caption = #35745#31639
      OnClick = btnTrainClick
    end
    object btn4: TButton
      Left = 258
      Top = 238
      Width = 75
      Height = 25
      Caption = #21462#28040
      OnClick = btnTrainClick
    end
  end
end
