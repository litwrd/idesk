unit ann_bp;

interface

(*//
http://www.docin.com/p-471046808.html
  BP 算法 (误差反传算法)

  输入层X  中间层(隐含层)  输出层Y

  feature: array of real 输入层各单元输出值
  th1: array of real 隐含层 各神经元的阈值
  th2: array of real 输出层 各神经元的阈值
  w1: array of array of real 输入层 与 隐含层 之间的权值
  w2: array of array of real 输出层 与 隐含层 之间的权值
  Y1, Y2: array of real 隐含层 输出层 各神经元的 输出
  D1, D2: array of real 隐含层 输出层 各神经元的 一般化误差  
//*)
implementation

end.
